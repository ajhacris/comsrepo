<html>

<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>

<script src="js/ComplaintSuggestionRegistrationJS.js"></script>
<script src="js/bootstrap-datetimepicker.min.js"></script>
<link href="css/bootstrap.min.css" rel="stylesheet"/>

<script src="js/Validation.js"></script>
<link href="css/bootstrap-datetimepicker.min.css" rel="stylesheet"/>

  <link href="css/css/bootstrap.min.css" rel="stylesheet"/>

    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css"/>

    <link href="css/cssdash/sb-admin.css" rel="stylesheet"/>
    
</head>
<%
 
        
String flag_sla="";

if(session.getAttribute("login_id")==null){
	
	System.out.println("******************Session Expired **********************");
    response.sendRedirect("AdminLogin.jsp");
}
flag_sla=session.getAttribute("flag_sla").toString();
        %>
<body id="page-top" style="overflow-x: hidden">
 <div class="container-fluid" style="padding:0px;">
     <nav class="navbar navbar-expand navbar-dark bg-dark static-top" style="background-color:rgb(33, 150, 243)!important;box-shadow:rgba(0, 0, 0, 0.3) 0px 1px 4px 0px!important;font-family:roboto;font-weight:bold;">

      <a class="navbar-brand mr-1" href="index.jsp">Content Management System</a>

      <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
        <i class="fas fa-bars"></i>
      </button>

      <!-- Navbar -->
      <ul class="navbar-nav ml-auto ml-md-0">
        
        <li class="nav-item dropdown no-arrow">
          <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-user-circle fa-fw"></i>
          </a>
         
        </li>
      </ul>

    </nav>
   
    
    
  <div  id="wrapper" >

  <!-- Sidebar -->
      <ul class="sidebar navbar-nav" style=" 
          background-color: #1195a2!important;
          font-family: roboto;
          font-weight: 400;
          margin-bottom: 0px;
          box-shadow: rgba(0, 0, 0, 0.3) 0px 1px 4px 0px!important;
          font-family: roboto;
          font-weight: bold;
          width: 2px;">
          
          
        <li class="nav-item active">
          <a class="nav-link" href="dashboard.jsp">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span>
          </a>
        </li>
      
        <%if(Integer.parseInt(flag_sla)<4){ %>
        <li class="nav-item active">
          <a class="nav-link" href="ComplaintList.jsp">
            <i class="fas fa-fw fa-table"></i>
            <span>Assigned To Me</span></a>
        </li>
         <%} %>
         
         
         <%if(Integer.parseInt(flag_sla)==4){ %>
         <li class="nav-item active">
          <a class="nav-link" href="UpdateSlaTime.jsp">
            <i class="fas fa-fw fa-table"></i>
            <span>SLA Update</span></a>
        </li>
        <%} %>
        
         <%if(Integer.parseInt(flag_sla)<4){ %>
        <li class="nav-item active">
          <a class="nav-link" href="ComplaintSuggestionRegistration.jsp">
            <i class="fas fa-fw fa-table"></i>
            <span> Register Complaint</span></a>
        </li>
        <%} %>
        
        
         <%if(Integer.parseInt(flag_sla)<4){ %>
         <li class="nav-item active">
          <a class="nav-link" href="SearchPerticularComplaint.jsp">
             <i class="fas fa-fw fa-search"></i>
            <span>Search Particular Complaint</span></a>
        </li>
         <%} %>
         
          <%if(Integer.parseInt(flag_sla)==4){ %>
         <li class="nav-item active">
          <a class="nav-link" href="SearchComplaintAdmin.jsp">
             <i class="fas fa-fw fa-search"></i>
            <span>Search Complaint</span></a>
        </li>
         <%} %>
         
         
           <%if(Integer.parseInt(flag_sla)==5){ %>
         <li class="nav-item active">
          <a class="nav-link" href="incoming_messages.jsp">
             <i class="fas fa-fw fa-search"></i>
            <span>Incoming Messages</span></a>
        </li>
        
         <li class="nav-item active">
          <a class="nav-link" href="junk_messages_list.jsp">
             <i class="fas fa-fw fa-search"></i>
            <span>Junk Messages</span></a>
        </li>
        
        <li class="nav-item active">
          <a class="nav-link" href="search_messages_list.jsp">
             <i class="fas fa-fw fa-search"></i>
            <span>Search</span></a>
        </li>
         <%} %>
         
         
        
        <%if(flag_sla.equals("2")){ %>
           <li class="nav-item active">
          <a class="nav-link" href="SuggestionsList.jsp">
             <i class="fas fa-fw fa-search"></i>
            <span>Suggestions</span></a>
        </li>
        
        <%} %>
        <li class="nav-item active">
          <a class="nav-link" href="Logout.jsp">
          <i class="fas fa-sign-out-alt"></i>
           
            <span>Logout</span></a>
        </li>
        
      </ul>
    


 
    


    
<div class="container-fluid" style=" padding-top:35px;width:100%; padding-bottom:80px;">

<div class="col-md-12 ">


<div class="card" >


<div class="card-header bg-info text-white"> <span> </span> <p style="font-size:15px;"><b>Complaints & Suggestions Registration Form</b></p></div>
<div class="card-body" style="background-color:#F7F7F7;" >
<div class="row container-fluid">
<div class="col-md-12" >
<form method="post" action="ComplaintSuggestionRegistration" enctype="multipart/form-data" >
<div id="detail_panel">


    

    <div class="row">
	   <p style="color:#f05f40;">Complaint Detail  </p>
    </div>
	<div class="row"  >
	    <div class="col-md-4">
                  <div class="form-group">
                   <label for="mode_type">Complaint Action<span style="color:#f05f40;">*</span></label>
               
                  <select class='form-control' id='search_type' name='search_type' required>
                  <option value="">--Select--</option>
                   <option value="J">Junk</option>
                   <option value="M">Merge</option>
                   <option value="A">Assign</option>
                  </select>
                                    
                 </div>
                </div>
                
                
       
  <div class="col-md-4">
           <div class="form-group">
               <label for="incident_dt">From Date <span style="color:#f05f40;">*</span></label>
               <input type='text' class="form-control" id='incident_dt' required name="incident_dt" title="Incident Date"/>
           </div>
      </div>


       <div class="col-md-4">
           <div class="form-group">
               <label for="incident_dt_to">To Date <span style="color:#f05f40;">*</span></label>
               <input type='text' class="form-control" id='incident_dt_to' required name="incident_dt_to" title="Incident Date"/>
           </div>
      </div>
   </div>
			    

			
			     
			     <div class="row">
			     <div class="col-md-4">
                <div class="form-group">
                    
                    <button id="submit" type="submit" name="submit" class="btn btn-primary" >Submit</button>
                    
                </div>
            </div>
			     </div></div>
			     
			     
			     
			     
			     </form>

</div>
</div>
</div>

</div>





</div>
<div class="col-md-2"></div>
</div>

</div>

 

        <footer style=" clear: both;
    position: relative;
    
   background-color: #d3e2e2;">
          
            <div class="copyright text-center ">
              <span>Copyright �  2018</span>
            </div>
         
        </footer>


</div>
    <script src="js/js/bootstrap.bundle.min.js"></script>

    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <script src="js/jsdash/sb-admin.min.js"></script>
</body>




</html>