<html>

<head>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
		
		<script src="js/bootstrap.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js" type="text/javascript"></script>
		<script src="js/ComplaintSuggestionRegistrationJS.js"></script>
		<script src="js/complaintintrain.js?version=1"></script>
		<script src="js/Validation.js"></script>
		
	
		<link href="css/bootstrap.min.css" rel="stylesheet"/>
 		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
 		<link href="css/bootstrap-datetimepicker.css" rel="stylesheet"/>
		<link href="css/bootstrap-datetimepicker.min.css" rel="stylesheet"/>
	
	
	
	
	</head>

	<%   
		String zone="";
		String div="";
		String groupid="";
		String complaint=null;
		String mobile=null;
		int id=0;
		if(session.getAttribute("login_id")==null){
			
			System.out.println("******************Session Expired **********************");
            response.sendRedirect("AdminLogin.jsp");
		}
		else{
			zone=session.getAttribute("zn_cd").toString();
			div=session.getAttribute("div_cd").toString();
			groupid=session.getAttribute("group_id").toString();
			
			
			
		}
		
		
		
	%>

	<body id="page-top" style="overflow-x: hidden">
  		<!--    header start  -->
    	<%@ include file="top.jsp" %>
    	<div  id="content-wrapper">    
     		<%@ include file="sidebar.jsp" %>	     
    		<!--    header end   -->   
			<div class="container-fluid" style=" padding-top:35px;width:100%; padding-bottom:120px;">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header bg-info text-white">
							<p style="font-size: 25px;">
								<b><center>
									<h2>Register Incoming Messages Complaint</h2>
								</center></b>
							</p>
						</div>
						<div class="card-body" style="background-color: #F7F7F7;">
							<div class="row container-fluid">
								<div class="col-md-12">
								
								<div class="row">
	   <p style="color:#f05f40;">Complaint Detail  </p>
    </div>
    
					<form method="post" action="ComplaintSuggestionRegistration" enctype="multipart/form-data" >
    	
    	 <div id="detail_panel">
           <div class="row" align="center">
 
					  <div class="col-md-12">
					  <font color="blue">
					                   <%if(request.getParameter("ref_no")!=null){
				   		out.print("Complaint Registration No :"+request.getParameter("ref_no"));
				   }%>
					</font>
  </div>
 
			  
			   </div>
			   
			   
			    <div class="row"  >
	        <div class="col-md-4">
            <div class="form-group">
                <label for="complaint_channel">Complaint Channel <span style="color:#f05f40;">*</span></label>
                      <div id="selectComplaintChannel" ></div>
            </div>
       </div>
	</div>
			   
			   
			   
	          <div class="row"  >
	          
	            <div class="col-md-4">
                  <div class="form-group">
                   <label for="mode_type">Place Of Occurrence<span style="color:#f05f40;">*</span></label>
                  <div id="place" >
                  <select class='form-control' id='place_type' name='place_type' required>
                  <option value="">--Select--</option>
                   <option value="s">On Station</option>
                   <option value="t">In Train</option>
                  </select>
                  </div>                   
                 </div>
                </div>
	          
	            <div class="col-md-4">
                  <div class="form-group">
                   <label for="complaint_type">Complaint <span style="color:#f05f40;">*</span></label>
                   <div id="selectComplaint" ></div>
                 </div>
                </div>
                <div class="col-md-4">
                 <div class="form-group">
                  <label for="sub_complaint_type">Sub Complaint <span style="color:#f05f40;">*</span></label>
                  <div id="selectSubComplaint" ></div>                   
                 </div>
               </div>
              
              </div>
              
              
               <div class="row"  >
			     
			       <div class="col-md-12">
                     <div class="form-group">
                       <label for="complaint_desc">Complaint Description <span style="color:#f05f40;">*</span></label>
                      <textarea id="complaint_desc" name="complaint_desc"  class="form-control"  ></textarea>
                     </div>
                  </div>
			     </div>
			     
			     
			      <div class="row"  >
	          
	            
	            
               
                 <div class="col-md-4">
                <div class="form-group">
                 <label for="incident_dt">Incident Date <span style="color:#f05f40;">*</span></label>
                <input type='date' class="form-control" id='incident_dt' required name="incident_dt" title="Incident Date"/>
               </div>
      </div>
             
              <div class="col-md-4">
                <div class="form-group">
                    <label for="file">Upload File </label>
                    <input id="file" type="file" name="file" title="File" class="form-control" >
                    </div>                    
                </div>    
               
              
              </div>
              
              
              <div id="details" >                 </div>   
               
               
               <div class="row" id="Trainno">
			   
			   
			    <div class="col-md-4">
                <div class="form-group">
                <label for="train_no">Train No <span style="color:#f05f40;font-size:30px">*</span></label>
                    
                     <div id="trainnoo" >
                   </div>   
                   
                          </div>
            </div>
            </div>
               
               
                 
                 
                 <div class="row" id="PNR">
			   
			   
			    <div class="col-md-4">
                <div class="form-group">
                    <label for="pnr_uts">PNR No <span style="color:#f05f40;font-size:30px">*</span></label>
                    <input id="pnr_uts" type="text" name="pnr_uts" class="form-control"  maxlength="10" title="Please Enter PNR Number">
                    
                </div>
            </div>
           <!--   <div class="col-md-4">
                <div class="form-group">
                    </br>  
                    <button id="pnr_utsbtn" type="button" name="pnr_utsbtn" class="btn btn-primary" >FETCH PNR</button>
                    
                </div>
            </div> -->
			   <div id="pnr_error" class="col-md-4" style="color:red;"></div>
			   
			  </div>
                 
			    
			     
              
              <div id="pnr_desc">
			  
			  <!--                  HIDDEN FIELDS                   -->
			  
			  
			   <input id="day" type="hidden" name="day" class="form-control"    >
			  <input id="month" type="hidden" name="month" class="form-control"    >
			   <input id="year" type="hidden" name="year" class="form-control"    >
			     <input id="nextstn" type="hidden" name="nextstn" class="form-control"  >
			  
			  <!--                  HIDDEN FIELDS ENDS                   -->
			  
			 <div class="row"  >
			    
            <div class="col-md-3">
                <div class="form-group">
                    <label for="train_no">Train Number</label>
                    <input id="train_no" type="text" name="train_no" class="form-control"   readonly >
                    
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="from_stn">From Station</label>
                    <input id="from_stn" type="text" name="from_stn" class="form-control"  readonly >
                   
                </div>
            </div>
        
       <div class="col-md-3">
                <div class="form-group">
                    <label for="to_stn">To Station</label>
                    <input id="to_stn" type="text" name="to_stn" class="form-control" readonly >
                    
                </div>
            </div>
			     
			      <div class="col-md-3">
                <div class="form-group">
                    <label for="board_stn">Boarding Station</label>
                    <input id="board_stn" type="text" name="board_stn" class="form-control" readonly >
                    
                </div>
            </div>
			      
			     
			     
			     </div>
			     
			    
			     
			   <div class="row"  >
			    
            <div class="col-md-3">
                <div class="form-group">
                    <label for="berth_class">Berth Class</label>
                    <input id="berth_class" type="text" name="berth_class" class="form-control"  readonly  >
                    
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="total_pass">Total Passenger</label>
                    <input id="total_pass" type="text" name="total_pass" class="form-control"  readonly >
                   
                </div>
            </div>
        
       <div class="col-md-3">
                <div class="form-group">
                    <label for="total_fare">Total Fare</label>
                    <input id="total_fare" type="text" name="total_fare" class="form-control" readonly >
                    
                </div>
            </div>
			     
			      <div class="col-md-3">
                <div class="form-group">
                    <label for="coach_no">Coach No.</label><div id="coach_condition">
                    <input id="coach_no" type="text" name="coach_no" class="form-control" readonly ></div>
                    
                </div>
            </div>
			      
			     
			     
			     </div> 
			     
			     <div class="row"> 
			      <div class="col-md-3">
                <div class="form-group">
                    <label for="berth_no">Berth/Seat No.</label><div id="berth_condition">
                    <input id="berth_no" type="text" name="berth_no" class="form-control" readonly ></div>
                    
                </div>
            </div> 


            <div class="col-md-9">
                <br>
                    <p  style="color:#f05f40;font-size:20px">Please confirm your journey details before proceeding...</p>
                   
                    
                
            </div> 
			     </div>
			     
			     
			     
			     
			     
			     
			     </div>
			     
			       <div class="row">
			   <p style="color:#f05f40;">Passenger Details  </p>
			   </div>
			     
			      <div class="row"  > 
			       
               <div class="col-md-4">
                <div class="form-group">
                    <label for="name">Name <span style="color:#f05f40;">*</span></label>
                    <input id="name" type="text" name="name"  class="form-control" title=" Name">
                    
                    
                </div>
            </div>
			      <div class="col-md-4">
                 <div class="form-group">
                  <label for="contact">Contact No. <span style="color:#f05f40;">*</span></label>
                 <input type="text" name="contact_no"  class="form-control" id ="contact_no" value="" >                
                 </div>
                 </div>
			     
          
			     
			     </div>
			     
			     
			     
			     
			        <div class="row">
			   <p style="color:#f05f40;">Complaint Uploaded by ON Duty Staff  </p>
			   </div>
			   <div class="row"  > 
			       
               <div class="col-md-4">
                <div class="form-group">
                    <label for="name_duty_person">Name <span style="color:#f05f40;">*</span></label>
                    <input id="name_duty_person" type="text" name="name_duty_person" required class="form-control" title="Duty Staff Name">
                    
                    
                </div>
            </div>
			     
			      <div class="col-md-4">
                <div class="form-group">
                    <label for="designation_duty_person">Designation <span style="color:#f05f40;">*</span></label>
                    <input id="designation_duty_person" type="text" name="designation_duty_person" required class="form-control" title="Designation" >
                    
                </div>
            </div>
          
			     
			     </div>
			     
			     
			     
			    
			     
			  
							
							
							
	                             <button  type="submit" class="btn btn-primary" >Register Complaint </button>
                    						
                                </form>		

</div>
</div>
</div>

</div>





</div>
<div class="col-md-2"></div>
</div>

</div>

 </div>

        <footer style=" clear: both;
    position: relative;
    
   background-color: #d3e2e2;">
          
            <div class="copyright text-center ">
              <span>Copyright �  2018</span>
            </div>
         
        </footer>


</div>
    <script src="js/js/bootstrap.bundle.min.js"></script>

    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <script src="js/jsdash/sb-admin.min.js"></script>
</body>




</html>