<%@page import="java.io.Console"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@page import ="utility.DbConnection" import="java.sql.*" %>
<html>
	<head>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
		
		<script src="js/bootstrap.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js" type="text/javascript"></script>
		<script src="js/IncomingMessageAssignJS.js?version=1"></script>
		<script src="js/complaintintrain.js?version=1"></script>
		<script src="js/Validation.js"></script>
		
	
		<link href="css/bootstrap.min.css" rel="stylesheet"/>
 		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
 		<link href="css/bootstrap-datetimepicker.css" rel="stylesheet"/>
		<link href="css/bootstrap-datetimepicker.min.css" rel="stylesheet"/>
	
	
	
	
	</head>
	<style>
		.bg {
    		background-position: center center;
    		background-size: cover;
		}
		
		
		 table {
            width: 100%;
        }

        thead, tbody, tr, td, th { display: block; }

        tr:after {
            content: ' ';
            display: block;
            visibility: hidden;
            clear: both;
        }

        thead th {
            height: 50px;

            /*text-align: left;*/
        }

        tbody {
            height: 425px;
            overflow-y: auto;
        }

        thead {
            /* fallback */
        }


        tbody td, thead th {
            width: 16.5%;
            float: left;
        }
	</style>

	<%   
		String zone="";
		String div="";
		String groupid="";
		String complaint=null;
		String mobile=null;
		int id=0;
		if(session.getAttribute("login_id")==null){
			
			System.out.println("******************Session Expired **********************");
            response.sendRedirect("AdminLogin.jsp");
		}
		else{
			zone=session.getAttribute("zn_cd").toString();
			div=session.getAttribute("div_cd").toString();
			groupid=session.getAttribute("group_id").toString();
			
		
			   
			 id=Integer.parseInt( request.getParameter("id"));  
			System.out.println("Id  :"+id);
			
			
			
			PreparedStatement stmt=null;
			ResultSet rs=null;
			DbConnection dbConnection=new DbConnection();
			Connection con=dbConnection.dbConnect();
			String query="SELECT from_mobile,sms_text FROM rly_incoming_sms WHERE id=?";
			
			stmt=con.prepareStatement(query);
			stmt.setInt(1,id);
			rs=stmt.executeQuery();
			while(rs.next()){
				complaint=rs.getString(2);
				mobile=rs.getString(1);
			}
			System.out.println("Complaint :"+complaint);
		}
		
		
		
	%>

	<body id="page-top" style="overflow-x: hidden">
  		<!--    header start  -->
    	<%@ include file="top.jsp" %>
    	<div  id="content-wrapper">    
     		<%@ include file="sidebar.jsp" %>	     
    		<!--    header end   -->   
			<div class="container-fluid" style=" padding-top:35px;width:100%; padding-bottom:120px;">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header bg-info text-white">
							<p style="font-size: 25px;">
								<b><center>
									<h2>Split Incoming Messages Complaint</h2>
								</center></b>
							</p>
						</div>
						<div class="card-body" style="background-color: #F7F7F7;">
							<div class="row container-fluid">
								<div class="col-md-12">
								
								<div class="row">
	   <p style="color:#f05f40;">Complaint Detail  </p>
    </div>
    
    	<form action="SplitComplaints" method="post"> 
    	
    	<input type="hidden" name="id" id="id" value="<%=id %>" >
    	
    	
    	  <div class="row"  >
	          
	           <div class="col-md-4">
                 <div class="form-group">
                  <label for="Mobile">Contact No. <span style="color:#f05f40;">*</span></label>
                 <input type="text" name="contact_no"  class="form-control" id ="contact_no" value=<%=mobile %> readonly>                
                 </div>
               </div>
	            
	            
                <div class="col-md-4">
                 <div class="form-group">
                  <label for="message">Complaint <span style="color:#f05f40;">*</span></label>
                 <textarea name="message"  class="form-control" id ="message" value="" > <%=complaint %>    </textarea>           
                 </div>
               </div>
               
                <div class="col-md-4">
                 <div class="form-group">
                  <label for="message">Complaint Split <span style="color:#f05f40;">*</span></label>
                <textarea name="message1"  class="form-control" id ="message1" value="" >     </textarea>            
                 </div>
               </div>
              
              </div>
    	
			     	
	                             <button  type="submit" class="btn btn-primary" >Split Complaint </button>
                    						
                                </form>		
						
						
						
						
									
									
								</div>
							</div>
						</div>
					</div>
					</div>
					</div>
					</div>
					
				  
     				     			<!--    footer start  -->
     				      			<%@ include file="footer.jsp" %>
		    		
	    		 			
	    		
			
	
		
    	<script src="js/jquery/jquery.min.js"></script>
    	<script src="js/js/bootstrap.bundle.min.js"></script>
    	<script src="js/jquery-easing/jquery.easing.min.js"></script>
    	<script src="js/jsdash/sb-admin.min.js"></script>
	</body>
</html>