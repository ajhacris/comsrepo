<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@page import ="utility.DbConnection" import="java.sql.*" %>
<html>
	<head>
	<meta charset="utf-8">
  		<meta name="viewport" content="width=device-width, initial-scale=1">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
		
		<script src="js/bootstrap.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js" type="text/javascript"></script>
		
		<script src="js/searchperticularcomplaint.js?version=1"></script>
		
		<script src="js/bootstrap-datetimepicker.min.js"></script>
		<link href="css/bootstrap.min.css" rel="stylesheet"/>
 		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
 		<link href="css/bootstrap-datetimepicker.css" rel="stylesheet"/>
		<link href="css/bootstrap-datetimepicker.min.css" rel="stylesheet"/>
	
	
	
	
	</head>
	<style>
		.bg {
    		background-position: center center;
    		background-size: cover;
		}
	</style>

	<%   
		String zone="";
		String div="";
		String groupid="";
		if(session.getAttribute("login_id")==null){
			
			System.out.println("******************Session Expired **********************");
            response.sendRedirect("AdminLogin.jsp");
		}
		else{
			zone=session.getAttribute("zn_cd").toString();
			div=session.getAttribute("div_cd").toString();
			groupid=session.getAttribute("group_id").toString();
		}
	%>

	<body id="page-top" style="overflow-x: hidden">
  		<!--    header start  -->
    	<%@ include file="top.jsp" %>
    	<div  id="content-wrapper">    
     		<%@ include file="sidebar.jsp" %>	     
    		<!--    header end   -->   
			<div class="container-fluid" style=" padding-top:35px;width:100%; padding-bottom:120px;">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header bg-info text-white">
							<p style="font-size: 25px;">
								<b><center>
									<h2>List Of Complaint</h2>
								</center></b>
							</p>
						</div>
						<div class="card-body" style="background-color: #F7F7F7;">
							<div class="row container-fluid">
								<div class="col-md-12">						
									<form method="post" action="SearchPerticularComplaint.jsp" name="compsearch" id="compsearch" onsubmit="return chkForm()">
						  				<div id="detail_panel">
						  					<div class="row">
												<div class="col-md-3">
													<div class="form-group">
														<label for="com_ref_no" >Complaint Reference Number: </label>
													</div>
												</div>
												<div class="col-md-3">
													<div class="form-group">
														<input id="com_ref_no" type="text" name="com_ref_no" <%if(request.getParameter("com_ref_no")!=null){ %>value="<%=request.getParameter("com_ref_no")%>"<%} %> class="form-control" >
													</div>
												</div>
												<div class="col-md-3">
                									<div class="form-group">
                    									<label for="mobile">Mobile Number:</label>                    							                 
                									</div>
            									</div>			  									  						
			  						    		<div class="col-md-3">
                									<div class="form-group">
                    									<input id="mobile" type="text" name="mobile" <%if(request.getParameter("mobile")!=null){ %>value="<%=request.getParameter("mobile")%>"<%} %>  class="form-control" >                    							                 
                									</div>
            									</div>
												
											</div>
											<div class="row">
			  						    		<div class="col-md-3">
                									<div class="form-group">
                    									<label for="zone">Zone:</label>                    							                 
                									</div>
            									</div>			  									  						
			  						    		<div class="col-md-3">
                									<div class="form-group">
                    									<input id="zone" type="text" name="zone" value="<%=zone %>" readonly class="form-control" >                    							                 
                									</div>
            									</div>
            									<div class="col-md-3">
                									<div class="form-group">
                    									<label for="division">Division:</label>                    							                 
                									</div>
            									</div>			  									  						
			  						    		<div class="col-md-3">
                									<div class="form-group">
                    									<input id="div" type="text" name="div" value="<%= div %>" readonly class="form-control"  >                   							                 
                									</div>
            									</div>           							
			  								</div>			  								
			  								<div class="row">
			     								<div class="col-md-12" style="text-align:center;">
                									<div class="form-group">                   
                    									<button id="searchall" type="submit" name="searchall" class="btn btn-primary" >Search All</button>
                    									<button id="searchself" type="submit" name="searchself" class="btn btn-primary" >Search Self</button>
                    									<button id="clear" type="submit" name="clear" class="btn btn-primary" >Clear</button>                    
                									</div>
            									</div>
            								</div>		
						  				</div>
									</form>
									<%
							         	if(request.getParameter("searchall")!=null || request.getParameter("searchself")!=null){
							         		int i=0;
											PreparedStatement stmt=null;
											ResultSet rs=null;
											DbConnection dbConnection=new DbConnection();
											Connection con=dbConnection.dbConnect();
											String complaint_ref_no="";											
											String mobile="";											
											complaint_ref_no=request.getParameter("com_ref_no");
											
											System.out.println("groupid :"+groupid);
											mobile=request.getParameter("mobile");											
											String condition="";																					
											String query="SELECT complaint_ref_no,created_on,STATUS,user_group_id,channel_type FROM rly_complaint";
											int count=0;
											if(request.getParameter("searchall")!=null){	
												if(complaint_ref_no!=null && !complaint_ref_no.equals("")){
													condition=query+" where complaint_ref_no=? ";
													stmt=con.prepareStatement(condition);
													stmt.setString(1, complaint_ref_no);
													count=1;
												}
												if(mobile!=null && !mobile.equals("")){
													condition=query+" where mobile=? ";
													stmt=con.prepareStatement(condition);
													stmt.setString(1, mobile);
													count=1;
												}
												if(complaint_ref_no!=null && !complaint_ref_no.equals("") && mobile!=null && !mobile.equals("")){
													condition=query+" where complaint_ref_no=? AND mobile=? ";
													stmt=con.prepareStatement(condition);
													stmt.setString(1, complaint_ref_no);
													stmt.setString(2, mobile);
													count=1;
												}
												
												if(count==0){
													stmt=con.prepareStatement(query);
												}
												
												
											}
											if(request.getParameter("searchself")!=null){
												query=query+" where user_group_id=? AND status!=?";												
												stmt=con.prepareStatement(query);
												stmt.setString(1, groupid);												
												stmt.setString(2, "Closed");
											}
											System.out.println("query : "+ query);
											rs=stmt.executeQuery();	
											
											
											
											
											 										
											
									%>
									<div class="row">
										<div class="col-md-12">
										<div class="table-responsive">   
											<table class='table table-hover' border="1px">
												<thead>
													<tr>
														<th>Reference No</th>
														<th>Registered Date</th>
														<th>Status</th>
														<th>Pending With</th>
														<th>Channel Type</th>
														<th>Action</th>
													</tr>
													<% while(rs.next())  {
														  i++;	
													%>
													<tr>
														<td><%=rs.getString(1) %></td>
														<td><%=rs.getString(2) %></td>
														<td><%=rs.getString(3) %></td>
														<td><%=rs.getString(4) %></td>
														<td><%=rs.getString(5) %></td>
														<td><a href="PerticularComplaintRecieptPrint.jsp?comp_ref_no=<%=rs.getString(1)%>">Print</a></td>
													</tr>
													<%} %>
		  										</thead>
											</table>
											</div>
										</div>
									</div>
									<% if(i==0){ %>
										<div class="row">
											<div class="col-md-12">
												<center><h5>Record Not found</h5></center>
											</div>
										</div>
									<%}} %>
								</div>
							</div>
						</div>
					</div>
					</div>
					</div>
					</div>
					
				  
     				     			<!--    footer start  -->
     				      			<%@ include file="footer.jsp" %>
		    		
	    		 			
	    		
			
	
		
    	<script src="js/jquery/jquery.min.js"></script>
    	<script src="js/js/bootstrap.bundle.min.js"></script>
    	<script src="js/jquery-easing/jquery.easing.min.js"></script>
    	<script src="js/jsdash/sb-admin.min.js"></script>
	</body>
</html>