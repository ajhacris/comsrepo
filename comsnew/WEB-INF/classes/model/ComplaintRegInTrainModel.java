package model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;



public class ComplaintRegInTrainModel {

	
	private Date complaintDt;
	private Date complaintRcdDt;
	private String name;
	private String mobileNumber;
	private String emailId;
	private String address1;
	private String address2;
	private String address3;
	private String address4;
	private String pnrNo;
	private String trainNo;
	private String stationCode;
	private String platformNo;
	private String staffName;
	private String complaintRefNo;
	private Integer incomingSmsId = 0;
	private String complaintMode;
	private Integer complaintType = 0;
	private Integer subComplaintType;
	private String pnrUtsFlag;
	private String complaint;
	private String status;
	private String berthCls;
	private String boardingStn;
	private String fromStation;
	private String nearbyStn;
	private String toStation;
	private Integer totalAmount;
	private Integer totalPass;
	private String utsNo;
	
	private boolean selectCoachNo;
	private List<String> coachNoList = new ArrayList<String>();
	private boolean selectBerthNo;
	private List<String> berthNoList = new ArrayList<String>();

	private Integer day;
	private Integer month;
	public Integer getMonth() {
		return month;
	}

	public void setMonth(Integer month) {
		this.month = month;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}
	public Integer getDay() {
		return day;
	}

	public void setDay(Integer day) {
		this.day = day;
	}

	private Integer year;
	
	// Extra Fields
	private String complaintModeFlag;
	private String complaintName;
	
	private boolean exSize = false;
	private boolean fetchError;

	private List<String> passengerList = new ArrayList<String>();
	private boolean selectName;
	private List<String> stationList = new ArrayList<String>();
	private String subComplaintName;
	private List<String> trainList = new ArrayList<String>();
	
	private String curDepartment;
	private String curStation;
	private String curZone;
	private String curDivision;
	private String curDesignation;
	private String channelType;
	private String channel;
	
	
	
	
public boolean isSelectCoachNo() {
		return selectCoachNo;
	}

	public void setSelectCoachNo(boolean selectCoachNo) {
		this.selectCoachNo = selectCoachNo;
	}

	public List<String> getCoachNoList() {
		return coachNoList;
	}

	public void setCoachNoList(List<String> coachNoList) {
		this.coachNoList = coachNoList;
	}

	public boolean isSelectBerthNo() {
		return selectBerthNo;
	}

	public void setSelectBerthNo(boolean selectBerthNo) {
		this.selectBerthNo = selectBerthNo;
	}

	public List<String> getBerthNoList() {
		return berthNoList;
	}

	public void setBerthNoList(List<String> berthNoList) {
		this.berthNoList = berthNoList;
	}

private String errMsg="";
	

	public String getErrMsg() {
	return errMsg;
}

public void setErrMsg(String errMsg) {
	this.errMsg = errMsg;
}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getChannelType() {
		return channelType;
	}

	public void setChannelType(String channelType) {
		this.channelType = channelType;
	}

	

	public Date getComplaintDt() {
		return complaintDt;
	}

	public void setComplaintDt(Date complaintDt) {
		this.complaintDt = complaintDt;
	}

	public Date getComplaintRcdDt() {
		return complaintRcdDt;
	}

	public void setComplaintRcdDt(Date complaintRcdDt) {
		this.complaintRcdDt = complaintRcdDt;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getAddress3() {
		return address3;
	}

	public void setAddress3(String address3) {
		this.address3 = address3;
	}

	public String getAddress4() {
		return address4;
	}

	public void setAddress4(String address4) {
		this.address4 = address4;
	}

	public String getPnrNo() {
		return pnrNo;
	}

	public void setPnrNo(String pnrNo) {
		this.pnrNo = pnrNo;
	}

	public String getTrainNo() {
		return trainNo;
	}

	public void setTrainNo(String trainNo) {
		this.trainNo = trainNo;
	}



	public String getStationCode() {
		return stationCode;
	}

	public void setStationCode(String stationCode) {
		this.stationCode = stationCode;
	}

	public String getPlatformNo() {
		return platformNo;
	}

	public void setPlatformNo(String platformNo) {
		this.platformNo = platformNo;
	}

	public String getStaffName() {
		return staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	public String getComplaintRefNo() {
		return complaintRefNo;
	}

	public void setComplaintRefNo(String complaintRefNo) {
		this.complaintRefNo = complaintRefNo;
	}

	public Integer getIncomingSmsId() {
		return incomingSmsId;
	}

	public void setIncomingSmsId(Integer incomingSmsId) {
		this.incomingSmsId = incomingSmsId;
	}

	public String getComplaintMode() {
		return complaintMode;
	}

	public void setComplaintMode(String complaintMode) {
		this.complaintMode = complaintMode;
	}

	public Integer getComplaintType() {
		return complaintType;
	}

	public void setComplaintType(Integer complaintType) {
		this.complaintType = complaintType;
	}

	public Integer getSubComplaintType() {
		return subComplaintType;
	}

	public void setSubComplaintType(Integer subComplaintType) {
		this.subComplaintType = subComplaintType;
	}

	public String getPnrUtsFlag() {
		return pnrUtsFlag;
	}

	public void setPnrUtsFlag(String pnrUtsFlag) {
		this.pnrUtsFlag = pnrUtsFlag;
	}

	public String getComplaint() {
		return complaint;
	}

	public void setComplaint(String complaint) {
		this.complaint = complaint;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getBerthCls() {
		return berthCls;
	}

	public void setBerthCls(String berthCls) {
		this.berthCls = berthCls;
	}

	public String getBoardingStn() {
		return boardingStn;
	}

	public void setBoardingStn(String boardingStn) {
		this.boardingStn = boardingStn;
	}

	public String getFromStation() {
		return fromStation;
	}

	public void setFromStation(String fromStation) {
		this.fromStation = fromStation;
	}

	public String getNearbyStn() {
		return nearbyStn;
	}

	public void setNearbyStn(String nearbyStn) {
		this.nearbyStn = nearbyStn;
	}

	public String getToStation() {
		return toStation;
	}

	public void setToStation(String toStation) {
		this.toStation = toStation;
	}

	public Integer getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Integer totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Integer getTotalPass() {
		return totalPass;
	}

	public void setTotalPass(Integer totalPass) {
		this.totalPass = totalPass;
	}

	public String getUtsNo() {
		return utsNo;
	}

	public void setUtsNo(String utsNo) {
		this.utsNo = utsNo;
	}

	public String getComplaintModeFlag() {
		return complaintModeFlag;
	}

	public void setComplaintModeFlag(String complaintModeFlag) {
		this.complaintModeFlag = complaintModeFlag;
	}

	public String getComplaintName() {
		return complaintName;
	}

	public void setComplaintName(String complaintName) {
		this.complaintName = complaintName;
	}


	public boolean isExSize() {
		return exSize;
	}

	public void setExSize(boolean exSize) {
		this.exSize = exSize;
	}

	public boolean isFetchError() {
		return fetchError;
	}

	public void setFetchError(boolean fetchError) {
		this.fetchError = fetchError;
	}

	

	public List<String> getPassengerList() {
		return passengerList;
	}

	public void setPassengerList(List<String> passengerList) {
		this.passengerList = passengerList;
	}

	public boolean isSelectName() {
		return selectName;
	}

	public void setSelectName(boolean selectName) {
		this.selectName = selectName;
	}

	public List<String> getStationList() {
		return stationList;
	}

	public void setStationList(List<String> stationList) {
		this.stationList = stationList;
	}

	public String getSubComplaintName() {
		return subComplaintName;
	}

	public void setSubComplaintName(String subComplaintName) {
		this.subComplaintName = subComplaintName;
	}

	public List<String> getTrainList() {
		return trainList;
	}

	public void setTrainList(List<String> trainList) {
		this.trainList = trainList;
	}

	public String getCurDepartment() {
		return curDepartment;
	}

	public void setCurDepartment(String curDepartment) {
		this.curDepartment = curDepartment;
	}

	public String getCurStation() {
		return curStation;
	}

	public void setCurStation(String curStation) {
		this.curStation = curStation;
	}

	public String getCurZone() {
		return curZone;
	}

	public void setCurZone(String curZone) {
		this.curZone = curZone;
	}

	public String getCurDivision() {
		return curDivision;
	}

	public void setCurDivision(String curDivision) {
		this.curDivision = curDivision;
	}

	public String getCurDesignation() {
		return curDesignation;
	}

	public void setCurDesignation(String curDesignation) {
		this.curDesignation = curDesignation;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
}
