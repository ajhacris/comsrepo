package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import utility.DbConnection;

public class ComplaintChannelDAO {
	
	public String getComplaintChannel(){
		String countent="";
		
		PreparedStatement stmt=null;
		DbConnection dbConnection=new DbConnection();
		Connection con=dbConnection.dbConnect();
		
		String selectString="<select class='form-control' id='complaint_channel' name='complaint_channel' required><option value=''>--Select--</option>";
		
		try{
			stmt=con.prepareStatement("SELECT menu_items,menu_item_flag FROM complaint_menu_items");		
			
			
		
		ResultSet rs=stmt.executeQuery();  
		
		
		while(rs.next())  {
			
		selectString=selectString+"<option value='"+rs.getString("menu_item_flag")+"'>"+rs.getString("menu_items")+"</option>";
		
		}
		selectString=selectString +"</select>";
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			if(con != null)
			{
				try {
					con.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		return selectString;
	}

}
