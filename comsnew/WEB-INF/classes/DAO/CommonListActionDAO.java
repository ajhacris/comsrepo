package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import utility.DbConnection;

public class CommonListActionDAO {
public String zone() throws SQLException{
		
		
		
		
		
		PreparedStatement stmt=null;
		DbConnection dbConnection=new DbConnection();
		Connection con=dbConnection.dbConnect();
		
		String selectString="<select class='form-control' id='zone' name='zone' required><option value=''>--Select--</option>";
		try{
			stmt=con.prepareStatement("SELECT zn_cd,zn_nm FROM mst_zones");		
			
			
		
		ResultSet rs=stmt.executeQuery();  
		
		
		while(rs.next())  {
			
		selectString=selectString+"<option value='"+rs.getString(1)+"'>"+rs.getString(2)+"</option>";
		
		}
		selectString=selectString +"</select>";
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			if(con != null)
			{
				con.close();
			}
		}
		
		
		
		
		
		return selectString;
		
		
		
	}



public String division(String zone) throws SQLException{
	
	
	
	
	
	PreparedStatement stmt=null;
	DbConnection dbConnection=new DbConnection();
	Connection con=dbConnection.dbConnect();
	
	String selectString="<select class='form-control' id='division' name='division' required><option value=''>--Select--</option>";
	try{
		//selectString=selectString+"<option value=''>"+zone+"</option>";
		stmt=con.prepareStatement("SELECT div_cd,div_nm FROM mst_divisions WHERE zn_cd=?");		
		stmt.setString(1, zone);
		
	
	ResultSet rs=stmt.executeQuery();  
	
	
	while(rs.next())  {
		
	selectString=selectString+"<option value='"+rs.getString(1)+"'>"+rs.getString(2)+"</option>";
	
	}
	selectString=selectString +"</select>";
	}
	catch(Exception e)
	{
		e.printStackTrace();
	}
	finally
	{
		if(con != null)
		{
			con.close();
		}
	}
	
	
	
	
	
	return selectString;
	
	
	
}

public String department() throws SQLException{
	
	
	
	
	
	PreparedStatement stmt=null;
	DbConnection dbConnection=new DbConnection();
	Connection con=dbConnection.dbConnect();
	
	String selectString="<select class='form-control' id='department' name='department' required><option value=''>--Select--</option>";
	try{
		stmt=con.prepareStatement("SELECT dept_code,dept_name FROM rly_department");		
		
		
	
	ResultSet rs=stmt.executeQuery();  
	
	
	while(rs.next())  {
		
	selectString=selectString+"<option value='"+rs.getString(1)+"'>"+rs.getString(2)+"</option>";
	
	}
	selectString=selectString +"</select>";
	}
	catch(Exception e)
	{
		e.printStackTrace();
	}
	finally
	{
		if(con != null)
		{
			con.close();
		}
	}
	
	
	
	
	
	return selectString;
	
	
	
}



public String designation(String zone, String div, String dept) throws SQLException{
	
	
	
	
	
	PreparedStatement stmt=null;
	DbConnection dbConnection=new DbConnection();
	Connection con=dbConnection.dbConnect();
	
	String selectString="<select class='form-control' id='designation' name='designation' required><option value=''>--Select--</option>";
	try{
		stmt=con.prepareStatement("SELECT DISTINCT group_id FROM rly_users WHERE zn_cd=? AND div_cd=? AND dept_cd=? AND flag_sla=?");		
		stmt.setString(1,zone);
		stmt.setString(2,div);
		stmt.setString(3,dept);
		stmt.setString(4,"1");
	
	ResultSet rs=stmt.executeQuery();  
	
	
	while(rs.next())  {
		
	selectString=selectString+"<option value='"+rs.getString(1)+"'>"+rs.getString(1)+"</option>";
	
	}
	selectString=selectString +"</select>";
	}
	catch(Exception e)
	{
		e.printStackTrace();
	}
	finally
	{
		if(con != null)
		{
			con.close();
		}
	}
	
	
	
	
	
	return selectString;
	
	
	
}






}
