package DAO;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import utility.DbConnection;

public class slaUpdate extends HttpServlet {

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		int i=Integer.parseInt(request.getParameter("i"));
		System.out.println(i);
		PreparedStatement stmt=null;
		DbConnection dbConnection=new DbConnection();
		Connection con=dbConnection.dbConnect();
		String query="UPDATE rly_heads_subheads_master SET sla2_time_hrs=?,sla3_time_hrs=? WHERE id=?";
		try {
		for(int j=1;j<i;j++) {
			stmt=con.prepareStatement(query);	
			if(request.getParameter("sla2"+j).equalsIgnoreCase("null")) {
				stmt.setString(1,"" );
			}
			else {
				stmt.setString(1,request.getParameter("sla2"+j) );	
			}
			if(request.getParameter("sla3"+j)==null) {
				stmt.setString(2,"" );
			}
			else {
				stmt.setString(2,request.getParameter("sla3"+j) );
			}
			
			
			stmt.setInt(3,Integer.parseInt(request.getParameter("id"+j)) );
			System.out.println("id  : "+request.getParameter("id"+j));
			System.out.println("sla2  : "+request.getParameter("sla2"+j));
			System.out.println("sla3  : "+request.getParameter("sla3"+j));
		stmt.executeUpdate();
		}
		System.out.println("Succes");
		response.sendRedirect("dashboard.jsp");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		finally
		{
			if(con != null)
			{
				try {
					con.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
	}

}
