package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import utility.DbConnection;

public class SearchComplaintDAO {
	
	public String complaintAll() throws SQLException{





		PreparedStatement stmt=null;
		DbConnection dbConnection=new DbConnection();
		Connection con=dbConnection.dbConnect();

		String selectString="<select class='form-control' id='complaint_type' name='complaint_type' ><option value=''>--Select--</option>";
		try{
			stmt=con.prepareStatement("SELECT id, head_subhead_en FROM rly_heads_subheads_master WHERE parent_id IS NULL ");		

			

			ResultSet rs=stmt.executeQuery();  


			while(rs.next())  {

				selectString=selectString+"<option value='"+rs.getString("id")+"'>"+rs.getString("head_subhead_en")+"</option>";

			}
			selectString=selectString +"</select>";
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			if(con != null)
			{
				con.close();
			}
		}





		return selectString;



	}

	
	
	public String zone() throws SQLException{





		PreparedStatement stmt=null;
		DbConnection dbConnection=new DbConnection();
		Connection con=dbConnection.dbConnect();

		String selectString="<select class='form-control' id='zone_cd' name='zone_cd' ><option value=''>--Select--</option>";
		try{
			stmt=con.prepareStatement("SELECT ZN_CD,ZN_NM FROM mst_zones");		

			

			ResultSet rs=stmt.executeQuery();  


			while(rs.next())  {

				selectString=selectString+"<option value='"+rs.getString("ZN_CD")+"'>"+rs.getString("ZN_NM")+"</option>";

			}
			selectString=selectString+"<option value='ALL'>ALL</option>";
			selectString=selectString +"</select>";
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			if(con != null)
			{
				con.close();
			}
		}





		return selectString;



	}

	
	
	public String department() throws SQLException{





		PreparedStatement stmt=null;
		DbConnection dbConnection=new DbConnection();
		Connection con=dbConnection.dbConnect();

		String selectString="<select class='form-control' id='department_cd' name='department_cd' ><option value=''>--Select--</option>";
		try{
			stmt=con.prepareStatement("SELECT dept_code,dept_name FROM rly_department");		

			

			ResultSet rs=stmt.executeQuery();  


			while(rs.next())  {

				selectString=selectString+"<option value='"+rs.getString("dept_code")+"'>"+rs.getString("dept_name")+"</option>";

			}
			selectString=selectString+"<option value='ALL'>ALL</option>";
			selectString=selectString +"</select>";
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			if(con != null)
			{
				con.close();
			}
		}





		return selectString;



	}
	
	
	
	
	public String division(String zone) throws SQLException{





		PreparedStatement stmt=null;
		DbConnection dbConnection=new DbConnection();
		Connection con=dbConnection.dbConnect();
		String selectString=null;
		if(zone=="ALL") { 
		 selectString="<input id='division_cd' name='division_cd' type='text'  class='form-control' value='ALL' readonly >";
			
		}
		else {
		 selectString="<select class='form-control' id='division_cd' name='division_cd' ><option value=''>--Select--</option>";
		try{
			stmt=con.prepareStatement("SELECT div_cd ,div_nm FROM mst_divisions WHERE zn_cd=?");		
            stmt.setString(1, zone);
			

			ResultSet rs=stmt.executeQuery();  


			while(rs.next())  {

				selectString=selectString+"<option value='"+rs.getString("div_cd")+"'>"+rs.getString("div_nm")+"</option>";

			}
			selectString=selectString+"<option value='ALL'>ALL</option>";
			selectString=selectString +"</select>";
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		
		
		finally
		{
			if(con != null)
			{
				con.close();
			}
		}
		}




		return selectString;



	}
	
	
}
