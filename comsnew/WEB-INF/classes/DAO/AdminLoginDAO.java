package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import utility.DbConnection;

public class AdminLoginDAO {
	
	public String getLoginDetails(String uname,String passord){
		
		PreparedStatement stmt=null;
		ResultSet rs=null;
		DbConnection dbConnection=new DbConnection();
		Connection con=dbConnection.dbConnect();
		int count=0;
		String result="false";
		
		String query="SELECT COUNT(*) FROM rly_users_sla1 WHERE login_id=? AND PASSWORD=? ";
		try{
			stmt=con.prepareStatement(query);	
			
			stmt.setString(1, uname);
			stmt.setString(2, passord);
			
		
		rs=stmt.executeQuery(); 
		while(rs.next()){
			count=rs.getInt(1);
			System.out.println("count: "+count);
		}
		if(count>0){
			result="true";
		}
		
		}
		catch(Exception e){
			
		}
		
		return result;
	}
	

}
