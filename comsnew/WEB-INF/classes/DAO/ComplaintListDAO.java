package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import utility.DbConnection;

public class ComplaintListDAO {
	
	
	public String getComplaintList(String group_id,String zn_cd,String div_cd,String dept_cd){
		
		PreparedStatement stmt=null;
		DbConnection dbConnection=new DbConnection();
		Connection con=dbConnection.dbConnect();
		String selectString="<div class='table-responsive'>   <table class='table table-hover' >" + 
				"    <thead>" + 
				"      <tr>" + 
				"        <th>Sr No</th>" + 
				"        <th>Reference No</th>" + 
				"        <th>Registered Date</th>" + 
				"        <th>Complaint Type</th>" + 
				"        <th>Department</th>" + 
				"        <th>Complaint Mode</th>" + 
				"        <th>Channel Type</th>" + 
				
				"        <th>Authority</th>" + 
				"        <th>Action</th>" + 
				"      </tr>" + 
				"    </thead>" + 
				"    <tbody>";




		try{
		/*	System.out.println("group_id: "+group_id); 
	         System.out.println("zn_cd: "+zn_cd); 
	         System.out.println("div_cd: "+div_cd); 
	         System.out.println("dept_cd: "+dept_cd); */
			
			ResultSet rs1=null;
			String query_fetch_complaint_details="SELECT complaint_ref_no, created_on,head_subhead_en,dept_name,user_group_id,complaint_mode,channel_type,rly_complaint.id "+
			" FROM rly_complaint,rly_heads_subheads_master,rly_department  "+
			"WHERE complaint_ref_no =? "+
			" AND rly_complaint.complaint_type=rly_heads_subheads_master.id AND rly_complaint.dept_cd=rly_department.dept_code ";
			stmt=con.prepareStatement("SELECT DISTINCT complaint_ref_no FROM rly_complaint_users WHERE user_group_id=? AND STATUS!=? ");
			 stmt.setString(1, group_id);
			 stmt.setString(2, "Closed");
			 rs1=stmt.executeQuery();  
			int i=1;
			 while(rs1.next()) {
				 stmt=con.prepareStatement(query_fetch_complaint_details);
				 stmt.setString(1,rs1.getString(1));
				ResultSet rs=stmt.executeQuery();  
	 			
			   while(rs.next())  {

						selectString=selectString+"<tr>";
						selectString=selectString+"<td><font color=red>"+i+"</font> </td>";
						selectString=selectString+"<td><font color=red>"+rs.getString(1)+" </font></td>";
						selectString=selectString+"<td><font color=red>"+rs.getString(2)+"</font></td>";
						selectString=selectString+"<td><font color=red>"+rs.getString(3)+"</font></td>";
						selectString=selectString+"<td><font color=red>"+rs.getString(4)+"</font></td>";
						if(rs.getString(6).equals("T")){
							selectString=selectString+"<td><font color=red>Train</font></td>";
						}
						if(rs.getString(6).equals("S")){
							selectString=selectString+"<td><font color=red>Station</font></td>";
						}
						
						if(rs.getString(7).equals("W")){
							selectString=selectString+"<td><font color=red>Web</font></td>";
						}
						if(rs.getString(7).equals("H")){
							selectString=selectString+"<td><font color=red>Helpline-138</font></td>";
						}
						if(rs.getString(7).equals("R")){
							selectString=selectString+"<td><font color=red>Helpline-182</font></td>";
						}
						if(rs.getString(7).equals("M")){
							selectString=selectString+"<td><font color=red>Social Media</font></td>";
						}
						if(rs.getString(7).equals("D")){
							selectString=selectString+"<td><font color=red>Manual Dak</font></td>";
						}
						if(rs.getString(7).equals("C")){
							selectString=selectString+"<td><font color=red>Catering Complaints, Rly Board</font></td>";
						}
						
						selectString=selectString+"<td><font color=red>"+rs.getString(5)+"</font></td>";
						selectString=selectString+"<td><a href=ComplaintAction.jsp?Id="+rs.getString(1)+"><img border=0 alt=view title=view src=img/new.jpg width=60 height=50></a></td>";
						i++;
						selectString=selectString+"</tr>";
			   	}
			 }
			


			selectString=selectString +"</tbody></table></div> ";

			//System.out.println("selectString: "+selectString);


		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			if(con != null)
			{
				try {
					con.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}





		return selectString;

	}

}
