package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import utility.DbConnection;

public class SuggestionDAO {
	
	
	public String complaintType() throws SQLException{





		PreparedStatement stmt=null;
		DbConnection dbConnection=new DbConnection();
		Connection con=dbConnection.dbConnect();

		String selectString="<select class='form-control' id='complaint_type' name='complaint_type' title='Please Enter any Suggestion' required style='font-size:20px!important;'>";
		try{
			stmt=con.prepareStatement("SELECT id,suggestions FROM rly_suggestion_type");		

			
			ResultSet rs=stmt.executeQuery();  


			while(rs.next())  {

				selectString=selectString+"<option value='"+rs.getString(1)+"'>"+rs.getString(2)+"</option>";

			}
			selectString=selectString +"</select>";
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			if(con != null)
			{
				con.close();
			}
		}





		return selectString;



	}
	

public String stationName() throws SQLException{
		
		
		
		
		
		PreparedStatement stmt=null;
		DbConnection dbConnection=new DbConnection();
		Connection con=dbConnection.dbConnect();
		
		
		
		
		
		
		String selectString="<input list='stationName' name='stationName' class='form-control' title='Please Enter Station Name' style='font-size:20px!important;'> <datalist id='stationName'style='font-size:20px!important;' >";
		try{
			stmt=con.prepareStatement("SELECT station_name ,station_cd FROM rly_station_master  ;");	
			
		//	stmt.setString(1, "!");
			
		
		ResultSet rs=stmt.executeQuery();  
		
		
		while(rs.next())  {
			
			selectString=selectString+"<option value='"+rs.getString(1)+" -> "+rs.getString(2)+""+"'>";
		//	selectString=selectString+"<option value='"+rs.getString(1)+"'>";
		
		}
		selectString=selectString +"</datalist>";
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			if(con != null)
			{
				con.close();
			}
		}
		
		
		
		
		
		return selectString;
		
		
		
	}

	public String trainNo() throws SQLException{





		PreparedStatement stmt=null;
		DbConnection dbConnection=new DbConnection();
		Connection con=dbConnection.dbConnect();






		String selectString="<input list='trainno' name='trainno' class='form-control' title='Please Enter Train Number/Name' style='font-size:20px!important;'> <datalist id='trainno' style='font-size:20px!important;'>";
		try{
			stmt=con.prepareStatement("SELECT train_no,train_name FROM rly_train_master");	

			//	stmt.setString(1, "!");


			ResultSet rs=stmt.executeQuery();  


			while(rs.next())  {
				selectString=selectString+"<option value='"+rs.getString(1)+" - "+rs.getString(2)+""+"'>";
			//	selectString=selectString+"<option value='"+rs.getString(1)+"'>"+rs.getString(1);

			}
			selectString=selectString +"</datalist>";
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			if(con != null)
			{
				con.close();
			}
		}





		return selectString;



	}

}
