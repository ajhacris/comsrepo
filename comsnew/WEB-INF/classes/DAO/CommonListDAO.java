package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import utility.DbConnection;



public class CommonListDAO {


	public String complaintType(String complaint_place) throws SQLException{





		PreparedStatement stmt=null;
		DbConnection dbConnection=new DbConnection();
		Connection con=dbConnection.dbConnect();

		String selectString="<select class='form-control' id='complaint_type' name='complaint_type' required >";
		try{
			stmt=con.prepareStatement("SELECT id, head_subhead_en FROM rly_heads_subheads_master WHERE parent_id IS NULL and flag=?");		

			stmt.setString(1,complaint_place );

			ResultSet rs=stmt.executeQuery();  

			selectString=selectString+" <option value=''>------ Select ------</option>";
			while(rs.next())  {
				
				selectString=selectString+"<option value='"+rs.getString("id")+"'>"+rs.getString("head_subhead_en")+"</option>";

			}
			selectString=selectString +"</select>";
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			if(con != null)
			{
				con.close();
			}
		}





		return selectString;



	}
	public String complaintType1(String complaint_place) throws SQLException{





		PreparedStatement stmt=null;
		DbConnection dbConnection=new DbConnection();
		Connection con=dbConnection.dbConnect();

		String selectString="<select class='form-control' id='complaint_type' name='complaint_type' required> <option value=''>--Select--</option>";
		try{
			stmt=con.prepareStatement("SELECT id, head_subhead_en FROM rly_heads_subheads_master WHERE parent_id IS NULL and flag=?");		

			stmt.setString(1,complaint_place );

			ResultSet rs=stmt.executeQuery();  


			while(rs.next())  {

				selectString=selectString+"<option value='"+rs.getString("id")+"'>"+rs.getString("head_subhead_en")+"</option>";

			}
			selectString=selectString +"</select>";
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			if(con != null)
			{
				con.close();
			}
		}





		return selectString;



	}

	
	
	
	
	
	
	
	
	public String subComplaintType(String parent_id) throws SQLException{





		PreparedStatement stmt=null;
		DbConnection dbConnection=new DbConnection();
		Connection con=dbConnection.dbConnect();

		String selectString="<select class='form-control' id='sub_complaint_type' name='sub_complaint_type' required style='font-size:20px!important;'> ";
		System.out.println(parent_id);
		try{
			stmt=con.prepareStatement("SELECT id, head_subhead_en FROM rly_heads_subheads_master WHERE parent_id =?");	

			stmt.setString(1, parent_id);


			ResultSet rs=stmt.executeQuery();  


			while(rs.next())  {

				selectString=selectString+"<option value='"+rs.getString("id")+"'>"+rs.getString("head_subhead_en")+"</option>";

			}
			selectString=selectString +"</select>";
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			if(con != null)
			{
				con.close();
			}
		}





		return selectString;



	}

	
	
	
	public String subComplaintType1(String parent_id) throws SQLException{





		PreparedStatement stmt=null;
		DbConnection dbConnection=new DbConnection();
		Connection con=dbConnection.dbConnect();

		String selectString="<select class='form-control' id='sub_complaint_type' name='sub_complaint_type' required>  <option value''>--Select--</option>";
		System.out.println(parent_id);
		try{
			stmt=con.prepareStatement("SELECT id, head_subhead_en FROM rly_heads_subheads_master WHERE parent_id =?");	

			stmt.setString(1, parent_id);


			ResultSet rs=stmt.executeQuery();  


			while(rs.next())  {

				selectString=selectString+"<option value='"+rs.getString("id")+"'>"+rs.getString("head_subhead_en")+"</option>";

			}
			selectString=selectString +"</select>";
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			if(con != null)
			{
				con.close();
			}
		}





		return selectString;



	}



public String stationName() throws SQLException{
		
		
		
		
		
		PreparedStatement stmt=null;
		DbConnection dbConnection=new DbConnection();
		Connection con=dbConnection.dbConnect();
		
		
		
		
		
		
		String selectString="<input list='stationName' name='stationName'  class='form-control' style='font-size:20px!important;'> <datalist id='stationName' >";
		try{
			stmt=con.prepareStatement("SELECT station_name ,station_cd FROM rly_station_master  ;");	
			
		//	stmt.setString(1, "!");
			
		
		ResultSet rs=stmt.executeQuery();  
		
		
		while(rs.next())  {
			
			selectString=selectString+"<option value='"+rs.getString(1)+" -> "+rs.getString(2)+""+"'>";
		//	selectString=selectString+"<option value='"+rs.getString(1)+"'>";
		
		}
		selectString=selectString +"</datalist>";
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			if(con != null)
			{
				con.close();
			}
		}
		
		
		
		
		
		return selectString;
		
		
		
	}

	public String trainNo() throws SQLException{





		PreparedStatement stmt=null;
		DbConnection dbConnection=new DbConnection();
		Connection con=dbConnection.dbConnect();






		String selectString="<input list='trainno' name='trainno' class='form-control' style='font-size:20px!important;'> <datalist id='trainno' >";
		try{
			stmt=con.prepareStatement("SELECT train_no,train_name FROM rly_train_master");	

			//	stmt.setString(1, "!");


			ResultSet rs=stmt.executeQuery();  


			while(rs.next())  {
				selectString=selectString+"<option value='"+rs.getString(1)+" - "+rs.getString(2)+""+"'>";
			//	selectString=selectString+"<option value='"+rs.getString(1)+"'>"+rs.getString(1);

			}
			selectString=selectString +"</datalist>";
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			if(con != null)
			{
				con.close();
			}
		}





		return selectString;



	}


	public String complaintType() throws SQLException{





		PreparedStatement stmt=null;
		DbConnection dbConnection=new DbConnection();
		Connection con=dbConnection.dbConnect();

		String selectString="<select class='form-control' id='complaint_type' name='complaint_type' required style='font-size:20px!important;'>";
		try{
			stmt=con.prepareStatement("SELECT id,  head_subhead_en FROM rly_heads_subheads_master WHERE parent_id IS NULL ");		



			ResultSet rs=stmt.executeQuery();  


			while(rs.next())  {

				selectString=selectString+"<option value='"+rs.getString("id")+"'>"+rs.getString("head_subhead_en")+"</option>";

			}
			selectString=selectString +"</select>";
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			if(con != null)
			{
				con.close();
			}
		}





		return selectString;



	}	

	public String SlaTime(String sla_time) throws SQLException{





		PreparedStatement stmt=null;
		DbConnection dbConnection=new DbConnection();
		Connection con=dbConnection.dbConnect();
		String selectString="<table class='table table-hover' >" + 
				"    <thead>" + 
				"      <tr>" + 
				"        <th>Complaint</th>" + 
				"        <th>SLA2 (Hrs.)</th>" + 
				"        <th>SLA3 (Hrs.)</th>" + 
				"      </tr>" + 
				"    </thead>" + 
				"    <tbody>";




		try{
			stmt=con.prepareStatement("SELECT id, head_subhead_en ,sla2_time_hrs,sla3_time_hrs FROM rly_heads_subheads_master WHERE parent_id=?");		

			stmt.setString(1,sla_time );

			ResultSet rs=stmt.executeQuery();  

			int i=1;
			while(rs.next())  {

				selectString=selectString+"<tr>";
				selectString=selectString+"<td><input type='hidden' name=id"+i+" value="+rs.getString(1)+" ></input> "+rs.getString(2)+" </td>";
				if(rs.getString(3)== null || rs.getString(3).equals("")){
					selectString=selectString+"<td><input type='text' name=sla2"+i+" value=''></input></td>";
				}
				
				else{
					selectString=selectString+"<td><input type='text' name=sla2"+i+" value="+rs.getString(3)+"></input></td>";
					}
				
				if(rs.getString(4)== null ||rs.getString(4).equals("")){
					selectString=selectString+"<td><input type='text' name=sla3"+i+" value=''></input></td>";
						}
				
				else{
					selectString=selectString+"<td><input type='text' name=sla3"+i+" value="+rs.getString(4)+"></input></td>";
						}
				
				
				
				i++;
				selectString=selectString+"</tr>";
			}

			selectString=selectString+"<tr>";

			selectString=selectString+"<td><input type='hidden' name=i value="+i+"></input></td>";
			selectString=selectString+"<td><button id='submitbtn' type='submit' name='submitbtn' class='btn btn-primary' >Submit</button></td>";
			selectString=selectString+"<td></td>";

			selectString=selectString+"</tr>";


			selectString=selectString +"</tbody></table> ";



		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			if(con != null)
			{
				con.close();
			}
		}





		return selectString;



	}
	public String SlaTimeView(String sla_time) throws SQLException{





		PreparedStatement stmt=null;
		DbConnection dbConnection=new DbConnection();
		Connection con=dbConnection.dbConnect();
		String selectString="<table class='table table-hover' >" + 
				"    <thead>" + 
				"      <tr>" + 
				"        <th>Complaint</th>" + 
				"        <th>SLA2 (Hrs.)</th>" + 
				"        <th>SLA3 (Hrs.)</th>" + 
				"      </tr>" + 
				"    </thead>" + 
				"    <tbody>";




		try{
			stmt=con.prepareStatement("SELECT id, head_subhead_en ,sla2_time_hrs,sla3_time_hrs FROM rly_heads_subheads_master WHERE parent_id=?");		

			stmt.setString(1,sla_time );

			ResultSet rs=stmt.executeQuery();  

			int i=1;
			while(rs.next())  {

				selectString=selectString+"<tr>";
				selectString=selectString+"<td><input type='hidden' name=id"+i+" value="+rs.getString(1)+" ></input> "+rs.getString(2)+" </td>";
				if(rs.getString(3)== null || rs.getString(3).equals("")){
					selectString=selectString+"<td></td>";
				}
				
				else{
					selectString=selectString+"<td>"+rs.getString(3)+"</td>";
					}
				
				if(rs.getString(4)== null ||rs.getString(4).equals("")){
					selectString=selectString+"<td></td>";
						}
				
				else{
					selectString=selectString+"<td>"+rs.getString(4)+"</td>";
						}
				
				
				
				i++;
				selectString=selectString+"</tr>";
			}

			selectString=selectString+"<tr>";

			/*selectString=selectString+"<td><input type='hidden' name=i value="+i+"></input></td>";
			selectString=selectString+"<td><button id='submitbtn' type='submit' name='submitbtn' class='btn btn-primary' >Submit</button></td>";
			*/selectString=selectString+"<td></td>";

			selectString=selectString+"</tr>";


			selectString=selectString +"</tbody></table> ";



		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			if(con != null)
			{
				con.close();
			}
		}





		return selectString;



	}

	public String compChannel() throws SQLException{





		PreparedStatement stmt=null;
		DbConnection dbConnection=new DbConnection();
		Connection con=dbConnection.dbConnect();

        String selectString="<select class='form-control' id='complaint_channel' name='complaint_channel' required><option value=''>--Select--</option>";
		
		try{
			stmt=con.prepareStatement("SELECT menu_items,menu_item_flag FROM complaint_menu_items where active_yn='1'");		
			
			
		
		ResultSet rs=stmt.executeQuery();  
		
		
		while(rs.next())  {
			
		selectString=selectString+"<option value='"+rs.getString("menu_item_flag")+"'>"+rs.getString("menu_items")+"</option>";
		
		}
		selectString=selectString +"</select>";
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			if(con != null)
			{
				con.close();
			}
		}





		return selectString;



	}
	public String complaintAll() throws SQLException{





		PreparedStatement stmt=null;
		DbConnection dbConnection=new DbConnection();
		Connection con=dbConnection.dbConnect();

		String selectString="<select class='form-control' id='complaint_type' name='complaint_type' required><option value=''>--Select--</option>";
		try{
			stmt=con.prepareStatement("SELECT id, head_subhead_en FROM rly_heads_subheads_master WHERE parent_id IS NULL ");		

			

			ResultSet rs=stmt.executeQuery();  


			while(rs.next())  {

				selectString=selectString+"<option value='"+rs.getString("id")+"'>"+rs.getString("head_subhead_en")+"</option>";

			}
			selectString=selectString +"</select>";
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			if(con != null)
			{
				con.close();
			}
		}





		return selectString;



	}
	
	
	public String complaintPlace(String parent_id) throws SQLException{





		PreparedStatement stmt=null;
		DbConnection dbConnection=new DbConnection();
		Connection con=dbConnection.dbConnect();
		System.out.println(parent_id);
		String selectString="<input type='text' class='form-control' id='complaint_place' name='complaint_place' required";
		try{
			stmt=con.prepareStatement("SELECT flag FROM rly_heads_subheads_master WHERE id =?");	

			stmt.setString(1, parent_id);


			ResultSet rs=stmt.executeQuery();  


			while(rs.next())  {
if(rs.getString(1).equals("s")) {selectString=selectString+" value='On Station";}
if(rs.getString(1).equals("t")) {selectString=selectString+" value='In Train";}
				
				System.out.println(rs.getString(1));
			}
			selectString=selectString +"' readonly>";
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			if(con != null)
			{
				con.close();
			}
		}





		return selectString;



	}

	
	public String details() throws SQLException{




System.out.println("in detail section");


		
		String selectString="<div class='row'>" +
				"			    <div class='col-md-4'" + 
				"                <div class='form-group'>" + 
				"                    <label for='plateform_no'>Platform No </label>" + 
				"                    <input id='platform_no' type='text' name='platform_no' class='form-control' required='required' data-error='Lastname is required.'>" + 
				"                </div>" + 
				"             <div class='col-md-4'>" + 
				"                <div class='form-group'>" + 
				"                   <label for='station_name'>Station Name </label>";  
			
				   
		
		PreparedStatement stmt=null;
		DbConnection dbConnection=new DbConnection();
		Connection con=dbConnection.dbConnect();
		
		
		
		
		
		
		selectString = selectString+"<input list='stationName' name='stationName' class='form-control'> <datalist id='stationName' >";
		try{
			stmt=con.prepareStatement("SELECT station_name ,station_cd FROM rly_station_master  ;");	
			
		//	stmt.setString(1, "!");
			
		
		ResultSet rs=stmt.executeQuery();  
		
		
		while(rs.next())  {
			
			selectString=selectString+"<option value='"+rs.getString(1)+" -> "+rs.getString(2)+""+"'>";
			//selectString=selectString+"<option value='"+rs.getString(1)+"'>";
		
		}
		selectString=selectString +"</datalist>";
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			if(con != null)
			{
				con.close();
			}
		}
		
		
		
	
		
		
		
 selectString = selectString+ "  </div>" + 
				"            </div>" + 
				"		</div>";
		




		return selectString;



	}
	
	public String details1() throws SQLException{




System.out.println("in detail section IN TRAIN");
String selectString=" <div class=\"row\">\r\n" + 
		"			   <p style=\"color:#f05f40;\">Journey Details  </p>  \r\n" + 
		"			   </div>\r\n" + 
		"			    \r\n" + 
		"			    <div class=\"row\">\r\n" + 
		"			   \r\n" + 
		"			   \r\n" + 
		"			    <div class=\"col-md-4\">\r\n" + 
		"                <div class=\"form-group\">\r\n" + 
		"                   <select  name=\"pmode\" id=\"pmode\" class='form-control' >\r\n" +
		"          <option value=\"\">--Select--</option>\r\n" + 
		"          <option value=\"PNR\">PNR</option>\r\n" + 
		"          <option value=\"UTS\">UTS</option>\r\n" + 
		"         \r\n" + 
		"	      \r\n" + 
		"          \r\n" + 
		"       </select> \r\n" + 
		"                </div>\r\n" + 
		"            </div>\r\n" + 
		"            </div>";

		
		/*String selectString="  <div class='row' id='PNR'>" + 
				
				"			    <div class='col-md-4'>" + 
				"                <div class='form-group'>" + 
				"                    <label for='pnr_uts'>PNR/UTS No *</label>" + 
				"                    <input id='pnr_uts' type='text' name='pnr_uts' class='form-control' >\r\n" + 
				
				"                </div>" + 
				"            </div>" + 
				"             <div class='col-md-4'>" + 
				"                <div class='form-group'>" + 
				"                    </br>  \r\n" + 
				"                    <button id='pnr_utsbtn' type='button' name='pnr_utsbtn' class='btn btn-primary' >FETCH PNR</button>\r\n" + 
				"                    \r\n" + 
				"                </div>\r\n" + 
				"            </div>\r\n" + 
				"			   <div id='pnr_error' class='col-md-4' style='color:red;'></div>\r\n" + 
				"			   \r\n" + 
				"			  </div>" ;*/


		return selectString;



	}



}
