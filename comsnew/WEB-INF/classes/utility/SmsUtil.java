package utility;

import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import javax.xml.rpc.ServiceException;

import com.comviva.cris.webservice.AcfEncryptDecryptManager;
import com.comviva.cris.webservice.AlertResponse;
import com.comviva.cris.webservice.AlertServiceImpl;
import com.comviva.cris.webservice.AlertServiceImplServiceLocator;
import com.comviva.cris.webservice.DefaultDataEncryptImpl;

public class SmsUtil {

	
	public void sendMessage(String mobileNumber,String msgText) throws RemoteException, ServiceException {
		System.out.println("mobileNumber : " +mobileNumber);
		System.out.println("msgText : " +msgText);
		AcfEncryptDecryptManager manager = AcfEncryptDecryptManager.getInstance();
		System.out.println("1" );
		DefaultDataEncryptImpl ecryptImpt = new DefaultDataEncryptImpl("!@#$%^&*");
		System.out.println("2 " );
		manager.setEncryptionImpl(ecryptImpt);
		System.out.println("3 " );
		
		AlertServiceImplServiceLocator locator = new AlertServiceImplServiceLocator();
		System.out.println("IRSMSA Code Check1");
		locator.setAlertServiceImplEndpointAddress(
				"http://10.64.26.139:9001/CrisAlertWebService/services/AlertServiceImpl");
		System.out.println("IRSMSA Code Check2");
		AlertServiceImpl service = locator.getAlertServiceImpl();
		String appId = manager.encrypt("cmsprs");
		String pass = manager.encrypt("cmsprs@com");
		
		AlertResponse response = service.sendAlertMessage(appId, pass, "IRSMSA", mobileNumber, msgText);
		System.out.println("Submission ID   : " + response.getSubmissionId());
		System.out.println("Status Code     : " + response.getStatusCode());
		System.out.println("Response Message : " + response.getStatusMessage());
		System.out.println("IRSMSA Client End");
	}
	
	
	/********* Send SMS to Passenger after registering Complaint *********/
	
	public void getMobileNumber(String group_id,String msgText){
		
		DbConnection dbConnection=new DbConnection();
		Connection con=dbConnection.dbConnect();
		ResultSet rs=null;
		PreparedStatement stmt=null;
		String query="";
		ArrayList<String> mobilenumber=new ArrayList<String>();
		
		try{
			System.out.println("group_id: "+group_id);
			System.out.println("msgText: "+msgText);
			query="SELECT DISTINCT mobile FROM rly_users WHERE group_id=?";
			stmt=con.prepareStatement(query);	
			stmt.setString(1, group_id);
			rs=stmt.executeQuery();  
			while(rs.next())  {
				mobilenumber.add(rs.getString(1));			
			}
			for(int i=0;i<mobilenumber.size();i++){
				System.out.println("mobilenumber: "+mobilenumber.get(i));
				sendMessage(mobilenumber.get(i),msgText);
			}
			
		}catch(Exception e){
			
		}
		
		
	}
	
public void sendMessageToAdmin(String mobileNumber,String msgText) throws RemoteException, ServiceException {
		
		AcfEncryptDecryptManager manager = AcfEncryptDecryptManager.getInstance();
		DefaultDataEncryptImpl ecryptImpt = new DefaultDataEncryptImpl("!@#$%^&*");
		manager.setEncryptionImpl(ecryptImpt);
		AlertServiceImplServiceLocator locator = new AlertServiceImplServiceLocator();
		System.out.println("IRSMSA Code Check1");
		locator.setAlertServiceImplEndpointAddress(
				"http://10.64.26.139:9001/CrisAlertWebService/services/AlertServiceImpl");
		System.out.println("IRSMSA Code Check2");
		AlertServiceImpl service = locator.getAlertServiceImpl();
		String appId = manager.encrypt("cmsprs");
		String pass = manager.encrypt("cmsprs@com");
		
		AlertResponse response = service.sendAlertMessage(appId, pass, "IRSMSA", mobileNumber, msgText);
		System.out.println("Submission ID   : " + response.getSubmissionId());
		System.out.println("Status Code     : " + response.getStatusCode());
		System.out.println("Response Message : " + response.getStatusMessage());
		System.out.println("IRSMSA Client End");
	}
}
