/**
 * CurStatPsgn.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package cris.prs.coms.wsdl;

public class CurStatPsgn  implements java.io.Serializable {
    private int[] age;

    private java.lang.String[] coachposition;

    private java.lang.String[] concessioncode;

    private java.lang.String[] gender;

    private java.lang.String[] lapOnebkgstat;

    private java.lang.String[] lapOnecurstat;

    private java.lang.String[] psgnname;

    private java.lang.String[] quotacode;

    public CurStatPsgn() {
    }

    public CurStatPsgn(
           int[] age,
           java.lang.String[] coachposition,
           java.lang.String[] concessioncode,
           java.lang.String[] gender,
           java.lang.String[] lapOnebkgstat,
           java.lang.String[] lapOnecurstat,
           java.lang.String[] psgnname,
           java.lang.String[] quotacode) {
           this.age = age;
           this.coachposition = coachposition;
           this.concessioncode = concessioncode;
           this.gender = gender;
           this.lapOnebkgstat = lapOnebkgstat;
           this.lapOnecurstat = lapOnecurstat;
           this.psgnname = psgnname;
           this.quotacode = quotacode;
    }


    /**
     * Gets the age value for this CurStatPsgn.
     * 
     * @return age
     */
    public int[] getAge() {
        return age;
    }


    /**
     * Sets the age value for this CurStatPsgn.
     * 
     * @param age
     */
    public void setAge(int[] age) {
        this.age = age;
    }


    /**
     * Gets the coachposition value for this CurStatPsgn.
     * 
     * @return coachposition
     */
    public java.lang.String[] getCoachposition() {
        return coachposition;
    }


    /**
     * Sets the coachposition value for this CurStatPsgn.
     * 
     * @param coachposition
     */
    public void setCoachposition(java.lang.String[] coachposition) {
        this.coachposition = coachposition;
    }


    /**
     * Gets the concessioncode value for this CurStatPsgn.
     * 
     * @return concessioncode
     */
    public java.lang.String[] getConcessioncode() {
        return concessioncode;
    }


    /**
     * Sets the concessioncode value for this CurStatPsgn.
     * 
     * @param concessioncode
     */
    public void setConcessioncode(java.lang.String[] concessioncode) {
        this.concessioncode = concessioncode;
    }


    /**
     * Gets the gender value for this CurStatPsgn.
     * 
     * @return gender
     */
    public java.lang.String[] getGender() {
        return gender;
    }


    /**
     * Sets the gender value for this CurStatPsgn.
     * 
     * @param gender
     */
    public void setGender(java.lang.String[] gender) {
        this.gender = gender;
    }


    /**
     * Gets the lapOnebkgstat value for this CurStatPsgn.
     * 
     * @return lapOnebkgstat
     */
    public java.lang.String[] getLapOnebkgstat() {
        return lapOnebkgstat;
    }


    /**
     * Sets the lapOnebkgstat value for this CurStatPsgn.
     * 
     * @param lapOnebkgstat
     */
    public void setLapOnebkgstat(java.lang.String[] lapOnebkgstat) {
        this.lapOnebkgstat = lapOnebkgstat;
    }


    /**
     * Gets the lapOnecurstat value for this CurStatPsgn.
     * 
     * @return lapOnecurstat
     */
    public java.lang.String[] getLapOnecurstat() {
        return lapOnecurstat;
    }


    /**
     * Sets the lapOnecurstat value for this CurStatPsgn.
     * 
     * @param lapOnecurstat
     */
    public void setLapOnecurstat(java.lang.String[] lapOnecurstat) {
        this.lapOnecurstat = lapOnecurstat;
    }


    /**
     * Gets the psgnname value for this CurStatPsgn.
     * 
     * @return psgnname
     */
    public java.lang.String[] getPsgnname() {
        return psgnname;
    }


    /**
     * Sets the psgnname value for this CurStatPsgn.
     * 
     * @param psgnname
     */
    public void setPsgnname(java.lang.String[] psgnname) {
        this.psgnname = psgnname;
    }


    /**
     * Gets the quotacode value for this CurStatPsgn.
     * 
     * @return quotacode
     */
    public java.lang.String[] getQuotacode() {
        return quotacode;
    }


    /**
     * Sets the quotacode value for this CurStatPsgn.
     * 
     * @param quotacode
     */
    public void setQuotacode(java.lang.String[] quotacode) {
        this.quotacode = quotacode;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CurStatPsgn)) return false;
        CurStatPsgn other = (CurStatPsgn) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.age==null && other.getAge()==null) || 
             (this.age!=null &&
              java.util.Arrays.equals(this.age, other.getAge()))) &&
            ((this.coachposition==null && other.getCoachposition()==null) || 
             (this.coachposition!=null &&
              java.util.Arrays.equals(this.coachposition, other.getCoachposition()))) &&
            ((this.concessioncode==null && other.getConcessioncode()==null) || 
             (this.concessioncode!=null &&
              java.util.Arrays.equals(this.concessioncode, other.getConcessioncode()))) &&
            ((this.gender==null && other.getGender()==null) || 
             (this.gender!=null &&
              java.util.Arrays.equals(this.gender, other.getGender()))) &&
            ((this.lapOnebkgstat==null && other.getLapOnebkgstat()==null) || 
             (this.lapOnebkgstat!=null &&
              java.util.Arrays.equals(this.lapOnebkgstat, other.getLapOnebkgstat()))) &&
            ((this.lapOnecurstat==null && other.getLapOnecurstat()==null) || 
             (this.lapOnecurstat!=null &&
              java.util.Arrays.equals(this.lapOnecurstat, other.getLapOnecurstat()))) &&
            ((this.psgnname==null && other.getPsgnname()==null) || 
             (this.psgnname!=null &&
              java.util.Arrays.equals(this.psgnname, other.getPsgnname()))) &&
            ((this.quotacode==null && other.getQuotacode()==null) || 
             (this.quotacode!=null &&
              java.util.Arrays.equals(this.quotacode, other.getQuotacode())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAge() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getAge());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getAge(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getCoachposition() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCoachposition());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCoachposition(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getConcessioncode() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getConcessioncode());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getConcessioncode(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getGender() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getGender());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getGender(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getLapOnebkgstat() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getLapOnebkgstat());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getLapOnebkgstat(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getLapOnecurstat() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getLapOnecurstat());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getLapOnecurstat(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getPsgnname() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPsgnname());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPsgnname(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getQuotacode() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getQuotacode());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getQuotacode(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CurStatPsgn.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://testPkg", "CurStatPsgn"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("age");
        elemField.setXmlName(new javax.xml.namespace.QName("http://testPkg", "age"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://webportal.prs.com", "item"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("coachposition");
        elemField.setXmlName(new javax.xml.namespace.QName("http://testPkg", "coachposition"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://webportal.prs.com", "item"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("concessioncode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://testPkg", "concessioncode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://webportal.prs.com", "item"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("gender");
        elemField.setXmlName(new javax.xml.namespace.QName("http://testPkg", "gender"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://webportal.prs.com", "item"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lapOnebkgstat");
        elemField.setXmlName(new javax.xml.namespace.QName("http://testPkg", "lapOnebkgstat"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://webportal.prs.com", "item"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lapOnecurstat");
        elemField.setXmlName(new javax.xml.namespace.QName("http://testPkg", "lapOnecurstat"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://webportal.prs.com", "item"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("psgnname");
        elemField.setXmlName(new javax.xml.namespace.QName("http://testPkg", "psgnname"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://webportal.prs.com", "item"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("quotacode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://testPkg", "quotacode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://webportal.prs.com", "item"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
