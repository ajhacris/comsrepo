/**
 * CurStatLap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package cris.prs.coms.wsdl;

public class CurStatLap  implements java.io.Serializable {
    private java.lang.String[] brdpt;

    private java.lang.String[] cls;

    private int[] day;

    private int[] month;

    private java.lang.String[] msgstn;

    private java.lang.String[] resupto;

    private java.lang.String[] stnfrom;

    private java.lang.String[] stnto;

    private java.lang.String[] trainName;

    private java.lang.String[] trainNo;

    private int[] year;

    public CurStatLap() {
    }

    public CurStatLap(
           java.lang.String[] brdpt,
           java.lang.String[] cls,
           int[] day,
           int[] month,
           java.lang.String[] msgstn,
           java.lang.String[] resupto,
           java.lang.String[] stnfrom,
           java.lang.String[] stnto,
           java.lang.String[] trainName,
           java.lang.String[] trainNo,
           int[] year) {
           this.brdpt = brdpt;
           this.cls = cls;
           this.day = day;
           this.month = month;
           this.msgstn = msgstn;
           this.resupto = resupto;
           this.stnfrom = stnfrom;
           this.stnto = stnto;
           this.trainName = trainName;
           this.trainNo = trainNo;
           this.year = year;
    }


    /**
     * Gets the brdpt value for this CurStatLap.
     * 
     * @return brdpt
     */
    public java.lang.String[] getBrdpt() {
        return brdpt;
    }


    /**
     * Sets the brdpt value for this CurStatLap.
     * 
     * @param brdpt
     */
    public void setBrdpt(java.lang.String[] brdpt) {
        this.brdpt = brdpt;
    }


    /**
     * Gets the cls value for this CurStatLap.
     * 
     * @return cls
     */
    public java.lang.String[] getCls() {
        return cls;
    }


    /**
     * Sets the cls value for this CurStatLap.
     * 
     * @param cls
     */
    public void setCls(java.lang.String[] cls) {
        this.cls = cls;
    }


    /**
     * Gets the day value for this CurStatLap.
     * 
     * @return day
     */
    public int[] getDay() {
        return day;
    }


    /**
     * Sets the day value for this CurStatLap.
     * 
     * @param day
     */
    public void setDay(int[] day) {
        this.day = day;
    }


    /**
     * Gets the month value for this CurStatLap.
     * 
     * @return month
     */
    public int[] getMonth() {
        return month;
    }


    /**
     * Sets the month value for this CurStatLap.
     * 
     * @param month
     */
    public void setMonth(int[] month) {
        this.month = month;
    }


    /**
     * Gets the msgstn value for this CurStatLap.
     * 
     * @return msgstn
     */
    public java.lang.String[] getMsgstn() {
        return msgstn;
    }


    /**
     * Sets the msgstn value for this CurStatLap.
     * 
     * @param msgstn
     */
    public void setMsgstn(java.lang.String[] msgstn) {
        this.msgstn = msgstn;
    }


    /**
     * Gets the resupto value for this CurStatLap.
     * 
     * @return resupto
     */
    public java.lang.String[] getResupto() {
        return resupto;
    }


    /**
     * Sets the resupto value for this CurStatLap.
     * 
     * @param resupto
     */
    public void setResupto(java.lang.String[] resupto) {
        this.resupto = resupto;
    }


    /**
     * Gets the stnfrom value for this CurStatLap.
     * 
     * @return stnfrom
     */
    public java.lang.String[] getStnfrom() {
        return stnfrom;
    }


    /**
     * Sets the stnfrom value for this CurStatLap.
     * 
     * @param stnfrom
     */
    public void setStnfrom(java.lang.String[] stnfrom) {
        this.stnfrom = stnfrom;
    }


    /**
     * Gets the stnto value for this CurStatLap.
     * 
     * @return stnto
     */
    public java.lang.String[] getStnto() {
        return stnto;
    }


    /**
     * Sets the stnto value for this CurStatLap.
     * 
     * @param stnto
     */
    public void setStnto(java.lang.String[] stnto) {
        this.stnto = stnto;
    }


    /**
     * Gets the trainName value for this CurStatLap.
     * 
     * @return trainName
     */
    public java.lang.String[] getTrainName() {
        return trainName;
    }


    /**
     * Sets the trainName value for this CurStatLap.
     * 
     * @param trainName
     */
    public void setTrainName(java.lang.String[] trainName) {
        this.trainName = trainName;
    }


    /**
     * Gets the trainNo value for this CurStatLap.
     * 
     * @return trainNo
     */
    public java.lang.String[] getTrainNo() {
        return trainNo;
    }


    /**
     * Sets the trainNo value for this CurStatLap.
     * 
     * @param trainNo
     */
    public void setTrainNo(java.lang.String[] trainNo) {
        this.trainNo = trainNo;
    }


    /**
     * Gets the year value for this CurStatLap.
     * 
     * @return year
     */
    public int[] getYear() {
        return year;
    }


    /**
     * Sets the year value for this CurStatLap.
     * 
     * @param year
     */
    public void setYear(int[] year) {
        this.year = year;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CurStatLap)) return false;
        CurStatLap other = (CurStatLap) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.brdpt==null && other.getBrdpt()==null) || 
             (this.brdpt!=null &&
              java.util.Arrays.equals(this.brdpt, other.getBrdpt()))) &&
            ((this.cls==null && other.getCls()==null) || 
             (this.cls!=null &&
              java.util.Arrays.equals(this.cls, other.getCls()))) &&
            ((this.day==null && other.getDay()==null) || 
             (this.day!=null &&
              java.util.Arrays.equals(this.day, other.getDay()))) &&
            ((this.month==null && other.getMonth()==null) || 
             (this.month!=null &&
              java.util.Arrays.equals(this.month, other.getMonth()))) &&
            ((this.msgstn==null && other.getMsgstn()==null) || 
             (this.msgstn!=null &&
              java.util.Arrays.equals(this.msgstn, other.getMsgstn()))) &&
            ((this.resupto==null && other.getResupto()==null) || 
             (this.resupto!=null &&
              java.util.Arrays.equals(this.resupto, other.getResupto()))) &&
            ((this.stnfrom==null && other.getStnfrom()==null) || 
             (this.stnfrom!=null &&
              java.util.Arrays.equals(this.stnfrom, other.getStnfrom()))) &&
            ((this.stnto==null && other.getStnto()==null) || 
             (this.stnto!=null &&
              java.util.Arrays.equals(this.stnto, other.getStnto()))) &&
            ((this.trainName==null && other.getTrainName()==null) || 
             (this.trainName!=null &&
              java.util.Arrays.equals(this.trainName, other.getTrainName()))) &&
            ((this.trainNo==null && other.getTrainNo()==null) || 
             (this.trainNo!=null &&
              java.util.Arrays.equals(this.trainNo, other.getTrainNo()))) &&
            ((this.year==null && other.getYear()==null) || 
             (this.year!=null &&
              java.util.Arrays.equals(this.year, other.getYear())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getBrdpt() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getBrdpt());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getBrdpt(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getCls() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCls());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCls(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getDay() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getDay());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getDay(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getMonth() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getMonth());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getMonth(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getMsgstn() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getMsgstn());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getMsgstn(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getResupto() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getResupto());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getResupto(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getStnfrom() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getStnfrom());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getStnfrom(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getStnto() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getStnto());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getStnto(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getTrainName() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTrainName());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTrainName(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getTrainNo() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTrainNo());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTrainNo(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getYear() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getYear());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getYear(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CurStatLap.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://testPkg", "CurStatLap"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("brdpt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://testPkg", "brdpt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://webportal.prs.com", "item"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cls");
        elemField.setXmlName(new javax.xml.namespace.QName("http://testPkg", "cls"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://webportal.prs.com", "item"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("day");
        elemField.setXmlName(new javax.xml.namespace.QName("http://testPkg", "day"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://webportal.prs.com", "item"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("month");
        elemField.setXmlName(new javax.xml.namespace.QName("http://testPkg", "month"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://webportal.prs.com", "item"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("msgstn");
        elemField.setXmlName(new javax.xml.namespace.QName("http://testPkg", "msgstn"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://webportal.prs.com", "item"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resupto");
        elemField.setXmlName(new javax.xml.namespace.QName("http://testPkg", "resupto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://webportal.prs.com", "item"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("stnfrom");
        elemField.setXmlName(new javax.xml.namespace.QName("http://testPkg", "stnfrom"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://webportal.prs.com", "item"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("stnto");
        elemField.setXmlName(new javax.xml.namespace.QName("http://testPkg", "stnto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://webportal.prs.com", "item"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("trainName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://testPkg", "trainName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://webportal.prs.com", "item"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("trainNo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://testPkg", "trainNo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://webportal.prs.com", "item"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("year");
        elemField.setXmlName(new javax.xml.namespace.QName("http://testPkg", "year"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://webportal.prs.com", "item"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
