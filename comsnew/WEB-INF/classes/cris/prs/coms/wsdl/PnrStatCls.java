/**
 * PnrStatCls.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package cris.prs.coms.wsdl;

public class PnrStatCls  implements java.io.Serializable {
    private java.lang.String chart_stat;

    private java.lang.String delay;

    private java.lang.String deptSource;

    private java.lang.String errormsg;

    private short errorno;

    private cris.prs.coms.wsdl.CurStatLap lapList;

    private int num_laps;

    private int num_psgns;

    private java.lang.String pnrno;

    private cris.prs.coms.wsdl.CurStatPsgn psgnList;

    private int racwltocnfstat;

    private int runwlstat;

    private long stockNo;

    private java.lang.String tktType;

    private java.lang.String tktlost_flag;

    private int totalFare;

    private java.lang.String trainStatus;

    private int wlinrac;

    public PnrStatCls() {
    }

    public PnrStatCls(
           java.lang.String chart_stat,
           java.lang.String delay,
           java.lang.String deptSource,
           java.lang.String errormsg,
           short errorno,
           cris.prs.coms.wsdl.CurStatLap lapList,
           int num_laps,
           int num_psgns,
           java.lang.String pnrno,
           cris.prs.coms.wsdl.CurStatPsgn psgnList,
           int racwltocnfstat,
           int runwlstat,
           long stockNo,
           java.lang.String tktType,
           java.lang.String tktlost_flag,
           int totalFare,
           java.lang.String trainStatus,
           int wlinrac) {
           this.chart_stat = chart_stat;
           this.delay = delay;
           this.deptSource = deptSource;
           this.errormsg = errormsg;
           this.errorno = errorno;
           this.lapList = lapList;
           this.num_laps = num_laps;
           this.num_psgns = num_psgns;
           this.pnrno = pnrno;
           this.psgnList = psgnList;
           this.racwltocnfstat = racwltocnfstat;
           this.runwlstat = runwlstat;
           this.stockNo = stockNo;
           this.tktType = tktType;
           this.tktlost_flag = tktlost_flag;
           this.totalFare = totalFare;
           this.trainStatus = trainStatus;
           this.wlinrac = wlinrac;
    }


    /**
     * Gets the chart_stat value for this PnrStatCls.
     * 
     * @return chart_stat
     */
    public java.lang.String getChart_stat() {
        return chart_stat;
    }


    /**
     * Sets the chart_stat value for this PnrStatCls.
     * 
     * @param chart_stat
     */
    public void setChart_stat(java.lang.String chart_stat) {
        this.chart_stat = chart_stat;
    }


    /**
     * Gets the delay value for this PnrStatCls.
     * 
     * @return delay
     */
    public java.lang.String getDelay() {
        return delay;
    }


    /**
     * Sets the delay value for this PnrStatCls.
     * 
     * @param delay
     */
    public void setDelay(java.lang.String delay) {
        this.delay = delay;
    }


    /**
     * Gets the deptSource value for this PnrStatCls.
     * 
     * @return deptSource
     */
    public java.lang.String getDeptSource() {
        return deptSource;
    }


    /**
     * Sets the deptSource value for this PnrStatCls.
     * 
     * @param deptSource
     */
    public void setDeptSource(java.lang.String deptSource) {
        this.deptSource = deptSource;
    }


    /**
     * Gets the errormsg value for this PnrStatCls.
     * 
     * @return errormsg
     */
    public java.lang.String getErrormsg() {
        return errormsg;
    }


    /**
     * Sets the errormsg value for this PnrStatCls.
     * 
     * @param errormsg
     */
    public void setErrormsg(java.lang.String errormsg) {
        this.errormsg = errormsg;
    }


    /**
     * Gets the errorno value for this PnrStatCls.
     * 
     * @return errorno
     */
    public short getErrorno() {
        return errorno;
    }


    /**
     * Sets the errorno value for this PnrStatCls.
     * 
     * @param errorno
     */
    public void setErrorno(short errorno) {
        this.errorno = errorno;
    }


    /**
     * Gets the lapList value for this PnrStatCls.
     * 
     * @return lapList
     */
    public cris.prs.coms.wsdl.CurStatLap getLapList() {
        return lapList;
    }


    /**
     * Sets the lapList value for this PnrStatCls.
     * 
     * @param lapList
     */
    public void setLapList(cris.prs.coms.wsdl.CurStatLap lapList) {
        this.lapList = lapList;
    }


    /**
     * Gets the num_laps value for this PnrStatCls.
     * 
     * @return num_laps
     */
    public int getNum_laps() {
        return num_laps;
    }


    /**
     * Sets the num_laps value for this PnrStatCls.
     * 
     * @param num_laps
     */
    public void setNum_laps(int num_laps) {
        this.num_laps = num_laps;
    }


    /**
     * Gets the num_psgns value for this PnrStatCls.
     * 
     * @return num_psgns
     */
    public int getNum_psgns() {
        return num_psgns;
    }


    /**
     * Sets the num_psgns value for this PnrStatCls.
     * 
     * @param num_psgns
     */
    public void setNum_psgns(int num_psgns) {
        this.num_psgns = num_psgns;
    }


    /**
     * Gets the pnrno value for this PnrStatCls.
     * 
     * @return pnrno
     */
    public java.lang.String getPnrno() {
        return pnrno;
    }


    /**
     * Sets the pnrno value for this PnrStatCls.
     * 
     * @param pnrno
     */
    public void setPnrno(java.lang.String pnrno) {
        this.pnrno = pnrno;
    }


    /**
     * Gets the psgnList value for this PnrStatCls.
     * 
     * @return psgnList
     */
    public cris.prs.coms.wsdl.CurStatPsgn getPsgnList() {
        return psgnList;
    }


    /**
     * Sets the psgnList value for this PnrStatCls.
     * 
     * @param psgnList
     */
    public void setPsgnList(cris.prs.coms.wsdl.CurStatPsgn psgnList) {
        this.psgnList = psgnList;
    }


    /**
     * Gets the racwltocnfstat value for this PnrStatCls.
     * 
     * @return racwltocnfstat
     */
    public int getRacwltocnfstat() {
        return racwltocnfstat;
    }


    /**
     * Sets the racwltocnfstat value for this PnrStatCls.
     * 
     * @param racwltocnfstat
     */
    public void setRacwltocnfstat(int racwltocnfstat) {
        this.racwltocnfstat = racwltocnfstat;
    }


    /**
     * Gets the runwlstat value for this PnrStatCls.
     * 
     * @return runwlstat
     */
    public int getRunwlstat() {
        return runwlstat;
    }


    /**
     * Sets the runwlstat value for this PnrStatCls.
     * 
     * @param runwlstat
     */
    public void setRunwlstat(int runwlstat) {
        this.runwlstat = runwlstat;
    }


    /**
     * Gets the stockNo value for this PnrStatCls.
     * 
     * @return stockNo
     */
    public long getStockNo() {
        return stockNo;
    }


    /**
     * Sets the stockNo value for this PnrStatCls.
     * 
     * @param stockNo
     */
    public void setStockNo(long stockNo) {
        this.stockNo = stockNo;
    }


    /**
     * Gets the tktType value for this PnrStatCls.
     * 
     * @return tktType
     */
    public java.lang.String getTktType() {
        return tktType;
    }


    /**
     * Sets the tktType value for this PnrStatCls.
     * 
     * @param tktType
     */
    public void setTktType(java.lang.String tktType) {
        this.tktType = tktType;
    }


    /**
     * Gets the tktlost_flag value for this PnrStatCls.
     * 
     * @return tktlost_flag
     */
    public java.lang.String getTktlost_flag() {
        return tktlost_flag;
    }


    /**
     * Sets the tktlost_flag value for this PnrStatCls.
     * 
     * @param tktlost_flag
     */
    public void setTktlost_flag(java.lang.String tktlost_flag) {
        this.tktlost_flag = tktlost_flag;
    }


    /**
     * Gets the totalFare value for this PnrStatCls.
     * 
     * @return totalFare
     */
    public int getTotalFare() {
        return totalFare;
    }


    /**
     * Sets the totalFare value for this PnrStatCls.
     * 
     * @param totalFare
     */
    public void setTotalFare(int totalFare) {
        this.totalFare = totalFare;
    }


    /**
     * Gets the trainStatus value for this PnrStatCls.
     * 
     * @return trainStatus
     */
    public java.lang.String getTrainStatus() {
        return trainStatus;
    }


    /**
     * Sets the trainStatus value for this PnrStatCls.
     * 
     * @param trainStatus
     */
    public void setTrainStatus(java.lang.String trainStatus) {
        this.trainStatus = trainStatus;
    }


    /**
     * Gets the wlinrac value for this PnrStatCls.
     * 
     * @return wlinrac
     */
    public int getWlinrac() {
        return wlinrac;
    }


    /**
     * Sets the wlinrac value for this PnrStatCls.
     * 
     * @param wlinrac
     */
    public void setWlinrac(int wlinrac) {
        this.wlinrac = wlinrac;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PnrStatCls)) return false;
        PnrStatCls other = (PnrStatCls) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.chart_stat==null && other.getChart_stat()==null) || 
             (this.chart_stat!=null &&
              this.chart_stat.equals(other.getChart_stat()))) &&
            ((this.delay==null && other.getDelay()==null) || 
             (this.delay!=null &&
              this.delay.equals(other.getDelay()))) &&
            ((this.deptSource==null && other.getDeptSource()==null) || 
             (this.deptSource!=null &&
              this.deptSource.equals(other.getDeptSource()))) &&
            ((this.errormsg==null && other.getErrormsg()==null) || 
             (this.errormsg!=null &&
              this.errormsg.equals(other.getErrormsg()))) &&
            this.errorno == other.getErrorno() &&
            ((this.lapList==null && other.getLapList()==null) || 
             (this.lapList!=null &&
              this.lapList.equals(other.getLapList()))) &&
            this.num_laps == other.getNum_laps() &&
            this.num_psgns == other.getNum_psgns() &&
            ((this.pnrno==null && other.getPnrno()==null) || 
             (this.pnrno!=null &&
              this.pnrno.equals(other.getPnrno()))) &&
            ((this.psgnList==null && other.getPsgnList()==null) || 
             (this.psgnList!=null &&
              this.psgnList.equals(other.getPsgnList()))) &&
            this.racwltocnfstat == other.getRacwltocnfstat() &&
            this.runwlstat == other.getRunwlstat() &&
            this.stockNo == other.getStockNo() &&
            ((this.tktType==null && other.getTktType()==null) || 
             (this.tktType!=null &&
              this.tktType.equals(other.getTktType()))) &&
            ((this.tktlost_flag==null && other.getTktlost_flag()==null) || 
             (this.tktlost_flag!=null &&
              this.tktlost_flag.equals(other.getTktlost_flag()))) &&
            this.totalFare == other.getTotalFare() &&
            ((this.trainStatus==null && other.getTrainStatus()==null) || 
             (this.trainStatus!=null &&
              this.trainStatus.equals(other.getTrainStatus()))) &&
            this.wlinrac == other.getWlinrac();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getChart_stat() != null) {
            _hashCode += getChart_stat().hashCode();
        }
        if (getDelay() != null) {
            _hashCode += getDelay().hashCode();
        }
        if (getDeptSource() != null) {
            _hashCode += getDeptSource().hashCode();
        }
        if (getErrormsg() != null) {
            _hashCode += getErrormsg().hashCode();
        }
        _hashCode += getErrorno();
        if (getLapList() != null) {
            _hashCode += getLapList().hashCode();
        }
        _hashCode += getNum_laps();
        _hashCode += getNum_psgns();
        if (getPnrno() != null) {
            _hashCode += getPnrno().hashCode();
        }
        if (getPsgnList() != null) {
            _hashCode += getPsgnList().hashCode();
        }
        _hashCode += getRacwltocnfstat();
        _hashCode += getRunwlstat();
        _hashCode += new Long(getStockNo()).hashCode();
        if (getTktType() != null) {
            _hashCode += getTktType().hashCode();
        }
        if (getTktlost_flag() != null) {
            _hashCode += getTktlost_flag().hashCode();
        }
        _hashCode += getTotalFare();
        if (getTrainStatus() != null) {
            _hashCode += getTrainStatus().hashCode();
        }
        _hashCode += getWlinrac();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PnrStatCls.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://testPkg", "PnrStatCls"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("chart_stat");
        elemField.setXmlName(new javax.xml.namespace.QName("http://testPkg", "chart_stat"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("delay");
        elemField.setXmlName(new javax.xml.namespace.QName("http://testPkg", "delay"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("deptSource");
        elemField.setXmlName(new javax.xml.namespace.QName("http://testPkg", "deptSource"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("errormsg");
        elemField.setXmlName(new javax.xml.namespace.QName("http://testPkg", "errormsg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("errorno");
        elemField.setXmlName(new javax.xml.namespace.QName("http://testPkg", "errorno"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lapList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://testPkg", "lapList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://testPkg", "CurStatLap"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("num_laps");
        elemField.setXmlName(new javax.xml.namespace.QName("http://testPkg", "num_laps"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("num_psgns");
        elemField.setXmlName(new javax.xml.namespace.QName("http://testPkg", "num_psgns"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pnrno");
        elemField.setXmlName(new javax.xml.namespace.QName("http://testPkg", "pnrno"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("psgnList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://testPkg", "psgnList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://testPkg", "CurStatPsgn"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("racwltocnfstat");
        elemField.setXmlName(new javax.xml.namespace.QName("http://testPkg", "racwltocnfstat"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("runwlstat");
        elemField.setXmlName(new javax.xml.namespace.QName("http://testPkg", "runwlstat"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("stockNo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://testPkg", "stockNo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tktType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://testPkg", "tktType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tktlost_flag");
        elemField.setXmlName(new javax.xml.namespace.QName("http://testPkg", "tktlost_flag"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totalFare");
        elemField.setXmlName(new javax.xml.namespace.QName("http://testPkg", "totalFare"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("trainStatus");
        elemField.setXmlName(new javax.xml.namespace.QName("http://testPkg", "trainStatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("wlinrac");
        elemField.setXmlName(new javax.xml.namespace.QName("http://testPkg", "wlinrac"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
