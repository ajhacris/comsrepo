/**
 * ResvnCancelWebServiceServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package cris.prs.coms.wsdl;

public class ResvnCancelWebServiceServiceLocator extends org.apache.axis.client.Service implements cris.prs.coms.wsdl.ResvnCancelWebServiceService {

    public ResvnCancelWebServiceServiceLocator() {
    }


    public ResvnCancelWebServiceServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public ResvnCancelWebServiceServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for ResvnCancelWebService
    private java.lang.String ResvnCancelWebService_address = "http://10.64.6.78:8080/IRCPTestProj/services/ResvnCancelWebService";

    public java.lang.String getResvnCancelWebServiceAddress() {
        return ResvnCancelWebService_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String ResvnCancelWebServiceWSDDServiceName = "ResvnCancelWebService";

    public java.lang.String getResvnCancelWebServiceWSDDServiceName() {
        return ResvnCancelWebServiceWSDDServiceName;
    }

    public void setResvnCancelWebServiceWSDDServiceName(java.lang.String name) {
        ResvnCancelWebServiceWSDDServiceName = name;
    }

    public cris.prs.coms.wsdl.ResvnCancelWebService_PortType getResvnCancelWebService() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(ResvnCancelWebService_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getResvnCancelWebService(endpoint);
    }

    public cris.prs.coms.wsdl.ResvnCancelWebService_PortType getResvnCancelWebService(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            cris.prs.coms.wsdl.ResvnCancelWebServiceSoapBindingStub _stub = new cris.prs.coms.wsdl.ResvnCancelWebServiceSoapBindingStub(portAddress, this);
            _stub.setPortName(getResvnCancelWebServiceWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setResvnCancelWebServiceEndpointAddress(java.lang.String address) {
        ResvnCancelWebService_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (cris.prs.coms.wsdl.ResvnCancelWebService_PortType.class.isAssignableFrom(serviceEndpointInterface)) {
                cris.prs.coms.wsdl.ResvnCancelWebServiceSoapBindingStub _stub = new cris.prs.coms.wsdl.ResvnCancelWebServiceSoapBindingStub(new java.net.URL(ResvnCancelWebService_address), this);
                _stub.setPortName(getResvnCancelWebServiceWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("ResvnCancelWebService".equals(inputPortName)) {
            return getResvnCancelWebService();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://webportal.prs.com", "ResvnCancelWebServiceService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://webportal.prs.com", "ResvnCancelWebService"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("ResvnCancelWebService".equals(portName)) {
            setResvnCancelWebServiceEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
