/**
 * ResvnCancelWebServiceService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package cris.prs.coms.wsdl;

public interface ResvnCancelWebServiceService extends javax.xml.rpc.Service {
    public java.lang.String getResvnCancelWebServiceAddress();

    public cris.prs.coms.wsdl.ResvnCancelWebService_PortType getResvnCancelWebService() throws javax.xml.rpc.ServiceException;

    public cris.prs.coms.wsdl.ResvnCancelWebService_PortType getResvnCancelWebService(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
