/**
 * AlertServiceImplServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.comviva.cris.webservice;

public class AlertServiceImplServiceLocator extends org.apache.axis.client.Service implements com.comviva.cris.webservice.AlertServiceImplService {

    public AlertServiceImplServiceLocator() {
    }


    public AlertServiceImplServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public AlertServiceImplServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for AlertServiceImpl
    private java.lang.String AlertServiceImpl_address = "http://localhost:9898/CriAlertWebService/services/AlertServiceImpl";

    public java.lang.String getAlertServiceImplAddress() {
        return AlertServiceImpl_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String AlertServiceImplWSDDServiceName = "AlertServiceImpl";

    public java.lang.String getAlertServiceImplWSDDServiceName() {
        return AlertServiceImplWSDDServiceName;
    }

    public void setAlertServiceImplWSDDServiceName(java.lang.String name) {
        AlertServiceImplWSDDServiceName = name;
    }

    public com.comviva.cris.webservice.AlertServiceImpl getAlertServiceImpl() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(AlertServiceImpl_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getAlertServiceImpl(endpoint);
    }

    public com.comviva.cris.webservice.AlertServiceImpl getAlertServiceImpl(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.comviva.cris.webservice.AlertServiceImplSoapBindingStub _stub = new com.comviva.cris.webservice.AlertServiceImplSoapBindingStub(portAddress, this);
            _stub.setPortName(getAlertServiceImplWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setAlertServiceImplEndpointAddress(java.lang.String address) {
        AlertServiceImpl_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.comviva.cris.webservice.AlertServiceImpl.class.isAssignableFrom(serviceEndpointInterface)) {
                com.comviva.cris.webservice.AlertServiceImplSoapBindingStub _stub = new com.comviva.cris.webservice.AlertServiceImplSoapBindingStub(new java.net.URL(AlertServiceImpl_address), this);
                _stub.setPortName(getAlertServiceImplWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("AlertServiceImpl".equals(inputPortName)) {
            return getAlertServiceImpl();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://webservice.cris.comviva.com", "AlertServiceImplService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://webservice.cris.comviva.com", "AlertServiceImpl"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("AlertServiceImpl".equals(portName)) {
            setAlertServiceImplEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
