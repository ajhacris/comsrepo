package com.comviva.cris.webservice;

import com.comviva.cris.webservice.IDataEncrypt;
import java.security.Key;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.KeySpec;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.IvParameterSpec;
import org.apache.log4j.Logger;

public class DefaultDataEncryptImpl
implements IDataEncrypt {
    private static final Logger LOGGER = Logger.getLogger((Class)DefaultDataEncryptImpl.class);
    private String key = null;
    private Cipher cipher = null;

    public DefaultDataEncryptImpl(String key) {
        this.key = key;
        this.initCipher();
    }

    private void initCipher() {
        try {
            this.cipher = Cipher.getInstance("DES/CBC/PKCS5Padding");
            DESKeySpec desKeySpec = new DESKeySpec(this.key.getBytes("UTF-8"));
            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
            SecretKey secretKey = keyFactory.generateSecret(desKeySpec);
            IvParameterSpec iv = new IvParameterSpec(this.key.getBytes("UTF-8"));
            this.cipher.init(1, (Key)secretKey, iv);
        }
        catch (Exception e) {
            LOGGER.error((Object)"Exception while initializing cipher", (Throwable)e);
        }
    }

    
    public String encrypt(String dataToEncrypt) {
        try {
            return DefaultDataEncryptImpl.toHexString(this.cipher.doFinal(dataToEncrypt.getBytes("UTF-8")));
        }
        catch (Exception e) {
            LOGGER.error((Object)"Exception while encrypting the data.Returing original data", (Throwable)e);
            return dataToEncrypt;
        }
    }

    private static String toHexString(byte[] b) {
        StringBuffer hexString = new StringBuffer();
        for (int i = 0; i < b.length; ++i) {
            String plainText = Integer.toHexString(255 & b[i]);
            if (plainText.length() < 2) {
                plainText = "0" + plainText;
            }
            hexString.append(plainText);
        }
        return hexString.toString();
    }
}