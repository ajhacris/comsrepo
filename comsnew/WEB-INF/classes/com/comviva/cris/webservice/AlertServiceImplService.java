/**
 * AlertServiceImplService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.comviva.cris.webservice;

public interface AlertServiceImplService extends javax.xml.rpc.Service {
    public java.lang.String getAlertServiceImplAddress();

    public com.comviva.cris.webservice.AlertServiceImpl getAlertServiceImpl() throws javax.xml.rpc.ServiceException;

    public com.comviva.cris.webservice.AlertServiceImpl getAlertServiceImpl(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
