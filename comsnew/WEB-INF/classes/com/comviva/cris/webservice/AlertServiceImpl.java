/**
 * AlertServiceImpl.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.comviva.cris.webservice;

public interface AlertServiceImpl extends java.rmi.Remote {
    public com.comviva.cris.webservice.AlertResponse sendAlertMessage(java.lang.String applicationId, java.lang.String password, java.lang.String senderAddress, java.lang.String mobileNumber, java.lang.String message) throws java.rmi.RemoteException;
}
