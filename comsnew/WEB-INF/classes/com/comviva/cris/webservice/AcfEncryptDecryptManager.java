package com.comviva.cris.webservice;
import com.comviva.cris.webservice.IDataDecrypt;
import com.comviva.cris.webservice.IDataEncrypt;

public class AcfEncryptDecryptManager {
    IDataEncrypt iDataEncrypt = null;
    IDataDecrypt iDataDecrypt = null;
    private static AcfEncryptDecryptManager acfEncryptDecryptManager = null;

    public static AcfEncryptDecryptManager getInstance() {
        if (acfEncryptDecryptManager == null) {
            acfEncryptDecryptManager = new AcfEncryptDecryptManager();
        }
        return acfEncryptDecryptManager;
    }

    public void setEncryptionImpl(IDataEncrypt iDataEncrypt) {
        this.iDataEncrypt = iDataEncrypt;
    }

    public void setDecryptionImpl(IDataDecrypt iDataDecrypt) {
        this.iDataDecrypt = iDataDecrypt;
    }

    public String encrypt(String dataToEncrypt) {
        if (this.iDataEncrypt != null) {
            return this.iDataEncrypt.encrypt(dataToEncrypt);
        }
        return dataToEncrypt;
    }

    public String decrypt(String dataToDecrypt) {
        if (this.iDataDecrypt != null) {
            return this.iDataDecrypt.decrypt(dataToDecrypt);
        }
        return dataToDecrypt;
    }
}