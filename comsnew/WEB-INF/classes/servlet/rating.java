package servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import utility.DbConnection;

public class rating extends HttpServlet {

  
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String rating=request.getParameter("input-1");
		String comp_ref_no=request.getParameter("comp_ref_no");
		System.out.print(comp_ref_no);
		
		System.out.print(rating);
		
		PreparedStatement stmt=null;
		ResultSet rs=null;
		DbConnection dbConnection=new DbConnection();
		Connection con=dbConnection.dbConnect();
		
		String query="INSERT INTO rly_feedback (complaint_ref_no,rating) VALUES (?,?)";
		
		try {
			stmt=con.prepareStatement(query);
			stmt.setString(1, comp_ref_no);
			stmt.setString(2, rating);
			stmt.execute();
			
			System.out.println("Stored");
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		
	 response.sendRedirect("feedback.jsp");
		
	}

}
