package servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import DAO.SuggestionDAO;

/**
 * Servlet implementation class Suggestion
 */
public class Suggestion extends HttpServlet {
	private static final long serialVersionUID = 1L;
   
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		SuggestionDAO sd=new SuggestionDAO();
		String flag=request.getParameter("flag");
		String content="";
		
		try {
			if(flag.equals("complaint"))
				 content=sd.complaintType();
			if(flag.equals("stationNameOnLoad"))
				 content=sd.stationName();
			if(flag.equals("trainnoOnLoad"))
				 content=sd.trainNo();
			
		}catch(Exception e){
			
		}
		response.getOutputStream().print(content);
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
