package servlet;

import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import DAO.CommonListDAO;

public class CommonList extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		CommonListDAO dao=new CommonListDAO();
		String flag=request.getParameter("flag");
		String complaint_place=request.getParameter("complaint_place");
		String id=request.getParameter("id");				
		String content="";	
		System.out.println("Flag:"+flag);
		System.out.println("complaint type :"+complaint_place);
		try {
			
			/********** use to find Complaint type *****************/
			if(flag.equals("complaint"))
				content=dao.complaintType(complaint_place);
			
			/********** use to find Complaint type with select option *****************/
			if(flag.equals("complaint1"))
				content=dao.complaintType1(complaint_place);
			
			/********** use to find Sub Complaint type *****************/
			if(flag.equals("subcomplaint"))
				content=dao.subComplaintType(request.getParameter("parent_id"));
			
			/********** use to find Sub Complaint type with select option *****************/
			if(flag.equals("subcomplaint1"))
				content=dao.subComplaintType1(request.getParameter("parent_id"));
			
			
			/********** use to find Station Name & Station Code *****************/
			if(flag.equals("stationNameOnLoad"))
				content=dao.stationName();
			
			/********** use to find Train Number & Train Code *****************/
			if(flag.equals("trainnoOnLoad"))
				content=dao.trainNo();
			
			/********** use to find Complaint Channel Type *****************/
			if(flag.equals("complaintChannel"))
				content=dao.compChannel();
			
			
			/********** For Internal Registration *****************/
			
			/********** use to find All Complaint Type *****************/
			if(flag.equals("complaintAll"))
				content=dao.complaintAll();
			
			/********** use to find Complaint Type (In Train & On Station) *****************/
			if(flag.equals("complaint_place"))
				content=dao.complaintPlace(request.getParameter("parent_id"));
			
			/********** use to find Station Name & Station Code *****************/
			if(flag.equals("details")&& id.equals("On Station"))
				content=dao.details();
			
			/********** use to find Train Number & Train Code *****************/
			if(flag.equals("details")&& id.equals("In Train"))
				content=dao.details1();
			
			
			/********** use to find Station Name & Station Code *****************/
			if(flag.equals("details")&& id.equals("s"))
				content=dao.details();
			
			/********** use to find Train Number & Train Code *****************/
			if(flag.equals("details")&& id.equals("t"))
				content=dao.details1();
			
			
		} catch (SQLException e) {			
			e.printStackTrace();
		}
		response.getOutputStream().print(content);
	}

}
