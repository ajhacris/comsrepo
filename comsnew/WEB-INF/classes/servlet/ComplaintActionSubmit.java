package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import utility.DbConnection;
import utility.SendEmail;
import utility.SmsUtil;


/************** Use to Forward & close Complaint ************************/

public class ComplaintActionSubmit extends HttpServlet {

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		PreparedStatement stmt=null;
		ResultSet rs=null;
		DbConnection dbConnection=new DbConnection();
		Connection con=dbConnection.dbConnect();
		String com_ref_no=request.getParameter("comp_ref_no");
		String zone=request.getParameter("zone");
		String division=request.getParameter("division");
		String department=request.getParameter("department");
		String designation=request.getParameter("designation");
		String status=request.getParameter("status");
		String remark=request.getParameter("remark");
		String previous_user_group_id=request.getParameter("group_id");
		String incident_date="";
		String complainant_name="";
		String mobile="";
		String email_id="";
		String pnr_uts_no="";
		String pnr_uts_flag="";
		String trainno="";
		String trainname="";
		String coach_no="";
		String berth_no="";
		String train_class="";
		String platform_no="";
		String staff_name="";
		String complaint_mode="";
		String complaint="";
		String channel_type="";
		String complaint_type="";
		String complaint_sub_type="";
		String station_name="";
		String sla_value="";
		String group_id_CML="";

		/********* Use to get zone, division, department & groupid across complaint number to close compalint *****************/
		
	//	String query_close="	SELECT zn_cd ,div_cd ,user_group_id,dept_cd  FROM rly_complaint WHERE complaint_ref_no=?";

		/********* Use to get sla Flag  *****************/
		String query_sla="SELECT flag_sla FROM  rly_users WHERE group_id=? ";
		
		/********* Use to get all record across complaint number to update forwarding complaint *****************/
		String query="SELECT  incident_date,complainant_name,mobile,email_id,pnr_uts_no,"+
				"pnr_uts_flag,trainno,coach_no,berth_no,train_class,platform_no,staff_name,"+
				"complaint_mode,complaint,channel_type,complaint_type,complaint_sub_type,station_name,trainname FROM rly_complaint_response WHERE complaint_ref_no=?";

		/********* Use to generate history of complaint *****************/
		String query1="INSERT INTO rly_complaint_response "+
				"(incident_date,complainant_name,mobile,email_id,platform_no,complaint_mode,complaint,channel_type,"+
				"complaint_type,complaint_sub_type,trainno,zn_cd,div_cd,dept_cd,pnr_uts_no,pnr_uts_flag,"+
				"coach_no,berth_no,train_class,staff_name,user_group_id,complaint_ref_no,STATUS,sla_flag,remarks,station_name,trainname,created_on,previous_user_group_id)"+
				"values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,now(),?)";  

		/********* Use to update forwarded complaint record *****************/
		String query_update="UPDATE rly_complaint SET zn_cd=?,div_cd=?,dept_cd=?,user_group_id=?,STATUS=?,sla_flag=?,remarks=? WHERE complaint_ref_no=? ";

		/********* Use to update status & remarks of closed complaint *****************/
		String query_update_close="UPDATE rly_complaint SET STATUS=?,remarks=?, user_group_id=?,zn_cd=?,div_cd=?,dept_cd=? WHERE complaint_ref_no=?  ";


		try{

			/********* Use to get zone, division, department & groupid across complaint number to close compalint *****************/
			if(zone==null || zone.equals("")){
				stmt=con.prepareStatement("SELECT zn_cd,div_cd,group_id,dept_cd FROM rly_users WHERE group_id=?");		
				stmt.setString(1,previous_user_group_id);
				rs=stmt.executeQuery();  
				while(rs.next())  {
					zone=rs.getString(1);
					division=rs.getString(2);
					designation=rs.getString(3);
					department=rs.getString(4);
				}
			}
			
			/*************************************************/

			/********* Use to get sla Flag  *****************/
			stmt=con.prepareStatement(query_sla);		
			stmt.setString(1,designation);
			rs=stmt.executeQuery();  
			while(rs.next())  {
				sla_value=rs.getString(1);
			}
			
			/*************************************************/

			/********* Use to get all record across complaint number to update forwarding complaint *****************/
			stmt=con.prepareStatement(query);		
			stmt.setString(1,com_ref_no);
			rs=stmt.executeQuery();  
			while(rs.next())  {
				incident_date=rs.getString(1);
				complainant_name=rs.getString(2);
				mobile=rs.getString(3);
				email_id=rs.getString(4);
				pnr_uts_no=rs.getString(5);
				pnr_uts_flag=rs.getString(6);
				trainno=rs.getString(7);
				coach_no=rs.getString(8);
				berth_no=rs.getString(9);
				train_class=rs.getString(10);
				platform_no=rs.getString(11);
				staff_name=rs.getString(12);
				complaint_mode=rs.getString(13);
				complaint=rs.getString(14);
				channel_type=rs.getString(15);
				complaint_type=rs.getString(16);
				complaint_sub_type=rs.getString(17);
				station_name=rs.getString(18);
				trainname=rs.getString(19);
			}	
			
			/*************************************************/

			/********* Use to generate history of complaint *****************/
			stmt=con.prepareStatement(query1);	
			stmt.setString(1,incident_date );
			stmt.setString(2,complainant_name );
			stmt.setString(3,mobile );
			stmt.setString(4,email_id );
			stmt.setString(5,platform_no );
			stmt.setString(6,complaint_mode );
			stmt.setString(7,complaint );
			stmt.setString(8,channel_type );
			stmt.setString(9,complaint_type );
			stmt.setString(10,complaint_sub_type );
			stmt.setString(11,trainno );
			stmt.setString(12,zone );
			stmt.setString(13,division );
			stmt.setString(14,department );
			stmt.setString(15,pnr_uts_no );
			stmt.setString(16,pnr_uts_flag );
			stmt.setString(17,coach_no );
			stmt.setString(18,berth_no );
			stmt.setString(19,train_class );
			stmt.setString(20,staff_name );
			stmt.setString(21,designation );
			stmt.setString(22,com_ref_no );
			stmt.setString(23,status );
			stmt.setString(24,sla_value );
			stmt.setString(25,remark );
			stmt.setString(26,station_name );
			stmt.setString(27,trainname );
			stmt.setString(28,previous_user_group_id );
			
			stmt.execute(); 

			/*************************************************/
			
			
			/********* Use to update status & remarks of closed complaint *****************/
			if(status.equals("Closed")){ 
				stmt=con.prepareStatement(query_update_close);		
				stmt.setString(1,status);
				stmt.setString(2,remark);
				stmt.setString(3,designation);
				stmt.setString(4,zone);
				stmt.setString(5,division);
				
				stmt.setString(6,department );
				stmt.setString(7,com_ref_no);
				stmt.executeUpdate();  
				
				/********* Use to update status in rly complaint user table *****************/	
				stmt=con.prepareStatement("UPDATE rly_complaint_users SET STATUS=? WHERE complaint_ref_no=?");
				stmt.setString(1,status);
				stmt.setString(2,com_ref_no );
				stmt.executeUpdate();  
				

				/********* Use to Send SMS to user of Closed complaint*****************/
				String msgText="Your Complaint "+com_ref_no+" has been resolved. Please give your feedback on http://203.176.113.183/comsnew/feedback.jsp?id="+com_ref_no;
				if(mobile==null || mobile.equals("")){
					SendEmail se=new SendEmail();
					se.sendMail("Complaint Details", msgText, email_id);
				}
				else{
					SmsUtil smsUtil= new SmsUtil();	
					smsUtil.sendMessage( mobile,msgText);
				}
				
						
				
				
				/*************************************************/

			}
			
			/*************************************************/

			/********* Use to update forwarded complaint record *****************/
			if(status.equals("Forwarded")){ 
				stmt=con.prepareStatement(query_update);		
				stmt.setString(1,zone);
				stmt.setString(2,division);
				stmt.setString(3,department);
				stmt.setString(4,designation);
				stmt.setString(5,status);
				stmt.setString(6,sla_value);
				stmt.setString(7,remark );
				stmt.setString(8,com_ref_no);
				stmt.executeUpdate(); 
				
				
				
				/********** get owninggroupid_CML & sla flag across zone,division and department **************************/
				query="SELECT group_id,flag_sla FROM rly_users WHERE zn_cd=? AND div_cd=? AND dept_cd=? AND flag_sla=?";
				stmt=con.prepareStatement(query);	
				stmt.setString(1, zone);
				stmt.setString(2, division);
				stmt.setString(3, "CML");
				stmt.setString(4, "1");
				rs=stmt.executeQuery();  
				while(rs.next())  {
					group_id_CML=rs.getString(1);
					
				}
				
				/********* Use to update status in rly complaint user table *****************/	
				stmt=con.prepareStatement("UPDATE rly_complaint_users SET user_group_id=?, STATUS=? WHERE complaint_ref_no=? AND user_group_id=?");
				stmt.setString(1,designation);
				stmt.setString(2,status );
				stmt.setString(3,com_ref_no);
				stmt.setString(4,previous_user_group_id);				
				stmt.executeUpdate();  
				
				
				

				stmt=con.prepareStatement("UPDATE rly_complaint_users SET user_group_id=? WHERE complaint_ref_no=? AND user_group_id!=? AND flag=?");
				stmt.setString(1,group_id_CML);
				stmt.setString(2,com_ref_no);
				stmt.setString(3,designation);	
				stmt.setString(4,"1");
				stmt.executeUpdate();  
				
				

				/********* Use to Send SMS to admin user of forwarded complaint*****************/
				String complainttype="";
				String subcomplainttype="";
				query="select head_subhead_en from rly_heads_subheads_master where id=?";
				stmt=con.prepareStatement(query);	
				stmt.setString(1, complaint_type);
				rs=stmt.executeQuery();
				while(rs.next()){
					complainttype=rs.getString(1);
				}
				stmt=con.prepareStatement(query);	
				stmt.setString(1, complaint_sub_type);
				rs=stmt.executeQuery();
				while(rs.next()){
					subcomplainttype=rs.getString(1);
				}

				String adminmsgText="Indian Railways-CD: Complain "+com_ref_no+" is assigned to you. Please attend and resolve "+
						"the issue. Mobile number of passenger: "+mobile+",Date of Incident: "+incident_date+", "+
						"Complaint Type: "+complainttype+", Sub Complaint Type: "+subcomplainttype+", "+
						"Train no.: "+trainno;

				if(pnr_uts_flag.equals("P")){
					adminmsgText=adminmsgText+", Coach no.: "+coach_no+", Berth no.: "+berth_no;
				}
				SmsUtil smsUtil= new SmsUtil();			
				smsUtil.getMobileNumber(designation, adminmsgText);
				
				/*************************************************/
				
			}

			/*************************************************/
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		response.sendRedirect("ComplaintList.jsp");
	}

}
