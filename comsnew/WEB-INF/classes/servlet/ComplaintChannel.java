package servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import DAO.ComplaintChannelDAO;


public class ComplaintChannel extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ComplaintChannelDAO ccd=new ComplaintChannelDAO();
		String content="";
		content=ccd.getComplaintChannel();		
		response.getOutputStream().print(content);

	}

}
