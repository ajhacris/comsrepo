package servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import utility.DbConnection;

public class SplitComplaints extends HttpServlet {
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PreparedStatement stmt=null;
		DbConnection dbConnection=new DbConnection();
		Connection con=dbConnection.dbConnect();
		ResultSet rs=null;
		
		String id=request.getParameter("id");
		String mobile=request.getParameter("contact_no");
		String complaint=request.getParameter("message");
		String complaint_split=request.getParameter("message1");
		
		System.out.println(id);
		System.out.println(mobile);
		System.out.println(complaint);
		System.out.println(complaint_split);
		
		
		String query="UPDATE rly_incoming_sms SET sms_flag=? WHERE id=?";
		String query_insert="INSERT INTO rly_incoming_sms ( from_mobile,sms_text,sms_flag,created_on) VALUES(?,?,?,now())";
		
		try {
			stmt=con.prepareStatement(query_insert);
			stmt.setString(1, mobile);
			stmt.setString(2, complaint);
			stmt.setString(3,"I");
			
			stmt.execute();
			
			System.out.println("Insert one");
			stmt=con.prepareStatement(query_insert);
			stmt.setString(1, mobile);
			stmt.setString(2, complaint_split);
			stmt.setString(3,"I");
			
			stmt.execute();
			System.out.println("Insert two");
			
			
			stmt=con.prepareStatement(query);
			stmt.setString(1,"S");
			stmt.setString(2, id);
			
					
			stmt.executeUpdate();
			System.out.println("Update");
			
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		response.sendRedirect("incoming_messages.jsp");
		
	}

}
