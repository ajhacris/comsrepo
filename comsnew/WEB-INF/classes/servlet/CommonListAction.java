package servlet;

import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import DAO.CommonListActionDAO;

public class CommonListAction extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		CommonListActionDAO dao=new CommonListActionDAO();
		String flag=request.getParameter("flag");
		String content="";
		try {
			
			/********** use to find Zone Name & Zone Code *****************/
			if(flag.equals("zone"))
				content=dao.zone();
			
			/********** use to find Division Name & Division Code *****************/
			if(flag.equals("division"))
				content=dao.division(request.getParameter("parent_id"));
			
			/********** use to find Department Name & Department Code *****************/
			if(flag.equals("department"))
				content=dao.department();
			
			/********** use to find User group id across zone code, division code & station Code *****************/
			if(flag.equals("dept")) {
				System.out.println("dept call");
				content=dao.designation(request.getParameter("zone_cd"),request.getParameter("div_cd"),request.getParameter("dept_cd"));
				
				
			}	
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		response.getOutputStream().print(content);

	}

}
