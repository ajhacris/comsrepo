package servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import utility.DbConnection;

/**
 * Servlet implementation class ComplainantLogin
 */
public class ComplainantLogin extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	PreparedStatement ps=null;
	ResultSet rs=null;
	DbConnection dbConnection=new DbConnection();
	Connection con=null;
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String strmsg="";
		String user_login="";
		int count=0;
		String username=request.getParameter("username");
		String paswsword=request.getParameter("password");
		String url=request.getParameter("url");
		System.out.println("username : "+username);
		System.out.println("paswsword : "+paswsword);
		HttpSession session=request.getSession();
		con=dbConnection.dbConnect();
		
		if(username==null || username.equals("") || paswsword==null || paswsword.equals("")){
			
		}
		else{
			String query="SELECT COUNT(*),NAME,mobile,email FROM rly_complainant_login WHERE user_name=? AND PASSWORD=?";
			try {
				ps=con.prepareStatement(query);
				ps.setString(1, username);
				ps.setString(2, paswsword);
				rs=ps.executeQuery();
				while(rs.next()){
					count=rs.getInt(1);
					session.setAttribute("user_name", username);
					session.setAttribute("name",rs.getString(2));
					session.setAttribute("mobile", rs.getString(3));
					session.setAttribute("email", rs.getString(4));
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(count==0){
				strmsg="Invalid username password";
				response.sendRedirect("signin.jsp?strMSG="+strmsg);
			}
			else{
				if(url==null || url.equals("")){
					response.sendRedirect("ChangePassword.jsp");
				}
				else{
					response.sendRedirect(url);
				}
					
			}
			
		}
	}

}
