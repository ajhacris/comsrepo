package servlet;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDateTime;
//import java.time.LocalDateTime;
//import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatter;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import utility.DbConnection;
import utility.SendEmail;
import utility.SmsUtil;


@MultipartConfig(maxFileSize = 16177215)
public class SaveComplaint extends HttpServlet {
	private static final long serialVersionUID = 1L;
  
	
 protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		
	//	PrintWriter out = response.getWriter(); 
		PreparedStatement stmt=null;
		DbConnection dbConnection=new DbConnection();
		Connection con=dbConnection.dbConnect();
		ResultSet rs=null;
		
		String flag="t";
		String complaint_type=request.getParameter("complaint_type");
		String sub_complaint_type=request.getParameter("sub_complaint_type");
		String incident_dt=request.getParameter("incident_dt");
		String complaint_desc=request.getParameter("complaint_desc");
		String platform_no=request.getParameter("plateform_no");
		String stationName=request.getParameter("stationName");
		String stn_cd="";
		String[] arrOfStr = stationName.split("> "); 
		  
        for (String a : arrOfStr) 
           stn_cd=a; 
        
        System.out.println("stncd :"+stn_cd);
		String contact_no=request.getParameter("contact_no");
		String name=request.getParameter("name");
		String email=request.getParameter("email");
		String staff_name=request.getParameter("staffname");
		System.out.print("Submitted");
		System.out.print("complaint_type :"+complaint_type+"<br>");
		System.out.print("sub_complaint_type :"+sub_complaint_type+"<br>");
		System.out.print("incident_dt :"+incident_dt+"<br>");
		System.out.print("complaint_desc :"+complaint_desc+"<br>");
		System.out.print(" platform_no:"+platform_no+"<br>");
		System.out.print("stationName :"+stationName+"<br>");
		System.out.print("contact_no :"+contact_no+"<br>");
		System.out.print("name :"+name+"<br>");
		System.out.print("email :"+email+"<br>");
		
		
		
		 InputStream inputStream = null; // input stream of the upload file
         
	        // obtains the upload file part in this multipart request
	        Part filePart = request.getPart("file");
	        if (filePart != null) {
	            // prints out some information for debugging
	            System.out.println(filePart.getName());
	            System.out.println(filePart.getSize());
	            System.out.println(filePart.getContentType());
	             
                 if(filePart.getSize()<=5242880) {
	            // obtains input stream of the upload file
	            inputStream = filePart.getInputStream();
	            }
	        }
		
		
		

		if(flag.equals("t")) {
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMdd");
			LocalDateTime now = LocalDateTime.now();
			String date1=dtf.format(now);
			String complaint_ref_no="";
			
			complaint_ref_no=date1;
		    String zone="";
		    String division="";
		    String concern_dept="";
		    String group_id="";
		    String group_id_CML="";
		
			
			String query="INSERT INTO rly_complaint "+
		    		"(incident_date,complainant_name,mobile,email_id,platform_no,complaint_mode,complaint,channel_type,"+
	    			"complaint_type,complaint_sub_type,station_name,zn_cd,div_cd,dept_cd,pnr_uts_no,pnr_uts_flag,"+
	    			"coach_no,berth_no,train_class,staff_name,user_group_id,complaint_ref_no,STATUS,sla_flag,remarks,trainno,trainname,created_on,image)"+
	    			"values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,now(),?)";  
		
			String query_zone_division="SELECT zn_cd,div_cd FROM rly_station_master WHERE station_cd=?";
			String query_dept="SELECT department_cd FROM rly_heads_subheads_master WHERE id=?";
			String query_user="SELECT group_id,flag_sla FROM rly_users WHERE zn_cd=? AND div_cd=? AND dept_cd=? AND flag_sla=? ";
			String query1="INSERT INTO rly_complaint_response "+
		    		"(incident_date,complainant_name,mobile,email_id,platform_no,complaint_mode,complaint,channel_type,"+
	    			"complaint_type,complaint_sub_type,station_name,zn_cd,div_cd,dept_cd,pnr_uts_no,pnr_uts_flag,"+
	    			"coach_no,berth_no,train_class,staff_name,user_group_id,complaint_ref_no,STATUS,sla_flag,remarks,trainno,trainname,created_on)"+
	    			"values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,now())";  
		
			String complaintid="select count(*) from rly_complaint";
			int id=0;
			try{
				stmt=con.prepareStatement(complaintid);	
				
				rs=stmt.executeQuery();  


				while(rs.next())  {
					id=rs.getInt(1);
	                
				}
				
			}
			catch(Exception e){
				
			}
			id=id+1;
			String idd=Integer.toString(id);
			if(idd.length()==1){
				idd="0000"+idd;
			}
			if(idd.length()==2){
				idd="000"+idd;
			}
			if(idd.length()==3){
				idd="00"+idd;
			}
			if(idd.length()==4){
				idd="0"+idd;
			}
			complaint_ref_no=complaint_ref_no+idd;
			
			
		
		try{
			 
		System.out.print("succesfully registered");
		//out.println("<br>");
		
		
		stmt=con.prepareStatement(query_zone_division);		
		stmt.setString(1,stn_cd );
		   rs=stmt.executeQuery();  
			while(rs.next())  {
		    zone=rs.getString(1);
		    division=rs.getString(2);
	      	}
		
		stmt=con.prepareStatement(query_dept);		
		stmt.setString(1,sub_complaint_type );
		rs=stmt.executeQuery();  
			while(rs.next())  {
			concern_dept=rs.getString(1);
			
		    }	
			
       stmt=con.prepareStatement(query_user);		
			
			stmt.setString(1,zone );
			stmt.setString(2,division );
			stmt.setString(3,concern_dept );
			stmt.setString(4, "1");
			rs=stmt.executeQuery();  
				while(rs.next())  {
				group_id=rs.getString(1);
				
			    }	
				
				stmt=con.prepareStatement(query_user);		
				
				stmt.setString(1,zone );
				stmt.setString(2,division );
				stmt.setString(3,"CML" );
				stmt.setString(4, "1");
				rs=stmt.executeQuery();  
					while(rs.next())  {
					group_id_CML=rs.getString(1);
					
				    }	
				
				
				stmt=con.prepareStatement(query);		
				
				stmt.setString(1,incident_dt );
				stmt.setString(2,name );
				stmt.setString(3,contact_no );
				stmt.setString(4,email );
				stmt.setString(5,platform_no );
				stmt.setString(6,"S" );
				stmt.setString(7,complaint_desc );
				stmt.setString(8,"W" );
				stmt.setString(9,complaint_type );
				stmt.setString(10,sub_complaint_type );
				stmt.setString(11,stationName );
				stmt.setString(12,zone );
				stmt.setString(13,division );
				stmt.setString(14,concern_dept );
				stmt.setString(15,"" );
				stmt.setString(16,"" );
				stmt.setString(17,"" );
				stmt.setString(18,"" );
				stmt.setString(19,"" );
				stmt.setString(20,staff_name );
				stmt.setString(21,group_id );
				stmt.setString(22,complaint_ref_no );
				stmt.setString(23,"Registered" );
				stmt.setString(24,"1" );
				stmt.setString(25,"Registered.." );
				stmt.setString(26,"" );
				stmt.setString(27,"" );
				

                // fetches input stream of the upload file for the blob column
                stmt.setBlob(28, inputStream);
           
							  		
		        stmt.execute(); 
				
			
       stmt=con.prepareStatement(query1);	
      		
			stmt.setString(1,incident_dt );
			stmt.setString(2,name );
			stmt.setString(3,contact_no );
			stmt.setString(4,email );
			stmt.setString(5,platform_no );
			stmt.setString(6,"S" );
			stmt.setString(7,complaint_desc );
			stmt.setString(8,"W" );
			stmt.setString(9,complaint_type );
			stmt.setString(10,sub_complaint_type );
			stmt.setString(11,stationName );
			stmt.setString(12,zone );
			stmt.setString(13,division );
			stmt.setString(14,concern_dept );
			stmt.setString(15,"" );
			stmt.setString(16,"" );
			stmt.setString(17,"" );
			stmt.setString(18,"" );
			stmt.setString(19,"" );
			stmt.setString(20,staff_name );
			stmt.setString(21,group_id );
			stmt.setString(22,complaint_ref_no );
			stmt.setString(23,"Registered" );
			stmt.setString(24,"1" );
			stmt.setString(25,"Registered.." );
			stmt.setString(26,"" );
			stmt.setString(27,"" );
			
	        stmt.execute();  		
	        
	        
	    	/******************* Insert query in Rly complaint user table *****************************/
			String rly_complaint_user="INSERT INTO rly_complaint_users (complaint_ref_no,user_group_id,STATUS) VALUES (?,?,?) ";
			stmt=con.prepareStatement(rly_complaint_user);	
			stmt.setString(1, complaint_ref_no);
			stmt.setString(2, group_id);
			stmt.setString(3, "Registered");
			stmt.execute();
			
			if(!group_id.equals(group_id_CML)) {
				stmt=con.prepareStatement(rly_complaint_user);	
				stmt.setString(1, complaint_ref_no);
				stmt.setString(2, group_id_CML);
				stmt.setString(3, "Registered");
				stmt.execute();
					
				
			}
			
	        
           String strMSG ="Complaint Registered Successfully";
            
			String msgText="Thank you for lodging complaint with us. Your Complaint Reference Number is: "+complaint_ref_no;
			
			String complainttype="";
			String subcomplainttype="";
			query="select head_subhead_en from rly_heads_subheads_master where id=?";
			stmt=con.prepareStatement(query);	
			stmt.setString(1, complaint_type);
			rs=stmt.executeQuery();
			while(rs.next()){
				complainttype=rs.getString(1);
			}
			stmt=con.prepareStatement(query);	
			stmt.setString(1, sub_complaint_type);
			rs=stmt.executeQuery();
			while(rs.next()){
				subcomplainttype=rs.getString(1);
			}
			
			String adminmsgText_CML="";
			
			adminmsgText_CML=adminmsgText_CML+"Indian Railways-CD: Complain "+complaint_ref_no+" has been  assigned to "+group_id+" .This  is for your information plz.";
			
			String adminmsgText="Indian Railways-CD: Complain "+complaint_ref_no+" is assigned to you. Please attend and resolve "+
								"the issue. ";
					
								if(contact_no==null || contact_no.equals("")){
									adminmsgText=adminmsgText+" Email Id of passenger: "+email+",";
									adminmsgText_CML=adminmsgText_CML+" Email Id of passenger: "+email+",";
								}
								else{
									adminmsgText=adminmsgText+ " Mobile number of passenger: "+contact_no+"," ;
									adminmsgText_CML=adminmsgText_CML+ " Mobile number of passenger: "+contact_no+"," ;
								}
			
								adminmsgText=adminmsgText+ "Date of Incident: "+incident_dt+", "+
								 "Complaint Type: "+complainttype+", Sub Complaint Type: "+subcomplainttype+", "+
								 "Station Name.: "+stationName+", Platform no.: "+platform_no;
								
								adminmsgText_CML=adminmsgText_CML+ "Date of Incident: "+incident_dt+", "+
										 "Complaint Type: "+complainttype+", Sub Complaint Type: "+subcomplainttype+", "+
										 "Station Name.: "+stationName+", Platform no.: "+platform_no;
			
			
			
			SmsUtil smsUtil= new SmsUtil();
			System.out.println("contact_no :"+contact_no);
			if(contact_no==null || contact_no.equals("")){
				SendEmail se=new SendEmail();
				se.sendMail("Complaint Details", msgText, email);
			}else{
				smsUtil.sendMessage( contact_no,msgText);
			}
			
			/*if((contact_no==null) || (contact_no.equals(""))){
				smsUtil.sendMessage( "9953277027",msgText);
			}
			else{
				smsUtil.sendMessage( contact_no,msgText);
			}*/
			
			smsUtil.getMobileNumber(group_id, adminmsgText);
			if(!group_id.equals(group_id_CML)) {
				smsUtil.getMobileNumber(group_id_CML, adminmsgText_CML);
			}
		
		
		
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		response.getOutputStream().print(" Your complaint has successfully registered and your complaint no. is :"+complaint_ref_no);
			
		
		
		
		}
	}

}
