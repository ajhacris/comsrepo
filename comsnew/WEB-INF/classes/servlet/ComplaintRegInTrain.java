package servlet;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.google.gson.Gson;
import DAO.ComplaintRegInTrainDAO;

import cris.prs.coms.wsdl.PnrStatCls;
import model.ComplaintRegInTrainModel;
import model.TrainScheduleViewDTO;
import utility.SmsUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import javax.ws.rs.core.MediaType;
/**
 * Servlet implementation class ComplaintRegInTrain
 */
public class ComplaintRegInTrain extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ComplaintRegInTrain() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String pnrNo=request.getParameter("pnrNo");
		ComplaintRegInTrainDAO dao=new 	ComplaintRegInTrainDAO();
		ComplaintRegInTrainModel complaint=new ComplaintRegInTrainModel();
		List<String> coachList= new ArrayList<String>();
		List<String> berthList= new ArrayList<String>();
		try {
			int count=0;
			int counter=0;

			PnrStatCls pnr=dao.fetchPnrData(pnrNo);

			System.out.println("respons:");
			if (!pnr.getErrormsg().equals("")) {

				complaint.setFetchError(true);
				//complaint.setErrMsg(pnr.getErrormsg());
				complaint.setErrMsg("Please Enter Valid PNR No.");
			} else {

				String currentStatus=null;

				if (pnr.getNum_psgns()>0) {						
					for(int i=0;i<pnr.getNum_psgns();i++) {
						String psgnCoachStatus=pnr.getPsgnList().getLapOnecurstat()[i];
						//String psgnCoachbkgStatus=pnr.getPsgnList().getLapOnebkgstat()[i];
						System.out.println("psgnCoachStatus : " +psgnCoachStatus);
						//System.out.println("psgnCoachbkgStatus : " +psgnCoachbkgStatus);

						if((psgnCoachStatus.contains("Can")) 		|| 
								(psgnCoachStatus.contains("W/L"))	|| 
								(psgnCoachStatus.contains("CNF")) 	|| 
								(psgnCoachStatus.contains("RAC"))){
							System.out.println("Waiting List : " +psgnCoachStatus);
						}
						else{

							if(psgnCoachStatus.contains("R")){

								currentStatus=psgnCoachStatus.substring(1, psgnCoachStatus.indexOf(" "));
								System.out.println("currentStatuscoachList:"+i+": "+currentStatus);
								coachList.add(currentStatus);
								System.out.println(coachList.get(i));


							}
							else{

								currentStatus=psgnCoachStatus.substring(0, psgnCoachStatus.indexOf(",") - 1);
								System.out.println("currentStatuscoachList:"+i+": "+currentStatus);
								coachList.add(currentStatus);
								System.out.println(coachList.get(i));							
							}
							count++;
						}

						/*//String psgnCoachStatus=pnr.getPsgnList().getLapOnebkgstat()[i];
		if(psgnCoachStatus.equals("CNF")){
			psgnCoachStatus=pnr.getPsgnList().getLapOnebkgstat()[i];
			System.out.println("CNF :"+psgnCoachStatus);
			currentStatus=psgnCoachStatus.substring(0, psgnCoachStatus.indexOf(",") - 1);
			System.out.println("currentStatuscoachList:"+i+": "+currentStatus);
			coachList.add(currentStatus);
			System.out.println(coachList.get(i));
		}
		else{
			System.out.println("psgnCoachStatus :"+psgnCoachStatus);
			currentStatus=psgnCoachStatus.substring(0, psgnCoachStatus.indexOf(",") - 1);
			System.out.println("currentStatuscoachList:"+i+": "+currentStatus);
			coachList.add(currentStatus);
			System.out.println(coachList.get(i));
		}	*/

					}							
					for(int i=0;i<pnr.getNum_psgns();i++) {							
						String psgnBerthStatus=pnr.getPsgnList().getLapOnecurstat()[i];   
						if((psgnBerthStatus.contains("Can")) 		|| 
								(psgnBerthStatus.contains("W/L")) 	|| 
								(psgnBerthStatus.contains("CNF")) 	|| 
								(psgnBerthStatus.contains("RAC"))){

							System.out.println("Waiting List : " +psgnBerthStatus);
						}
						else{
							if(psgnBerthStatus.contains("R")){	
								currentStatus=psgnBerthStatus.substring(psgnBerthStatus.indexOf(" ") + 3, psgnBerthStatus.length());
								System.out.println("currentStatusberthList:"+i+": "+currentStatus);
								berthList.add(currentStatus);
								System.out.println(berthList.get(i));


							}
							else{
								currentStatus=psgnBerthStatus.substring(psgnBerthStatus.indexOf(",") + 1, psgnBerthStatus.length());
								System.out.println("currentStatusberthList:"+i+": "+currentStatus);
								berthList.add(currentStatus);
								System.out.println(berthList.get(i));

							}
							counter++;
						}
						/*
		//String psgnBerthStatus=pnr.getPsgnList().getLapOnebkgstat()[i];
		System.out.println("psgnBerthStatus :"+psgnBerthStatus);
		if(psgnBerthStatus.equals("CNF")){
			psgnBerthStatus=pnr.getPsgnList().getLapOnebkgstat()[i];

			currentStatus=psgnBerthStatus.substring(psgnBerthStatus.indexOf(",") + 1, psgnBerthStatus.length());
			System.out.println("currentStatusberthList:"+i+": "+currentStatus);
			berthList.add(currentStatus);
			System.out.println(berthList.get(i));
		}
		else{

			currentStatus=psgnBerthStatus.substring(psgnBerthStatus.indexOf(",") + 1, psgnBerthStatus.length());
			System.out.println("currentStatusberthList:"+i+": "+currentStatus);
			berthList.add(currentStatus);
			System.out.println(berthList.get(i));
		}
						 */								
					}	


				}
				if(count>0 && counter >0){
					complaint.setFetchError(false);
					complaint.setTotalAmount(pnr.getTotalFare());
					complaint.setTotalPass(pnr.getNum_psgns());
					complaint.setBoardingStn(pnr.getLapList().getBrdpt()[0]);
					complaint.setFromStation(pnr.getLapList().getStnfrom()[0]);
					complaint.setToStation(pnr.getLapList().getStnto()[0]);
					complaint.setSelectCoachNo(true);
					complaint.setCoachNoList(coachList);
					complaint.setSelectBerthNo(true);
					complaint.setBerthNoList(berthList);						
					complaint.setBerthCls(pnr.getLapList().getCls()[0]);
					complaint.setTrainNo(pnr.getLapList().getTrainNo()[0] + " - " + pnr.getLapList().getTrainName()[0]);
					complaint.setDay(pnr.getLapList().getDay()[0]);
					complaint.setMonth(pnr.getLapList().getMonth()[0]);
					complaint.setYear(pnr.getLapList().getYear()[0]);
					complaint.setSelectName(true);
					complaint.setPassengerList(Arrays.asList(pnr.getPsgnList().getPsgnname()));

				}
				else{
					complaint.setFetchError(true);
					//complaint.setErrMsg(pnr.getErrormsg());
					complaint.setErrMsg("Lodging of complaint is only valid for confirmed passengers");
				}




			}
			Gson gson = new Gson();
			String Json=gson.toJson(complaint);
			System.out.print("json"+gson.toJson(pnr));
			response.getWriter().print(Json);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}



	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
