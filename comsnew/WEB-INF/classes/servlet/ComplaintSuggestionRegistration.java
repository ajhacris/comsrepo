package servlet;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import utility.DbConnection;
import utility.SmsUtil;


@MultipartConfig(maxFileSize = 16177215)
public class ComplaintSuggestionRegistration extends HttpServlet {
	
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter(); 
		PreparedStatement stmt=null;
		ResultSet rs=null;
		DbConnection dbConnection=new DbConnection();
		Connection con=dbConnection.dbConnect();
		
		//COMMON
		String complaint =request.getParameter("complaint_type");
		String complaint_channel =request.getParameter("complaint_channel");
		String sub_complaint_type =request.getParameter("sub_complaint_type");
		String incident_dt =request.getParameter("incident_dt");
		String complaint_desc =request.getParameter("complaint_desc");
		String staff_name =request.getParameter("staff_name");
		String complaint_place =request.getParameter("place_type");
		System.out.println("COMPLAINT PLACE :"+complaint_place);
		
		//FOR STATION
		String platform_no =request.getParameter("platform_no");
		String stationName =request.getParameter("stationName");
		String stn_cd="";
		if(stationName!=null && !stationName.equals("")){
			String[] arrOfStr = stationName.split("> "); 
			  
	        for (String a : arrOfStr) 
	           stn_cd=a; 
		}
		
		//FOR TRAIN
		String pnr_uts="";
		pnr_uts =request.getParameter("pnr_uts");
		String train_no =request.getParameter("train_no");
		String trainno="";
		String trainname="";
		String from_stn =request.getParameter("from_stn");
		String to_stn =request.getParameter("to_stn");
		String board_stn =request.getParameter("board_stn");
		String berth_class =request.getParameter("berth_class");
		String total_pass =request.getParameter("total_pass");
		String total_fare =request.getParameter("total_fare");
		String coach_no =request.getParameter("coach_no");
		String berth_no =request.getParameter("berth_no");
		String next_stn=request.getParameter("nextstn");
		
		//PERSONAL DETAILS
		String name =request.getParameter("name");
		String contact_no =request.getParameter("contact_no");
		String email =request.getParameter("email");
		
		//DUTY PERSON DETAIL
		String name_duty_person =request.getParameter("name_duty_person");
		String designation_duty_person =request.getParameter("designation_duty_person");
		
		
		
		 InputStream inputStream = null; // input stream of the upload file
         
	        // obtains the upload file part in this multipart request
	        Part filePart = request.getPart("file");
	        if (filePart != null) {
	            // prints out some information for debugging
	            System.out.println(filePart.getName());
	            System.out.println(filePart.getSize());
	            System.out.println(filePart.getContentType());
	             
	            // obtains input stream of the upload file
	            inputStream = filePart.getInputStream();
	        }
		
	        System.out.println("TESTING 1");
		   
		String pnr_uts_flag="P";
		if(train_no!=null && ! train_no.equals("")){
			trainno=train_no.substring(0, 5);
			trainname=train_no.substring(8);
			pnr_uts_flag="U";
		}
		System.out.println("Train_no"+train_no);
		System.out.println("Train_1"+trainno);
		
		if (complaint_place.equals("t")) {
			complaint_place="T";		}
		if (complaint_place.equals("s")) {
			complaint_place="S";    	}
		
		
		
		String complaint_ref_no  ="";
		String zone              ="";
	    String division          ="";
	    String concern_dept      ="";
	    String group_id          ="";
	    String flag_sla          ="";
	    
	    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMdd");
		LocalDateTime now = LocalDateTime.now();
		String date1=dtf.format(now);
		
		
		complaint_ref_no=date1;
		
	   System.out.println("TESTING 2");
	    String query_zone_division_S="SELECT zn_cd,div_cd FROM rly_station_master WHERE station_cd=?";
	    String query_zone_division_T="SELECT owning_rly_zn_cd,owning_rly_div_cd FROM rly_train_master WHERE train_no=?";
		
	    String query_dept="SELECT department_cd FROM rly_heads_subheads_master WHERE id=?";
		
		
		
		
		String query_user="SELECT DISTINCT (group_id),flag_sla FROM rly_users WHERE zn_cd=? AND div_cd= ? AND dept_cd=? AND flag_sla=?";
		
		
		String query="INSERT INTO rly_complaint "+
	    		"(incident_date,complainant_name,mobile,email_id,platform_no,complaint_mode,complaint,channel_type,"+
    			"complaint_type,complaint_sub_type,trainno,zn_cd,div_cd,dept_cd,pnr_uts_no,pnr_uts_flag,"+
    			"coach_no,berth_no,train_class,staff_name,user_group_id,complaint_ref_no,STATUS,sla_flag,remarks,created_on,duty_staff_name , duty_staff_designation,trainname,station_name,image)"+
    			"values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,now(),?,?,?,?,?)"; 
		
		String query_res="INSERT INTO rly_complaint_response "+
	    		"(incident_date,complainant_name,mobile,email_id,platform_no,complaint_mode,complaint,channel_type,"+
    			"complaint_type,complaint_sub_type,trainno,zn_cd,div_cd,dept_cd,pnr_uts_no,pnr_uts_flag,"+
    			"coach_no,berth_no,train_class,staff_name,user_group_id,complaint_ref_no,STATUS,sla_flag,remarks,created_on,duty_staff_name , duty_staff_designation,trainname,station_name)"+
    			"values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,now(),?,?,?,?)"; 
		//COMPLAINT REGISTRATION NUMBER GENERATE
		String complaintid="select count(*) from rly_complaint";
		int id=0;
		try{
			stmt=con.prepareStatement(complaintid);	
			
		rs=stmt.executeQuery();  


			while(rs.next())  {
				id=rs.getInt(1);
                
			}
			
		}
		catch(Exception e){
			
		}
		id=id+1;
		String idd=Integer.toString(id);
		if(idd.length()==1){
			idd="0000"+idd;
		}
		if(idd.length()==2){
			idd="000"+idd;
		}
		if(idd.length()==3){
			idd="00"+idd;
		}
		if(idd.length()==4){
			idd="0"+idd;
		}
		complaint_ref_no=complaint_ref_no+idd;
		
		
		System.out.println("stationName :"+stationName);
		System.out.println("train_no :"+train_no);
		
		
		try {
			//ZONE DIVISION FETCH
			if(stationName==null || stationName.equals("")){
				
				if( next_stn == null || next_stn.equals("") ) {
				stmt=con.prepareStatement(query_zone_division_T);	
				stmt.setString(1,trainno );
				   rs=stmt.executeQuery();  
					while(rs.next())  {
				    zone=rs.getString(1);
				    division=rs.getString(2);
			      	}
				}
				if( (next_stn != null) && (!next_stn.equals("")) ) {
					stmt=con.prepareStatement("SELECT zn_cd,div_cd FROM rly_station_master WHERE station_cd=?");	
					stmt.setString(1, next_stn);
					rs=stmt.executeQuery();  
					while(rs.next())  {
						zone=rs.getString(1);
						division=rs.getString(2);
					}
				
			}
			
				
			}
			System.out.println("train_no1 :"+trainno);
			if(trainno==null || trainno.equals("")){
				stmt=con.prepareStatement(query_zone_division_S);	
				stmt.setString(1,stn_cd );
				   rs=stmt.executeQuery();  
					while(rs.next())  {
				    zone=rs.getString(1);
				    division=rs.getString(2);
			      	}
				
					
				}
			//CONCERN DEPARTMENT
			stmt=con.prepareStatement(query_dept);		
			stmt.setString(1,sub_complaint_type );
			   rs=stmt.executeQuery();  
				while(rs.next())  {
				concern_dept=rs.getString(1); }
				
			//GROUP ID	
	        stmt=con.prepareStatement(query_user);		
			stmt.setString(1,zone );
			stmt.setString(2,division );
			stmt.setString(3,concern_dept );
			stmt.setString(4,"1" );
			 rs=stmt.executeQuery();  
					while(rs.next())  {
					group_id=rs.getString(1);
					flag_sla=rs.getString(2);
					}	
					
			//INSERT INTO RAILWAY COMPLAINT
			stmt=con.prepareStatement(query);		
			
			stmt.setString(1,incident_dt );
			stmt.setString(2,name );
			stmt.setString(3,contact_no );
			stmt.setString(4,email );
			stmt.setString(5,platform_no );
			stmt.setString(6,complaint_place );
			stmt.setString(7,complaint_desc );
			stmt.setString(8,complaint_channel );
			stmt.setString(9,complaint );
			stmt.setString(10,sub_complaint_type );
			
				stmt.setString(11,trainno );		
			
			
			stmt.setString(12,zone );
			stmt.setString(13,division );
			stmt.setString(14,concern_dept );
			stmt.setString(15,pnr_uts );
			if(pnr_uts.equals("")) {stmt.setString(16,"" );}
			if(!pnr_uts.equals("")) {stmt.setString(16,pnr_uts_flag );}
			   			
			
			stmt.setString(17,coach_no);
			stmt.setString(18,berth_no );
			stmt.setString(19,berth_class );
			stmt.setString(20,staff_name );
			stmt.setString(21,group_id );
			stmt.setString(22,complaint_ref_no );
			stmt.setString(23,"Registered" );
			stmt.setString(24,flag_sla );
			stmt.setString(25,"Registered.." );
			stmt.setString(26,name_duty_person );
			stmt.setString(27,designation_duty_person );
			stmt.setString(28,trainname);
			stmt.setString(29,stationName );
			
			   // fetches input stream of the upload file for the blob column
            stmt.setBlob(30, inputStream);
       
						  		
	        stmt.execute(); 
	        
	        
	      //INSERT INTO RAILWAY COMPLAINT RESPONSE
			stmt=con.prepareStatement(query_res);		
			
			stmt.setString(1,incident_dt );
			stmt.setString(2,name );
			stmt.setString(3,contact_no );
			stmt.setString(4,email );
			stmt.setString(5,platform_no );
			stmt.setString(6,complaint_place );
			stmt.setString(7,complaint_desc );
			stmt.setString(8,complaint_channel );
			stmt.setString(9,complaint );
			stmt.setString(10,sub_complaint_type );
			
				stmt.setString(11,trainno );			
			
			
			stmt.setString(12,zone );
			stmt.setString(13,division );
			stmt.setString(14,concern_dept );
			stmt.setString(15,pnr_uts );
			
			if(pnr_uts.equals("")) {stmt.setString(16,"" );}
			if(!pnr_uts.equals("")) {stmt.setString(16,pnr_uts_flag );}
			   			   			
			
			stmt.setString(17,coach_no);
			stmt.setString(18,berth_no );
			stmt.setString(19,berth_class );
			stmt.setString(20,staff_name );
			stmt.setString(21,group_id );
			stmt.setString(22,complaint_ref_no );
			stmt.setString(23,"Registered" );
			stmt.setString(24,flag_sla );
			stmt.setString(25,"Registered.." );
			stmt.setString(26,name_duty_person );
			stmt.setString(27,designation_duty_person );
			stmt.setString(28,trainname );
			stmt.setString(29,stationName );
						  		
	        stmt.execute();
	        
	        
	        
	        String msgText="Thank you for lodging complaint with us. Your Complaint Reference Number is: "+complaint_ref_no;
			
			String complainttype="";
			String subcomplainttype="";
			query="select head_subhead_en from rly_heads_subheads_master where id=?";
			stmt=con.prepareStatement(query);	
			stmt.setString(1, complaint);
			rs=stmt.executeQuery();
			while(rs.next()){
				complainttype=rs.getString(1);
			}
			stmt=con.prepareStatement(query);	
			stmt.setString(1, sub_complaint_type);
			rs=stmt.executeQuery();
			while(rs.next()){
				subcomplainttype=rs.getString(1);
			}
			
			
			String adminmsgText="Indian Railways-CD: Complain "+complaint_ref_no+" is assigned to you. Please attend and resolve "+
								"the issue. Mobile number of passenger: "+contact_no+",Date of Incident: "+incident_dt+", "+
								 "Complaint Type: "+complainttype+", Sub Complaint Type: "+subcomplainttype+", "+
								 "Train no.: "+trainno;
			
			
				adminmsgText=adminmsgText+", Coach no.: "+coach_no+", Berth no.: "+berth_no;
			
			
			SmsUtil smsUtil= new SmsUtil();			
			smsUtil.sendMessage( contact_no,msgText);
			smsUtil.getMobileNumber(group_id, adminmsgText);
	        
	
		}
		catch(Exception ex) { System.out.print(ex);}
		
		
		
		
		
		
		
		
		response.sendRedirect("ComplaintSuggestionRegistration.jsp?ref_no="+complaint_ref_no);
	
	
	}
}
