package servlet;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import utility.DbConnection;
import utility.SendEmail;
import utility.SmsUtil;

@MultipartConfig(maxFileSize = 16177215)
public class ComplaintOnTrain extends HttpServlet {
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		DbConnection dbConnection=new DbConnection();
		Connection con=dbConnection.dbConnect();
		ResultSet rs=null;
		PreparedStatement stmt=null;
		String query="";
		String strMSG = "";	
		String complaint_ref_no="";

		String incident_date=request.getParameter("incident_dt");		
		String complainant_name=request.getParameter("name");		
		String mobile=request.getParameter("contact_no");	
		String email_id=request.getParameter("email");		
		String pnr_uts_no="";
		char pnr_uts_flag='P';

		/************** Image uploading  ************/
		InputStream inputStream = null; // input stream of the upload file

		// obtains the upload file part in this multipart request
		Part filePart = request.getPart("file");
		if (filePart != null) {
			// prints out some information for debugging
			System.out.println(filePart.getName());
			System.out.println(filePart.getSize());
			System.out.println(filePart.getContentType());

			if(filePart.getSize()<=5242880) {
				// obtains input stream of the upload file
				inputStream = filePart.getInputStream();
			}
		}
		/*********************************************/


		String PNRUTS=request.getParameter("pmode");

		/************ Check PNR/UTS complaint type******************/
		if(PNRUTS.equals("PNR")){
			pnr_uts_flag='P';
		}
		if(PNRUTS.equals("UTS")){
			pnr_uts_flag='U';
		}
		/*********************************************/

		/************* for UTS ***************/
		//String trainstn=request.getParameter("pmode1");	
		String train="";
		String trainno="";
		String trainname="";
		String stationname="";	
		String stn_cd="";
		String next_stn="";
		
		if(PNRUTS.equals("UTS")){

			/************ For Train ***************/
			//if(trainstn.equals("Train")){
			train=request.getParameter("trainno");
			trainno=train.substring(0, 5);
			trainname=train.substring(8);			
			//}
			/****************************************/

			/************ For Station ***************/
          /*
			if(trainstn.equals("Station")){
				stationname=request.getParameter("stationName");				
				String[] arrOfStr = stationname.split("> "); 

				for (String a : arrOfStr) 
					stn_cd=a; 				
			}	*/		
		}
		/****************************************/

		String coach_no="";
		String berth_no="";
		String train_class="";

		/***************** FOR PNR *******************/
		if(PNRUTS.equals("PNR")){
			pnr_uts_no=request.getParameter("pnr_uts");
			train=request.getParameter("train_no");
			trainno=train.substring(0, 5);
			trainname=train.substring(8);			
			coach_no=request.getParameter("coach_no");
			berth_no=request.getParameter("berth_no");
			train_class=request.getParameter("berth_class");	
			next_stn=request.getParameter("nextstn");
			System.out.println("Next Station Code : "+next_stn);
		}
		/****************************************/

		String platform_no="";
		String staff_name="";
		staff_name=request.getParameter("staffname");
		char complaint_mode='T';
		String complaint=request.getParameter("complaint_desc");		
		char channel_type='W';
		String complaint_type=request.getParameter("complaint_type");		
		String complaint_sub_type=request.getParameter("sub_complaint_type");	
		String usergroupid="";
		String usergroupid_CML="";
		String usergroupid_CML_mobile="";
		
		String zone="";
		String div="";
		String dept="";
		char sla_flag='0';
		String owning_zone="";
		String owning_div="";
		String owning_user_mobile="";
		String owning_group_id="";
		String owning_group_id_CML="";
		String owning_group_id_CML_mobile="";
		
		int owning_flag=0;

		try{
			
			
			/********** get department across Sub Complaint type **************************/
			query="SELECT department_cd FROM rly_heads_subheads_master WHERE id=?";
			stmt=con.prepareStatement(query);	
			stmt.setString(1, complaint_sub_type);
			rs=stmt.executeQuery();  
			while(rs.next())  {
				dept=rs.getString(1);
			}

			/************************************************************************/
			
			/********** get zone & division across train no **************************/
			
			if(!trainno.equals("")){
				if( next_stn == null || next_stn.equals("") ) {
					query="SELECT owning_rly_zn_cd,owning_rly_div_cd FROM rly_train_master WHERE train_no=?";
					stmt=con.prepareStatement(query);	
					stmt.setString(1, trainno);
					rs=stmt.executeQuery();  
					while(rs.next())  {
						zone=rs.getString(1);
						div=rs.getString(2);
					}
					System.out.println("ZONE IS :"+zone+" "+"DIVISION IS :"+div+" "+"ON GOING ");
					}
					else {
						
							query="SELECT zn_cd,div_cd FROM rly_station_master WHERE station_cd=?";
							stmt=con.prepareStatement(query);	
							stmt.setString(1, next_stn);
							rs=stmt.executeQuery();  
							while(rs.next())  {
								zone=rs.getString(1);
								div=rs.getString(2);
							}
							System.out.println("ZONE IS :"+zone+" "+"DIVISION IS :"+div+" "+"NEXT STATION : "+next_stn);
							
							
							//OWNING USER
							
							query="SELECT owning_rly_zn_cd,owning_rly_div_cd FROM rly_train_master WHERE train_no=?";
							stmt=con.prepareStatement(query);	
							stmt.setString(1, trainno);
							rs=stmt.executeQuery();  
							while(rs.next())  {
								owning_zone=rs.getString(1);
								owning_div=rs.getString(2);
							}
							
							query="SELECT mobile ,group_id FROM rly_users WHERE zn_cd=? AND div_cd=? AND dept_cd=? AND flag_sla=?";
							stmt=con.prepareStatement(query);	
							stmt.setString(1, owning_zone);
							stmt.setString(2, owning_div);
							stmt.setString(3, dept);
							stmt.setString(4, "1");
							rs=stmt.executeQuery();  
							while(rs.next())  {
								owning_user_mobile=rs.getString(1);
								owning_group_id=rs.getString(2);
								owning_flag++;
							}
						System.out.println("OWNING USER MOBILE :"+owning_user_mobile);
						System.out.println("OWNING GROUP ID  :"+owning_group_id);

					}
				
			}

			/************************************************************************/

			/********** get zone & division across Station name **************************/
			if(!stationname.equals("")){	
				query="SELECT zn_cd,div_cd FROM rly_station_master WHERE station_cd=?";
				stmt=con.prepareStatement(query);	
				stmt.setString(1, stn_cd);
				rs=stmt.executeQuery();  
				while(rs.next())  {
					zone=rs.getString(1);
					div=rs.getString(2);
				}
			}

			/************************************************************************/

			

			/********** get usergroupid & sla flag across zone,division and department **************************/
			query="SELECT group_id,flag_sla FROM rly_users WHERE zn_cd=? AND div_cd=? AND dept_cd=? AND flag_sla=?";
			stmt=con.prepareStatement(query);	
			stmt.setString(1, zone);
			stmt.setString(2, div);
			stmt.setString(3, dept);
			stmt.setString(4, "1");
			rs=stmt.executeQuery();  
			while(rs.next())  {
				usergroupid=rs.getString(1);
				sla_flag=rs.getString(2).charAt(0);
			}
			System.out.println("GROUP ID  :"+usergroupid);

			/************************************************************************/
			
			
			
			
			/********** get usergroupid_CML & sla flag across zone,division and department **************************/
			query="SELECT group_id FROM rly_users WHERE zn_cd=? AND div_cd=? AND dept_cd=? AND flag_sla=?";
			stmt=con.prepareStatement(query);	
			stmt.setString(1, zone);
			stmt.setString(2, div);
			stmt.setString(3, "CML");
			stmt.setString(4, "1");
			rs=stmt.executeQuery();  
			while(rs.next())  {
				usergroupid_CML=rs.getString(1);
				
			}
			System.out.println(" GROUP ID FOR CML :"+usergroupid_CML);

			/************************************************************************/
			
			/********** get owninggroupid_CML & sla flag across zone,division and department **************************/
			query="SELECT group_id,flag_sla FROM rly_users WHERE zn_cd=? AND div_cd=? AND dept_cd=? AND flag_sla=?";
			stmt=con.prepareStatement(query);	
			stmt.setString(1, owning_zone);
			stmt.setString(2, owning_div);
			stmt.setString(3, "CML");
			stmt.setString(4, "1");
			rs=stmt.executeQuery();  
			while(rs.next())  {
				owning_group_id_CML=rs.getString(1);
				
			}
            System.out.println("OWNING GROUP ID FOR CML :"+owning_group_id_CML);
			/************************************************************************/
			

			/******************* get number of complaint registered in complaint table *****************************/

			String complaintid="select count(*) from rly_complaint";
			int id=0;
			stmt=con.prepareStatement(complaintid);	
			rs=stmt.executeQuery();  
			while(rs.next())  {
				id=rs.getInt(1);
			}

			/************************************************************************/

			/******************* Use to generate complaint ref number *****************************/

			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMdd");
			LocalDateTime now = LocalDateTime.now();
			String date1=dtf.format(now);			
			complaint_ref_no=date1;	

			id=id+1;
			String idd=Integer.toString(id);
			if(idd.length()==1){
				idd="0000"+idd;
			}
			if(idd.length()==2){
				idd="000"+idd;
			}
			if(idd.length()==3){
				idd="00"+idd;
			}
			if(idd.length()==4){
				idd="0"+idd;
			}				
			complaint_ref_no=complaint_ref_no+idd;

			/************************************************************************/

			/******************* Insert query in complaint table *****************************/

			String complaint_query="INSERT INTO rly_complaint (complaint_ref_no,incident_date,"
					+"complainant_name,mobile,email_id,pnr_uts_no,pnr_uts_flag,trainno,station_name,"
					+"coach_no,berth_no,train_class,platform_no,staff_name,complaint_mode,complaint,"
					+"channel_type,complaint_type,complaint_sub_type,zn_cd,div_cd,dept_cd,"
					+"user_group_id,status,sla_flag,remarks,trainname,created_on,image)"
					+"VALUE(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,NOW(),?)";

			/******************* Insert query in complaint response table *****************************/

			String complaint_response_query="INSERT INTO rly_complaint_response (complaint_ref_no,incident_date,"
					+"complainant_name,mobile,email_id,pnr_uts_no,pnr_uts_flag,trainno,station_name,"
					+"coach_no,berth_no,train_class,platform_no,staff_name,complaint_mode,complaint,"
					+"channel_type,complaint_type,complaint_sub_type,zn_cd,div_cd,dept_cd,"
					+"user_group_id,status,sla_flag,remarks,trainname,created_on)"
					+"VALUE(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,NOW())";


			/************************************************************************/

			/******************* Insert query in complaint table *****************************/
			stmt=con.prepareStatement(complaint_query);	
			stmt.setString(1, complaint_ref_no);
			stmt.setString(2, incident_date);
			stmt.setString(3, complainant_name);
			stmt.setString(4, mobile);
			stmt.setString(5, email_id);
			stmt.setString(6, pnr_uts_no);
			stmt.setString(7,String.valueOf(pnr_uts_flag));
			stmt.setString(8, trainno);
			stmt.setString(9, stationname);
			stmt.setString(10, coach_no);
			stmt.setString(11, berth_no);
			stmt.setString(12, train_class);
			stmt.setString(13, platform_no);
			stmt.setString(14, staff_name);
			stmt.setString(15,String.valueOf(complaint_mode));
			stmt.setString(16, complaint);
			stmt.setString(17,String.valueOf(channel_type));
			stmt.setString(18, complaint_type);
			stmt.setString(19, complaint_sub_type);
			stmt.setString(20, zone);
			stmt.setString(21, div);
			stmt.setString(22, dept);
			stmt.setString(23, usergroupid);
			stmt.setString(24, "Registered");
			stmt.setString(25,String.valueOf(sla_flag));
			stmt.setString(26, "Registered...");
			stmt.setString(27, trainname);
			stmt.setBlob(28, inputStream);
			stmt.execute();

			/************************************************************************/

			/******************* Insert query in complaint response table *****************************/

			stmt=con.prepareStatement(complaint_response_query);	
			stmt.setString(1, complaint_ref_no);
			stmt.setString(2, incident_date);
			stmt.setString(3, complainant_name);
			stmt.setString(4, mobile);
			stmt.setString(5, email_id);
			stmt.setString(6, pnr_uts_no);
			stmt.setString(7,String.valueOf(pnr_uts_flag));
			stmt.setString(8, trainno);
			stmt.setString(9, stationname);
			stmt.setString(10, coach_no);
			stmt.setString(11, berth_no);
			stmt.setString(12, train_class);
			stmt.setString(13, platform_no);
			stmt.setString(14, staff_name);
			stmt.setString(15,String.valueOf(complaint_mode));
			stmt.setString(16, complaint);
			stmt.setString(17,String.valueOf(channel_type));
			stmt.setString(18, complaint_type);
			stmt.setString(19, complaint_sub_type);
			stmt.setString(20, zone);
			stmt.setString(21, div);
			stmt.setString(22, dept);
			stmt.setString(23, usergroupid);
			stmt.setString(24, "Registered");
			stmt.setString(25,String.valueOf(sla_flag));
			stmt.setString(26, "Registered...");
			stmt.setString(27, trainname);
			stmt.execute();

			/************************************************************************/
			
			
			/******************* Send SMS to Complainant and admin user *****************************/

			String msgText="Thank you for lodging complaint with us. Your Complaint Reference Number is: "+complaint_ref_no;
			String complainttype="";
			String subcomplainttype="";
			query="select head_subhead_en from rly_heads_subheads_master where id=?";
			stmt=con.prepareStatement(query);	
			stmt.setString(1, complaint_type);
			rs=stmt.executeQuery();
			while(rs.next()){
				complainttype=rs.getString(1);
			}
			stmt=con.prepareStatement(query);	
			stmt.setString(1, complaint_sub_type);
			rs=stmt.executeQuery();
			while(rs.next()){
				subcomplainttype=rs.getString(1);
			}

			String adminmsgText="Indian Railways-CD: Complain "+complaint_ref_no+" is assigned to you. Please attend and resolve "+
					"the issue.";
			
			
			String owningText="Indian Railways-CD: Complain "+complaint_ref_no+" has been  assigned to "+usergroupid+" .This  is for your information plz.";
					if(mobile==null || mobile.equals("")){
						adminmsgText=adminmsgText+" Email Id of passenger: "+email_id+",";
						
						owningText=owningText+" Email Id of passenger: "+email_id+",";
						
					}
					else{
						adminmsgText=adminmsgText+ " Mobile number of passenger: "+mobile+"," ;
						owningText=owningText+ " Mobile number of passenger: "+mobile+"," ;
					}
					
					adminmsgText=adminmsgText+ "Date of Incident: "+incident_date+", "+
					"Complaint Type: "+complainttype+", Sub Complaint Tyep: "+subcomplainttype+", "+
					"Train no.: "+trainno;
					
					owningText=owningText+ "Date of Incident: "+incident_date+", "+
							"Complaint Type: "+complainttype+", Sub Complaint Tyep: "+subcomplainttype+", "+
							"Train no.: "+trainno;
					

			if(PNRUTS.equals("PNR")){
				adminmsgText=adminmsgText+", Coach no.: "+coach_no+", Berth no.: "+berth_no;
			}

			SmsUtil smsUtil= new SmsUtil();	
			System.out.println("mobile no:"+mobile);
			if(mobile==null || mobile.equals("")){
				SendEmail se=new SendEmail();
				se.sendMail("Complaint Details", msgText, email_id);
			}else{
				smsUtil.sendMessage( mobile,msgText);
				/*if(owning_flag!=0)
				   {
					smsUtil.sendMessage( owning_user_mobile,owningText);
					}*/
				
			}
			
			

			/************************************************************************/
			
			
			
			
			/******************* Insert query in Rly complaint user table *****************************/
			String rly_complaint_user="INSERT INTO rly_complaint_users (complaint_ref_no,user_group_id,STATUS,flag) VALUES (?,?,?,?) ";
			stmt=con.prepareStatement(rly_complaint_user);	
			stmt.setString(1, complaint_ref_no);
			stmt.setString(2, usergroupid);
			stmt.setString(3, "Registered");
			stmt.setString(4, "1");
			stmt.execute();
			System.out.println("1");
			smsUtil.getMobileNumber(usergroupid, adminmsgText);
			
			if(!usergroupid.equals(usergroupid_CML) && usergroupid!=usergroupid_CML)
			{
				stmt=con.prepareStatement(rly_complaint_user);	
				stmt.setString(1, complaint_ref_no);
				stmt.setString(2, usergroupid_CML);
				stmt.setString(3, "Registered");
				stmt.setString(4, "1");
				stmt.execute();
				System.out.println("CML NOT EQUALS USER GROUP ID");
				smsUtil.getMobileNumber(usergroupid_CML, owningText);
			}
			if(owning_flag!=0 && !usergroupid.equals(owning_group_id))
			{
				stmt=con.prepareStatement(rly_complaint_user);	
				stmt.setString(1, complaint_ref_no);
				stmt.setString(2, owning_group_id);
				stmt.setString(3, "Registered");
				stmt.setString(4, "0");
				stmt.execute();
				System.out.println("OWNING GROUP ID NOT EQUALS USER GROUP ID");
				smsUtil.getMobileNumber(owning_group_id, owningText);
				if(!owning_group_id.equals(owning_group_id_CML))
				{
					stmt=con.prepareStatement(rly_complaint_user);	
					stmt.setString(1, complaint_ref_no);
					stmt.setString(2, owning_group_id_CML);
					stmt.setString(3, "Registered");
					stmt.setString(4, "0");
					stmt.execute();
					System.out.println("OWNING GROUP ID NOT EQUALS TO CML OWNING GROUP ID");
					smsUtil.getMobileNumber(owning_group_id_CML, owningText);
				}
			}
			

			/************************************************************************/
			

		

		}
		catch(Exception e){
			System.out.println(e);
			strMSG = "Error";
		}
		response.getOutputStream().print(" Your complaint has successfully registered and your complaint no. is :"+complaint_ref_no);

	}

}
