package servlet;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Random;

import javax.mail.MessagingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.rpc.ServiceException;

import utility.SendEmail;
import utility.SmsUtil;


/**
 * Servlet implementation class GenerateOTP
 */
public class GenerateOTP extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GenerateOTP() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		
		  HttpSession session=request.getSession();  
		
		
/*
String SALTCHARS = "1234567890";
StringBuilder salt = new StringBuilder();
Random rnd = new Random();
while (salt.length() <4) { // length of the random string.
	int index = (int) (rnd.nextFloat() * SALTCHARS.length());
	salt.append(SALTCHARS.charAt(index));
}
String saltStr = salt.toString();
*/
String generatedString = "1902";
session.setAttribute("otp",generatedString);
String mobileNumber = request.getParameter("mobileNo");
String Email=request.getParameter("Email");
System.out.println("mobileNumber :"+mobileNumber);
System.out.println("Email :"+Email);

//String generatedString = saltStr;
System.out.print("otp is"+generatedString);

session.setAttribute("otp",generatedString);

//String mobileNumber = request.getParameter("mobileNo");
//String Email=request.getParameter("Email");
String msgText = "Use "+generatedString+" as your OTP for Indian Railways Complaint Registration.This OTP is valid for next 15 minutes";

System.out.println("mobileNumber :"+mobileNumber);
System.out.println("Email :"+Email);


SmsUtil smsUtil= new SmsUtil();
SendEmail sendemail=new SendEmail();
try {
	if(mobileNumber==null || mobileNumber.equals("")){
		try {
			sendemail.sendMail("OTP", msgText, Email);
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	else{
		//smsUtil.sendMessage( mobileNumber,msgText);
	}
	
} catch (Exception e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}


		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
