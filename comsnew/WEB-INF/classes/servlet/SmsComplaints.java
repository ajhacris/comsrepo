package servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.rpc.ServiceException;

import utility.DbConnection;
import utility.SmsUtil;


public class SmsComplaints extends HttpServlet {
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


		PreparedStatement stmt=null;
		DbConnection dbConnection=new DbConnection();
		Connection con=dbConnection.dbConnect();
		ResultSet rs=null;
		String coach_no="";
		String berth_no="";
		String train_class="";
		String pnr_uts_no="";
		String next_station="";
		String train="";
		String trainno="";
		String trainname="";
		String PNRUTS="";
		String stationName="";
		String platform_no="";
		char pnr_uts_flag='P';
		String usergroupid="";
		char sla_flag='0';
		String zone="";
		String division="";
		String department="";
		
		
		//COMMON FIELDS
		String complaint_type=request.getParameter("complaint_type");
		String sub_complaint_type=request.getParameter("sub_complaint_type");
		String complaint_desc=request.getParameter("complaint_desc");
		String mobile=request.getParameter("contact_no");
		String sms_id=request.getParameter("sms_id");
		
		String place_type=request.getParameter("place_type");
		
		//ON STATION COMPLAINT
		if(place_type.equals("s")) {
			place_type="S";
		platform_no=request.getParameter("platform_no");
		stationName=request.getParameter("stationName");
		}
		
		String[] arrOfStr = stationName.split("> "); 
		  
        String stn_cd ="";
		for (String a : arrOfStr) 
           stn_cd=a;
		
		//IN TRAIN COMPLAINT
		if(place_type.equals("t")) {
			place_type="T";
				PNRUTS=request.getParameter("pmode");
		
				/************ Check PNR/UTS complaint type******************/
				if(PNRUTS.equals("PNR")){
					pnr_uts_flag='P';
				}
				if(PNRUTS.equals("UTS")){
					pnr_uts_flag='U';
				}
				
		       
				/************* for UTS ***************/
								
				if(PNRUTS.equals("UTS")){
		
					train=request.getParameter("trainno");
					trainno=train.substring(0, 5);
					trainname=train.substring(8);			
				}
				
				
				
				/************* for PNR ***************/
								
				if(PNRUTS.equals("PNR")){
					pnr_uts_no=request.getParameter("pnr_uts");
					train=request.getParameter("train_no");
					trainno=train.substring(0, 5);
					trainname=train.substring(8);			
					coach_no=request.getParameter("coach_no");
					berth_no=request.getParameter("berth_no");
					train_class=request.getParameter("berth_class");
					next_station=request.getParameter("nextstn");
				}
		}
		
		
		
		try {
		/********** get zone & division across train no **************************/
			String query_zone_div=null;
		if(!trainno.equals("")){
			
			System.out.println("IN TRAIN ");
		if( next_station == null || next_station.equals("") ) {
			query_zone_div="SELECT owning_rly_zn_cd,owning_rly_div_cd FROM rly_train_master WHERE train_no=?";
			stmt=con.prepareStatement(query_zone_div);	
			stmt.setString(1, trainno);
			rs=stmt.executeQuery();  
			while(rs.next())  {
				zone=rs.getString(1);
				division=rs.getString(2);
			}
			System.out.println("ZONE IS :"+zone+" "+"DIVISION IS :"+division+" "+"ON GOING ");
			}
		if( next_station != null && ! next_station.equals("") ) {
			query_zone_div="SELECT zn_cd,div_cd FROM rly_station_master WHERE station_cd=?";
			stmt=con.prepareStatement(query_zone_div);	
			stmt.setString(1, next_station);
			rs=stmt.executeQuery();  
			while(rs.next())  {
		    	zone=rs.getString(1);
				division=rs.getString(2);
				}
			System.out.println("ZONE IS :"+zone+" "+"DIVISION IS :"+division+" "+"NEXT STATION : "+next_station);
				
			}
		}
		else {
			query_zone_div="SELECT zn_cd,div_cd FROM rly_station_master WHERE station_cd=?";
			
			stmt=con.prepareStatement(query_zone_div);		
			stmt.setString(1,stn_cd );
			   rs=stmt.executeQuery();  
				while(rs.next())  {
			    zone=rs.getString(1);
			    division=rs.getString(2);
		      	}
			
			
			
		}
		
		
		
		
		
		/********** get department across Sub Complaint type **************************/
		String query_dept="SELECT department_cd FROM rly_heads_subheads_master WHERE id=?";
		stmt=con.prepareStatement(query_dept);	
		stmt.setString(1, sub_complaint_type);
		rs=stmt.executeQuery();  
		while(rs.next())  {
			department=rs.getString(1);
		}
		
		
		/********** get usergroupid & sla flag across zone,division and department **************************/
		String query="SELECT group_id,flag_sla FROM rly_users WHERE zn_cd=? AND div_cd=? AND dept_cd=? AND flag_sla=?";
		
			stmt=con.prepareStatement(query);
			stmt.setString(1, zone);
			stmt.setString(2, division);
			stmt.setString(3, department);
			stmt.setString(4, "1");
			rs=stmt.executeQuery();  
			while(rs.next())  {
				usergroupid=rs.getString(1);
				sla_flag=rs.getString(2).charAt(0);
			}
		
		
		String complaintid="select count(*) from rly_complaint";
		int id=0;
		
		
			stmt=con.prepareStatement(complaintid);	
			rs=stmt.executeQuery();
			while(rs.next())  {
				id=rs.getInt(1);
			}

		

		/******************* Use to generate complaint ref number *****************************/
        String complaint_ref_no="";
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMdd");
		LocalDateTime now = LocalDateTime.now();
		String date1=dtf.format(now);			
		complaint_ref_no=date1;	

		id=id+1;
		String idd=Integer.toString(id);
		if(idd.length()==1){
			idd="0000"+idd;
		}
		if(idd.length()==2){
			idd="000"+idd;
		}
		if(idd.length()==3){
			idd="00"+idd;
		}
		if(idd.length()==4){
			idd="0"+idd;
		}				
		complaint_ref_no=complaint_ref_no+idd;
		
		
		/******************* Insert query in complaint table *****************************/

		String complaint_query="INSERT INTO rly_complaint (complaint_ref_no,incident_date,"
				+"complainant_name,mobile,email_id,pnr_uts_no,pnr_uts_flag,trainno,station_name,"
				+"coach_no,berth_no,train_class,platform_no,staff_name,complaint_mode,complaint,"
				+"channel_type,complaint_type,complaint_sub_type,zn_cd,div_cd,dept_cd,"
				+"user_group_id,status,sla_flag,remarks,trainname,created_on)"
				+"VALUE(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,NOW())";

		/******************* Insert query in complaint response table *****************************/

		String complaint_response_query="INSERT INTO rly_complaint_response (complaint_ref_no,incident_date,"
				+"complainant_name,mobile,email_id,pnr_uts_no,pnr_uts_flag,trainno,station_name,"
				+"coach_no,berth_no,train_class,platform_no,staff_name,complaint_mode,complaint,"
				+"channel_type,complaint_type,complaint_sub_type,zn_cd,div_cd,dept_cd,"
				+"user_group_id,status,sla_flag,remarks,trainname,created_on)"
				+"VALUE(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,NOW())";
		
		
		
		
		/******************* Insert query in complaint table *****************************/
		
		stmt=con.prepareStatement(complaint_query);	
		stmt.setString(1, complaint_ref_no);
		stmt.setString(2, "");
		stmt.setString(3, "");
		stmt.setString(4, mobile);
		stmt.setString(5, "");
		stmt.setString(6, pnr_uts_no);
		
		if(place_type.equals("T")){
		    stmt.setString(7,String.valueOf(pnr_uts_flag));}
		if(place_type.equals("S")){
			stmt.setString(7,"");}
		
		stmt.setString(8, trainno);
		stmt.setString(9, stationName);
		stmt.setString(10, coach_no);
		stmt.setString(11, berth_no);
		stmt.setString(12, train_class);
		stmt.setString(13, platform_no);
		stmt.setString(14, "");
		stmt.setString(15,place_type);
		stmt.setString(16, complaint_desc);
		stmt.setString(17,"S");
		stmt.setString(18, complaint_type);
		stmt.setString(19, sub_complaint_type);
		stmt.setString(20, zone);
		stmt.setString(21, division);
		stmt.setString(22, department);
		stmt.setString(23, usergroupid);
		stmt.setString(24, "Registered");
		stmt.setString(25,String.valueOf(sla_flag));
		stmt.setString(26, "Registered...");
		stmt.setString(27, trainname);
		
		stmt.execute();

		/************************************************************************/

		/******************* Insert query in complaint response table *****************************/

		stmt=con.prepareStatement(complaint_response_query);	
		stmt.setString(1, complaint_ref_no);
		stmt.setString(2, "");
		stmt.setString(3, "");
		stmt.setString(4, mobile);
		stmt.setString(5, "");
		stmt.setString(6, pnr_uts_no);
		
		if(place_type.equals("T")){
		    stmt.setString(7,String.valueOf(pnr_uts_flag));}
		if(place_type.equals("S")){
			stmt.setString(7,"");}
		
		stmt.setString(8, trainno);
		stmt.setString(9, "");
		stmt.setString(10, coach_no);
		stmt.setString(11, berth_no);
		stmt.setString(12, train_class);
		stmt.setString(13, platform_no);
		stmt.setString(14, "");
		stmt.setString(15,place_type);
		stmt.setString(16, complaint_desc);
		stmt.setString(17,"S");
		stmt.setString(18, complaint_type);
		stmt.setString(19, sub_complaint_type);
		stmt.setString(20, zone);
		stmt.setString(21, division);
		stmt.setString(22, department);
		stmt.setString(23, usergroupid);
		stmt.setString(24, "Registered");
		stmt.setString(25,String.valueOf(sla_flag));
		stmt.setString(26, "Registered...");
		stmt.setString(27, trainname);
		stmt.execute();

		/************************************************************************/
		
		String query_rly_incoming_update="UPDATE rly_incoming_sms SET sms_flag=? , complaint_ref_no=? WHERE id=? ";
		stmt=con.prepareStatement(query_rly_incoming_update);	
		stmt.setString(1, "A");
		stmt.setString(2, complaint_ref_no);
		stmt.setString(3, sms_id);
		stmt.executeUpdate();	
		
		
		/******************* Send SMS to Complainant and admin user *****************************/
		
		String msgText="Thank you for lodging complaint with us. Your Complaint Reference Number is: "+complaint_ref_no;
		String complainttype="";
		String subcomplainttype="";
		query="select head_subhead_en from rly_heads_subheads_master where id=?";
		stmt=con.prepareStatement(query);	
		stmt.setString(1, complaint_type);
		rs=stmt.executeQuery();
		while(rs.next()){
			complainttype=rs.getString(1);
		}
		stmt=con.prepareStatement(query);	
		stmt.setString(1, sub_complaint_type);
		rs=stmt.executeQuery();
		while(rs.next()){
			subcomplainttype=rs.getString(1);
		}

		String adminmsgText=null;
		if(place_type=="T" ) {
		adminmsgText="Indian Railways-CD: Complain "+complaint_ref_no+" is assigned to you. Please attend and resolve "+
				"the issue. Mobile number of passenger: "+mobile+
				"Complaint Type: "+complainttype+", Sub Complaint Tyep: "+subcomplainttype+", "+
				"Train no.: "+trainno;
		}
		if(place_type=="S" ) {
			adminmsgText="Indian Railways-CD: Complain "+complaint_ref_no+" is assigned to you. Please attend and resolve "+
					"the issue. Mobile number of passenger: "+mobile+" "+
					 "Complaint Type: "+complainttype+", Sub Complaint Type: "+subcomplainttype+", "+
					 "Station Name.: "+stationName+", Platform no.: "+platform_no;

			
			
		}
		
		
		

		if(PNRUTS.equals("PNR")){
			adminmsgText=adminmsgText+", Coach no.: "+coach_no+", Berth no.: "+berth_no;
		}
System.out.println("TEST NUMBER 1");
		SmsUtil smsUtil= new SmsUtil();	
		System.out.println("mobile no:"+mobile);
		if(mobile==null || mobile.equals("")){
			smsUtil.sendMessage( "9953277027",msgText);
		}else{
			smsUtil.sendMessage( mobile,msgText);
		}
		
		smsUtil.getMobileNumber(usergroupid, adminmsgText);
		
		
		
      } catch (SQLException e) {
			
			e.printStackTrace();
		} catch (ServiceException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
		response.sendRedirect("incoming_messages.jsp");
		
		
		/*System.out.println("Common  data");
		System.out.println("complaint_type :"+complaint_type);
		System.out.println("sub_complaint_type"+sub_complaint_type);
		System.out.println("complaint_desc :"+complaint_desc);
		System.out.println("Mobile no. :"+mobile);
		
		System.out.println("On Station data");
		System.out.println("platform_no :"+platform_no);
		System.out.println("stationName"+stationName);
		
		System.out.println("UTS/PNR data");
		System.out.println("pnr_uts_flag :"+pnr_uts_flag);
		
		System.out.println("IN Train data");
		System.out.println("pnr_uts_no :"+pnr_uts_no);
		System.out.println("Train"+train);
		System.out.println("trainno :"+trainno);
		System.out.println("trainname: "+trainname);
		System.out.println("coach_no :"+coach_no);
		System.out.println("berth_no :"+berth_no);
		System.out.println("train_class :"+train_class);
		System.out.println("next_station :"+next_station);*/
	}
			

}
