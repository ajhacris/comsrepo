package servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import utility.DbConnection;
import utility.SendEmail;
import utility.SmsUtil;

/**
 * Servlet implementation class SaveSuggestion
 */
public class SaveSuggestion extends HttpServlet {
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		DbConnection dbConnection=new DbConnection();
		Connection con=dbConnection.dbConnect();
		ResultSet rs=null;
		PreparedStatement stmt=null;
		String query="";
		String strMSG = "";
		
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMdd");
		LocalDateTime now = LocalDateTime.now();
		String date1=dtf.format(now);
		String suggestion_ref_no="";
		
		suggestion_ref_no=date1;
		
		
		String suggestion_type=request.getParameter("complaint_type");
		System.out.println("suggestion_type: "+suggestion_type);
		String suggestion_desc=request.getParameter("complaint_desc");
		System.out.println("suggestion_desc: "+suggestion_desc);
		String trainstn=request.getParameter("pmode1");
		System.out.println("trainstn: "+trainstn);
		String train="";
		String trainno="";
		String trainname="";
		String stationname="";	
		String stn_cd="";
		char suggestion_mode='T';
		char channel_type='W';
		if(trainstn.equals("Train")){
			suggestion_mode='T';
			train=request.getParameter("trainno");
			System.out.println("train: "+train);
			trainno=train.substring(0, 5);
			System.out.println("trainno: "+trainno);
			trainname=train.substring(8);
			System.out.println("trainname: "+trainname);
		}
		if(trainstn.equals("Station")){
			suggestion_mode='S';
			stationname=request.getParameter("stationName");			
			System.out.println("stationname: "+stationname);
			String[] arrOfStr = stationname.split("> "); 
			  
	        for (String a : arrOfStr) 
	           stn_cd=a; 
	        
	        System.out.println("stncd :"+stn_cd);
		}
		String name=request.getParameter("name");
		System.out.println("name: "+name);

		String mobile=request.getParameter("contact_no");
		System.out.println("mobile: "+mobile);

		String email_id=request.getParameter("email");
		System.out.println("email: "+email_id);
		
		
		String usergroupid="";
		String zone="";
		String div="";
		String dept="";
		char sla_flag='0';
		try{
		if(!trainno.equals("")){
			
				query="SELECT owning_rly_zn_cd,owning_rly_div_cd FROM rly_train_master WHERE train_no=?";
				stmt=con.prepareStatement(query);	
				stmt.setString(1, trainno);
				rs=stmt.executeQuery();  
				while(rs.next())  {
					zone=rs.getString(1);
					div=rs.getString(2);
				}
			
			
		}
		if(!stationname.equals("")){	
			
				query="SELECT zn_cd,div_cd FROM rly_station_master WHERE station_cd=?";
				stmt=con.prepareStatement(query);	
				stmt.setString(1, stn_cd);
				rs=stmt.executeQuery();  
				while(rs.next())  {
					zone=rs.getString(1);
					div=rs.getString(2);
				}
			
						
		}

		System.out.println("zone: "+zone);
		System.out.println("div: "+div);
		
		query="SELECT dept_cd FROM rly_suggestion_type WHERE id=?";
		stmt=con.prepareStatement(query);	
		stmt.setString(1, suggestion_type);
		rs=stmt.executeQuery();  


		while(rs.next())  {
			dept=rs.getString(1);

		}
		
		
		
		
		String complaintid="select count(*) from rly_suggestion";
		int id=0;
		try{
			stmt=con.prepareStatement(complaintid);	
			
			rs=stmt.executeQuery();  


			while(rs.next())  {
				id=rs.getInt(1);
                
			}
			
		}
		catch(Exception e){
			
		}
		id=id+1;
		String idd=Integer.toString(id);
		if(idd.length()==1){
			idd="0000"+idd;
		}
		if(idd.length()==2){
			idd="000"+idd;
		}
		if(idd.length()==3){
			idd="00"+idd;
		}
		if(idd.length()==4){
			idd="0"+idd;
		}
		suggestion_ref_no=suggestion_ref_no+idd;
		
		
		
		
		query="SELECT group_id,flag_sla FROM rly_users WHERE zn_cd=? AND div_cd=? AND dept_cd=? AND flag_sla=?";
		stmt=con.prepareStatement(query);	
		stmt.setString(1, zone);
		stmt.setString(2, div);
		stmt.setString(3, dept);
		stmt.setString(4, "2");
		rs=stmt.executeQuery();  


		while(rs.next())  {
			usergroupid=rs.getString(1);
            sla_flag=rs.getString(2).charAt(0);
           
		}
		
		
		System.out.println("zone: "+zone);
		System.out.println("div: "+div);
		System.out.println("dept: "+dept);
		System.out.println("usergroupid: "+usergroupid);
		
		
		
		
		String suggestion_query="INSERT INTO rly_suggestion (suggestion_ref_no,suggestion_type,train_no,train_name,station_name,suggestion_desc,suggestion_mode,channel_type,"+
								"NAME,mobile,email,zn_cd,div_cd,dept_cd,group_id,sla_flag,created_on) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,NOW())";
		
		stmt=con.prepareStatement(suggestion_query);
		stmt.setString(1, suggestion_ref_no);
		stmt.setString(2, suggestion_type);
		stmt.setString(3, trainno);
		stmt.setString(4, trainname);
		stmt.setString(5, stationname);
		stmt.setString(6, suggestion_desc);
		stmt.setString(7, String.valueOf(suggestion_mode));
		stmt.setString(8, String.valueOf(channel_type));
		stmt.setString(9, name);
		stmt.setString(10, mobile);
		stmt.setString(11, email_id);
		stmt.setString(12, zone);
		stmt.setString(13, div);
		stmt.setString(14, dept);
		stmt.setString(15, usergroupid);
		stmt.setString(16, String.valueOf(sla_flag));
		stmt.execute();
		System.out.println("Suggestions register");
		
		
		String msgText="Thank you for giving Suggestions on COMS Portal.";
		
		
		
		String adminmsgText="You have got one Suggestion "+suggestion_ref_no+" on COMS Portal. May please see to it.";
		
		SmsUtil smsUtil= new SmsUtil();	
		if(mobile==null || mobile.equals("")){
			SendEmail se=new SendEmail();
			se.sendMail("Complaint Details", msgText, email_id);
		}
		else{
			smsUtil.sendMessage( mobile,msgText);
		}
		
		smsUtil.getMobileNumber(usergroupid, adminmsgText);
		
		}catch(Exception e){
			System.out.println(e);
		}
		response.getOutputStream().print(suggestion_ref_no);
	}

}
