package servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import utility.DbConnection;


public class AdminLogin extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		/********* Login Details ***************/	
		String uname=request.getParameter("uname");
		String password=request.getParameter("password");

		PreparedStatement stmt=null;
		ResultSet rs=null;
		DbConnection dbConnection=new DbConnection();
		Connection con=dbConnection.dbConnect();		
		HttpSession session=request.getSession();         	        
		int count=0;
		String result="false";
		String query=" SELECT count(*),login_id,group_id,zn_cd,div_cd,dept_cd,flag_sla FROM rly_users WHERE login_id=? AND PASSWORD=? ";
		try{
			stmt=con.prepareStatement(query);				
			stmt.setString(1, uname);
			stmt.setString(2, password);					
			rs=stmt.executeQuery(); 
			while(rs.next()){
				count=rs.getInt(1);
				session.setAttribute("login_id",rs.getString(2));
				session.setAttribute("group_id",rs.getString(3));
				session.setAttribute("zn_cd",rs.getString(4));
				session.setAttribute("div_cd",rs.getString(5));
				session.setAttribute("dept_cd",rs.getString(6));
				session.setAttribute("flag_sla",rs.getString(7));
			}
			if(count==0){
				result="Invalid username password";
				response.sendRedirect("AdminLogin.jsp?strMSG="+result);
			}
			else{
				response.sendRedirect("dashboard.jsp");	
			}
		}
		catch(Exception e){

		}
								
	}
	
}
