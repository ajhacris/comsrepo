package servlet;

import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import com.comviva.cris.webservice.AcfEncryptDecryptManager;
import com.comviva.cris.webservice.AlertResponse;
import com.comviva.cris.webservice.AlertServiceImpl;
import com.comviva.cris.webservice.AlertServiceImplServiceLocator;
import com.comviva.cris.webservice.DefaultDataEncryptImpl;

public class SendSMS {

	public static void main(String[] args) throws ServiceException, RemoteException {
		// TODO Auto-generated method stub
		
		String mobileNumber="8130797483";
		String msgText="Test Message";
		
		AcfEncryptDecryptManager manager = AcfEncryptDecryptManager.getInstance();
		DefaultDataEncryptImpl ecryptImpt = new DefaultDataEncryptImpl("!@#$%^&*");
		manager.setEncryptionImpl(ecryptImpt);
		AlertServiceImplServiceLocator locator = new AlertServiceImplServiceLocator();
		System.out.println("IRSMSA Code Check1");
		locator.setAlertServiceImplEndpointAddress(
				"http://10.64.26.139:9001/CrisAlertWebService/services/AlertServiceImpl");
		System.out.println("IRSMSA Code Check2");
		AlertServiceImpl service = locator.getAlertServiceImpl();
		String appId = manager.encrypt("cmsprs");
		String pass = manager.encrypt("cmsprs@com");
		
		AlertResponse response = service.sendAlertMessage(appId, pass, "IRSMSA", mobileNumber, msgText);
		System.out.println("Submission ID   : " + response.getSubmissionId());
		System.out.println("Status Code     : " + response.getStatusCode());
		System.out.println("Response Message : " + response.getStatusMessage());
		System.out.println("IRSMSA Client End");
	}

}


