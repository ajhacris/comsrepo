package servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.Certificate;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.google.gson.Gson;

public class StationNearBy extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		// TODO Auto-generated method stub
		String day=request.getParameter("day");
		String month=request.getParameter("month");
		String year=request.getParameter("year");
		String fromStn=request.getParameter("starting_point");
		String trainNo=request.getParameter("trainNo");
		if(trainNo!=null && ! trainNo.equals("")){trainNo=trainNo.substring(0, 5);}
		
		if(month.equals("1"))  {month="Jan";}
		if(month.equals("2"))  {month="Feb";}
		if(month.equals("3"))  {month="Mar";}
		if(month.equals("4"))  {month="Apr";}
		if(month.equals("5"))  {month="May";}
		if(month.equals("6"))  {month="Jun";}
		if(month.equals("7"))  {month="Jul";}
		if(month.equals("8"))  {month="Aug";}
		if(month.equals("9"))  {month="Sep";}
		if(month.equals("10")) {month="Oct";}
		if(month.equals("11")) {month="Nov";}
		if(month.equals("12")) {month="Dec";}
		
		
		System.out.println("");
		System.out.println("DAY :"+day);System.out.println("Month :"+month);System.out.println("Year :"+year);
		System.out.println("Starting Station"+fromStn);System.out.println("Train No :"+trainNo);
		
		String date=day+"-"+month+"-"+year;
		System.out.println("Date : "+date);
		String url=null;
	url="https://enquiry.indianrail.gov.in/crisntes/Services?serviceType=SpotTrain&trainNo="+trainNo
		+"&jStation="+fromStn+"&jDate="+date+"&jEvent=A&usrId=PRSUSR&paswd=ruby676";
		
//	url="https://enquiry.indianrail.gov.in/crisntes/Services?serviceType=SpotTrain&trainNo=12802&jStation=NDLS&jDate=26-Nov-2018&jEvent=A&usrId=PRSUSR&paswd=ruby676";
		System.out.println("URL IS :"+url);
		StringBuffer response1 = new StringBuffer();
		try {
		
			
			System.setProperty("http.proxyHost","172.16.1.61") ;
			System.setProperty("http.proxyPort", "8080") ;
			System.setProperty("https.proxyHost","172.16.1.61") ;
			System.setProperty("https.proxyPort", "8080") ;
				
		        URL obj = new URL(url);
		        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		        
		       
		        int responseCode = con.getResponseCode();
		        System.out.println("\nSending 'GET' request to URL : " + url);
		        System.out.println("Response Code : " + responseCode);
		        BufferedReader in = new BufferedReader(
		                new InputStreamReader(con.getInputStream()));
		        String inputLine;
		        
		        while ((inputLine = in.readLine()) != null) {
		        	response1.append(inputLine);
		        }
		        in.close();
		        
		        //print in String
		        System.out.println(response1.toString());
		        
		        
		        
		}
		catch (Exception e) {
			System.out.println("Error :"+e);
		}
		
		try {
			JSONObject jsonObj = new JSONObject(response1.toString());
			response.getWriter().print(jsonObj);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}
	
	
	  
			
		  
	}


