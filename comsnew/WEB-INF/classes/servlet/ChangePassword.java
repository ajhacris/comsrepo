package servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import utility.DbConnection;

/**
 * Servlet implementation class ChangePassword
 */
public class ChangePassword extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	PreparedStatement ps=null;
	ResultSet rs=null;
	DbConnection dbConnection=new DbConnection();
	Connection con=null;
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String oldPassword=request.getParameter("oldPassword");
		String newPassword=request.getParameter("newPassword");
		String userName=request.getParameter("userName");
		int count=0;
		String strmsg="";
		con=dbConnection.dbConnect();
		 if(oldPassword==null || oldPassword.equals("")){
				strmsg="Old Password can't be blank";
			}
		
		else if(newPassword==null || newPassword.equals("")){
			strmsg="New Password can't be blank";
		}
		else{
			try{
				String checkQuery="SELECT COUNT(*) FROM rly_complainant_login WHERE user_name=? AND PASSWORD=?";
				ps=con.prepareStatement(checkQuery);
				ps.setString(1, userName);
				ps.setString(2, oldPassword);
				rs=ps.executeQuery();
				while(rs.next()){
					count=rs.getInt(1);
				}
				if(count>0){
					String updateQuery="UPDATE rly_complainant_login SET PASSWORD=? WHERE user_name=? AND PASSWORD=?";
					ps=con.prepareStatement(updateQuery);
					ps.setString(1, newPassword);
					ps.setString(2, userName);
					ps.setString(3, oldPassword);
					ps.executeUpdate();
					strmsg="Password successfuly changed";
				}
				else{
					strmsg="Old Password is wrong";
				}
				
				
			}
			catch(Exception e){
				strmsg="Error";
				e.printStackTrace();
			}
			
		}
		response.sendRedirect("ChangePassword.jsp?msg="+strmsg);
	}

}
