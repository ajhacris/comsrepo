package servlet;

import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import DAO.CommonListDAO;

public class CommonList_1 extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		CommonListDAO dao=new CommonListDAO();
		String flag=request.getParameter("flag");
		String content="";
		try {
			if(flag.equals("complaint"))
				content=dao.complaintType(request.getParameter("parent_id"));
			if(flag.equals("SLATime"))
				content=dao.SlaTime(request.getParameter("parent_id"));
			if(flag.equals("SLATimeView"))
				content=dao.SlaTimeView(request.getParameter("parent_id"));
			/********** use to find Sub Complaint type *****************/
			if(flag.equals("subcomplaint"))
				content=dao.subComplaintType(request.getParameter("parent_id"));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		response.getOutputStream().print(content);
	}
 
}
