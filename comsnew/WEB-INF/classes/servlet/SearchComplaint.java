package servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import DAO.SearchComplaintDAO;


public class SearchComplaint extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		SearchComplaintDAO dao=new SearchComplaintDAO();
		String flag=request.getParameter("flag");
		String parent_id=request.getParameter("parent_id");
		
		
		String content="";
		
		try {
			
			if(flag.equals("complaintAll"))
				 content=dao.complaintAll();
			if(flag.equals("zone"))
				 content=dao.zone();
			if(flag.equals("division"))
				 content=dao.division(request.getParameter("parent_id"));
			if(flag.equals("department"))
				 content=dao.department();
			
		}catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		response.getOutputStream().print(content);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
