package servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import DAO.ComplaintListDAO;


public class ComplaintList extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		ComplaintListDAO CLD=new ComplaintListDAO();
		String flag=request.getParameter("flag");
		String content="";
		HttpSession session=request.getSession(false); 
		String login_id="";
		String group_id="";
		String zn_cd="";
		String div_cd="";
		String dept_cd="";

		/********* Get details form session of current login user *************/
		if(session!=null){  
			login_id=(String)session.getAttribute("login_id");  
			group_id=(String)session.getAttribute("group_id");
			zn_cd=(String)session.getAttribute("zn_cd");
			div_cd=(String)session.getAttribute("div_cd");
			dept_cd=(String)session.getAttribute("dept_cd");	        	       
		}
		/******************************************************/
		
		try {			
			if(flag.equals("complaintlist"))
			{
				content=CLD.getComplaintList(group_id,zn_cd,div_cd,dept_cd);
			}										
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		response.getOutputStream().print(content);

	}

}
