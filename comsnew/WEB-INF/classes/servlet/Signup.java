package servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import utility.DbConnection;
import utility.SendEmail;
import utility.SmsUtil;

/**
 * Servlet implementation class Signup
 */
public class Signup extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	PreparedStatement ps=null;
	ResultSet rs=null;
	DbConnection dbConnection=new DbConnection();
	Connection con=null;
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String strmsg="";
		String name=request.getParameter("name");
		String email=request.getParameter("email");
		String mobile=request.getParameter("mobile");
		String username="";
		int count=0;
		
		 int ll=8;
         String Capital_chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
         String Small_chars = "abcdefghijklmnopqrstuvwxyz";
         String numbers = "0123456789";
        // String symbols = "!@#$%^&*_=+-/?<>)";
         String value= Capital_chars + Small_chars +
                         numbers;
  
                 Random rndm_method = new Random();
  
         char[] password = new char[ll];
  
         for (int ii = 0; ii < ll; ii++)
         {
            
             password[ii] =
               value.charAt(rndm_method.nextInt(value.length()));
  
         }
         String pass = new String(password);
         
		con=dbConnection.dbConnect();
		
		System.out.println("name : "+name);
		System.out.println("email : "+email);
		System.out.println("mobile : "+mobile);
		
		if(name==null || name.equals("")){
			strmsg="Name can't be blank";
		}
		else if((mobile==null || mobile.equals("")) && (email==null || email.equals(""))){
			strmsg="Email and Mobile both can't be blank";
		}
		else{
			if(mobile==null || mobile.equals("")){
				username=email;
			}
			else{
				username=mobile;
			}
			try {
			String query_check="SELECT COUNT(*)  FROM rly_complainant_login WHERE user_name=?";
			ps=con.prepareStatement(query_check);
			ps.setString(1, username);
			rs=ps.executeQuery();
			while(rs.next()){
				count=rs.getInt(1);
			}
			if(count==0){
				String query="INSERT INTO rly_complainant_login (NAME,email,mobile,user_name,PASSWORD,created_on) VALUES(?,?,?,?,?,NOW())";
				
				ps=con.prepareStatement(query);
				ps.setString(1, name);
				ps.setString(2, email);
				ps.setString(3, mobile);
				ps.setString(4, username);
				ps.setString(5, pass);
				ps.execute();
				//strmsg="Registration Successful";
				strmsg="Thank you for registering in COMS Portal. Your login Id is : "+username+" And Password is: "+pass;
				
				String msgText="Thank you for registering in COMS Portal. Your login Id is : "+username+" And Password is: "+pass;
				
				if(mobile==null || mobile.equals("")){
					SendEmail se=new SendEmail();
					se.sendMail("Complaint Details", msgText, username);
				}
				else{
					SmsUtil smsUtil= new SmsUtil();	
					smsUtil.sendMessage( mobile,msgText);
				}
			}
			else{
				strmsg=username+" Already Registered";
			}
			
				
				
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		}
		response.sendRedirect("signin.jsp?strMSG="+strmsg);	
	}

}
