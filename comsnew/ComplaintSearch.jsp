<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@page import ="utility.DbConnection" import="java.sql.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js" type="text/javascript"></script>
<script src="js/bootstrap-datetimepicker.min.js"></script>
<link href="css/bootstrap.min.css" rel="stylesheet"/>
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
 <link href="css/bootstrap-datetimepicker.css" rel="stylesheet"/>
<link href="css/bootstrap-datetimepicker.min.css" rel="stylesheet"/>

<title>Complaint Search</title>
</head>
 <script>
 function chkForm(){
	 var comp_ref_no  = document.compsearch.com_ref_no.value;
	// var name  = document.compsearch.name.value;
	 var contact_no  = document.compsearch.contact_no.value;
	// var incident_dt  = document.compsearch.incident_dt.value;
	// var email  = document.compsearch.email.value;
	 
	 if(comp_ref_no==""){
         alert("Please Enter Complaint Reference Number");
         document.compsearch.com_ref_no.focus();
         return false;
     }
	/*  if(contact_no==""){
         alert("Please Enter Contact Number");
         document.compsearch.contact_no.focus();
         return false;
     } */
	 
	/* if(name=="" && contact_no=="" && incident_dt=="" && email==""){
         alert("Please enter the value of One field");
         document.compsearch.name.focus();
         return false;
     }*/
	 
 }
 </script>
 <style>
.bg {
   
    background-image: url(img/trainpic.jpg);
    background-position: center center;
    background-size: cover;
}
</style>
<body class="bg">
<div class="container-fluid" style="padding:0px;" >
 <%@ include file="header_main.jsp" %>
 </div>
	<div class="container-fluid" style=" padding-top:100px;width:100%; padding-bottom:120px;">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header bg-info text-white">
					<div class="row">
					<!-- <div class="col-md-12" align="right">
						 <a href="index.jsp" class="close-thin" Style="text-decoration:none;color:white;font-size: 22px;">X</a>
					</div> -->
						 <div class="col-md-11">
						 <center><div style="font-size:22px;"><b>Track Complaint</b> </div>  </center>
						
						 </div>
						 <div class="col-md-1" align="right">
						 <a href="index.jsp" class="close-thin" Style="text-decoration:none;color:white;font-size: 22px;">X</a>
					</div>
					</div>
				</div>
				<div class="card-body" style="background-color: #F7F7F7;font-size:20px;">
					<div class="row container-fluid">
						<div class="col-md-12">
							
							<%
							if(request.getParameter("submitbtn")==null){%>
							<form method="post" action="ComplaintSearch.jsp" name="compsearch" id="compsearch" onsubmit="return chkForm()">
								<div id="detail_panel">
									<div class="row">
									<div class="col-md-3"></div>
									
										<div class="col-md-2">
											<div class="form-group">
												<label for="name" >Complaint Ref. No.: <span style="color:#f05f40;font-size:30px;">*</span></label>
											</div>
										</div>
										<div class="col-md-3">
											<div class="form-group">
												<input id="com_ref_no" type="text" name="com_ref_no" <%if(request.getParameter("com_ref_no")!=null){%>value="<%=request.getParameter("com_ref_no")%>"<%}%> class="form-control" title="Please Enter Complaint ref. Number">
											</div>
										</div>
										
										<!-- <div class="col-md-2">
											<div class="form-group">                   
                    							<button id="submitbtn" type="submit" name="submitbtn" class="btn btn-primary btn-lg" >Submit</button>                    
                							</div>
										</div>
										<div class="col-md-3">
											<div class="form-group">
												<input id="contact_no" type="text" name="contact_no" <%if(request.getParameter("contact_no")!=null){%>value="<%=request.getParameter("contact_no")%>"<%}%> class="form-control" title="Please Enter Contact Detail" >
											</div> -->
										</div>
										
									</div>
										
			  							  									  						
			  							
			  						<div class="row">
			     						<div class="col-md-12" style="text-align:center;">
                							<div class="form-group">                   
                    							<button id="submitbtn" type="submit" name="submitbtn" class="btn btn-primary btn-lg" >Submit</button>                    
                							</div>
            							</div>
            						</div>		  						
								</div>
							</form>
							
							<%} %>
							
							<%
							if(request.getParameter("submitbtn")!=null){
								int i=0;
								PreparedStatement stmt=null;
								DbConnection dbConnection=new DbConnection();
								Connection con=dbConnection.dbConnect();
								String complaint_ref_no="";
								String name="";
								String incdate="";
								String mobile="";
								String email="";
								complaint_ref_no=request.getParameter("com_ref_no");
								name=request.getParameter("name");
								incdate=request.getParameter("incident_dt");
								mobile=request.getParameter("contact_no");
								email=request.getParameter("email");
								
								String subquery="select head_subhead_en from rly_heads_subheads_master where id=?";
								
								String query="SELECT created_on,head_subhead_en,complaint_sub_type,STATUS,remarks,zn_cd,div_cd,dept_cd,user_group_id "+ 
										"FROM rly_complaint,rly_heads_subheads_master "+
										"WHERE complaint_ref_no=? "+
										"AND rly_complaint.complaint_type=rly_heads_subheads_master.id";
								
								/*String query="SELECT created_on,head_subhead_en,complaint_sub_type,STATUS,remarks,ZN_NM,DIV_NM,dept_name,user_group_id "+ 
										"FROM rly_complaint,rly_department,mst_zones,mst_divisions,rly_heads_subheads_master "+
										"WHERE complaint_ref_no=?  AND rly_complaint.dept_cd=rly_department.dept_code "+
										"AND rly_complaint.zn_cd=mst_zones.ZN_CD AND rly_complaint.div_cd=mst_divisions.DIV_CD "+
										"AND rly_complaint.complaint_type=rly_heads_subheads_master.id "; */
										//"AND ( complainant_name=? OR mobile=? OR incident_date=? OR email_id=? )";
								stmt=con.prepareStatement(query);	
								stmt.setString(1,complaint_ref_no);
								/* stmt.setString(2,mobile);
								stmt.setString(3,mobile); */
								/*stmt.setString(2,name);
								stmt.setString(3,mobile);
								stmt.setString(4,incdate);
								stmt.setString(5,email);*/
								
								//System.out.print(complaint_ref_no);
								//System.out.print(name);
								//System.out.print(mobile);
								//System.out.print(incdate);
								//System.out.print(email);
								ResultSet rs=stmt.executeQuery();  
									
										
								 while(rs.next())  {
									 i++;
							%>
							<div class="row">
	<div class="col-md-12">
	 <div class="table-responsive">   
		 <table class='table table-hover' border="1px">
		   <thead>
		    <tr>
		    <th rowspan="2">
		     Reference No
		    </th>
		    <th rowspan="2">
		     Registered Date
		    </th>
		   <th rowspan="2">
		     Complaint Type
		    </th>
		   <th rowspan="2">
		     Sub Complaint Type
		    </th>
		    <th rowspan="2">
		     Status
		    </th>
		    <th rowspan="2">
		     Comments
		    </th>
		    <th  colspan="4">
		     <center>Currently with</center>
		    </th>
		    <th rowspan="2">
		     Action
		    </th>
		   
		    </tr>
		    <tr>
		    <th>
		     Zone
		    </th>
		    <th >
		     Division
		    </th>
		    <th >
		     Department
		    </th>
		    <th >
		     Designation
		    </th>
		    </tr>
		   </thead>
		    
		     <tr>
		     <td>
		     <%=complaint_ref_no %>
		     </td>
		     <td>
		     <%=rs.getString(1) %>
		     </td>
		      <td>
		     <%=rs.getString(2) %>
		     </td>
		      <td>
		      <%
		      stmt=con.prepareStatement(subquery);	
			 stmt.setString(1,rs.getString(3));
			 ResultSet rs1=stmt.executeQuery();  
			 while(rs1.next()){
		      %>
		     <%=rs1.getString(1) %>
		     <%} %>
		     </td>
		      <td>
		     <%=rs.getString(4) %>
		     </td>
		      <td>
		     <%=rs.getString(5) %>
		     </td>
		      <td>
		     <%=rs.getString(6) %>
		     </td>
		      <td>
		     <%=rs.getString(7) %>
		     </td>
		      <td>
		     <%=rs.getString(8) %>
		     </td>
		      <td>
		     <%=rs.getString(9) %>
		     </td>
		     
		     <td>
		     <a href="complaint_reciept_print.jsp?comp_ref_no=<%= complaint_ref_no%>">Print</a>
		     </td>
		     </tr>
		   <%} %>
		 </table>	
		</div>
		</div>	
		
	</div>
	<% if(i==0){ %>
	<div class="row">
	<div class="col-md-12">
	<center><h5>Record Not found</h5></center>
	</div>
	</div>
	<%}} %>
	
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</div>
	
	
</body>
</html>