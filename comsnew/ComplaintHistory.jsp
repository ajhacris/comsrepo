<%@page import ="utility.DbConnection" import="java.sql.*" %>

<html>


<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>

<script src="js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js" type="text/javascript"></script>
<script src="js/CompalintActionJS.js"></script>
<script src="js/bootstrap-datetimepicker.min.js"></script>
<link href="css/bootstrap.min.css" rel="stylesheet"/>
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
 <link href="css/bootstrap-datetimepicker.css" rel="stylesheet"/>
<link href="css/bootstrap-datetimepicker.min.css" rel="stylesheet"/>
    
</head>
<style>
.bg {
   
   
   
    background-position: center center;
    background-size: cover;
}
.w3-example {
    width: 115%;
    background-color: #d4cece;
    padding: 0.01em 0px;
    margin: 5px 0;
    box-shadow: 0 2px 4px 0 rgba(0,0,0,0.16),0 2px 10px 0 rgba(0,0,0,0.12)!important;
}
</style>

<%
/*
String login_id="";
login_id=(String)session.getAttribute("login_id"); 

if(login_id==null || login_id.equals("")){
	response.sendRedirect("index.jsp");
}

*/
%>

<%
 
        
        if(session.getAttribute("login_id")==null)
            {

                System.out.println("******************Session Expired **********************");


                    response.sendRedirect("AdminLogin.jsp");

          }
        %>

<%

PreparedStatement stmt=null;
DbConnection dbConnection=new DbConnection();
Connection con=dbConnection.dbConnect();
String complaint_ref_no="";
complaint_ref_no=request.getParameter("Id");

String query_comp="select head_subhead_en from rly_heads_subheads_master where id=?";
String query="SELECT complaint_ref_no,channel_type,complaint_type,complaint_sub_type,complaint,complaint_mode,"
+"platform_no,trainno,STATUS,dept_name,complainant_name,email_id,mobile,image,station_name,incident_date FROM rly_complaint,rly_department "
+"WHERE complaint_ref_no=?  AND rly_complaint.dept_cd=rly_department.dept_code";


String query_history="SELECT zn_cd,div_cd,dept_cd, user_group_id,remarks FROM  rly_complaint_response WHERE complaint_ref_no=?";
System.out.print("query: "+query);
stmt=con.prepareStatement(query);	

stmt.setString(1,complaint_ref_no);
System.out.print(complaint_ref_no);
 ResultSet rs=stmt.executeQuery();  
	while(rs.next())  {
	
	
   
%>
<body id="page-top">

    <%@ include file="top.jsp" %>


    <div  id="content-wrapper">
   
     <%@ include file="sidebar.jsp" %>
 <div class="container-fluid" style="padding-top:35px;">
  
    
  <div class="col-md-12">
  
       <div class="card-header bg-info text-white"> <span> </span> <p style="font-size:25px;"><b><center><h2>Complaint History</h2></center></b></p>
       </div>
       <div class="card-body" style="background-color:#F7F7F7;" >
        <div class="row container-fluid">
         <div class="col-md-8" >
         
           
           
           
            <table class="table">
    <tbody>
      <tr>
        <td>Reference No</td>
        <td><%=rs.getString(1) %></td>
     </tr>
   
    <%if(rs.getString(16)==null || rs.getString(16).equals("")){ %>
      <%}else{ %>
      <tr >
        <td>Incident Date</td>
        <td><%=rs.getString(16)%></td>
        
      </tr>
      <%} %>
    
      <tr>
        <td>Source</td>
        <td><%if(rs.getString(2).equals("W")){ %>Web<%} %>
        <%if(rs.getString(2).equals("L")){ %>Helpline-138<%} %>
        <%if(rs.getString(2).equals("P")){ %>Helpline-182<%} %>
        <%if(rs.getString(2).equals("M")){ %>Social Media<%} %>
        <%if(rs.getString(2).equals("D")){ %>Manual Dak<%} %>
        <%if(rs.getString(2).equals("C")){ %>Catering Complaints, Rly Board<%} %>
         <%if(rs.getString(2).equals("H")){ %>Helpline-139<%} %>
        <%if(rs.getString(2).equals("S")){ %>SMS<%} %>
        </td>
        
      </tr>      
      <tr >
        <td>Complaint</td>
        <td><% stmt=con.prepareStatement(query_comp);
               stmt.setString(1,rs.getString(3));
               ResultSet rs1=stmt.executeQuery();  
	           while(rs1.next())  {%><%=rs1.getString(1) %><%} %> </td>
        
      </tr>
     
      <tr >
      <td>Sub Complaint</td>
        <td><% stmt=con.prepareStatement(query_comp);
               stmt.setString(1,rs.getString(4));
               ResultSet rs2=stmt.executeQuery();  
	           while(rs2.next())  {%><%=rs2.getString(1) %><%} %> </td>
        
      </tr>
      
       <tr >
        <td>Complaint Description</td>
        <td><textarea class="form-control" rows="5"  ><%=rs.getString(5) %></textarea></td>
        
      </tr>
      <%if(rs.getString(14)==null || rs.getString(14).equals("")){ %>
      <%}else{ %>
      <tr >
        <td>Complaint Image</td>
        <td><img  class="img-thumbnail" src="image.jsp?Id=<%=rs.getString(1)%>" width=200px height=100px></td>
        
      </tr>
      <%} %>
       <tr >
        <td>Complaint Mode </td>
        <td><%if(rs.getString(6).equals("S")){ %>Station<%} %>
        <%if(rs.getString(6).equals("T")){ %>Train<%} %>
        </td>
        
      </tr>
      
       <%if(rs.getString(7)==null || rs.getString(7).equals("")){ }else{%>
       <tr >
        <td>Platform No</td>
        <td><%=rs.getString(7) %></td>
        
      </tr>
      <%} %>
      
      <tr >
      <%if(rs.getString(8)==null || rs.getString(8).equals("")){%>
        <td>Station Name </td>
        <td><%=rs.getString(15) %></td>
        <%}else{ %>
        <td>Train No. </td>
        <td><%=rs.getString(8) %></td>
        <%} %>
      </tr>
      
       <tr >
        <td>Department </td>
        <td><%=rs.getString(10) %></td>
        
      </tr>
      
       <tr >
        <td>Status </td>
        <td><%=rs.getString(9) %></td>
        
      </tr>
      
    </tbody>
  </table>   
              
         

        </div>
        <div class="col-md-4"  >
<h4>Contact Details</h4>
   
            <table class="table" style="background-color:#caaaaa">
    <tbody>
     <%if(rs.getString(11)!=null && !rs.getString(11).equals("")){ %>
      <tr class="success">
        <td>Name</td>
        <td><%=rs.getString(11) %></td>
     </tr>
   <%} %>
    
    <%if(rs.getString(12)!=null && !rs.getString(12).equals("")){ %>
      <tr class="success">
        <td>Email</td>
        <td><%=rs.getString(12) %></td>
        
      </tr>
      <%} %>   
    <%if(rs.getString(13)!=null && !rs.getString(13).equals("")){ %>
        <tr class="success">
        <td>Contact No</td>
        <td><%=rs.getString(13) %></td>
        
      </tr> 
      <%} %> 
       </tbody>
  </table> 
     
       <%} %>
  
 <center> <h3>History :</h3></center>
  <% 
  int i=0;
  stmt=con.prepareStatement(query_history);
               stmt.setString(1,complaint_ref_no);
               ResultSet rs2=stmt.executeQuery();  
	           while(rs2.next())  { if(i>0){ %>
  <center> <img src="img/arrow.png" width="50" height="40"></img></center>
  <%} %>
   <div class="w3-example">
   <table class="table table-condensed">
   <tbody>
   <tr>
   <td>Zone </td>
   <td><%=rs2.getString(1) %></td>
    <td>Division </td>
   <td><%=rs2.getString(2) %></td>
   </tr>
   
   <tr>
   <td>Department </td>
   <td><%=rs2.getString(3) %></td>
    <td>Concern person </td>
   <td><%=rs2.getString(4) %></td>
   </tr>
   
    <tr>
   <td>Remark </td>
   <td colspan="3" ><%=rs2.getString(5) %></td>
   
   </tr>
   
   </tbody>
   </table>


</div>
<% i++;} %>
     
     
      </div>
     
      </div>
    </div>

 
</div>
 
          
      


</div>


</div>
     

    <%@ include file="footer.jsp" %>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
      
    </a>
 </div>
</div>
    <script src="js/jquery/jquery.min.js"></script>
    <script src="js/js/bootstrap.bundle.min.js"></script>

    <script src="js/jquery-easing/jquery.easing.min.js"></script>

    <script src="js/jsdash/sb-admin.min.js"></script>

</body>




</html>