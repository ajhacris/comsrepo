<%@page import="java.io.Console"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@page import ="utility.DbConnection" import="java.sql.*" %>
<html>
	<head>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
		
		<script src="js/bootstrap.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js" type="text/javascript"></script>
		
		<script src="js/SearchComplaintAdminJS.js?version=1"></script>
		
		<script src="js/bootstrap-datetimepicker.min.js"></script>
		<link href="css/bootstrap.min.css" rel="stylesheet"/>
 		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
 		<link href="css/bootstrap-datetimepicker.css" rel="stylesheet"/>
		<link href="css/bootstrap-datetimepicker.min.css" rel="stylesheet"/>
	
	
	
	
	</head>
	<style>
		.bg {
    		background-position: center center;
    		background-size: cover;
		}
	</style>

	<%   
		String zone="";
		String div="";
		String groupid="";
		if(session.getAttribute("login_id")==null){
			
			System.out.println("******************Session Expired **********************");
            response.sendRedirect("AdminLogin.jsp");
		}
		else{
			zone=session.getAttribute("zn_cd").toString();
			div=session.getAttribute("div_cd").toString();
			groupid=session.getAttribute("group_id").toString();
		}
	%>

	<body id="page-top" style="overflow-x: hidden">
  		<!--    header start  -->
    	<%@ include file="top.jsp" %>
    	<div  id="content-wrapper">    
     		<%@ include file="sidebar.jsp" %>	     
    		<!--    header end   -->   
			<div class="container-fluid" style=" padding-top:35px;width:100%; padding-bottom:120px;">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header bg-info text-white">
							<p style="font-size: 25px;">
								<b><center>
									<h2>List Of Complaint</h2>
								</center></b>
							</p>
						</div>
						<div class="card-body" style="background-color: #F7F7F7;">
							<div class="row container-fluid">
								<div class="col-md-12">						
									<form method="post" action="SearchComplaintAdmin.jsp" name="compsearch" id="compsearch" ">
						  				<div id="detail_panel">
						  					<div class="row">
												<div class="col-md-3">
													<div class="form-group">
														<label for="com_ref_no" >Complaint Reference Number: </label>
													</div>
												</div>
												<div class="col-md-3">
													<div class="form-group">
														<input id="com_ref_no" type="text" name="com_ref_no" <%if(request.getParameter("com_ref_no")!=null){ %>value="<%=request.getParameter("com_ref_no")%>"<%} %> class="form-control" >
													</div>
												</div>
												<div class="col-md-3">
                									<div class="form-group">
                    									<label for="mobile">Mobile Number:</label>                    							                 
                									</div>
            									</div>			  									  						
			  						    		<div class="col-md-3">
                									<div class="form-group">
                    									<input id="mobile" type="text" name="mobile" <%if(request.getParameter("mobile")!=null){ %>value="<%=request.getParameter("mobile")%>"<%} %>  class="form-control" >                    							                 
                									</div>
            									</div>
												
											</div>
											<div class="row">
			  						    		<div class="col-md-1">
                									<div class="form-group">
                    									<label for="zone">Zone:</label>                    							                 
                									</div>
            									</div>			  									  						
			  						    		<div class="col-md-3">
                									<div class="form-group">
                    									<div id="zone"></div>                    							                 
                									</div>
            									</div>
            									<div class="col-md-1">
                									<div class="form-group">
                    									<label for="division">Division:</label>                    							                 
                									</div>
            									</div>			  									  						
			  						    		<div class="col-md-3">
                									<div class="form-group">
                    									<div id="division"></div>                      							                 
                									</div>
            									</div>    
            									<div class="col-md-1">
                									<div class="form-group">
                    									<label for="division">Department:</label>                    							                 
                									</div>
            									</div>			  									  						
			  						    		<div class="col-md-3">
                									<div class="form-group">
                    									<div id="department"></div>                      							                 
                									</div>
            									</div> 
            									
            									       							
			  								</div>	
			  								
			  								
			  								
			  										  								
			  								<div class="row">
			     								<div class="col-md-12" style="text-align:center;">
                									<div class="form-group">                   
                    									<button id="searchall" type="submit" name="searchall" class="btn btn-primary" >Search All </button>
                    							<!-- 		<button id="searchself" type="submit" name="searchself" class="btn btn-primary" >Search Self</button>
                    									<a href="SearchComplaintAdmin.jsp"><button id="clear" type="submit" name="clear" class="btn btn-primary" >Clear</button> </a>     -->               
                									</div>
            									</div>
            								</div>		
						  				</div>
									</form>
									<%
							         	if(request.getParameter("searchall")!=null ){
							         		int i=0;
											PreparedStatement stmt=null;
											ResultSet rs=null;
											DbConnection dbConnection=new DbConnection();
											Connection con=dbConnection.dbConnect();
											String complaint_ref_no="";											
											String mobile="";
											String zone_cd="";
											String div_cd="";
											String dep_cd="";
											
											zone_cd=request.getParameter("zone_cd");
											dep_cd=request.getParameter("department_cd");
											div_cd=request.getParameter("division_cd");
											
											complaint_ref_no=request.getParameter("com_ref_no");
											mobile=request.getParameter("mobile");	
											
											System.out.println("zone  :" +zone_cd );
											System.out.println(" division  :" +div_cd);
											System.out.println(" department   :" +dep_cd);
											
											String condition="";																					
											String query="SELECT complaint_ref_no,created_on,STATUS,user_group_id,channel_type FROM rly_complaint ";
											int count=0;
											if(request.getParameter("searchall")!=null){
												
												if( zone_cd.equals("ALL") && (dep_cd!=null && !dep_cd.equals(""))){
													System.out.println("ALL check");
													condition=query+" where dept_cd=? ";
													stmt=con.prepareStatement(condition);
													stmt.setString(1, dep_cd);
													count=1;
													System.out.println("All zone and deptarment :" +zone+"Department"+dep_cd);
												}
												
												if( zone_cd.equals("ALL") && (dep_cd==null || dep_cd.equals(""))){
													
													count=0;
												}
												else{
													
																								
												if(complaint_ref_no!=null && !complaint_ref_no.equals("")){
													condition=query+" where complaint_ref_no=? ";
													stmt=con.prepareStatement(condition);
													stmt.setString(1, complaint_ref_no);
													count=1;
													System.out.println("Complaint no search :" +complaint_ref_no);
												}
												if(mobile!=null && !mobile.equals("")){
													condition=query+" where mobile=? ";
													stmt=con.prepareStatement(condition);
													stmt.setString(1, mobile);
													count=1;
													System.out.println("Mobile no search :" +mobile);
												}
												if(complaint_ref_no!=null && !complaint_ref_no.equals("") && mobile!=null && !mobile.equals("")){
													condition=query+" where complaint_ref_no=? AND mobile=? ";
													stmt=con.prepareStatement(condition);
													stmt.setString(1, complaint_ref_no);
													stmt.setString(2, mobile);
													count=1;
													System.out.println("Complaint and mobile no search :" +complaint_ref_no +" "+mobile);
												}
												
												
												/******* Zone Div DEP      *******/
												
												
	                                              if((zone_cd!=null && ! zone_cd.equals("")) && (dep_cd==null || dep_cd.equals("")) && (div_cd==null || div_cd.equals(""))){
														condition=query+" where zn_cd=? ";
														System.out.println("Zone code "+zone_cd);
														stmt=con.prepareStatement(condition);
														stmt.setString(1, zone_cd);
														count=1;
														System.out.println("zone  search :" +zone_cd);
													}
													
	                                              if((zone_cd==null ||  zone_cd.equals("")) && (dep_cd!=null && !dep_cd.equals("")) && (div_cd==null ||  div_cd.equals(""))){
														condition=query+" where dept_cd=? ";
														stmt=con.prepareStatement(condition);
														stmt.setString(1, dep_cd);
														count=1;
														System.out.println("department  search :" +dep_cd);
													}
													
	                                              if((zone_cd==null ||  zone_cd.equals("")) && (dep_cd==null || dep_cd.equals("")) && (div_cd!=null && !div_cd.equals(""))){
														condition=query+" where div_cd=? ";
														stmt=con.prepareStatement(condition);
														stmt.setString(1, div_cd);
														count=1;
														System.out.println("division  search :" +div_cd);
													}
													
													
	                                              if((zone_cd!=null && ! zone_cd.equals("")) && (dep_cd!=null && !dep_cd.equals("")) && (div_cd!=null && !div_cd.equals(""))){
														condition=query+" where zn_cd=? AND dept_cd=? AND div_cd=? ";
														stmt=con.prepareStatement(condition);
														stmt.setString(1, zone_cd);
														stmt.setString(2, dep_cd);
														stmt.setString(3, div_cd);
														System.out.println("zone division department  search :" +zone_cd +" "+dep_cd+" "+div_cd);
														count=1;
													}
													
	                                              if((zone_cd!=null && ! zone_cd.equals("")) && (dep_cd==null || dep_cd.equals("")) && (div_cd!=null && !div_cd.equals(""))){
														condition=query+" where zn_cd=?  AND div_cd=? ";
														stmt=con.prepareStatement(condition);
														stmt.setString(1, zone_cd);
														stmt.setString(2, div_cd);
														System.out.println("zone division   search :" +zone_cd +"  "+div_cd);
														count=1;
													}
													
	                                              if((zone_cd!=null && ! zone_cd.equals("")) && (dep_cd!=null && !dep_cd.equals("")) && (div_cd==null || div_cd.equals(""))){
														condition=query+" where zn_cd=?  AND dept_cd=? ";
														stmt=con.prepareStatement(condition);
														stmt.setString(1, zone_cd);
														stmt.setString(2, dep_cd);
														System.out.println("zone  department  search :" +zone_cd +" "+dep_cd+" ");
														count=1;
													}
													
	                                              if((zone_cd==null ||  zone_cd.equals("")) && (dep_cd!=null && !dep_cd.equals("")) && (div_cd!=null && !div_cd.equals(""))){
														condition=query+" where  dept_cd=? AND div_cd=? ";
														stmt=con.prepareStatement(condition);
														stmt.setString(1, dep_cd);
														stmt.setString(2, div_cd);
														System.out.println(" division department  search : "+dep_cd+" "+div_cd);
														count=1;
													}
												
												/******* Zone Div DEP END     *******/
												
												}
												
												
												
												
												if(count==0){
													stmt=con.prepareStatement(query);
													System.out.println("All  search :" +zone_cd +" Department "+dep_cd+" Division "+div_cd);
												}
												
												
											}
											
											
											
											
											
											rs=stmt.executeQuery();	
											
											
											
											
											 										
											
									%>
									<div class="row">
										<div class="col-md-12">
											<table class='table table-hover' border="1px">
												<thead>
													<tr>
														<th>Reference No</th>
														<th>Registered Date</th>
														<th>Status</th>
														<th>Pending With</th>
														<th>Channel Type</th>
														<th>Action</th>
													</tr>
													<% while(rs.next())  {
														  i++;	
													%>
													<tr>
														<td><%=rs.getString(1) %></td>
														<td><%=rs.getString(2) %></td>
														<td><%=rs.getString(3) %></td>
														<td><%=rs.getString(4) %></td>
														<td><%=rs.getString(5) %></td>
														<td><a href="PerticularComplaintRecieptPrint.jsp?comp_ref_no=<%=rs.getString(1)%>">Print</a></td>
													</tr>
													<%} %>
		  										</thead>
											</table>
										</div>
									</div>
									<% if(i==0){ %>
										<div class="row">
											<div class="col-md-12">
												<center><h5>Record Not found</h5></center>
											</div>
										</div>
									<%}} %>
								</div>
							</div>
						</div>
					</div>
					</div>
					</div>
					</div>
					
				  
     				     			<!--    footer start  -->
     				      			<%@ include file="footer.jsp" %>
		    		
	    		 			
	    		
			
	
		
    	<script src="js/jquery/jquery.min.js"></script>
    	<script src="js/js/bootstrap.bundle.min.js"></script>
    	<script src="js/jquery-easing/jquery.easing.min.js"></script>
    	<script src="js/jsdash/sb-admin.min.js"></script>
	</body>
</html>