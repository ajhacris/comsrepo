<%@page import="java.io.Console"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@page import ="utility.DbConnection" import="java.sql.*" %>
<html>
	<head>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
		
		<script src="js/bootstrap.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js" type="text/javascript"></script>
		
		<script src="js/SearchComplaintAdminJS.js?version=1"></script>
		
		<script src="js/bootstrap-datetimepicker.min.js"></script>
		<link href="css/bootstrap.min.css" rel="stylesheet"/>
 		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
 		<link href="css/bootstrap-datetimepicker.css" rel="stylesheet"/>
		<link href="css/bootstrap-datetimepicker.min.css" rel="stylesheet"/>
	
	
	
	
	</head>
	<style>
		.bg {
    		background-position: center center;
    		background-size: cover;
		}
		table {
	width: 100%;
}

thead, tbody, tr, td, th {
	display: block;
}

tr:after {
	content: ' ';
	display: block;
	visibility: hidden;
	clear: both;
}

thead th {
	height: 50px;

	/*text-align: left;*/
}

tbody {
	height: 482px;
	overflow-y: auto;
}

thead {
	/* fallback */
	
}

tbody td, thead th {
	width: 16.5%;
	float: left;
}
		
	</style>

	<%   
		String zone="";
		String div="";
		String groupid="";
		if(session.getAttribute("login_id")==null){
			
			System.out.println("******************Session Expired **********************");
            response.sendRedirect("AdminLogin.jsp");
		}
		else{
			zone=session.getAttribute("zn_cd").toString();
			div=session.getAttribute("div_cd").toString();
			groupid=session.getAttribute("group_id").toString();
		}
	%>

	<body id="page-top" style="overflow-x: hidden">
  		<!--    header start  -->
    	<%@ include file="top.jsp" %>
    	<div  id="content-wrapper">    
     		<%@ include file="sidebar.jsp" %>	     
    		<!--    header end   -->   
			<div class="container-fluid" style=" padding-top:35px;width:100%; padding-bottom:120px;">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header bg-info text-white">
							<p style="font-size: 25px;">
								<b><center>
									<h2>SLA Breach Report</h2>
								</center></b>
							</p>
						</div>
						<div class="card-body" style="background-color: #F7F7F7;">
							<div class="row container-fluid">
								<div class="col-md-12">						
									<form method="post" action="SlaBreachReport.jsp" name="compsearch" id="compsearch" ">
						  				<div id="detail_panel">
						  					<div class="row">
												<div class="col-md-3">
													<div class="form-group">
														<label for="com_ref_no" >Choose SLA Breach Level: </label>
													</div>
												</div>
												<div class="col-md-3">
													<div class="form-group">
													<select class='form-control' id='sla_level' name='sla_level' required '>
													<option value=''>-- Select SLA Level --</option>
													<option value='2'>SLA 2 Breach</option>
													<option value='3'>SLA 3 Breach</option>
													</select>
														</div>
												</div>
												
												
											</div>
											<div class="row">
			  						    		<div class="col-md-1">
                									<div class="form-group">
                    									<label for="zone">Zone:</label>                    							                 
                									</div>
            									</div>			  									  						
			  						    		<div class="col-md-3">
                									<div class="form-group">
                    									<div id="zone"></div>                    							                 
                									</div>
            									</div>
            									<div class="col-md-1">
                									<div class="form-group">
                    									<label for="division">Division:</label>                    							                 
                									</div>
            									</div>			  									  						
			  						    		<div class="col-md-3">
                									<div class="form-group">
                    									<div id="division"></div>                      							                 
                									</div>
            									</div>    
            									<div class="col-md-1">
                									<div class="form-group">
                    									<label for="division">Department:</label>                    							                 
                									</div>
            									</div>			  									  						
			  						    		<div class="col-md-3">
                									<div class="form-group">
                    									<div id="department"></div>                      							                 
                									</div>
            									</div> 
            									
            									       							
			  								</div>	
			  								
			  								
			  								
			  										  								
			  								<div class="row">
			     								<div class="col-md-12" style="text-align:center;">
                									<div class="form-group">                   
                    									<button id="searchall" type="submit" name="searchall" class="btn btn-primary" >Search All </button>
                    							<!-- 		<button id="searchself" type="submit" name="searchself" class="btn btn-primary" >Search Self</button>
                    									<a href="SearchComplaintAdmin.jsp"><button id="clear" type="submit" name="clear" class="btn btn-primary" >Clear</button> </a>     -->               
                									</div>
            									</div>
            								</div>		
						  				</div>
									</form>
									<%
							         	if(request.getParameter("searchall")!=null ){
							         		int i=0;
											PreparedStatement stmt=null;
											ResultSet rs=null;
											DbConnection dbConnection=new DbConnection();
											Connection con=dbConnection.dbConnect();
											String sla_level="";											
											String zone_cd="";
											String div_cd="";
											String dep_cd="";
											
											zone_cd=request.getParameter("zone_cd");
											dep_cd=request.getParameter("department_cd");
											div_cd=request.getParameter("division_cd");
											sla_level=request.getParameter("sla_level");
											
											
										    System.out.println("zone  :" +zone_cd );
											System.out.println(" division  :" +div_cd);
											System.out.println(" department   :" +dep_cd);
											System.out.println(" sla_level   :" +sla_level); 
											String sla_breach_3_condition ="";
											String sla_breach_2_condition="";
											
											if((zone_cd!=null &&  !zone_cd.equals("")) && (dep_cd==null || dep_cd.equals("")) && (div_cd==null ||  div_cd.equals(""))){
												sla_breach_3_condition=" AND rly.zn_cd = '"+zone_cd+"'";
												sla_breach_2_condition=" AND rly.zn_cd = '"+zone_cd+"'";
												System.out.println("zone  :" +zone_cd );
												
											}
											if((zone_cd!=null &&  !zone_cd.equals("")) && (dep_cd==null || dep_cd.equals("")) && (div_cd!=null &&  !div_cd.equals(""))){
												sla_breach_3_condition=" AND rly.zn_cd = '"+zone_cd +"' AND rly.div_cd = '"+div_cd+"'" ;
												sla_breach_2_condition=" AND rly.zn_cd = '"+zone_cd +"' AND rly.div_cd = '"+div_cd+"'" ;
												System.out.println("zone  :" +zone_cd +" division  :" +div_cd );
											}

											if((zone_cd!=null &&  !zone_cd.equals("")) && (dep_cd!=null && !dep_cd.equals("")) && (div_cd==null ||  div_cd.equals(""))){
												sla_breach_3_condition=" AND rly.zn_cd = '"+zone_cd +"' AND rly.dept_cd = '"+dep_cd +"'";
												sla_breach_2_condition=" AND rly.zn_cd = '"+zone_cd +"' AND rly.dept_cd = '"+dep_cd +"'";
												System.out.println("zone  :" +zone_cd +" department  :" +dep_cd );
											}
											if((zone_cd==null ||  zone_cd.equals("")) && (dep_cd!=null && !dep_cd.equals("")) && (div_cd==null ||  div_cd.equals(""))){
												sla_breach_3_condition=" AND rly.dept_cd = '"+dep_cd +"'";
												sla_breach_2_condition=" AND rly.dept_cd = '"+dep_cd +"'" ;
												System.out.println(" department  :" +dep_cd );
											}
											if((zone_cd!=null &&  !zone_cd.equals("")) && (dep_cd!=null && !dep_cd.equals("")) && (div_cd!=null ||  !div_cd.equals(""))){
												sla_breach_3_condition=" AND rly.zn_cd = '"+zone_cd +"' AND rly.dept_cd = '"+dep_cd +"' AND rly.div_cd = '"+div_cd+"'" ;
												sla_breach_2_condition=" AND rly.zn_cd = '"+zone_cd +"' AND rly.dept_cd = '"+dep_cd +"' AND rly.div_cd = '"+div_cd+"'";
												System.out.println("zone  :" +zone_cd +" department  :" +dep_cd );
											}
											
											String sla_breach_3="SELECT rly.complaint_ref_no , rly.zn_cd ,rly.div_cd ,rly.dept_cd , TIME_FORMAT(TIMEDIFF(NOW(), created_on),\"%H\"),sla3_time_hrs "+
												     " FROM rly_complaint AS rly LEFT OUTER JOIN rly_heads_subheads_master AS comp ON rly.complaint_sub_type=comp.id, rly_users AS USER "+
												     " WHERE rly.zn_cd=user.zn_cd AND rly.div_cd=user.div_cd AND STATUS!='Closed' AND user.flag_sla=3   " + sla_breach_3_condition;
//System.out.println(sla_breach_3);
										    String sla_breach_2="SELECT rly.complaint_ref_no , rly.zn_cd ,rly.div_cd ,rly.dept_cd , TIME_FORMAT(TIMEDIFF(NOW(), created_on),\"%H\"),sla2_time_hrs FROM rly_complaint AS rly "+
												     "LEFT OUTER JOIN rly_heads_subheads_master AS comp ON rly.complaint_sub_type=comp.id, rly_users AS USER "+
												     "WHERE rly.zn_cd=user.zn_cd AND rly.div_cd=user.div_cd AND rly.dept_cd=user.dept_cd AND"+
												     "  STATUS!='Closed' AND user.flag_sla=2  "+sla_breach_2_condition;
//System.out.println(sla_breach_2);											
										  
										    if(sla_level.equals("2")){
										    	stmt=con.prepareStatement(sla_breach_2);
										    	System.out.println("sla_breach_2");
										    	}
										    if(sla_level.equals("3")){
										    	stmt=con.prepareStatement(sla_breach_3);
										    	System.out.println("sla_breach_3");
										    	}
										    rs=stmt.executeQuery();											
											
									%>
									 <div class="row">
										<div class="col-md-12">
											<table class="table table-striped">
										     <thead>
											     <tr>
												     
														<th>Complaint ref.no</th>
														<th>Zone</th>
														<th>Division</th>
														<th>Department</th>
														<th>Time Passed(Hrs.)</th>
														<th>History</th>
													</tr>
													</thead>
										<tbody>
													<% while(rs.next())  {
														if(rs.getInt(5)>=rs.getInt(6)){
														  i++;	
													%>
													<tr>
														<td class="filterable-cell"><%=rs.getString(1) %></td>
														<td class="filterable-cell"><%=rs.getString(2) %></td>
														<td class="filterable-cell"><%=rs.getString(3) %></td>
														<td class="filterable-cell"><%=rs.getString(4) %></td>
														<td class="filterable-cell"><%=rs.getString(5) %></td>
														<td class="filterable-cell ctd" style="width: 12.5%;"><a href="ComplaintHistory.jsp?Id=<%=rs.getString(1)%>">
									             <button type="button" class="btn btn-info" id="history" >  History </button> </a></td>
													
													
													</tr>
													<%}} %>
		  										</tbody>
											</table>
										</div>
									</div>
									<% if(i==0){ %>
										<div class="row">
											<div class="col-md-12">
												<center><h5>Record Not found</h5></center>
											</div>
										</div>
									<%}} %>
								</div>
							</div>
						</div>
					</div>
					</div>
					</div>
					</div>
					
				  
     				     			<!--    footer start  -->
     				      			<%@ include file="footer.jsp" %>
		    		
	    		 			
	    		
			
	
		
    	<script src="js/jquery/jquery.min.js"></script>
    	<script src="js/js/bootstrap.bundle.min.js"></script>
    	<script src="js/jquery-easing/jquery.easing.min.js"></script>
    	<script src="js/jsdash/sb-admin.min.js"></script>
	</body>
</html>