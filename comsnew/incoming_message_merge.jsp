<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@page import ="utility.DbConnection" import="java.sql.*" %>
	<%@page import="java.util.ArrayList"%>
<html>
	<head>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
		
		<script src="js/bootstrap.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js" type="text/javascript"></script>
		
		
		
		<script src="js/bootstrap-datetimepicker.min.js"></script>
		<link href="css/bootstrap.min.css" rel="stylesheet"/>
 		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
 		<link href="css/bootstrap-datetimepicker.css" rel="stylesheet"/>
		<link href="css/bootstrap-datetimepicker.min.css" rel="stylesheet"/>
	
	
	
	
	</head>
	<style>
		.bg {
    		background-position: center center;
    		background-size: cover;
		}
		
		
		table {
	width: 100%;
}

thead, tbody, tr, td, th {
	display: block;
}

tr:after {
	content: ' ';
	display: block;
	visibility: hidden;
	clear: both;
}

thead th {
	height: 50px;

	/*text-align: left;*/
}

tbody {
	height: 425px;
	overflow-y: auto;
}

thead {
	/* fallback */
	
}

tbody td, thead th {
	width: 20.5%;
	float: left;
}
	</style>

	<%   
		String zone="";
		String div="";
		String groupid="";
		int count=0;
		int counter=0;
		ResultSet rs=null;
		PreparedStatement stmt=null;
		DbConnection dbConnection=new DbConnection();
		Connection con=dbConnection.dbConnect();
		if(session.getAttribute("login_id")==null){
			
			System.out.println("******************Session Expired **********************");
            response.sendRedirect("AdminLogin.jsp");
		}
		else{
			zone=session.getAttribute("zn_cd").toString();
			div=session.getAttribute("div_cd").toString();
			groupid=session.getAttribute("group_id").toString();
			
			

 count=Integer.parseInt(request.getParameter("count"));
//System.out.println("count:"+count);
ArrayList<String> Id=new ArrayList<String>();
for(int i=1;i<=count;i++){
	Id.add(request.getParameter("id"+i));
}
for(int j=0;j<Id.size();j++){
	System.out.println(Id.get(j));
}


String sms="";
String mobile="";
String query="SELECT sms_text,from_mobile,id FROM rly_incoming_sms WHERE id IN (?";
for(int i=1;i<count;i++){
	query=query+",?";
}
query=query+")";

stmt=con.prepareStatement(query);
int l=0;
for(int k=1;k<=count;k++){
	stmt.setString(k,Id.get(l));
	l++;
}

rs=stmt.executeQuery();  
			
		}
	%>

	<body id="page-top" style="overflow-x: hidden">
  		<!--    header start  -->
    	<%@ include file="top.jsp" %>
    	<div  id="content-wrapper">    
     		<%@ include file="sidebar.jsp" %>	     
    		<!--    header end   -->   
			<div class="container-fluid" style=" padding-top:35px;width:100%; padding-bottom:120px;">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header bg-info text-white">
							<p style="font-size: 25px;">
								<b><center>
									<h2>List Of Complaint</h2>
								</center></b>
							</p>
						</div>
						<div class="card-body" style="background-color: #F7F7F7;">
							<div class="row container-fluid">
								<div class="col-md-12">

<%

%>	
<form action="sms_merge.jsp">

<table class="table table-striped">
<thead>
<tr>
<th>Id</th>
<th>Mobile</th>
<th>Message</th>
<th>Priority</th>
</tr>
</thead>
<tbody>
<%
while(rs.next())  {
	counter++;
%>
<tr>
 <td class="filterable-cell">
  <input  type="text" name="sr<%=counter %>" id="sr<%=counter %>" value=<%=counter %> readonly class="form-control">
 </td>
 <td class="filterable-cell">
  <input type="text" name="mobile<%=counter %>" id="mobile<%=counter %>" value=<%=rs.getString(2) %> readonly class="form-control">
 </td>
 <td class="filterable-cell">
  <textarea name="message<%=counter %>" id="message<%=counter %>"  readonly class="form-control"> <%=rs.getString(1) %> </textarea>
 </td class="filterable-cell">
 <td class="filterable-cell">
   <select  name="priority<%=counter %>" id="priority<%=counter %>" class="form-control">
          <%for(int ll=1;ll<=count;ll++){ %>
          <option value="<%=ll%>"><%=ll%></option>
         
	      <%} %>
          
       </select> 
 </td>
 <td>
  <input type="hidden" name="id<%=counter %>" id="id<%=counter %>" value=<%=rs.getString(3) %> readonly>
 </td>
</tr>

<%


}



%>


<tr>
<td>
<input type="hidden" name="counter" id="counter" value=<%=counter %> readonly>
</td>
<td>
<center>
<button id="submitbtn" type="submit" name="submitbtn" class="btn btn-primary" align="center" >Submit</button>
</center>

</td>

</tr>
</tbody>

</table>


</form>


</div>
							</div>
						</div>
					</div>
					</div>
					</div>
					</div>
					
				  
     				     			<!--    footer start  -->
     				      			<%@ include file="footer.jsp" %>
		    		
	    		 			
	    		
			
	
		
    	<script src="js/jquery/jquery.min.js"></script>
    	<script src="js/js/bootstrap.bundle.min.js"></script>
    	<script src="js/jquery-easing/jquery.easing.min.js"></script>
    	<script src="js/jsdash/sb-admin.min.js"></script>
	</body>
</html>

</body>
</html>