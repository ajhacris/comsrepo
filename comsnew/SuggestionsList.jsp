<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@page import ="utility.DbConnection" import="java.sql.*" %>
<html>


<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>

<script src="js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js" type="text/javascript"></script>

<link href="css/bootstrap.min.css" rel="stylesheet"/>
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
 <link href="css/bootstrap-datetimepicker.css" rel="stylesheet"/>
<link href="css/bootstrap-datetimepicker.min.css" rel="stylesheet"/>
    
</head>
<style>
.bg {
   
   
    background-position: center center;
    background-size: cover;
}
</style>

<%
 
       String  groupid="";
        if(session.getAttribute("login_id")==null)
            {

                System.out.println("******************Session Expired **********************");


                    response.sendRedirect("AdminLogin.jsp");

          }
        else{
        	groupid=session.getAttribute("group_id").toString();
        }
		
        %>

<body id="page-top" style="overflow-x: hidden">

    <%@ include file="top.jsp" %>


    <div  id="content-wrapper">
    
      
     <%@ include file="sidebar.jsp" %>

<div class="container" align="center" style="padding-top:10px;padding-left:20px;">


<div class="col-md-12">


<div class="card" >
<div class="card-header bg-info text-white"> <span> </span> <p style="font-size:25px;"><b><center><h2>Suggestions List</h2></center></b></p></div>
<div class="card-body" style="background-color:#F7F7F7;" >
<div class="row container-fluid">
<div class="col-md-12" >

<div id="detail_panel">

	<%
	
	PreparedStatement stmt=null;
	DbConnection dbConnection=new DbConnection();
	Connection con=dbConnection.dbConnect();
	ResultSet rs=null;
	
	
	String query="SELECT DATE_FORMAT(created_on,'%d-%m-%Y'),suggestions,dept_name,suggestion_mode,channel_type,group_id FROM rly_suggestion,rly_suggestion_type,rly_department "+
			     "WHERE rly_suggestion.suggestion_type=rly_suggestion_type.id AND rly_department.dept_code=rly_suggestion.dept_cd AND group_id=?";
	
	stmt=con.prepareStatement(query);
	stmt.setString(1, groupid);
	rs=stmt.executeQuery();  
	int i=1;
	
	%>		    
			    
			    
			   
	<div class="row">
	<div class="col-md-12">
		 <table class="table table-hover" >
		 	<thead>
		 		<tr>
		 			<th>Sr No.</th>
		 			<th>Registered Date</th>
		 			<th>Suggestion Type</th>
		 			<th>Department</th>
		 			<th>Suggestion Mode</th>
		 			<th>Channel Type</th>
		 			<th>Authority</th>		 			
		 		</tr>
		 	</thead>		 	
		 	<tbody>
		 	<%
		 		while(rs.next()){
		 	%>
		 		<tr>
		 			<td><%=i %></td>
		 			<td><%=rs.getString(1) %></td>
		 			<td><%=rs.getString(2) %></td>
		 			<td><%=rs.getString(3) %></td>
		 			<%if(rs.getString(4).equals("T")){ %>
		 			<td>In Train</td>
		 			<%} %>
		 			<%if(rs.getString(4).equals("S")){ %>
		 			<td>On Station</td>
		 			<%} %>
		 			<%if(rs.getString(5).equals("W")){ %>
		 			<td>Web</td>
		 			<%} %>
		 			<td><%=rs.getString(6) %></td>
		 		</tr>
		 		<%} %>
		 	</tbody>		 	
		 </table>
	</div>	
	</div>		     
			     
			     
			
			     
			    
			     
			   
			   
			     
			  
			     
			   
			    
			     
			   

</div>
</div>
</div>

</div>





</div>
<div class="col-md-2"></div>

 </div>
     

  
 </div>
</div>

  <%@ include file="footer.jsp" %>
    <!-- Scroll to Top Button-->

</div>
    <script src="js/jquery/jquery.min.js"></script>
    <script src="js/js/bootstrap.bundle.min.js"></script>

    <script src="js/jquery-easing/jquery.easing.min.js"></script>

    <script src="js/jsdash/sb-admin.min.js"></script>

</body>




</html>