<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@page import ="utility.DbConnection" import="java.sql.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js" type="text/javascript"></script>
<script src="js/bootstrap-datetimepicker.min.js"></script>
<link href="css/bootstrap.min.css" rel="stylesheet"/>
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
 <link href="css/bootstrap-datetimepicker.css" rel="stylesheet"/>
<link href="css/bootstrap-datetimepicker.min.css" rel="stylesheet"/>

<title>Frequently Asked Questions</title>
</head>
 <style>
.bg {
   
    background-image: url(img/trainpic.jpg);
    background-position: center center;
    background-size: cover;
}
</style>
<body class="bg">
<div class="container-fluid" >
 <%@ include file="header_main.jsp" %>
 </div>
 <div class="container-fluid" style="padding-top:10px;padding-bottom:10px;  margin-left: auto;
    margin-right: auto;
    width: 100%;  min-height: 100%;">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header bg-info text-white">
					<div class="row">
					<div class="col-md-12" align="right">
						 <a href="index.jsp" class="close-thin" Style="text-decoration:none;color:white;font-size: 22px;">X</a>
					</div>
						 <div class="col-md-12">
						 <center><p style="font-size:22px;"><b>Frequently Asked Questions</b> </p>  </center>
						
						 </div>
					</div>
				</div>
				<div class="card-body" style="background-color: #F7F7F7;font-size:20px;">
					<div class="row container-fluid">
						<div class="col-md-12">
						
						 <div class="table-responsive">   
																								<table border="0" cellpadding="0" cellspacing="0" width="99%" align="center">
																									<tbody>
																										<tr>
																										<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
																									
																											<td style="padding-bottom: 5px;" valign="top" width="48%" align="center">
																												<p align="justify"><font color="#0000FF">
									<img src="/criscm/img/image/arr1.jpg" height="12" width="12"></font><font color="#0000FF" face="Tahoma" size="2"> </font><a class="left_links" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/AdvanceReservation.pdf" target="_blank"><font face="Tahoma" size="2" color="#0090FF"><span style="text-decoration: none">Advance Reservation </span></font></a></p>
																												<ul>
																													<li>
																													<p align="justify"><font face="Tahoma" size="2"><span style="text-decoration: none"><a target="_blank" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/AdvanceReservation.pdf"><span style="text-decoration: none">Can I&nbsp; Book a ticket From Nagpur To Bangalore at Howrah</span></a></span></a><a target="_blank" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/AdvanceReservation.pdfp"><span style="text-decoration: none">?</span></a></font></p></li>
																													<li>
																													<p align="justify"><font face="Tahoma" size="2"><a target="_blank" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/AdvanceReservation.pdf"><span style="text-decoration: none">How many days in advance can I reserve my ticket?</span></a></font></p></li>
																													<li>
																													<p align="justify"><font face="Tahoma" size="2"><a target="_blank" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/AdvanceReservation.pdf"><span style="text-decoration: none">How do I know the reservation status <br>
																													of my ticket?</span></a></font></p></li>
																												</ul>
																												<p align="justify"><font color="#0000FF">
									<img src="/criscm/img/image/arr1.jpg" height="12" width="12"></font><font color="#0000FF" face="Tahoma" size="2"> </font><a class="left_links" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/Boarding_Train.pdf" target="_blank"><font face="Tahoma" size="2" color="#0090FF"><span style="text-decoration: none">Boarding the Train</span></font></a><font face="Tahoma" color="#0090FF"> </font></p>
																												<ul>
																													<li>
																													<p align="justify"><font face="Tahoma" size="2"><a target="_blank" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/AdvanceReservation.pdf"><span style="text-decoration: none">Can I board the train with an RAC?</span></a></font></p></li>
																													<li>
																													<p align="justify"><font face="Tahoma" size="2"><a target="_blank" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/AdvanceReservation.pdf"><span style="text-decoration: none">Can I board the reserved coach with Waitlisted Ticket?</span></a></font></p></li>
																													<li>
																													<p align="justify"><font face="Tahoma" size="2"><a target="_blank" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/AdvanceReservation.pdf"><span style="text-decoration: none">How can I board a train in the reserved coach if I am not able to get a waitlisted ticket?</span></a></font></p></li>
																												</ul>
																												<p align="justify"><font color="#0000FF">
									<img src="/criscm/img/image/arr1.jpg" height="12" width="12"></font><font color="#0000FF" face="Tahoma" size="2"> </font><a class="left_links" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/Rly_Trn_Enq.pdf" target="_blank"><font face="Tahoma" size="2" color="#0090FF"><span style="text-decoration: none">Railway Train Enquiry</span></font></a><font face="Tahoma" color="#0090FF"> </font></p>
																												<ul>
																													<li>
																													<p align="justify"><font face="Tahoma" size="2" color="#0000FF"><a target="_blank" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/Rly_Trn_Enq.pdf"><span style="text-decoration: none">How do I find out the current status of my unconfirmed ticket?</span></a></font></p></li>
																												</ul>
																												<p align="justify"><font color="#0000FF">
									<img src="/criscm/img/image/arr1.jpg" height="12" width="12"></font><font color="#0000FF" face="Tahoma" size="2"> </font><a class="left_links" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/Tatkal.pdf" target="_blank"><font face="Tahoma" size="2" color="#0090FF"><span style="text-decoration: none">Tatkal Reservation</span></font></a></p>
																												<ul>
																													<li>
																													<p align="justify"><font face="Tahoma" size="2"><a target="_blank" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/Tatkal.pdf"><span style="text-decoration: none">Am I required to carry proof of identity during journey in Reserved Classes?</span></a></font></p></li>
																												</ul>
																												<p align="justify"><font color="#0000FF">
									<img src="/criscm/img/image/arr1.jpg" height="12" width="12"></font><font color="#0000FF" face="Tahoma" size="2"> </font><a class="left_links" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/Upgradation.pdf" target="_blank"><font face="Tahoma" size="2" color="#0090FF"><span style="text-decoration: none">Upgradation</span></font></a><font face="Tahoma" color="#0090FF"> </font></p>
																												<ul>
																													<li>
																													<p align="justify"><font face="Tahoma" color="#0000FF" size="2"><a target="_blank" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/Tatkal.pdf"><span style="text-decoration: none">Up-gradation Scheme</span></a></font></p></li>
																												</ul>
																												<p align="justify"><font color="#0000FF">
									<img src="/criscm/img/image/arr1.jpg" height="12" width="12"></font><font color="#0000FF" face="Tahoma" size="2"> </font><a class="left_links" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/SpecialFacilities.pdf" target="_blank"><font face="Tahoma" size="2" color="#0090FF"><span style="text-decoration: none">Special Facilities for Foreigners</span></font></a><font face="Tahoma" color="#0090FF"> </font></p>
																												<ul>
																													<li>
																													<p align="justify"><font face="Tahoma" color="#0000FF" size="2"><a target="_blank" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/Upgradation.pdf"><span style="text-decoration: none">Are there any special facilities provided by Indian Railways for foreign tourists?</span></a></font></p></li>
																												</ul>
																												<p align="justify"><font color="#0000FF">
									<img src="/criscm/img/image/arr1.jpg" height="12" width="12"></font><font color="#0000FF" face="Tahoma" size="2"> </font><a class="left_links" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/Change_TravelPlan.pdf" target="_blank"><font face="Tahoma" size="2" color="#0090FF"><span style="text-decoration: none">Change in travel plan</span></font></a><font face="Tahoma" color="#0090FF"> </font></p>
																												<ul>
																													<li>
																													<p align="justify"><font face="Tahoma" color="#0000FF" size="2"><a target="_blank" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/Change_TravelPlan.pdf"><span style="text-decoration: none">Can I prepone or postpone my reservation?</span></a></font></p></li>
																													<li>
																													<p align="justify"><font face="Tahoma" size="2"><a target="_blank" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/Change_TravelPlan.pdf"><span style="text-decoration: none">Can I change the boarding station?</span></a></font></p></li>
																													<li>
																													<p align="justify"><font face="Tahoma" size="2"><a target="_blank" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/Change_TravelPlan.pdf"><span style="text-decoration: none">Can I break my Journey at any Intermediate station?</span></a></font></p></li>
																													<li>
																													<p align="justify"><font face="Tahoma" size="2"><a target="_blank" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/Change_TravelPlan.pdf"><span style="text-decoration: none">Can I extend my Journey beyond the original destination?</span></a></font></p></li>
																													<li>
																													<p align="justify"><font face="Tahoma" size="2"><a target="_blank" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/Change_TravelPlan.pdf"><span style="text-decoration: none">Can my confirmed ticket be transferred to somebody else?</span></a></font></p></li>
																												</ul>
																												<p align="justify"><font color="#0000FF">
									<img src="/criscm/img/image/arr1.jpg" height="12" width="12"></font><font color="#0000FF" face="Tahoma" size="2"> </font><a class="left_links" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/Circular_Journey_Tickets.pdf" target="_blank"><font face="Tahoma" size="2" color="#0090FF"><span style="text-decoration: none">Circular Journey Tickets</span></font></a><font face="Tahoma" color="#0090FF"> </font></p>
																												<ul>
																													<li>
																													<p align="justify"><font face="Tahoma" color="#0000FF" size="2"><a target="_blank" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/Change_TravelPlan.pdf"><span style="text-decoration: none">What are the circular Journey Tickets?</span></a></font></p></li>
																													<li>
																													<p align="justify"><font face="Tahoma" color="#0000FF" size="2"><a target="_blank" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/Change_TravelPlan.pdf"><span style="text-decoration: none">What are the advantage of booking Circular journey ticket?</span></a></font></p></li>
																													<li>
																													<p align="justify"><font face="Tahoma" color="#0000FF" size="2"><a target="_blank" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/Change_TravelPlan.pdf"><span style="text-decoration: none">What is the booking procedure for circular Journey ticket?</span></a></font></p></li>
																												</ul>
																												<p align="justify">&nbsp;</p>
																												<p align="justify"><font color="#0000FF">
									<img src="/criscm/img/image/arr1.jpg" height="12" width="12"></font><font color="#0000FF" face="Tahoma" size="2"> </font><a class="left_links" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/ReservingSpecialCarriage.pdf" target="_blank"><font face="Tahoma" size="2" color="#0090FF"><span style="text-decoration: none">Reserving Special Carriages</span></font></a><font face="Tahoma" color="#0090FF"> </font></p>
																												<ul>
																													<li>
																													<p align="justify"><font face="Tahoma" color="#0000FF" size="2"><a target="_blank" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/Change_TravelPlan.pdf"><span style="text-decoration: none">Tell me about he tourist car and Reserverd carriages</span></a></font></p></li>
																													<li>
																													<p align="justify"><font face="Tahoma" color="#0000FF" size="2"><a target="_blank" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/Change_TravelPlan.pdf"><span style="text-decoration: none">How do I book tourist Car/Reserved Carriages?</span></a></font></p></li>
																													<li>
																													<p align="justify"><font face="Tahoma" color="#0000FF" size="2"><a target="_blank" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/Change_TravelPlan.pdf"><span style="text-decoration: none">How much does it cost to book a Tourist Car/Reserved Carriage?</span></a></font></p></li>
																												</ul>
																												<p align="left"><font color="#0000FF"><img src="/criscm/img/image/arr1.jpg" height="12" width="12"></font><font color="#0000FF" face="Tahoma" size="2"> </font><a class="left_links" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/VigilanceOrgn.pdf" target="_blank"><font face="Tahoma" size="2" color="#0090FF"><span style="text-decoration: none">Vigilance Organisation</span></font></a></p>
																												<ul>
																													<li>
																													<p align="left"><font face="Tahoma" size="2"><a target="_blank" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/VigilanceOrgn.pdf"><span style="text-decoration: none">What is the role of Vigilance in Railways?</span></a></font></p></li>
																												</ul>
																												<p align="left"><font color="#0000FF">
									<img src="/criscm/img/image/arr1.jpg" height="12" width="12"></font><font color="#0000FF" face="Tahoma" size="2"> </font><a class="left_links" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/ExplanatoryNotes.pdf" target="_blank"><font face="Tahoma" size="2" color="#0090FF"><span style="text-decoration: none">Explanatory Notes for Fares</span></font></a><font face="Tahoma" color="#0090FF"> </font></p>
																												<ul>
																													<li>
																													<p align="left"><font face="Tahoma" color="#0000FF" size="2"><a target="_blank" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/ExplanatoryNotes.pdf"><span style="text-decoration: none">Explanatory Notes</span></a></font></p></li>
																												</ul>
																												<p align="left"><font color="#0000FF">
									<img src="/criscm/img/image/arr1.jpg" height="12" width="12"></font><font color="#0000FF" face="Tahoma" size="2"> </font><a class="left_links" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/RPF.pdf" target="_blank"><font face="Tahoma" size="2" color="#0090FF"><span style="text-decoration: none">Railway Protection Force</span></font></a><ul>
																													<li>
																													<p align="left"><font face="Tahoma" size="2"><a target="_blank" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/RPF.pdf"><span style="text-decoration: none">What is the role of Railway Protection Force in the Railways?</span></a></font></li>
																													<li>
																													<p align="left"><font face="Tahoma" size="2"><a href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/RPF.pdf"><span style="text-decoration: none">Is security related assistance available to ladies on trains?</span></a></font></li>
																												</ul>
																												<p align="justify">&nbsp;</p>
																												<p align="justify">&nbsp;</p>
																												<p align="left"><font face="Tahoma" color="#0000FF"> &nbsp;</font></td>
																												
																												<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
																												
																											<td valign="top" width="52%" align="center">
																												<p align="left"><font color="#0000FF">
									<img src="/criscm/img/image/arr1.jpg" height="12" width="12"></font><font color="#0000FF" face="Tahoma" size="2"> </font><a class="left_links" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/Booking_Luggage.pdf" target="_blank"><font face="Tahoma" size="2" color="#0090FF"><span style="text-decoration: none">Booking Luggage </span></font></a></p>
																												<ul>
																													<li>
																													<p align="left"><font face="Tahoma" size="2"><a target="_blank" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/Booking_Luggage.pdf"><span style="text-decoration: none">How much luggage am I allowed to carry?</span></a></font></p></li>
																													<li>
																													<p align="left"><font face="Tahoma" size="2"><a target="_blank" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/Booking_Luggage.pdf"><span style="text-decoration: none">What if my luggage exceeds the free allowance?</span></a></font></p></li>
																													<li>
																													<p align="left"><font face="Tahoma" size="2"><a target="_blank" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/Booking_Luggage.pdf"><span style="text-decoration: none">What if my luggage stolen in route?</span></a></font></p></li>
																													<li>
																													<p align="left"><font face="Tahoma" size="2"><a target="_blank" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/Booking_Luggage.pdf"><span style="text-decoration: none">If my booked luggage is lost are damaged, how much compensation will I get?</span></a></font></p></li>
																												</ul>
																												<p align="left"><font color="#0000FF">
									<img src="/criscm/img/image/arr1.jpg" height="12" width="12"></font><font color="#0000FF" face="Tahoma" size="2"> </font><a class="left_links" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/AdvanceReservation_Internet.pdf" target="_blank"><font face="Tahoma" size="2" color="#0090FF"><span style="text-decoration: none">Advance Reservation Through Internet</span></font></a><font face="Tahoma" color="#0090FF"> </font></p>
																												<ul>
																													<li>
																													<p align="left"><font face="Tahoma" color="#0000FF"><a target="_blank" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/AdvanceReservation_Internet.pdf"><font size="2"><span style="text-decoration: none">Advance Reservation</span></font></a> </font></p></li>
																												</ul>
																												<p align="left"><font color="#0000FF">
									<img src="/criscm/img/image/arr1.jpg" height="12" width="12"></font><font color="#0000FF" face="Tahoma" size="2"> </font><a class="left_links" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/RefundRules.pdf" target="_blank"><font face="Tahoma" size="2" color="#0090FF"><span style="text-decoration: none">Refund Rules</span></font></a><font face="Tahoma" color="#0090FF"> </font></p>
																												<ul>
																													<li>
																													<p align="left"><a href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/RefundRules.pdf"><span style="background-position: 0 0">C</span></a><font face="Tahoma" color="#0000FF" size="2"><a target="_blank" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/RefundRules.pdf"><span style="text-decoration: none">an I get refunds across the counter?</span></a></font></p></li>
																													<li>
																													<p align="left"><a href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/RefundRules.pdf"><span style="background-position: 0 0">W</span></a><font face="Tahoma" color="#0000FF" size="2"><a target="_blank" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/RefundRules.pdf"><span style="text-decoration: none">hat are the prescribed limit for refund?</span></a></font></p></li>
																													<li>
																													<p align="left"><font face="Tahoma" color="#0000FF" size="2"><a target="_blank" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/RefundRules.pdf"><span style="text-decoration: none">Suppose I am unable to approach the concerned authorities, what do I do?</span></a></font></p></li>
																													<li>
																													<p align="left"><font face="Tahoma" color="#0000FF" size="2"><a target="_blank" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/RefundRules.pdf"><span style="text-decoration: none">Will the entire amount be refunded or will I lose some money?</span></a></font></p></li>
																													<li>
																													<p align="left"><font face="Tahoma" color="#0000FF" size="2"><a target="_blank" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/RefundRules.pdf"><span style="text-decoration: none">How much will be deducted as cancellation charges?</span></a></font></p></li>
																													<li>
																													<p align="left"><a href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/RefundRules.pdf"><span style="background-position: 0 0">C</span></a><font face="Tahoma" size="2"><a target="_blank" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/RefundRules.pdf"><span style="text-decoration: none">an refund be claimed if&nbsp; AC fails During the journey?</span></a></font></p></li>
																													<li>
																													<p align="left"><font face="Tahoma" size="2"><a target="_blank" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/RefundRules.pdf"><span style="text-decoration: none">Can I claim if I had to travel in lower class, even though I had a higher class ticket?</span></a></font></p></li>
																													<li>
																													<p align="left"><font face="Tahoma" size="2"><a target="_blank" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/RefundRules.pdf"><span style="text-decoration: none">I had a reserved ticket but I was not provided accommodation. Can I ask for full refund?</span></a></font></p></li>
																													<li>
																													<p align="left"><font face="Tahoma" size="2"><a target="_blank" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/RefundRules.pdf"><span style="text-decoration: none">Will I be able to get a refund if I do not wish to travel due to late running of trains?</span></a></font></p></li>
																													<li>
																													<p align="left"><font face="Tahoma" size="2"><a target="_blank" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/RefundRules.pdf"><span style="text-decoration: none">I could not continue my journey due to late running of connecting train. Can I claim a refund?</span></a></font></p></li>
																													<li>
																													<p align="left"><font face="Tahoma" size="2"><a target="_blank" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/RefundRules.pdf"><span style="text-decoration: none">Can I claim full refund if my train is cancelled?</span></a></font></p></li>
																													<li>
																													<p align="left"><font face="Tahoma" size="2"><a target="_blank" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/RefundRules.pdf"><span style="text-decoration: none">What about refund for a ticket where some passengers are Confirmed and others are Waitlisted?</span></a></font></p></li>
																													<li>
																													<p align="left"><font face="Tahoma" size="2"><a target="_blank" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/RefundRules.pdf"><span style="text-decoration: none">Can I get a refund on a ticket that I have lost?</span></a></font></p></li>
																													<li>
																													<p align="left"><font face="Tahoma" size="2"><a target="_blank" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/RefundRules.pdf"><span style="text-decoration: none">&nbsp;How much will it cost me to get a duplicate ticket?</span></a></font></p></li>
																													<li>
																													<p align="left"><font face="Tahoma" size="2"><a target="_blank" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/RefundRules.pdf"><span style="text-decoration: none">What is clerkage charge and when is it levied?</span></a></font></p></li>
																													<li>
																													<p align="left"><font face="Tahoma" size="2"><a target="_blank" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/RefundRules.pdf"><span style="text-decoration: none">How much refund is given on surrendering unused unreserved ticket?</span></a></font></p></li>
																													<li>
																													<p align="left"><font face="Tahoma" size="2"><a target="_blank" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/RefundRules.pdf"><span style="text-decoration: none">Are the duplicate ticket charges refundable if I find the ticket?</span></a></font></p></li>
																													<li>
																													<p align="left"><font face="Tahoma" size="2"><a target="_blank" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/RefundRules.pdf"><span style="text-decoration: none">My little son tore my ticket. Can I get a duplicate ticket</span></a></font></p></li>
																												</ul>
																												<p align="left"><font color="#0000FF">
									<img src="/criscm/img/image/arr1.jpg" height="12" width="12"></font><font color="#0000FF" face="Tahoma" size="2"> </font><a class="left_links" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/RailTravelConcession.pdf" target="_blank"><font face="Tahoma" size="2" color="#0090FF"><span style="text-decoration: none">Rail Travel Concessions</span></font></a><font face="Tahoma" color="#0090FF"> </font></p>
																												<ul>
																													<li>
																													<p align="left"><font face="Tahoma" size="2"><a target="_blank" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/RailTravelConcession.pdf"><span style="text-decoration: none">What rail travel concessions are available?</span></a></font></p></li>
																												</ul>
																												<p align="left"><font color="#0000FF">
									<img src="/criscm/img/image/arr1.jpg" height="12" width="12"></font><font color="#0000FF" face="Tahoma" size="2"> </font><a class="left_links" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/PassengerAmenities.pdf" target="_blank"><font face="Tahoma" size="2" color="#0090FF"><span style="text-decoration: none">Passenger Amenities</span></font></a><font face="Tahoma" color="#0090FF"> </font></p>
																												<ul>
																													<li>
																													<p align="left"><font face="Tahoma" color="#0000FF" size="2"><a target="_blank" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/PassengerAmenities.pdf"><span style="text-decoration: none">Are bedroll given free to the passenger traveling in AC Coaches?</span></a></font></p></li>
																													<li>
																													<p align="left"><font face="Tahoma" color="#0000FF" size="2"><a target="_blank" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/PassengerAmenities.pdf"><span style="text-decoration: none">Do you provide catering&nbsp; facilities in all the train?</span></a></font></p></li>
																													<li>
																													<p align="left"><font face="Tahoma" color="#0000FF" size="2"><a target="_blank" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/PassengerAmenities.pdf"><span style="text-decoration: none">How do I avail of the catering facilities on board?</span></a></font></p></li>
																													<li>
																													<p align="left"><font face="Tahoma" color="#0000FF" size="2"><a target="_blank" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/PassengerAmenities.pdf"><span style="text-decoration: none">what about catering facilities at stations?</span></a></font></p></li>
																													<li>
																													<p align="left"><font face="Tahoma" color="#0000FF" size="2"><a target="_blank" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/PassengerAmenities.pdf"><span style="text-decoration: none">How can I be sure of the quality of&nbsp; catering services on trains and at stations?</span></a></font></p></li>
																													<li>
																													<p align="left"><font face="Tahoma" color="#0000FF" size="2"><a target="_blank" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/PassengerAmenities.pdf"><span style="text-decoration: none">Is medical assistance available on the trains?</span></a></font></p></li>
																													<li>
																													<p align="left"><a href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/PassengerAmenities.pdf"><span style="background-position: 0 0">I</span></a><font face="Tahoma" color="#0000FF" size="2"><a target="_blank" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/PassengerAmenities.pdf"><span style="text-decoration: none">f I am breaking my journey or waiting for a connecting train, can I keep my luggage in the clock room?</span></a></font></p></li>
																													<li>
																													<p align="left"><font face="Tahoma" color="#0000FF" size="2"><a target="_blank" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/PassengerAmenities.pdf"><span style="text-decoration: none">Do I need to pay&nbsp; to use the waiting room at a station?</span></a></font></p></li>
																													<li>
																													<p align="left"><font face="Tahoma" color="#0000FF" size="2"><a target="_blank" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/PassengerAmenities.pdf"><span style="text-decoration: none">Is there any transit accommodation available at stations?</span></a></font></p></li>
																													<li>
																													<p align="left"><font face="Tahoma" color="#0000FF" size="2"><a target="_blank" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/PassengerAmenities.pdf"><span style="text-decoration: none">What are the other amenities available to passengers?</span></a></font></p></li>
																													<li>
																													<p align="left"><font face="Tahoma" color="#0000FF" size="2"><a target="_blank" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/PassengerAmenities.pdf"><span style="text-decoration: none">Whom do I contact if I have any complaints?</span></a></font></p></li>
																													<li>
																													<p align="left"><font face="Tahoma" color="#0000FF" size="2"><a target="_blank" href="http://www.indianrailways.gov.in/railwayboard/uploads/directorate/coaching/TAG_2014-15/PassengerAmenities.pdf"><span style="text-decoration: none">If I have any suggestions or complaints about the catering facilities or other passenger amenities, whom do I get in <br>
																													touch with ?</span></a></font></p></li>
																												</ul>
																												<p align="left"><font color="#0000FF">
									<img src="/criscm/img/image/arr1.jpg" height="12" width="12">&nbsp;&nbsp;&nbsp; </font><font color="#0090FF">
																												Others</font></td>
																										
																										</tr>
																									</tbody>
																								</table>
					</div>
						
						
						
						
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</div>
	
	
</body>
</html>