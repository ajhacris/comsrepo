<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
   <style>
  .navbar-inverse{
  	background-color:#ac0202;
  }
  body
{
font-family: 'Arial Narrow',Arial, sans-serif;
font-size:13px!important;
}
  </style>
</head>
<body>
<div class="container-fluid" style="padding:0px;">
   
<div class="row" style="background-color:#FFFFFF;padding-top:10px;padding-bottom:20px;">
<div class="col-md-1" align="center" >
<a href="">
</a>
<img class="img-responsive" alt="National Emblem of India" src="img/logo.png" style="height:100px">
</div>
<div class="col-md-1">
<img class="img-responsive" alt="Mahatma Gandhi" src="img/Mahatma.PNG" align="right" >
</div>

<div class="col-md-8"><center>
<font style="font-size:40px;text-align:center;">
SUGGESTIONS AND COMPLAINTS<div style="margin-top: -24px;"> PORTAL</br>
</font></center>
</div>


            
<div class="col-md-2"  align="center">

<img class="img-responsive" alt="Cris" src="img/indianrail_cris_logo.jpg" height=100px;>
</div>
</div>

 </div>
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="index.jsp"><img src="img/home.png" style="height:30px;"></a>
    </div>
    
    <ul class="nav navbar-nav navbar-right">
    
      <%
     			if(session.getAttribute("user_name")==null){
     		%>
      
      <li><a href="signin.jsp" style="color: #f5f5f5;font-size: 22px;"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
      <%}else{ %>
       <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#" style="color: #f5f5f5;font-size: 22px;">Hi <%= session.getAttribute("name").toString()%> <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="usercomplaintHistory.jsp" style="color: #333;font-size: 22px;">Previous Complaint History </a></li>
          <li><a href="changePassword.jsp" style="color: #333;font-size: 22px;">Change Password</a></li>
          <li><a href="Logout.jsp" style="color: #333;font-size: 22px;">Logout</a></li>
        </ul>      
      </li>
     
      <%} %>
    </ul>
  </div>
</nav>
 


</body>
</html>
