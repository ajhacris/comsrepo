<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ page import="java.util.*" %>
<%@ page import="com.google.gson.Gson"%>
<%@ page import="com.google.gson.JsonObject"%>

<%@page import ="utility.DbConnection" import="java.sql.*" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="css/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css"/>
    <link href="css/cssdash/sb-admin.css" rel="stylesheet"/>
<style>

.w3-example {
    background-color: white;
    padding: 0.01em 16px;
    margin: 20px -10px;
    box-shadow: 0 2px 4px 0 rgba(0,0,0,0.16),0 2px 10px 0 rgba(0,0,0,0.12)!important;
}

</style>
</head>
<body id="page-top">

    <%@ include file="top.jsp" %>


    <div  id="content-wrapper">
    
     
     <%@ include file="sidebar.jsp" %>

     
<%
     	if(session.getAttribute("login_id")==null){
     	
     	System.out.println("Session Value: "+session.getAttribute("login_id"));
     	System.out.println("******************Session Expired **********************");
         response.sendRedirect("AdminLogin.jsp");
     }
     int pendingcomplaint=0;
     int sla2BreachCount=0;
     int sla3BreachCount=0;
     int closedcomplaint=0;
     int receivedComplaint=0;
     int weekCompliantCountTrain=0;
     int monthCompliantCountTrain=0;
     int yearCompliantCountTrain=0;
     int weekCompliantCountStation=0;
     int monthCompliantCountStation=0;
     int yearCompliantCountStation=0;
     int weekCompliantCountTrainClosed=0;
     int monthCompliantCountTrainClosed=0;
     int yearCompliantCountTrainClosed=0;
     int weekCompliantCountStationClosed=0;
     int monthCompliantCountStationClosed=0;
     int yearCompliantCountStationClosed=0;
     int Helpline138=0;
     int Helpline182=0;
     int SocialMedia=0;
     int ManualDak=0;
     int CateringComplaints=0;
     int Helpline139=0;
     int SMS=0;
     int Web=0;
     int ratingExcellent =0;
     int ratingGood=0;
     int ratingNeutral=0;
     int ratingBad=0;
     int ratingWorst=0;
     String condition="";
     String condition1="";
     String sla_breach_2_condition="";
     String sla_breach_3_condition="";
     String compliant_count_closed=null;
     String compliant_count=null;
     String channelwise_count=null;
     String rating=null;


     String groupid=session.getAttribute("group_id").toString();
     String zone_code=session.getAttribute("zn_cd").toString();
     String div_code=session.getAttribute("div_cd").toString();
     String dept_code=session.getAttribute("dept_cd").toString();

     PreparedStatement stmt=null;
     ResultSet rs=null;
     DbConnection dbConnection=new DbConnection();
     Connection con=dbConnection.dbConnect();
     if(flag_sla == 3){ 
     	condition="zn_cd=? AND div_cd=?  AND STATUS !=?";
     	condition1="zn_cd=? AND div_cd=?  AND STATUS =?";
     	}
     if(flag_sla == 1 || flag_sla == 2){
     	condition="zn_cd=? AND div_cd=? AND STATUS !=?  AND dept_cd=? ";
     	condition1="zn_cd=? AND div_cd=? AND STATUS =?  AND dept_cd=? ";
     	
     }
     if(flag_sla == 4){
     	condition=" STATUS !=?   ";
     	condition1=" STATUS =?  ";
     	
     }



     String query="SELECT COUNT(*) FROM rly_complaint WHERE "+condition;
     String query_closed="SELECT COUNT(*) FROM rly_complaint WHERE "+condition1;


     if(flag_sla == 2){ 
     	sla_breach_2_condition="AND rly.zn_cd= ? AND rly.div_cd= ?  AND rly.dept_cd= ?";
     }

     String sla_breach_2="SELECT TIME_FORMAT(TIMEDIFF(NOW(), created_on),\"%H\"),sla2_time_hrs FROM rly_complaint AS rly "+
     "LEFT OUTER JOIN rly_heads_subheads_master AS comp ON rly.complaint_sub_type=comp.id, rly_users AS USER "+
     "WHERE rly.zn_cd=user.zn_cd AND rly.div_cd=user.div_cd AND rly.dept_cd=user.dept_cd AND"+
     "  STATUS!='Closed' AND user.flag_sla=2  "+sla_breach_2_condition;


     if(flag_sla == 3){ 
     	sla_breach_3_condition="AND rly.zn_cd= ? AND rly.div_cd= ?";
     }

     String sla_breach_3="SELECT TIME_FORMAT(TIMEDIFF(NOW(), created_on),\"%H\"),sla3_time_hrs "+
     " FROM rly_complaint AS rly LEFT OUTER JOIN rly_heads_subheads_master AS comp ON rly.complaint_sub_type=comp.id, rly_users AS USER "+
     " WHERE rly.zn_cd=user.zn_cd AND rly.div_cd=user.div_cd AND		STATUS!='Closed' AND user.flag_sla=3   "+sla_breach_3_condition;


     if(flag_sla == 1 || flag_sla == 2 ){ 
     compliant_count="SELECT COUNT(*) FROM rly_complaint WHERE TIME_FORMAT(TIMEDIFF(NOW(), created_on),\"%H\")<=? AND complaint_mode=?  AND zn_cd=? AND div_cd=? AND dept_cd=? ";
     channelwise_count="SELECT COUNT(*) FROM rly_complaint WHERE channel_type=? AND zn_cd=? AND div_cd=? AND dept_cd=?";
     rating="SELECT F.rating FROM rly_feedback AS F , rly_complaint AS rly WHERE rly.zn_cd=? AND rly.div_cd=? AND rly.dept_cd=? AND F.complaint_ref_no=rly.complaint_ref_no";
     compliant_count_closed="SELECT COUNT(*) FROM rly_complaint WHERE TIME_FORMAT(TIMEDIFF(NOW(), created_on),\"%H\")<=? AND complaint_mode=? AND STATUS ='Closed' AND zn_cd=? AND div_cd=? AND dept_cd=?";
     }
     if(flag_sla == 3){
     	compliant_count="SELECT COUNT(*) FROM rly_complaint WHERE TIME_FORMAT(TIMEDIFF(NOW(), created_on),\"%H\")<=? AND complaint_mode=?  AND zn_cd=? AND div_cd=? ";
     	channelwise_count="SELECT COUNT(*) FROM rly_complaint WHERE channel_type=? AND zn_cd=? AND div_cd=? ";
     	rating="SELECT F.rating FROM rly_feedback AS F , rly_complaint AS rly WHERE rly.zn_cd=? AND rly.div_cd=? AND  F.complaint_ref_no=rly.complaint_ref_no";
     	compliant_count_closed="SELECT COUNT(*) FROM rly_complaint WHERE TIME_FORMAT(TIMEDIFF(NOW(), created_on),\"%H\")<=? AND complaint_mode=? AND STATUS ='Closed' AND zn_cd=? AND div_cd=? ";
     }

     if(flag_sla == 4){
     	compliant_count="SELECT COUNT(*) FROM rly_complaint WHERE TIME_FORMAT(TIMEDIFF(NOW(), created_on),\"%H\")<=? AND complaint_mode=?  ";
     	channelwise_count="SELECT COUNT(*) FROM rly_complaint WHERE channel_type=?  ";
     	rating="SELECT F.rating FROM rly_feedback AS F , rly_complaint AS rly WHERE   F.complaint_ref_no=rly.complaint_ref_no";
     	compliant_count_closed="SELECT COUNT(*) FROM rly_complaint WHERE TIME_FORMAT(TIMEDIFF(NOW(), created_on),\"%H\")<=? AND complaint_mode=? AND STATUS ='Closed'  ";
     }





     stmt=con.prepareStatement(query);

     if(flag_sla == 1 || flag_sla == 2 || flag_sla == 3){
     stmt.setString(1,zone_code);
     stmt.setString(2,div_code);
     stmt.setString(3,"Closed");
     if(!session.getAttribute("flag_sla").toString().equals("3")){ 
     stmt.setString(4,dept_code);
     }
     }
     if(session.getAttribute("flag_sla").toString().equals("4")){
     	stmt.setString(1,"Closed");
     	
     }

     rs=stmt.executeQuery();  
     while(rs.next())  {
     	pendingcomplaint=rs.getInt(1);
     }

     stmt=con.prepareStatement(query_closed);
     if(flag_sla == 1 || flag_sla == 2 || flag_sla == 3){
     stmt.setString(1,zone_code);
     stmt.setString(2,div_code);
     stmt.setString(3,"Closed");
     if(!session.getAttribute("flag_sla").toString().equals("3")){ 
     stmt.setString(4,dept_code);
     }
     }
     if(session.getAttribute("flag_sla").toString().equals("4")){
     	stmt.setString(1,"Closed");
     	
     }

     rs=stmt.executeQuery();  
     while(rs.next())  {
     	closedcomplaint=rs.getInt(1);
     }
     //System.out.println("FLAG SLA VALUE : :"+session.getAttribute("flag_sla").toString());
     if( flag_sla == 2   ){
     stmt=con.prepareStatement(sla_breach_2);
     stmt.setString(1,zone_code);
     stmt.setString(2,div_code);
     stmt.setString(3,dept_code);
     rs=stmt.executeQuery();  
     while(rs.next())  {
     	if(rs.getInt(1)>=rs.getInt(2)){
     		sla2BreachCount++;
     	}}
     }

     if(flag_sla == 3  ){ 
     stmt=con.prepareStatement(sla_breach_3);
     stmt.setString(1,zone_code);
     stmt.setString(2,div_code);

     rs=stmt.executeQuery();  
     while(rs.next())  {
     	if(rs.getInt(1)>=rs.getInt(2)){
     		sla3BreachCount++;
     	}}
     }

     if(flag_sla == 4 ){ 
     stmt=con.prepareStatement(sla_breach_3);
     rs=stmt.executeQuery();  
     while(rs.next())  {
     	if(rs.getInt(1)>=rs.getInt(2)){
     		sla3BreachCount++;
     	}}
     stmt=con.prepareStatement(sla_breach_2);
     rs=stmt.executeQuery();  
     while(rs.next())  {
     	if(rs.getInt(1)>=rs.getInt(2)){
     		sla2BreachCount++;
     	}}

     }
     receivedComplaint=pendingcomplaint+closedcomplaint;

     /* DAY WISE COUNT OF COMPLAINTS START */
     stmt=con.prepareStatement(compliant_count);
     stmt.setInt(1,168);
     stmt.setString(2,"S");
     if(flag_sla == 1  || flag_sla == 2  || flag_sla == 3 ){
     stmt.setString(3,zone_code);
     stmt.setString(4,div_code);
     if(flag_sla != 3 ){ 
     	stmt.setString(5,dept_code);
     }}
     rs=stmt.executeQuery();  
     while(rs.next())  {
     weekCompliantCountStation=rs.getInt(1);
     }

     stmt=con.prepareStatement(compliant_count);
     stmt.setInt(1,720);
     stmt.setString(2,"S");
     if(flag_sla == 1  || flag_sla == 2  || flag_sla == 3 ){
     stmt.setString(3,zone_code);
     stmt.setString(4,div_code);
     if(flag_sla != 3 ){ 
     	stmt.setString(5,dept_code);
     }}
     rs=stmt.executeQuery();  
     while(rs.next())  {
     monthCompliantCountStation=rs.getInt(1);
     }

     stmt=con.prepareStatement(compliant_count);
     stmt.setInt(1,8760);
     stmt.setString(2,"S");
     if(flag_sla == 1  || flag_sla == 2  || flag_sla == 3 ){
     stmt.setString(3,zone_code);
     stmt.setString(4,div_code);
     if(flag_sla != 3 ){ 
     	stmt.setString(5,dept_code);
     }}
     rs=stmt.executeQuery();  
     while(rs.next())  {
     yearCompliantCountStation=rs.getInt(1);
     }

     stmt=con.prepareStatement(compliant_count);
     stmt.setInt(1,168);
     stmt.setString(2,"T");
     if(flag_sla == 1  || flag_sla == 2  || flag_sla == 3 ){
     stmt.setString(3,zone_code);
     stmt.setString(4,div_code);
     if(flag_sla != 3 ){ 
     	stmt.setString(5,dept_code);
     }}
     rs=stmt.executeQuery();  
     while(rs.next())  {
   //  	System.out.print(rs.getInt(1));
     weekCompliantCountTrain=rs.getInt(1);
     }

     stmt=con.prepareStatement(compliant_count);
     stmt.setInt(1,720);
     stmt.setString(2,"T");
     if(flag_sla == 1  || flag_sla == 2  || flag_sla == 3 ){
     stmt.setString(3,zone_code);
     stmt.setString(4,div_code);
     if(flag_sla != 3 ){ 
     	stmt.setString(5,dept_code);
     }}
     rs=stmt.executeQuery();  
     while(rs.next())  {
     monthCompliantCountTrain=rs.getInt(1);
     }

     stmt=con.prepareStatement(compliant_count);
     stmt.setInt(1,8760);
     stmt.setString(2,"T");
     if(flag_sla == 1  || flag_sla == 2  || flag_sla == 3 ){
     stmt.setString(3,zone_code);
     stmt.setString(4,div_code);
     if(flag_sla != 3 ){ 
     	stmt.setString(5,dept_code);
     }}
     rs=stmt.executeQuery();  
     while(rs.next())  {
     yearCompliantCountTrain=rs.getInt(1);
     }


     stmt=con.prepareStatement(compliant_count_closed);
     stmt.setInt(1,168);
     stmt.setString(2,"S");
     if(flag_sla == 1  || flag_sla == 2  || flag_sla == 3 ){
     stmt.setString(3,zone_code);
     stmt.setString(4,div_code);
     if(flag_sla != 3 ){ 
     	stmt.setString(5,dept_code);
     }}
     rs=stmt.executeQuery();  
     while(rs.next())  {
     weekCompliantCountStationClosed=rs.getInt(1);
     }

     stmt=con.prepareStatement(compliant_count_closed);
     stmt.setInt(1,720);
     stmt.setString(2,"S");
     if(flag_sla == 1  || flag_sla == 2  || flag_sla == 3 ){
     stmt.setString(3,zone_code);
     stmt.setString(4,div_code);
     if(flag_sla != 3 ){ 
     	stmt.setString(5,dept_code);
     }}
     rs=stmt.executeQuery();  
     while(rs.next())  {
     monthCompliantCountStationClosed=rs.getInt(1);
     }

     stmt=con.prepareStatement(compliant_count_closed);
     stmt.setInt(1,8760);
     stmt.setString(2,"S");
     if(flag_sla == 1  || flag_sla == 2  || flag_sla == 3 ){
     stmt.setString(3,zone_code);
     stmt.setString(4,div_code);
     if(flag_sla != 3 ){ 
     	stmt.setString(5,dept_code);
     }}
     rs=stmt.executeQuery();  
     while(rs.next())  {
     yearCompliantCountStationClosed=rs.getInt(1);
     }

     stmt=con.prepareStatement(compliant_count_closed);
     stmt.setInt(1,168);
     stmt.setString(2,"T");
     if(flag_sla == 1  || flag_sla == 2  || flag_sla == 3 ){
     stmt.setString(3,zone_code);
     stmt.setString(4,div_code);
     if(flag_sla != 3 ){ 
     	stmt.setString(5,dept_code);
     }}
     rs=stmt.executeQuery();  
     while(rs.next())  {
     weekCompliantCountTrainClosed=rs.getInt(1);
     }

     stmt=con.prepareStatement(compliant_count_closed);
     stmt.setInt(1,720);
     stmt.setString(2,"T");
     if(flag_sla == 1  || flag_sla == 2  || flag_sla == 3 ){
     stmt.setString(3,zone_code);
     stmt.setString(4,div_code);
     if(flag_sla != 3 ){ 
     	stmt.setString(5,dept_code);
     }}
     rs=stmt.executeQuery();  
     while(rs.next())  {
     monthCompliantCountTrainClosed=rs.getInt(1);
     }

     stmt=con.prepareStatement(compliant_count_closed);
     stmt.setInt(1,8760 );
     	stmt.setString(2, "T");
     	if (flag_sla == 1 || flag_sla == 2 || flag_sla == 3) {
     		stmt.setString(3, zone_code);
     		stmt.setString(4, div_code);
     		if (flag_sla != 3) {
     			stmt.setString(5, dept_code);
     		}
     	}
     	rs = stmt.executeQuery();
     	while (rs.next()) {
     		yearCompliantCountTrainClosed = rs.getInt(1);
     	}
     	/* DAY WISE COUNT OF COMPLAINTS END */

     	/* CHANNEL WISE COUNT START */
     	stmt = con.prepareStatement(channelwise_count);
     	stmt.setString(1, "L");
     	if (flag_sla == 1 || flag_sla == 2 || flag_sla == 3) {
     		stmt.setString(2, zone_code);
     		stmt.setString(3, div_code);
     		if (flag_sla != 3) {
     			stmt.setString(4, dept_code);
     		}
     	}
     	rs = stmt.executeQuery();
     	while (rs.next()) {
     		Helpline138 = rs.getInt(1);
     	}

     	stmt = con.prepareStatement(channelwise_count);
     	stmt.setString(1, "P");
     	if (flag_sla == 1 || flag_sla == 2 || flag_sla == 3) {
     		stmt.setString(2, zone_code);
     		stmt.setString(3, div_code);
     		if (flag_sla != 3) {
     			stmt.setString(4, dept_code);
     		}
     	}
     	rs = stmt.executeQuery();
     	while (rs.next()) {
     		Helpline182 = rs.getInt(1);
     	}

     	stmt = con.prepareStatement(channelwise_count);
     	stmt.setString(1, "M");
     	if (flag_sla == 1 || flag_sla == 2 || flag_sla == 3) {
     		stmt.setString(2, zone_code);
     		stmt.setString(3, div_code);
     		if (flag_sla != 3) {
     			stmt.setString(4, dept_code);
     		}
     	}
     	rs = stmt.executeQuery();
     	while (rs.next()) {
     		SocialMedia = rs.getInt(1);
     	}

     	stmt = con.prepareStatement(channelwise_count);
     	stmt.setString(1, "D");
     	if (flag_sla == 1 || flag_sla == 2 || flag_sla == 3) {
     		stmt.setString(2, zone_code);
     		stmt.setString(3, div_code);
     		if (flag_sla != 3) {
     			stmt.setString(4, dept_code);
     		}
     	}
     	rs = stmt.executeQuery();
     	while (rs.next()) {
     		ManualDak = rs.getInt(1);
     	}

     	stmt = con.prepareStatement(channelwise_count);
     	stmt.setString(1, "C");
     	if (flag_sla == 1 || flag_sla == 2 || flag_sla == 3) {
     		stmt.setString(2, zone_code);
     		stmt.setString(3, div_code);
     		if (flag_sla != 3) {
     			stmt.setString(4, dept_code);
     		}
     	}
     	rs = stmt.executeQuery();
     	while (rs.next()) {
     		CateringComplaints = rs.getInt(1);
     	}

     	stmt = con.prepareStatement(channelwise_count);
     	stmt.setString(1, "H");
     	if (flag_sla == 1 || flag_sla == 2 || flag_sla == 3) {
     		stmt.setString(2, zone_code);
     		stmt.setString(3, div_code);
     		if (flag_sla != 3) {
     			stmt.setString(4, dept_code);
     		}
     	}
     	rs = stmt.executeQuery();
     	while (rs.next()) {
     		Helpline139 = rs.getInt(1);
     	}

     	stmt = con.prepareStatement(channelwise_count);
     	stmt.setString(1, "S");
     	if (flag_sla == 1 || flag_sla == 2 || flag_sla == 3) {
     		stmt.setString(2, zone_code);
     		stmt.setString(3, div_code);
     		if (flag_sla != 3) {
     			stmt.setString(4, dept_code);
     		}
     	}
     	rs = stmt.executeQuery();
     	while (rs.next()) {
     		SMS = rs.getInt(1);
     	}
     	stmt = con.prepareStatement(channelwise_count);
     	stmt.setString(1, "W");
     	if (flag_sla == 1 || flag_sla == 2 || flag_sla == 3) {
     		stmt.setString(2, zone_code);
     		stmt.setString(3, div_code);
     		if (flag_sla != 3) {
     			stmt.setString(4, dept_code);
     		}
     	}
     	rs = stmt.executeQuery();
     	while (rs.next()) {
     		Web = rs.getInt(1);
     	}

     	/* CHANNEL WISE COUNT END */

     	/* RATING COUNT */
     	stmt = con.prepareStatement(rating);
     	if (flag_sla == 1 || flag_sla == 2 || flag_sla == 3) {
     		stmt.setString(1, zone_code);
     		stmt.setString(2, div_code);
     		if (!session.getAttribute("flag_sla").toString().equals("3")) {
     			stmt.setString(3, dept_code);
     		}
     	}
     	rs = stmt.executeQuery();
     	while (rs.next()) {
     	//	System.out.println(rs.getFloat(1));
     		if (rs.getFloat(1) <= 1) {
     			ratingWorst++;
     		}
     		if (rs.getFloat(1) > 1 && rs.getFloat(1) <= 2) {
     			ratingBad++;
     		}
     		if (rs.getFloat(1) > 2 && rs.getFloat(1) <= 3) {
     			ratingNeutral++;
     		}
     		if (rs.getFloat(1) > 3 && rs.getFloat(1) <= 4) {
     			ratingGood++;
     		}
     		if (rs.getFloat(1) > 4 && rs.getFloat(1) <= 5) {
     			ratingExcellent++;
     		}
     	}
     %>


<%if( flag_sla ==5 ){}else{  %>
          <!-- Icon Cards-->
        <div class="row" style="padding-top:10px;padding-left:20px; min-height: 100%;     min-width: 80%;">
             <div class="col-sm-3">
              <div class="card text-white bg-primary o-hidden ">
                <div class="card-body">
                  <div class="card-body-icon">
                    <i class="fas fa-fw fa-comments"></i>
                  </div>
                  <div class="mr-5"><a href ="ComplaintRecieved.jsp" style="color:white;"><%=receivedComplaint %> Received </a></div>
                </div>
               
              </div>
            </div>
            
        
            <div class="col-sm-3">
              <div class="card text-white bg-danger o-hidden ">
                <div class="card-body">
                  <div class="card-body-icon">
                    <i class="fas fa-fw fa-life-ring"></i>
                  </div>
                  <div class="mr-5"> <a href="ComplaintClosed.jsp" style="color:white;"><%=closedcomplaint %> Closed Complaint </a></div> 
                  <!-- <div class="mr-5">26 New Messages!</div> -->
                </div>
                
              </div>
            </div>
           
            <div class="col-sm-3">
              <div class="card text-white bg-warning o-hidden ">
                <div class="card-body">
                  <div class="card-body-icon">
                    <i class="fas fa-fw fa-list"></i>
                  </div>
                  <div class="mr-5"><a href="ComplaintPending.jsp" style="color:white;"><%=pendingcomplaint %> Pending Complaint</a></div>
                  <!-- <div class="mr-5">26 New Messages!</div> -->
                </div>
                
              </div>
            </div>
            
            <div class="col-sm-3">
           
              <div class="card text-white bg-success o-hidden ">
                <div class="card-body">
                  <div class="card-body-icon">
                    <i class="fas fa-fw fa-shopping-cart"></i>
                  </div>
                  <div class="mr-5">
                  <%if (flag_sla== 2 || flag_sla ==3){ %>
                  <a href="ComplaintBreachSlaTime.jsp" style="color:white;">
                 <%} %>
                 <%if (flag_sla== 4){ %>
                 <a href="SlaBreachReport.jsp" style="color:white;">
                 <%} %>
                  <%if(sla2BreachCount !=0){ %>
                  <%=sla2BreachCount %> SLA 2 Breached 
                  <%} %>
                  <%if(sla3BreachCount !=0){ %><br>
                  <%=sla3BreachCount %> SLA 3 Breached 
                  <%} %>
                  
                  </a>
                  </div>
                  <!-- <div class="mr-5">26 New Messages!</div> -->
                </div>
                
              </div>
                       
             
            </div>
            
            
    
            
            
            <div class="col-sm-4">
              <div id="piechart"></div>
            </div> 
             <div class="col-sm-4">
              <div id="piechartchannel"></div>
            </div> 
            
            
             <div class="col-sm-4  ">
             
        	   <table class="table  w3-example" style=" font-size: 11px ;    width: 91%;">
				    <thead style="background-color: #e0a3a3 ">
				      <!-- <tr>
				      <th colspan="4"><center>Previous Data In:</center></th>
				      </tr> -->
				      <tr>
				        <th>Complaints Registered In Previous Days</th>
				        <th> 7 </th>
				        <th> 30 </th>
				        <th> 365 </th>
				      </tr>
				    </thead>
				    <tbody style="background-color:#5accd2">  
				     <tr class="info">
				        <td>In Train</td>
				        <td><%=weekCompliantCountTrain %></td>
				        <td><%=monthCompliantCountTrain %></td>
				        <td><%=yearCompliantCountTrain %></td>
				      </tr>
				       <tr class="info">
				        <td>On Station</td>
						<td><%=weekCompliantCountStation %></td>
				        <td><%=monthCompliantCountStation %></td>
				        <td><%=yearCompliantCountStation %></td>
				      </tr>
				       
				      </tbody>
				      
				      <thead style="background-color: #e0a3a3">
				      <!-- <tr>
				      <th colspan="4"><center>Previous Data In:</center></th>
				      </tr> -->
				      <tr>
				        <th>Complaints Closed In Previous Days</th>
				        <th> 7 </th>
				        <th> 30 </th>
				        <th> 365 </th>
				      </tr>
				    </thead>
				    <tbody style="background-color:#5accd2">  
				     <tr class="info">
				        <td>In Train</td>
				        <td><%=weekCompliantCountTrainClosed %></td>
				        <td><%=monthCompliantCountTrainClosed %></td>
				        <td><%=yearCompliantCountTrainClosed %></td>
				      </tr>
				       <tr class="info">
				        <td>On Station</td>
						<td><%=weekCompliantCountStationClosed %></td>
				        <td><%=monthCompliantCountStationClosed %></td>
				        <td><%=yearCompliantCountStationClosed %></td>
				      </tr>
				       
				      </tbody>
				      </table>
           
            </div>
            
        
            <div class="col-sm-3  ">
             <div class="w3-example">
             <img src="img/good.PNG" width="50px" height="50px"><b>Good</b> <br> <h6 style="text-align: right;">Rating <%=ratingExcellent %></h6>
            </div>
            </div>
             
             <div class="col-sm-3 ">
              <div class="w3-example">
             <img src="img/Satisfactory.PNG" width="50px" height="50px"><b>Satisfactory </b> <br> <h6 style="text-align: right;">Rating <%=ratingGood %></h6>
            </div>
            </div>
           
             <div class="col-sm-3 ">
             <div class="w3-example">
             <img src="img/bad.PNG" width="50px" height="50px"><b>Unsatisfactory</b> <br> <h6 style="text-align: right;">Rating <%=ratingNeutral %></h6>
            </div> 
            </div>
            <%--  <div class="col-sm-2 ">
              <div class="w3-example">
             <img src="img/bad.png" width="50px" height="50px"><b>Bad</b> <br> <h6 style="text-align: right;">Rating <%=ratingBad %></h6>
            </div></div>
            
             <div class="col-sm-2 ">
              <div class="w3-example">
             <img src="img/worst.png" width="50px" height="50px"><b>Worst</b> <br> <h6 style="text-align: right;">Rating <%=ratingWorst %></h6>
            </div></div>
            --%>
            
          </div>
          
          
          <%} %>
          
          
     
          
         </div>
         
         
        
         
      </div>
      
      
      <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script type="text/javascript">
// Load google charts
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);

// Draw the chart and set the chart values

 function drawChart() {
	  var data = google.visualization.arrayToDataTable([
	  ['Task', 'Hours per Day'],
	  ['Receive', <%=receivedComplaint %>],
	  ['Close', <%=closedcomplaint %>],
	  ['Pending', <%=pendingcomplaint %>],
	  
	]);
  
  // Optional; add a title and set the width and height of the chart
  var options = {'title':'', 'width':350, 'height':300};

  // Display the chart inside the <div> element with id="piechart"
  var chart = new google.visualization.PieChart(document.getElementById('piechart'));
  chart.draw(data, options);
}
</script>


<!-- CHANNEL WISE GOOGLE CHART -->
<script type="text/javascript">
// Load google charts
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);

// Draw the chart and set the chart values

 function drawChart() {
	  var data = google.visualization.arrayToDataTable([
	  ['Task', 'Hours per Day'],
	  ['Web', <%=Web %>],
	  ['SMS', <%=SMS %>],
	  ['Helpline 139', <%=Helpline139 %>],
	  ['Catering Complaints, Rly Board', <%=CateringComplaints %>],
	  ['Manual Dak', <%=ManualDak %>],
	  ['Social Media', <%=SocialMedia %>],
	  ['Helpline 182', <%=Helpline182 %>],
	  ['Helpline 138', <%=Helpline138 %>],
	  
	  
	]);
  
  // Optional; add a title and set the width and height of the chart
  var options = {'title':'', 'width':350, 'height':300};

  // Display the chart inside the <div> element with id="piechart"
  var chart = new google.visualization.PieChart(document.getElementById('piechartchannel'));
  chart.draw(data, options);
}
</script>

      

  
  
  
    
  <%@ include file="footer.jsp" %>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
      
    </a>
    
 </div>
   
 

    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="js/js/bootstrap.bundle.min.js"></script>

    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <script src="js/jsdash/sb-admin.min.js"></script>

  </body>

</html>
