$(document).ready(function (e){
	
	$('#register-submit').attr("disabled", true);
	
	$("#otp").keyup(function(){
		var otp= $('#otp').val();
		var otp_length=$('#otp').val().length;
		if(otp_length<4){
			$('#register-submit').attr("disabled", true);
		}
		else{
			if(otp!=""){
				$.ajax({
					 method: "post",
	    	          url: "CheckOtp.jsp",
	    	          
	    	         data:{
	    	    
	    	          otp:$('#otp').val()
	    	         },
	    	          
	    	         
	    	          success: function(data)
	    	          {
	    	          var msg="";
	    	          
	    	            if(data.trim()!="true"){
	                              alert("Wrong OTP");
	                              $('#register-submit').attr("disabled", true);
	    	            }
	    	              if(data.trim()=="true"){
	    	            	  $('#register-submit').removeAttr("disabled");
	    	           
	    	              }
	    	            
	    	          }
				});
			}
		}
		
	});
});