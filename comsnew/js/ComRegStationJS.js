$(document).ready(function (e){
	$('#incident_dt').datetimepicker({
		  locale: 'pt-br',
		  minDate:new Date((new Date().getTime() - (5 * (24 * 60 * 60 * 1000)))),
		  maxDate: moment()
		});
    	$('#pnr_desc').hide();
    	$('#otp_panel').hide();
    	$('#result_register').hide();
    	$.ajax({
			method : 'GET',
			url : 'CommonList',
			data : {
					flag : 'complaint1',
					complaint_place:'s'
				
				},
			success : function(responseText) {
		
		$("#selectComplaint").append(responseText);
		
				$.ajax({
	 				method : 'GET',
	 				url : 'CommonList',
	 				data : {
	 				flag : 'subcomplaint',
	 					parent_id : $('#complaint_type').val()
	 					
	 				
	 				},
	 				success : function(responseText) {
	 					$("#selectSubComplaint").empty() ;
	 			$("#selectSubComplaint").append(responseText);
	 			
	 				},
	 				 error: function (xhr, status, thrownError) {
	 			     }
	 			});
		
			},
			 error: function (xhr, status, thrownError) {
		     }
		});
    	
    	
    	
    	 $("#selectComplaint").on('change', '#complaint_type', function(){ 
 			$.ajax({
 				method : 'GET',
 				url : 'CommonList',
 				data : {
 				flag : 'subcomplaint',
 					parent_id : $('#complaint_type').val()
 					
 				
 				},
 				success : function(responseText) {
 					$("#selectSubComplaint").empty() ;
 			$("#selectSubComplaint").append(responseText);
 			
 				},
 				 error: function (xhr, status, thrownError) {
 			     }
 			});
 			});
    	 
    	//*****************************HAIDER 
    	 $.ajax({
				method : 'GET',
				url : 'CommonList',
				data : {
				flag : 'stationNameOnLoad',
				
				},
				success : function(responseText) {
					$("#stationName1").empty() ;
			$("#stationName1").append(responseText);
			
			
				},
				 error: function (xhr, status, thrownError) {
			     }
			});
    	//*****************************HAIDER
 		
    	
    	 $("#pnr_utsbtn").click(function(){
    		 
    		 $('#pnr_desc').hide();
    		 
    			$.ajax({
    				method : 'GET',
    				url : 'ComplaintRegInTrain',
    				data : {
    						
    						pnrNo:$('#pnr_uts').val()
    					
    					},
    				success : function(responseText) {
    			var pnrData=jQuery.parseJSON(responseText);
    			
    			if(pnrData.errMsg!=''){
    				$('#pnr_error').html(pnrData.errMsg);
    				$('#train_no').val("");
    				$('#from_stn').val("");
    				$('#to_stn').val("");
    				$('#board_stn').val("");
    				$('#berth_class').val("");
    				$('#total_pass').val("");
    				$('#total_fare').val("");
    				$('#coach_no').val("");
    				$('#berth_no').val("");
    				$('#name_condition').html("");
    				$('#name_condition').html("<input id='name' type='text' name='name' class='form-control' >");
    			}
    	
    			else{
    				
    				$('#pnr_error').html("");
    				$('#train_no').val(pnrData.trainNo);
    				$('#from_stn').val(pnrData.fromStation);
    				$('#to_stn').val(pnrData.toStation);
    				$('#board_stn').val(pnrData.boardingStn);
    				$('#berth_class').val(pnrData.berthCls);
    				$('#total_pass').val(pnrData.totalPass);
    				$('#total_fare').val(pnrData.totalAmount);
    				$('#coach_no').val(pnrData.coachNo);
    				$('#berth_no').val(pnrData.berthNo);
    				$('#name_condition').html("");
    				var htmlStr="<select id='name' name='name' class='form-control'><option value=''>---select---</option>";
    				for(i=0;i<pnrData.passengerList.length;i++){
    					
    					htmlStr=htmlStr+"<option value='"+pnrData.passengerList[i]+"'>"+pnrData.passengerList[i]+"</option>";
    					
    				}
    				htmlStr=htmlStr+"</select>";
    				$('#name_condition').html(htmlStr);
    				
    				
    			}
    			$('#pnr_desc').show();
    			
    				},
    				 error: function (xhr, status, thrownError) {
    					 alert("internal fetch error");
    					 $('#pnr_error').html("");
    	    				$('#train_no').val("");
    	    				$('#from_stn').val("");
    	    				$('#to_stn').val("");
    	    				$('#board_stn').val("");
    	    				$('#berth_class').val("");
    	    				$('#total_pass').val("");
    	    				$('#total_fare').val("");
    	    				$('#coach_no').val("");
    	    				$('#berth_no').val("");
    	    				$('#name_condition').html("");
    	    				$('#name_condition').html("<input id='name' type='text' name='name' class='form-control' >");
    					 $('#pnr_desc').show();
    			     }
    			}); 
    		 
    		 
    	 });
    	 
    	 
    	 
    	 $("#generate").click(function(){
    		 
    		 var contact	  =     $('#contact_detail').val();
    		 var mobileNo	  =     $('#contact_no').val();
    		 var email	      =     $('#email').val();
    		 var complaint=$('#complaint_type').val();
    		 var complainttype=$('#sub_complaint_type').val();
    		 var incidentdt=$('#incident_dt').val();
    		
    		 
    		 var station=$('#stationName').val();
    		 var name1=$('#name').val();
    	//	 var filesize     = 	$("#file")[0].files[0].size;
    		 
    	//	 if(filesize<=5242880){
    		 if(contact=='Contact'){
    			 if(mobileNo==''|| complaint=='' || complainttype=='' || incidentdt==''  || name1==''){
      			   alert("Please fill all manditory fields");
      	          }
  				  else if (!(mobileNo.charAt(0)=="9" || mobileNo.charAt(0)=="8" || mobileNo.charAt(0)=="7"))
  	        	    	   {
  	        	    	        alert("Mobile No. should start with 9 ,8 or 7 ");    			        	    	       
  	        	    	        
  	        	    	   		}
      		    else{
  	    			
  	 				$.ajax({
  	 					method : 'GET',
  	 					url : 'GenerateOTP',
  	 					data : {
  	 							
  	 							mobileNo:mobileNo
  	 						
  	 						},
  	 					success : function(responseText) {
  	 						$('#detail_panel').hide();
  	 						$('#otp_panel').show();
  	 				
  	 				
  	 					},
  	 					 error: function (xhr, status, thrownError) {
  	 						 alert("internal error");
  	 				     }
  	 				});
  	 			
      		     }
    		 }
    		 else if(contact=='Email'){
    			 if(email==''|| complaint=='' || complainttype=='' || incidentdt==''  || name1==''){
        			   alert("Please fill all manditory fields");
        	          }
    				 
        		    else{
    	    			
    	 				$.ajax({
    	 					method : 'GET',
    	 					url : 'GenerateOTP',
    	 					data : {
    	 							
    	 						Email:email
    	 						
    	 						},
    	 					success : function(responseText) {
    	 						$('#detail_panel').hide();
    	 						$('#otp_panel').show();
    	 				
    	 				
    	 					},
    	 					 error: function (xhr, status, thrownError) {
    	 						 alert("internal error");
    	 				     }
    	 				});
    	 			
        		     }
    		 }
    		   
    		 
    		    /*	 }
    		 else{
    			 alert("File size is not more than 5MB");
    		 }
    		 */
    		 
    		 
    		 
    	 
    	 });
    	 
    	 
    	 $("#submitbtn").click(function(e){
    		 e.preventDefault();
    	 var otp= $('#otp').val();
    	// alert(otp);
    	 
    	 
    	 
    	 
    	    if(otp!=""){
    	   
    	        $.ajax({
    	          method: "post",
    	          url: "CheckOtp.jsp",
    	          
    	         data:{
    	    
    	          otp:$('#otp').val()
    	         },
    	          
    	         
    	          success: function(data)
    	          {
    	          var msg="";
    	          
    	            if(data.trim()!="true"){
                        //      alert("false");
    	              $("#result").html("OTP is not matched."); 
    	              $("#result").addClass("alert alert-danger");
    	            }
    	              if(data.trim()=="true"){
    	                $("#result").html(" "); 
    	                $("#result").removeClass("alert alert-danger");
    	                
    	                
    	                 
    	                
    	                var formData = new FormData(document.getElementById("complaint"));
      	              
	       	             $.ajax({
	       	                 url: 'SaveComplaint',
	       	                 type: 'POST',
	       	                 data: formData,
	       	                 async: false,
	       	                 cache: false,
	       	                 contentType: false,
	       	                 enctype: 'multipart/form-data',
	       	                 processData: false,
	       	                 success: function (response) {
	       	                	 $('#otp_panel').hide();
	            				 $('#result_register').show();
	       	                 //  alert("hello")
	            				 
	            				 $("#comp_id").append(response);
	       	                 },
	    					 error: function (xhr, status, thrownError) {
	    						 alert("internal error");
	    				     }
	       	             
	       	             });
    	                
        			
       	             
       	             
    	           
    	              }
    	            
    	          }
    	        });
    	    
    	    
    	      }
    	    
    	  });
            });