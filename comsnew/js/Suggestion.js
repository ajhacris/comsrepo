$(document).ready(function (e){
	$('#otp_panel').hide();
	$('#result_register').hide();
	
	$.ajax({
		method : 'GET',
		url : 'Suggestion',
		data : {
				flag : 'complaint',		
			},
		success : function(responseText) {
	
	$("#selectComplaint").append(responseText);
	
		},
		 error: function (xhr, status, thrownError) {
	     }
	});
	
	
	$.ajax({
		method : 'GET',
		url : 'Suggestion',
		data : {
			flag : 'stationNameOnLoad',
			
			},
		success : function(responseText) {
	
	$("#stationName1").append(responseText);
	
		},
		 error: function (xhr, status, thrownError) {
	     }
	});
	
	
	
	$.ajax({
		method : 'GET',
		url : 'Suggestion',
		data : {
			flag : 'trainnoOnLoad',
			
			},
		success : function(responseText) {
	
	$("#trainnoo").append(responseText);
	
		},
		 error: function (xhr, status, thrownError) {
	     }
	});
	
	 $("#generate").click(function(){
		 
		 var contact	  =     $('#contact_detail').val();
		 var mobileNo	  =     $('#contact_no').val();
		 var email	      =     $('#email').val();
		 var complaint_type=$('#complaint_type').val();
		 var name=$('#name').val();
		
		
		 
		
		 var station_name =     $('#stationName').val();
		 
		 var flag_uts     =     $('#pmode1').val();
		 
		
		 var complaint_desc=$('#complaint_desc').val();
		 
		
		 
		 
		 
		 
		 if(contact=='Contact'){
			 if(mobileNo=='' || complaint_type=='' || name=='' ||complaint_desc=='' ){
				 
				 alert("Please enter mandatory field");
			 }
					
				 
			 else if (!(mobileNo.charAt(0)=="9" || mobileNo.charAt(0)=="8" || mobileNo.charAt(0)=="7"))
	  	   {
	  	        alert("Mobile No. should start with 9 ,8 or 7 ");    			        	    	       
	  	        
	  	   		} 
				 
			
			 else{
				 $.ajax({
						method : 'GET',
						url : 'GenerateOTP',
						data : {
								
								mobileNo:mobileNo
							
							},
						success : function(responseText) {
							$('#detail_panel').hide();
							$('#otp_panel').show();
							
					
						},
						 error: function (xhr, status, thrownError) {
							 alert("internal error");
					     }
					});
			 }
		 }
		 else if(contact=='Email'){
			 if(email=='' || complaint_type=='' || name=='' ||complaint_desc=='' ){
				 
				 alert("Please enter mandatory field");
			 }
					
				 
			 
				 
			
			 else{
				 $.ajax({
						method : 'GET',
						url : 'GenerateOTP',
						data : {
								
							Email:email
							
							},
						success : function(responseText) {
							$('#detail_panel').hide();
							$('#otp_panel').show();
							
					
						},
						 error: function (xhr, status, thrownError) {
							 alert("internal error");
					     }
					});
			 }
		 }
		
		 
		
		 
		 
		 
		 
		 
		 
	 
	 });
	 
	 
	 $("#submitbtn").click(function(e){
		 e.preventDefault();
	 var otp= $('#otp').val();
	// alert(otp);
	 
	 
	 
	 
	    if(otp!=""){
	   
	        $.ajax({
	          method: "post",
	          url: "CheckOtp.jsp",
	          
	         data:{
	    
	          otp:$('#otp').val()
	         },
	          
	         
	          success: function(data)
	          {
	          var msg="";
	          
	            if(data.trim()!="true"){
                    //      alert("false");
	              $("#result").html("OTP is not matched."); 
	              $("#result").addClass("alert alert-danger");
	            }
	              if(data.trim()=="true"){
	                $("#result").html(" "); 
	                $("#result").removeClass("alert alert-danger");
	                
	                
	                
    				$.ajax({
    					method : 'post',
    					url : 'SaveSuggestion',
    					data :$('#complaint').serialize()+ "&compMode=t",
    					success : function(responseText) {
    				//	alert("success");
    					$('#otp_panel').hide();
    					$('#result_register').show();
    					console.log(responseText);
						$("#suggestion_id").append(responseText);
    				
    					},
    					 error: function (xhr, status, thrownError) {
    						 alert("internal error");
    				     }
    				});
	           
	              }
	            
	          }
	        });
	    
	    
	      }
	    
	  });
	
    	
});