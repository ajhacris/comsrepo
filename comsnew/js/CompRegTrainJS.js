$(document).ready(function (e){
	$('#incident_dt').datetimepicker({
		  locale: 'pt-br',
		  minDate:new Date((new Date().getTime() - (5 * (24 * 60 * 60 * 1000)))),
		  maxDate: moment()
		});
    	$('#pnr_desc').hide();
    	$('#otp_panel').hide();
    	$('#result_register').hide();
    	
    	
    	
    	$.ajax({
			method : 'GET',
			url : 'CommonList',
			data : {
					flag : 'complaint1',
					complaint_place:'t'
				
				},
			success : function(responseText) {
		
		$("#selectComplaint").append(responseText);
		
				$.ajax({
	 				method : 'GET',
	 				url : 'CommonList',
	 				data : {
	 				flag : 'subcomplaint',
	 					parent_id : $('#complaint_type').val()
	 					
	 				
	 				},
	 				success : function(responseText) {
	 					$("#selectSubComplaint").empty() ;
	 			$("#selectSubComplaint").append(responseText);
	 			
	 				},
	 				 error: function (xhr, status, thrownError) {
	 			     }
	 			});
		
			},
			 error: function (xhr, status, thrownError) {
		     }
		});
    	
    	
    	
    	 $("#selectComplaint").on('change', '#complaint_type', function(){ 
 			$.ajax({
 				method : 'GET',
 				url : 'CommonList',
 				data : {
 				flag : 'subcomplaint',
 					parent_id : $('#complaint_type').val()
 					
 				
 				},
 				success : function(responseText) {
 					$("#selectSubComplaint").empty() ;
 			$("#selectSubComplaint").append(responseText);
 			
 				},
 				 error: function (xhr, status, thrownError) {
 			     }
 			});
 			});
 		
    	
    	 $("#pnr_uts").keyup(function(){
    	   
    		 var pnr_length=$('#pnr_uts').val().length;
    		 var pnr =$('#pnr_uts').val();
    		 if(pnr_length<10){}
    		 else{
    		// alert(pnr_length+"Value is :"+pnr);
    		 
    		 
    	
    	 
    	 
    	
    		 
    		 $('#pnr_desc').hide();
    		 
    			$.ajax({
    				method : 'GET',
    				url : 'ComplaintRegInTrain',
    				data : {
    						
    						pnrNo:$('#pnr_uts').val()
    					
    					},
    				success : function(responseText) {
    			var pnrData=jQuery.parseJSON(responseText);
    			
    			if(pnrData.errMsg!=''){
    				$('#pnr_error').html(pnrData.errMsg);
/*    				$('#train_no').val("");
    				$('#from_stn').val("");
    				$('#to_stn').val("");
    				$('#board_stn').val("");
    				$('#berth_class').val("");
    				$('#total_pass').val("");
    				$('#total_fare').val("");
    				$('#coach_no').val("");
    				$('#berth_no').val("");
    				$('#name_condition').html("");
    				$('#name_condition').html("<input id='name' type='text' name='name' class='form-control' >");*/
    				$('#pnr_desc').hide()
    			}
    	
    			else{
    				
    				$('#pnr_error').html("");
    				$('#day').val(pnrData.day),
					$('#month').val(pnrData.month),
					$('#year').val(pnrData.year),
    				$('#train_no').val(pnrData.trainNo);
    				$('#from_stn').val(pnrData.fromStation);
    				$('#to_stn').val(pnrData.toStation);
    				$('#board_stn').val(pnrData.boardingStn);
    				$('#berth_class').val(pnrData.berthCls);
    				$('#total_pass').val(pnrData.totalPass);
    				$('#total_fare').val(pnrData.totalAmount);
    				
/*    				$('#coach_no').val(pnrData.coachNo);
    				$('#berth_no').val(pnrData.berthNo);*/
    				
    				$('#coach_condition').html("");
    				var htmlStr="<select id='coach_no' name='coach_no' class='form-control'><option value=''>---select---</option>";
    				console.log(pnrData.coachNoList);
    				for(i=0;i<pnrData.coachNoList.length;i++){
    					
    					htmlStr=htmlStr+"<option value='"+pnrData.coachNoList[i]+"'>"+pnrData.coachNoList[i]+"</option>";
    					
    				}
    				htmlStr=htmlStr+"</select>";
    				$('#coach_condition').html(htmlStr);
    				
    				$('#berth_condition').html("");
    				var htmlStr="<select id='berth_no' name='berth_no' class='form-control'><option value=''>---select---</option>";
    				for(i=0;i<pnrData.berthNoList.length;i++){
    					
    					htmlStr=htmlStr+"<option value='"+pnrData.berthNoList[i]+"'>"+pnrData.berthNoList[i]+"</option>";
    					
    				}
    				htmlStr=htmlStr+"</select>";
    				$('#berth_condition').html(htmlStr);
    				
    				$('#name_condition').html("");
    				var htmlStr="<select id='name' name='name' class='form-control'><option value=''>---select---</option>";
    				for(i=0;i<pnrData.passengerList.length;i++){
    					
    					htmlStr=htmlStr+"<option value='"+pnrData.passengerList[i]+"'>"+pnrData.passengerList[i]+"</option>";
    					
    				}
    				htmlStr=htmlStr+"</select>";
    				$('#name_condition').html(htmlStr);
    				
    				
    				
    				
    	    		/*		NEAR BY STATION START*/
    	    			
    	    			
    	    				
    	    				$.ajax({
    	    					
    	    					method : 'GET',
    	    					url : 'StationNearBy',
    	    					data : {
    	    							day : $('#day').val(),
    	    							month:$('#month').val(),
    	    							year:$('#year').val(),
    	    							starting_point:$('#from_stn').val(),
    	    						    trainNo:$('#train_no').val()
    	    						},
    	    						
    	    					success : function(responseText) {
    	    						var ntesData=jQuery.parseJSON(responseText);
    	    				
    	    			//	alert(ntesData.NextPTTStation);
    	    				if(ntesData.ServiceDataFlag=='N'){
    	    					
    	    					$('#nextstn').val("");
    	    				}
    	    				else{
    	    					$('#nextstn').val(ntesData.NextPTTStation);
    	    				}
    	    				
    	    				
    	    					},
    	    					 error: function (xhr, status, thrownError) {
    	    				     }
    	    				});
    	    			
    	    				
    	    				
    	    				
    	    				/*		NEAR BY STATION CODE END*/	
    				
    				
    				$('#pnr_desc').show();
    				
    				
    			}
    			

    				},
    				 error: function (xhr, status, thrownError) {
    					 //alert("internal fetch error");
    					 $('#pnr_error').html("PNR service is not available.");
    	    				$('#train_no').val("");
    	    				$('#from_stn').val("");
    	    				$('#to_stn').val("");
    	    				$('#board_stn').val("");
    	    				$('#berth_class').val("");
    	    				$('#total_pass').val("");
    	    				$('#total_fare').val("");
    	    				$('#coach_no').val("");
    	    				$('#berth_no').val("");
    	    				$('#name_condition').html("");
    	    				$('#name_condition').html("<input id='name' type='text' name='name' class='form-control' >");
    					 $('#pnr_desc').hide();
    			     }
    			}); 
    		 
    		 
    		 }
     	} );
    	 
    	 
    	 
    	 
    	 
  
    	 
    	 
    	 
    	 
    	 
    	 
$("#generate").click(function(){
    		 
	
         //    var filesize     = 	$("#file")[0].files[0].size;
	
			 var contact	  =     $('#contact_detail').val();
    		 var mobileNo	  =     $('#contact_no').val();
    		 var email	      =     $('#email').val();
    		 var complaint    =     $('#complaint_type').val();
    		 var comp_sub     =     $('#sub_complaint_type').val();
    		 var incident_dt  =     $('#incident_dt').val();
    		 //var inc_day=str.substring(1, 4);
    		 var train_no     =     $('#trainno').val();
    		 var pnr          =     $('#pnr_uts').val();
    		// var station_name =     $('#stationName').val();
    		 var name         =     $('#name').val();
    		 var flag_pnr     =     $('#pmode').val();
    	//	 var flag_uts     =     $('#pmode1').val();
    		 var train_no_pnr =     $('#train_no').val();
    		 var inc_day=parseFloat(incident_dt.substring(3, 5));
    		 var inc_month=parseFloat(incident_dt.substring(0, 2));
    		 var inc_year=incident_dt.substring(6, 10);
    		
    		 var day=$('#day').val();
    		 var month=$('#month').val();
    		 var year=$('#year').val();
    		
    		 
    		 
    		 
    		 
    		 
    	//	alert("TRAIN NO IS :"+train_no );
    	//	 if(filesize<=5242880){
    		    	if(flag_pnr == 'PNR'){
    		    		
    		    		if(year>inc_year){
    		    			alert("Incident year is wrong");
    		    		}
    		    		else if(month>inc_month){
    		    			alert(month);
    		    			alert("Incident month"+inc_month);
    		    			alert("Incident month is wrong");
    		    		}
    		    		else if(day>inc_day){
    		    			alert("Incident date is wrong");
    		    		}
    		    		
    		    		
    		    		else if(contact=='Contact'){
    		    			if(pnr=='' ){ 
        		    	   		alert("Please enter mandatory field");
        				     }
        			        else if(mobileNo == '' || complaint == '' || comp_sub == '' ||  incident_dt == ''||  name== '' ||train_no_pnr==''){
            			             alert("Please enter mandatory field");
            			          }
        			       
    							  else if (!(mobileNo.charAt(0)=="9" || mobileNo.charAt(0)=="8" || mobileNo.charAt(0)=="7"))
    	        	    	   {
    	        	    	        alert("Mobile No. should start with 9 ,8 or 7 ");    			        	    	       
    	        	    	        
    	        	    	   		}
            		       		 else{
            		        		$.ajax({
    	        				    	method : 'GET',
    	        					    url : 'GenerateOTP',
    	        					    data : {
    	        							mobileNo:mobileNo
    	        						},
    	        					    
    	        						success : function(responseText) {
    		        						$('#detail_panel').hide();
    		        						$('#otp_panel').show();
    		        					},
    	        					  
    	        						error: function (xhr, status, thrownError) {
    	        							alert("internal error");
    	        				        }
            				        });
            		             }
    		    		}
    		    		
    		    		else if(contact=='Email'){
    		    			if(pnr=='' ){ 
        		    	   		alert("Please enter mandatory field");
        				     }
        			        else if(email == '' || complaint == '' || comp_sub == '' ||  incident_dt == ''||  name== '' ||train_no_pnr==''){
            			             alert("Please enter mandatory field");
            			          }
        			       
    							 
            		       		 else{
            		        		$.ajax({
    	        				    	method : 'GET',
    	        					    url : 'GenerateOTP',
    	        					    data : {
    	        							Email:email
    	        						},
    	        					    
    	        						success : function(responseText) {
    		        						$('#detail_panel').hide();
    		        						$('#otp_panel').show();
    		        					},
    	        					  
    	        						error: function (xhr, status, thrownError) {
    	        							alert("internal error");
    	        				        }
            				        });
            		             }
    		    		}
    		    		
    		    	
    		    	   	
    			    }
    		 
    		        else if(flag_pnr == 'UTS'){
    		        	if(contact=='Contact'){
    		        		if(train_no==''){
			    				 alert("Please enter mandatory field");
			    				  alert("train ");
		        	    	 	}
   	 
		        	    	 	else if(mobileNo == '' || complaint == '' || comp_sub == '' ||  incident_dt == ''||  name== ''){
						        			 alert("Please enter mandatory field");
						        			  alert("TRain1 ");
						        			 
		        	    	 			}
		 else if (!(mobileNo.charAt(0)=="9" || mobileNo.charAt(0)=="8" || mobileNo.charAt(0)=="7"))
   	    	   {
   	    	        alert("Mobile No. should start with 9 ,8 or 7 ");    			        	    	       
   	    	        
   	    	   		}
		        	    	 		 else{
			  
					        				$.ajax({
					        					method : 'GET',
					        					url : 'GenerateOTP',
					        					data : {
					        							
					        							mobileNo:mobileNo
					        						
					        						},
					        					success : function(responseText) {
					        						$('#detail_panel').hide();
					        						$('#otp_panel').show();
					        				
					        				
					        					},
					        					 error: function (xhr, status, thrownError) {
					        						 alert("internal error");
					        				     }
					        				});
					        			
		        	    	 		 	}
    		        	}
    			        	     
    			        	    	 	
    		        	 else if(contact=='Email'){
    		        		 if(train_no==''){
			    				 alert("Please enter mandatory field");
			    				//  alert("train ");
		        	    	 	}
   	 
		        	    	 	else if(email == '' || complaint == '' || comp_sub == '' ||  incident_dt == ''||  name== ''){
						        			 alert("Please enter mandatory field");
						        	//		  alert("TRain1 ");
						        			 
		        	    	 			}
		
		        	    	 		 else{
			  
					        				$.ajax({
					        					method : 'GET',
					        					url : 'GenerateOTP',
					        					data : {
					        							
					        						Email:email
					        						
					        						},
					        					success : function(responseText) {
					        						$('#detail_panel').hide();
					        						$('#otp_panel').show();
					        				
					        				
					        					},
					        					 error: function (xhr, status, thrownError) {
					        						 alert("internal error");
					        				     }
					        				});
					        			
		        	    	 		 	}
    	    		        }
    			     
			          }
    		       
    		
		});
    	 
    	 
    	 $("#submitbtn").click(function(e){
    		 e.preventDefault();
    	 var otp= $('#otp').val();
    	// alert(otp);
    	 
    	 
    	 
    	 
    	    if(otp!=""){
    	   
    	        $.ajax({
    	          method: "post",
    	          url: "CheckOtp.jsp",
    	          
    	         data:{
    	    
    	          otp:$('#otp').val()
    	         },
    	          
    	         
    	          success: function(data)
    	          {
    	          var msg="";
    	          
    	            if(data.trim()!="true"){
                        //      alert("false");
    	              $("#result").html("OTP is not matched."); 
    	              $("#result").addClass("alert alert-danger");
    	            }
    	              if(data.trim()=="true"){
    	                $("#result").html(" "); 
    	                $("#result").removeClass("alert alert-danger");
    	                
    	                
    	                var formData = new FormData(document.getElementById("complaint"));
      	              
       	             $.ajax({
       	                 url: 'ComplaintOnTrain',
       	                 type: 'POST',
       	                 data: formData,
       	                 async: false,
       	                 cache: false,
       	                 contentType: false,
       	                 enctype: 'multipart/form-data',
       	                 processData: false,
       	                 success: function (response) {
       	                	 $('#otp_panel').hide();
            				 $('#result_register').show();
            				 
            			//	 console.log(response);
     						$("#suggestion_id").append(response);
            				 
            				 
       	                 //  alert("hello")
       	                 },
    					 error: function (xhr, status, thrownError) {
    						 alert("internal error");
    				     }
       	             
       	             });
    	                
        			/*	$.ajax({
        					method : 'post',
        					url : 'ComplaintOnTrain',
        					data :$('#complaint').serialize()+ "&compMode=t",
        					success : function(responseText) {
        				//	alert("success");
        					$('#otp_panel').hide();
        					$('#result_register').show();
        				
        				
        					},
        					 error: function (xhr, status, thrownError) {
        						 alert("internal error");
        				     }
        				});*/
    	           
    	              }
    	            
    	          }
    	        });
    	    
    	    
    	      }
    	    
    	  });
    	 
    	 
    	 
    	 
    	 
    	
            });