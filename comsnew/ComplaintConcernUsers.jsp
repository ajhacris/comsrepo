<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@page import ="utility.DbConnection" import="java.sql.*" %>
<html>
	<head>
	<meta charset="utf-8">
  		<meta name="viewport" content="width=device-width, initial-scale=1">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
		
		<script src="js/bootstrap.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js" type="text/javascript"></script>
		
		<script src="js/searchperticularcomplaint.js?version=1"></script>
		
		<script src="js/bootstrap-datetimepicker.min.js"></script>
		<link href="css/bootstrap.min.css" rel="stylesheet"/>
 		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
 		<link href="css/bootstrap-datetimepicker.css" rel="stylesheet"/>
		<link href="css/bootstrap-datetimepicker.min.css" rel="stylesheet"/>
	
	
	
	
	</head>
	<style>
		.bg {
    		background-position: center center;
    		background-size: cover;
		}
	</style>

	<%   
		String zone="";
		String div="";
		String groupid="";
		if(session.getAttribute("login_id")==null){
			
			System.out.println("******************Session Expired **********************");
            response.sendRedirect("AdminLogin.jsp");
		}
		else{
			zone=session.getAttribute("zn_cd").toString();
			div=session.getAttribute("div_cd").toString();
			groupid=session.getAttribute("group_id").toString();
		}
	%>

	<body id="page-top" style="overflow-x: hidden">
  		<!--    header start  -->
    	<%@ include file="top.jsp" %>
    	<div  id="content-wrapper">    
     		<%@ include file="sidebar.jsp" %>	     
    		<!--    header end   -->   
			<div class="container-fluid" style=" padding-top:35px;width:100%; padding-bottom:120px;">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header bg-info text-white">
							<p style="font-size: 25px;">
								<b><center>
									<h2>List Of Complaint</h2>
								</center></b>
							</p>
						</div>
						<div class="card-body" style="background-color: #F7F7F7;">
							<div class="row container-fluid">
								<div class="col-md-12">						
									<form method="post" action="ComplaintConcernUsers.jsp" name="compsearch" id="compsearch" onsubmit="return chkForm()">
						  				<div id="detail_panel">
						  					<div class="row">
												<div class="col-md-3">
													<div class="form-group">
														<label for="com_ref_no" >Complaint Reference Number: </label>
													</div>
												</div>
												<div class="col-md-3">
													<div class="form-group">
														<input id="com_ref_no" type="text" name="com_ref_no" <%if(request.getParameter("com_ref_no")!=null){ %>value="<%=request.getParameter("com_ref_no")%>"<%} %> class="form-control" >
													</div>
												</div>
												
												
											</div>
											  								
			  								<div class="row">
			     								<div class="col-md-12" style="text-align:center;">
                									<div class="form-group">                   
                    									<button id="searchall" type="submit" name="searchall" class="btn btn-primary" >Search </button>
                    									                    
                									</div>
            									</div>
            								</div>		
						  				</div>
									</form>
									<%
							         	if(request.getParameter("searchall")!=null ){
							         		int i=0;
											PreparedStatement stmt=null;
											ResultSet rs=null;
											DbConnection dbConnection=new DbConnection();
											Connection con=dbConnection.dbConnect();
											String complaint_ref_no="";											
																					
											complaint_ref_no=request.getParameter("com_ref_no");
											
											System.out.println(complaint_ref_no);											
											String condition="";																					
											String query="SELECT complaint_ref_no ,user_group_id FROM rly_complaint_users WHERE complaint_ref_no=?";
											stmt=con.prepareStatement(query);
											stmt.setString(1,complaint_ref_no);
											rs=stmt.executeQuery();	
											int count=0;
											
											
											
											
											
											
											
											 										
											
									%>
									<div class="row">
										<div class="col-md-12">
										<div class="table-responsive">   
											<table class='table table-hover' border="1px">
												<thead>
													<tr>
														<th>Reference No</th>
														<th>Group Id</th>
														
													</tr>
													<% while(rs.next())  {
														  i++;	
													%>
													<tr>
														<td><%=rs.getString(1) %></td>
														<td><%=rs.getString(2) %></td>
														
													</tr>
													<%} %>
		  										</thead>
											</table>
											</div>
										</div>
									</div>
									<% if(i==0){ %>
										<div class="row">
											<div class="col-md-12">
												<center><h5>Record Not found</h5></center>
											</div>
										</div>
									<%}} %>
								</div>
							</div>
						</div>
					</div>
					</div>
					</div>
					</div>
					
				  
     				     			<!--    footer start  -->
     				      			<%@ include file="footer.jsp" %>
		    		
	    		 			
	    		
			
	
		
    	<script src="js/jquery/jquery.min.js"></script>
    	<script src="js/js/bootstrap.bundle.min.js"></script>
    	<script src="js/jquery-easing/jquery.easing.min.js"></script>
    	<script src="js/jsdash/sb-admin.min.js"></script>
	</body>
</html>