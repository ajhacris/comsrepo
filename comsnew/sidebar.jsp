<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="css/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css"/>
    <link href="css/cssdash/sb-admin.css" rel="stylesheet"/>
</head>
<%   
		int flag_sla=0;
		
		if(session.getAttribute("login_id")==null){
			
			System.out.println("******************Session Expired **********************");
            response.sendRedirect("AdminLogin.jsp");
		}
		flag_sla=Integer.parseInt(session.getAttribute("flag_sla").toString());
		
	%>

<div  id="wrapper" >

      <!-- Sidebar -->
      <ul class="sidebar navbar-nav" style=" 
          background-color: #1195a2!important;
          font-family: roboto;
          font-weight: 400;
          margin-bottom: 0px;
          box-shadow: rgba(0, 0, 0, 0.3) 0px 1px 4px 0px!important;
          font-family: roboto;
          font-weight: bold;
          width: 2px;">
          
          
        <li class="nav-item active">
          <a class="nav-link" href="dashboard.jsp">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span>
          </a>
        </li>
      
        <%if((flag_sla)<2){ %>
        <li class="nav-item active">
          <a class="nav-link" href="ComplaintList.jsp">
            <i class="fas fa-fw fa-table"></i>
            <span>Assigned To Me</span></a>
        </li>
         <%} %>
         
          <%if((flag_sla)==1){ %>
        <li class="nav-item active">
          <a class="nav-link" href="ComplaintRelated.jsp">
            <i class="fas fa-fw fa-table"></i>
            <span> Related Complaint</span></a>
        </li>
         <%} %>
         
          <%if((flag_sla)==4){ %>
        <li class="nav-item active">
          <a class="nav-link" href="UpdateSlaTime.jsp">
            <i class="fas fa-fw fa-table"></i>
            <span> SLA Update</span></a>
        </li>
         <li class="nav-item active">
          <a class="nav-link" href="ComplaintConcernUsers.jsp">
             <i class="fas fa-fw fa-search"></i>
            <span>Search Complaint Assignment</span></a>
        </li>
        
        
         <%} %>
         
         <%if((flag_sla)==1 || (flag_sla)==2 || (flag_sla)==3 ){ %>
         <li class="nav-item active">
          <a class="nav-link" href="SlaTimeView.jsp">
            <i class="fas fa-fw fa-table"></i>
            <span>SLA View</span></a>
        </li>
        <%} %>
        
         <%if((flag_sla)<2){ %>
        <li class="nav-item active">
          <a class="nav-link" href="ComplaintSuggestionRegistration.jsp">
            <i class="fas fa-fw fa-table"></i>
            <span> Register Complaint</span></a>
        </li>
        <%} %>
        
         <%if((flag_sla)<4){ %>
        <li class="nav-item active">
          <a class="nav-link" href="ComplaintClosed.jsp">
            <i class="fas fa-fw fa-table"></i>
            <span> Closed Complaint</span></a>
        </li>
        <%} %>
         <%if((flag_sla)==1){ %>
        <li class="nav-item active">
          <a class="nav-link" href="ForwardedComplaints.jsp">
            <i class="fas fa-fw fa-table"></i>
            <span> Forwarded Complaint</span></a>
        </li>
         <%} %>
        <%if((flag_sla)>1 && (flag_sla)<4 ){ %>
         <li class="nav-item active">
          <a class="nav-link" href="ComplaintBreachSlaTime.jsp">
             <i class="fas fa-fw fa-table"></i>
            <span>Complaint Breach SLA</span></a>
        </li>
         <%} %>
        
         <%if((flag_sla)<2){ %>
         <li class="nav-item active">
          <a class="nav-link" href="SearchPerticularComplaint.jsp">
             <i class="fas fa-fw fa-search"></i>
            <span>Search Particular Complaint</span></a>
        </li>
         <%} %>
         
          <%if((flag_sla)==4){ %>
         <li class="nav-item active">
          <a class="nav-link" href="SlaBreachReport.jsp">
             <i class="fas fa-fw fa-search"></i>
            <span>SLA Breach Report</span></a>
        </li>
         <%} %>
         
          <%if((flag_sla)>1 && (flag_sla)<5 ){ %>
         <li class="nav-item active">
          <a class="nav-link" href="SearchComplaintAdmin.jsp">
             <i class="fas fa-fw fa-search"></i>
            <span>Search Complaint</span></a>
        </li>
         <%} %>
         
         
           <%if((flag_sla)==5){ %>
         <li class="nav-item active">
          <a class="nav-link" href="incoming_messages.jsp">
             <i class="fas fa-fw fa-search"></i>
            <span>Incoming Messages</span></a>
        </li>
        
         <li class="nav-item active">
          <a class="nav-link" href="junk_messages_list.jsp">
             <i class="fas fa-fw fa-search"></i>
            <span>Junk Messages</span></a>
        </li>
        
        <li class="nav-item active">
          <a class="nav-link" href="search_messages_list.jsp">
             <i class="fas fa-fw fa-search"></i>
            <span>Search</span></a>
        </li>
         <%} %>
         
         
        
        <%if(flag_sla == 2){ %>
           <li class="nav-item active">
          <a class="nav-link" href="SuggestionsList.jsp">
             <i class="fas fa-fw fa-search"></i>
            <span>Suggestions</span></a>
        </li>
        
        <%} %>
        <li class="nav-item active">
          <a class="nav-link" href="Logout.jsp">
          <i class="fas fa-sign-out-alt"></i>
           
            <span>Logout</span></a>
        </li>
        
      </ul>
    


 