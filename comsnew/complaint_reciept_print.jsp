<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@page import ="utility.DbConnection" import="java.sql.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta charset="utf-8">
  		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js" type="text/javascript"></script>
		<script src="js/CompRegTrainJS.js?version=1"></script>
		<script src="js/bootstrap-datetimepicker.min.js"></script>
		<link href="css/bootstrap.min.css" rel="stylesheet"/>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
		<link href="css/bootstrap-datetimepicker.css" rel="stylesheet"/>
		<link href="css/bootstrap-datetimepicker.min.css" rel="stylesheet"/>
		<title>IR Web Portal</title>
	</head>
	
	<%
	
		PreparedStatement stmt=null;
		DbConnection dbConnection=new DbConnection();
		Connection con=dbConnection.dbConnect();
		String complaint_ref_no="";
		complaint_ref_no=request.getParameter("comp_ref_no");
		int count=0;
		String subquery="select head_subhead_en from rly_heads_subheads_master where id=?";
		String query="SELECT complaint_ref_no,pnr_uts_no,train_class,complainant_name,mobile,email_id,"+
					"incident_date,created_on,head_subhead_en,complaint_sub_type,staff_name,"+
					"complaint_mode,trainno,coach_no,berth_no,platform_no,STATUS,"+
					"complaint,remarks,zn_cd,div_cd,dept_cd,user_group_id,station_name "+ 
					"FROM rly_complaint,rly_heads_subheads_master "+ 
					"WHERE complaint_ref_no=? AND rly_complaint.complaint_type=rly_heads_subheads_master.id "+ 
					"";
		stmt=con.prepareStatement(query);
		stmt.setString(1,complaint_ref_no);
 		ResultSet rs=stmt.executeQuery();  
		while(rs.next())  {
			count++;	
	%>
<style>
	.bg {
   
    background-image: url(img/trainpic.jpg);
    background-position: center center;
    background-size: cover;
}
body
{
font-family: 'Arial Narrow',Arial, sans-serif;
font-size:13px!important;
}
</style>
<body class="bg">
		<div class="container-fluid" style="padding:0px;">
 			<%@ include file="header_main.jsp" %>
 		</div>
		<div class="container-fluid" style="padding-top:10px;padding-bottom:10px;  margin-left: auto;
    margin-right: auto;
    width: 100%;  min-height: 100%;">
			<div class="col-md-12" >
				<div class="card" style="font-size:20px!important;">
					<div class="card-header bg-info text-white">
					<div class="row">
						<div class="col-md-12" align="right">
						 	<a href="index.jsp" class="close-thin" Style="text-decoration:none;color:white;font-size: 22px;">X</a>
						</div>
						<div class="col-md-12">
						 	<center><p style="font-size:22px;"><b>Indian Railways Complaints Suggestions</b> </p>  </center>						
						</div>
						</div>
					</div>
					<br>
					<div class="container" >
					<div class="row" >
				 		<div class="col-md-3">
				     		<label>Compliant reference Number: </label>
				  		</div>
				  		<div class="col-md-3">
				     		<%=rs.getString(1) %>
				  		</div>
					</div>
					<div class="row">
				 		<div class="col-md-3">
				     		<label style="color:#f05f40;">PNR Details  </label>  
				  		</div>				  
					</div>
					<div class="row">
				 		<div class="col-md-3">
				     		<label>PNR Number: </label>
				  		</div>
				  		<div class="col-md-3">
				  		<%if(rs.getString(2)==null){
				  			%>
				  		<%}else{ %>
				     		<%=rs.getString(2) %>
				     		<%} %>
				  		</div>
					</div>
					<div class="row">
				 		<div class="col-md-3">
				     		<label>Class: </label>
				  		</div>
				  		<div class="col-md-3">
				     		<%=rs.getString(3) %>
				  		</div>
					</div>
				
					<div class="row">
				 		<div class="col-md-3">
				     		<label style="color:#f05f40;">Passenger Details  </label>  
				  		</div>				  
					</div>
					<div class="row">
				 		<div class="col-md-3">
				     		<label>Name: </label>
				  		</div>
				  		<div class="col-md-3">
				     		<%=rs.getString(4) %>
				  		</div>
					</div>
					
					<div class="row">
				 		<div class="col-md-3">
				     		<label>Contact Detail: </label>
				  		</div>
				  		<div class="col-md-3">
				  		<%if(rs.getString(5)!=null && !rs.getString(5).equals("")){
				  			%>
				  		
				     		<%=rs.getString(5) %>
				     		<%} %>
				     		<%if(rs.getString(6)!=null && !rs.getString(6).equals("")){
				  			%>
				  		
				     		<%=rs.getString(6) %>
				     		<%} %>
				  		</div>
					</div>
					<%-- <div class="row">
				 		<div class="col-md-3">
				     		<label>Email: </label>
				  		</div>
				  		<div class="col-md-3">
				     		<%=rs.getString(6) %>
				  		</div>
					</div> --%>
				
					<div class="row">
				 		<div class="col-md-3">
				     		<label style="color:#f05f40;">Cause Of Complaint  </label>  
				  		</div>				  
					</div>
					<%-- <div class="row">
				 		<div class="col-md-3">
				     		<label>Incident Date Time: </label>
				  		</div>
				  		<div class="col-md-3">
				     		<%=rs.getString(7) %>
				  		</div>
					</div>
					<div class="row">
				 		<div class="col-md-3">
				     		<label>Complaint Reported On: </label>
				  		</div>
				  		<div class="col-md-3">
				     		<%=rs.getString(8) %>
				  		</div>
					</div> --%>
					<div class="row">
				 		<div class="col-md-3">
				     		<label>Complaint Type: </label>
				  		</div>
				  		<div class="col-md-3">
				     		<%=rs.getString(9) %>
				  		</div>
					</div>
					<div class="row">
				 		<div class="col-md-3">
				     		<label>Sub Complaint Type: </label>
				  		</div>
				  		<div class="col-md-3">
				   			<%
		      					stmt=con.prepareStatement(subquery);	
			 					stmt.setString(1,rs.getString(10));
			 					ResultSet rs1=stmt.executeQuery();  
			 					while(rs1.next()){
		      						%>
				     				<%=rs1.getString(1) %>
				     			<%} 
				     		%>
				  		</div>
					</div>
					<%-- <div class="row">
				 		<div class="col-md-3">
				     		<label>Complaint against the staff: </label>
				  		</div>
				  		<div class="col-md-3">
				     		<%=rs.getString(11) %>
				  		</div>
					</div> --%>
				
					<div class="row">
				 		<div class="col-md-3">
				     		<label style="color:#f05f40;">Journey Details  </label>  
				  		</div>				  
					</div>
					<div class="row">
				 		<div class="col-md-3">
				     		<label>Complaint Mode: </label>
				  		</div>
				  		<div class="col-md-3">
				     		<%if(rs.getString(12).equals("S")){ %>
				      			On Station
				      		<%}%>
				       		<%if(rs.getString(12).equals("T")){ %>
				      			In Train
				      		<%}%>
				  		</div>
					</div>
					<%if(rs.getString(12).equals("T")){ %>
						<%if(!rs.getString(13).equals("")){ %>
						<div class="row">
				 			<div class="col-md-3">
				     			<label>Train Number: </label>
				  			</div>
				  			<div class="col-md-3">
				     			<%=rs.getString(13) %>
				  			</div>
						</div>
						<div class="row">
				 			<div class="col-md-3">
				     			<label>Coach Number: </label>
				  			</div>
				  			<div class="col-md-3">
				     			<%=rs.getString(14) %>
				  			</div>
						</div>
						<div class="row">
				 			<div class="col-md-3">
				     			<label>Berth/Seat Number: </label>
				  			</div>
				  			<div class="col-md-3">
				     			<%=rs.getString(15) %>
				  			</div>
						</div>
						<%}else{ %>
						<div class="row">
				 			<div class="col-md-3">
				     			<label>Station Name: </label>
				  			</div>
				  			<div class="col-md-3">
				     			<%=rs.getString(24) %>
				  			</div>
						</div>
						<%} %>
					<%}%>
				
					<%if(rs.getString(12).equals("S")){ %>
						<div class="row">
				 			<div class="col-md-3">
				     			<label>Platform Number: </label>
				  			</div>
				  			<div class="col-md-3">
				     			<%=rs.getString(16) %>
				  			</div>
						</div>
						<div class="row">
				 			<div class="col-md-3">
				     			<label>Station Name: </label>
				  			</div>
				  			<div class="col-md-3">
				     			<%=rs.getString(24) %>
				  			</div>
						</div>
					<%}%>
					<div class="row">
				 		<div class="col-md-3">
				     		<label>Status: </label>
				  		</div>
				  		<div class="col-md-3">
				     		<%=rs.getString(17) %>
				  		</div>
					</div>
				
					<div class="row">
				 		<div class="col-md-3">
				     		<label style="color:#f05f40;">Complaint Details  </label>  
				  		</div>				  
					</div>
					<div class="row">
				 		<div class="col-md-3">
				     		<label>Complaint Description: </label>
				  		</div>
				  		<div class="col-md-9">
				     		<%=rs.getString(18) %>
				  		</div>
					</div>
					<div class="row">
				 		<div class="col-md-3">
				     		<label>Remarks: </label>
				  		</div>
				  		<div class="col-md-9">
				     		<%=rs.getString(19) %>
				  		</div>
					</div>
				
					<div class="row">
				 		<div class="col-md-3">
				     		<label style="color:#f05f40;">Currently With  </label>  
				  		</div>				  
					</div>
					<div class="row">
				 		<div class="col-md-3">
				     		<label>Zone: </label>
				  		</div>
				  		<div class="col-md-3">
				  		
				  		<% stmt=con.prepareStatement("SELECT ZN_NM FROM mst_zones WHERE ZN_CD=?");
				  		    stmt.setString(1,rs.getString(20));
				  		    ResultSet rs_1=stmt.executeQuery();
				  		    while(rs_1.next()){
				  		%>
				  		
				  		
				     		<%=rs_1.getString(1) %>
				     		
				     		<%} %>
				  		</div>
					</div>
					<div class="row">
				 		<div class="col-md-3">
				     		<label>Division: </label>
				  		</div>
				  		<div class="col-md-3">
				     		<% stmt=con.prepareStatement("SELECT DIV_NM FROM mst_divisions WHERE DIV_CD=?");
				  		    stmt.setString(1,rs.getString(21));
				  		    rs_1=stmt.executeQuery();
				  		    while(rs_1.next()){
				  		%>
				  		
				  		
				     		<%=rs_1.getString(1) %>
				     		
				     		<%} %>
				  		</div>
					</div>
					<div class="row">
				 		<div class="col-md-3">
				     		<label>Department: </label>
				  		</div>
				  		<div class="col-md-3">
				     		<% stmt=con.prepareStatement("SELECT dept_name FROM rly_department WHERE dept_code=?");
				  		    stmt.setString(1,rs.getString(22));
				  		    rs_1=stmt.executeQuery();
				  		    while(rs_1.next()){
				  		%>
				  		
				  		
				     		<%=rs_1.getString(1) %>
				     		
				     		<%} %>
				  		</div>
					</div>
					<div class="row">
				 		<div class="col-md-3">
				     		<label>Designation: </label>
				  		</div>
				  		<div class="col-md-3">
				     		<%=rs.getString(23) %>
				  		</div>
					</div>
				
					<div class="row">
				 		<div class="col-md-4">
				  		</div>
				  		<div class="col-md-4">
				    		<input id="complaintRegister:print" type="submit" name="complaintRegister:print" value="Print Receipt" onclick="window.print();return false;" >
				  		</div>
					</div>
				</div>
				<br>
				<br>
			</div>
			</div>
			<div class="col-md-2">
			</div>
		</div>
		<div class="container-fluid" style="padding:0px;">
 			<%@ include file="footer_main.html" %>
 		</div>
	</body>
	<%} 
	if(count==0){
   		response.sendRedirect("ComplaintSearch.jsp");				
	}%>
</html>