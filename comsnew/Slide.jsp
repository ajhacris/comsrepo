
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>COMS</title>
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/fontawesome-free/css/all.css" rel="stylesheet" type="text/css">
   
    <link href="css/creative.min.css" rel="stylesheet"> 
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</head>
<style>

body
{
font-family: 'Arial Narrow',Arial, sans-serif;
font-size:13px!important;
}
.btn_station {
    background-color: #0c407a;
    border-left: 10px solid #fe7600;
    padding: 20px 25px;
    color: white;
    cursor: pointer;
    font-size: 21px;
    text-align: left;
    
}
.bg-light-blue-active, .modal-primary .modal-header, .modal-primary .modal-footer {
    background-color: #357ca5 !important;
}
.btn{

    text-align: left;
    text-decoration: none;
    display: inline-block;
    font-size: 15px;
    margin: 4px 2px;
    -webkit-transition-duration: 0.4s;
    transition-duration: 0.4s;
    cursor: pointer;
    width: 50%;
}
.bg-dark {
    background-color: #343a40!important;
}
 
 
 @media (max-width:1024px) and (min-width:320px){
 .font-class{
  font-size: larger!important;
 }
   .btn{

    text-align: left;
    text-decoration: none;
    display: inline-block;
    font-size: 15px;
    margin: 4px 2px;
    -webkit-transition-duration: 0.4s;
    transition-duration: 0.4s;
    cursor: pointer;
    width: 100%;
}
.btn_train {
    background-color: #034895;
    border-left: 10px solid #de4638;
    padding: 20px 25px;
    text-decoration: none!important;
    padding: 0px;
    font-size: large!important;
    color: white;
    cursor: pointer;
    font-size: 21px;
    text-align: left;
} 
.btn_station {
    background-color: #0c407a;
    border-left: 10px solid #fe7600;
    padding: 20px 25px;
    color: white;
    cursor: pointer;
    font-size: 21px;
    text-align: left;
    text-decoration: none!important;
    padding: 0px;
    font-size: large!important;
    
}

 }
.btn_train {
    background-color: #034895;
    border-left: 10px solid #de4638;
   padding: 20px 25px;
    font-size: large!important;
    color: white;
    cursor: pointer;
    font-size: 21px;
    text-align: left;
} 

button, input, optgroup, select, keygen::-webkit-keygen-select, select[_moz-type="-mozilla-keygen"], textarea {
    color: inherit;
    font: inherit;
    margin: 0;
    margin-top: 0.5em;
}
button, input, select, textarea {
    font-family: inherit;
    font-size: inherit;
    line-height: inherit;
}
button, html input[type=button], input[type=reset], input[type=submit] {
    -webkit-appearance: button;
    cursor: pointer;
}
.image {
  opacity: 1;
  display: block;
  width: 100%;
  height: auto;
  transition: .5s ease;
  backface-visibility: hidden;
}

. :hover .image {
  opacity: 0.3;
}
header.masthead {
   
    background-image: url(img/trainpic.jpg);
    background-position: center center;
    background-size: cover;
}
</style>












<style>

#titleBar p {
  font-family: "Arial";
  font-size: 3em;
  float: left;
  width: 100%;
  display: inline-block;
  margin: 0;
  padding-left: 10px;
}

ul {
  list-style-type: none;
  margin: 0;
  padding: 0;
  overflow: hidden;
  width: auto;
  display: inline-block;
  float: right;
  margin-top: -40px;
}

li {
  display: inline;
}

li a {
  display: inline;
  margin-right: 46px;
  color: black;
  font-family: "Arial";
  padding: 14px 16px;
  text-decoration: none;
}

li a:hover:not(.active) {
  background-color: #ffffff;
}

.crossfade > figure {
  animation: imageAnimation 30s linear infinite 0s;
  backface-visibility: hidden;
  background-size: cover;
  background-position: center center;
  color: transparent;
  height: 100%;
  left: 0px;
  opacity: 0;
  position: absolute;
  top: 0px;
  width: 100%;
  z-index: -1;
}

.crossfade > figure:nth-child(1) {
  background-image: url('https://pbs.twimg.com/profile_images/434258933721489409/Hg0Z1blu.jpeg');
}

.crossfade > figure:nth-child(2) {
  animation-delay: 6s;
  background-image: url('http://www.drodd.com/images15/2-22.png');
}

.crossfade > figure:nth-child(3) {
  animation-delay: 12s;
  background-image: url('http://www.freeiconspng.com/uploads/number-3-icon-17.png');
}

.crossfade > figure:nth-child(4) {
  animation-delay: 18s;
  background-image: url('https://pbs.twimg.com/profile_images/708068901620486144/QO17QaY-.jpg');
}

.crossfade > figure:nth-child(5) {
  animation-delay: 24s;
  background-image: url('http://www.kidsmathgamesonline.com/images/pictures/numbers600/number5.jpg');
}

.crossfade > figure:nth-child(6) {
  animation-delay: 30s;
  background-image: url('https://pbs.twimg.com/profile_images/863084976476233729/iKoEcmWt.jpg');
}

.crossfade > figure:nth-child(7) {
  animation-delay: 36s;
  background-image: url('https://pbs.twimg.com/profile_images/613986789586542592/vDhMOvlR.jpg');
}

@keyframes imageAnimation {
  0% {
    animation-timing-function: ease-in;
    opacity: 0;
  }
  8% {
    animation-timing-function: ease-out;
    opacity: 1;
  }
  17% {
    opacity: 1
  }
  25% {
    opacity: 0
  }
  100% {
    opacity: 0
  }
}
</style>

<body style="overflow-x:hidden;">
   
   <%@ include file="header_main.jsp" %>

    <header class="masthead text-center text-white d-flex">

<div class="crossfade">
  <figure></figure>
  <figure></figure>
  <figure></figure>
  <figure></figure>
  <figure></figure>
</div>

<div class="container ">
        
          <div class="col-lg-12 " align="center" style="padding-top:80px;">
            <h1 class="text-uppercase" style="color:white;/*font-size:52px;*/text-shadow: 1px 2px #7b0909;">
             <strong>SUGGESTIONS  AND  COMPLAINTS PORTAL</strong> 
            </h1>
            <hr>
          </div>
         <div class="row">
         <div class="col-lg-2">
         
            <!-- <p class="text-faded mb-5" style="color:white;    background-color: #0d520900;
            padding-top: 29px"><b>A Passenger is the most important visitor in our premises.<br></b> </p>-->
         </div>
         <div class="col-lg-8   col-md-6 ">
          
          <div class="col-xs-12" align="center">
          <a href="Suggestions.jsp"  target="_self" style="text-decoration:none!important;"><button class=" btn btn_train "  align="center" style="border-radius: 0px!important;">
            <span style="color:#f63e2d">Suggestions</span>
             
          </button></a>
          </div>
     
          <div class="col-xs-12" align="center">
          <a href="ComplaintOnTrain.jsp"  target="_self" style="text-decoration:none!important;"><button class=" btn btn_train "  align="center" style="border-radius: 0px!important;">
            <span style="color:#f63e2d">Complaints</span> -   In Train
             
          </button></a>
      </div>
      <div class="col-xs-12" style="margin-top:2px;" align="center">
          <a href="ComplaintOnStation_new.jsp"  target="_self" style="text-decoration:none!important;"><button class="btn btn_train "  align="center" style="border-radius: 0px!important;">
            <span style="color:#f63e2d">Complaints</span>
             - On Station
            
          </button></a>
      </div>
      <div class="col-xs-12" align="center"> 
          <a href="ComplaintSearch.jsp"  target="_self" style="text-decoration:none!important;"><button class=" btn btn_train" style="border-radius: 0px!important;">
            <span style="color:#f63e2d" align="center">Track Complaint</span>
             
          </button></a>
     
    </div>
  </div>
  </div>
 </div>
</header>

<section id="contact" class="bg-dark text-white" >
      <div class="container text-center">
        <div class="row" >
          
          <div class="col-lg-12 " align="center" style="color:white;">
          <h2 class="section-heading">Let's Get In Touch!</h2>
            <hr class="my-4" >
             <div class="container">
        <div class="row" align="center">
           <div class="col-md-12"> <h3 class="section-heading">Complaints on SMS <a href="tel:+919717630982"><i class="fas fa-fw fa-mobile" style="font-size:48px;color:white!important;"></i>+919717630982</a></h3></div>
           <div class="col-md-12 " ><center><h3 class="font-class" align="center">Helplines  :
            182- Security  <span style="border-left: 6px solid white;"></span>
           138- Others Complaints/Suggestions</h3></center></div>
          </div>
          </div>
          </div>
        </div>
      </div>
    </section>


   

   
<!-- 
    <section class="p-0" id="portfolio">
      <div class="container-fluid p-0">
        <div class="row no-gutters popup-gallery">
          <div class="col-lg-4 col-sm-6  ">
           
              <img class="img-fluid image" src="img/portfolio/thumbnails/01.jpg" alt="">
            
            
          </div>
          <div class="col-lg-4 col-sm-6  ">
            
              <img class="img-fluid image" src="img/portfolio/thumbnails/coach.jpg" alt="">
             
           
          </div>
          <div class="col-lg-4 col-sm-6  ">
            
              <img class="img-fluid image" src="img/portfolio/thumbnails/food.jpg" alt="">
             
            
          </div>
          <div class="col-lg-4 col-sm-6  ">
            
              <img class="img-fluid image" src="img/portfolio/thumbnails/coach1.jpg" alt="">
            
           
          </div>
          <div class="col-lg-4 col-sm-6  ">
        
              <img class="img-fluid image" src="img/portfolio/thumbnails/clean2.jpg" alt="">
             
           
          </div>
          <div class="col-lg-4 col-sm-6  ">
            
              <img class="img-fluid image" src="img/portfolio/thumbnails/catering.jpg"  alt="">
       
          
          </div>
          <div class="col-lg-4 col-sm-6  ">
            
              <img class="img-fluid image" src="img/portfolio/thumbnails/dirty.jpg" alt="">
            
           
          </div>
          <div class="col-lg-4 col-sm-6  ">
        
              <img class="img-fluid image" src="img/portfolio/thumbnails/dirty2.jpg" alt="">
             
           
          </div>
          <div class="col-lg-4 col-sm-6  ">
            
              <img class="img-fluid image" src="img/portfolio/thumbnails/food2.jpg"  alt="">
       
          
          </div>
          <div class="col-lg-4 col-sm-6  ">
            
              <img class="img-fluid image" src="img/portfolio/thumbnails/dirty4.jpg" alt="">
            
           
          </div>
          <div class="col-lg-4 col-sm-6  ">
        
              <img class="img-fluid image" src="img/portfolio/thumbnails/dirty5.jpg" alt="">
             
           
          </div>
          <div class="col-lg-4 col-sm-6  ">
            
              <img class="img-fluid image" src="img/portfolio/thumbnails/dirty6.jpg"  alt="">
       
          
          </div>
        </div>
      </div>
    </section> -->

    <section class="bg-dark text-white">
      <div class="container text-center">
        <h2 class="mb-4" style="color:white;">You may download Complaint Management System Mobile App Here </h2>
        <a class="btn btn-light btn-xl sr-button"  style="text-align: center;" href="https://play.google.com/store/apps/details?id=com.cris.org&hl=en"><img src="img/android_app.png" class="img-fluid"></a>
      </div>
    </section>
    <%@ include file="footer_main.html" %>
  </div>
    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    
    <!-- Custom scripts for this template -->
    <script src="js/creative.min.js"></script>
  </body>
</html>