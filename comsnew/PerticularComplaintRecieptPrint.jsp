<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@page import ="utility.DbConnection" import="java.sql.*" %>
<html>
	<head>
	<meta charset="utf-8">
  		<meta name="viewport" content="width=device-width, initial-scale=1">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
		
		<script src="js/bootstrap.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js" type="text/javascript"></script>
		
		<script src="js/searchperticularcomplaint.js?version=1"></script>
		
		<script src="js/bootstrap-datetimepicker.min.js"></script>
		<link href="css/bootstrap.min.css" rel="stylesheet"/>
 		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
 		<link href="css/bootstrap-datetimepicker.css" rel="stylesheet"/>
		<link href="css/bootstrap-datetimepicker.min.css" rel="stylesheet"/>
	
	
	
	
	</head>
	<style>
		.bg {
    		background-position: center center;
    		background-size: cover;
		}
	</style>

	<%   
		String zone="";
		String div="";
		String groupid="";
		if(session.getAttribute("login_id")==null){
			
			System.out.println("******************Session Expired **********************");
            response.sendRedirect("AdminLogin.jsp");
		}
		
	%>
<%
	
		PreparedStatement stmt=null;
		DbConnection dbConnection=new DbConnection();
		Connection con=dbConnection.dbConnect();
		String complaint_ref_no="";
		complaint_ref_no=request.getParameter("comp_ref_no");
		int count=0;
		String subquery="select head_subhead_en from rly_heads_subheads_master where id=?";
		String query="SELECT channel_type,complaint_ref_no,pnr_uts_no,trainno,train_class,"+
				"complainant_name,mobile,email_id,"+
				"incident_date,created_on,head_subhead_en,complaint_sub_type,staff_name,"+
				"complaint_mode,coach_no,berth_no,platform_no,station_name,STATUS,"+
				"complaint,remarks,duty_staff_name,duty_staff_designation "+
				"FROM rly_complaint,rly_heads_subheads_master "+
				"WHERE complaint_ref_no=? AND rly_complaint.complaint_type=rly_heads_subheads_master.id  ";
				
		stmt=con.prepareStatement(query);
		stmt.setString(1,complaint_ref_no);
 		ResultSet rs=stmt.executeQuery();  
		while(rs.next())  {
			count++;	
	%>
	<body id="page-top" style="overflow-x: hidden">
  		<!--    header start  -->
    	<%@ include file="top.jsp" %>
    	<div  id="content-wrapper">    
     		<%@ include file="sidebar.jsp" %>	     
    		<!--    header end   -->   
			<div class="container-fluid" style=" padding-top:35px;width:100%; padding-bottom:120px;">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header bg-info text-white">
							<p style="font-size: 25px;">
								<b><center>
									<h2>Complaint Detail</h2>
								</center></b>
							</p>
						</div>
						<div class="container" >
							<div class="row" >
								<div class="col-md-3">
									<label style="color:#f05f40;">Complaint Type  </label>  
								</div>
							</div>
							<div class="row" >
				 				<div class="col-md-3">
				     				<label>Compliant Source: </label>
				  				</div>
				  				<div class="col-md-3">
				     				<%if(rs.getString(1).equals("W")){ %>
				     				WEB
				     				<%} %>
				     				<%if(rs.getString(1).equals("H")){ %>
				     				Helpline-138
				     				<%} %>
				     				<%if(rs.getString(1).equals("R")){ %>
				     				Helpline-182
				     				<%} %>
				     				<%if(rs.getString(1).equals("M")){ %>
				     				Social Media
				     				<%} %>
				     				<%if(rs.getString(1).equals("D")){ %>
				     				Manual Dak
				     				<%} %>
				     				<%if(rs.getString(1).equals("C")){ %>
				     				Catering Complaints, Rly Board
				     				<%} %>
				  				</div>
							</div>
							<div class="row" >
								<div class="col-md-3">
									<label style="color:#f05f40;">Complaint Reference Number  </label>  
								</div>
							</div>
							<div class="row" >
				 				<div class="col-md-3">
				     				<label>Compliant Reference Number: </label>
				  				</div>
				  				<div class="col-md-3">
				     				<%=rs.getString(2) %>			     				
				  				</div>
							</div>
								<%if(rs.getString(14).equals("T")){ %>
							<div class="row" >
								<div class="col-md-3">
									<label style="color:#f05f40;">PNR Details  </label>  
								</div>
							</div>
							<div class="row" >
				 				<div class="col-md-3">
				     				<label>PNR Number: </label>
				  				</div>
				  				<div class="col-md-3">
				  				<%if(rs.getString(3)==null){ }else{%>
				     				<%=rs.getString(3) %>
				     				<%} %>			     				
				  				</div>
							</div>
							<div class="row" >
				 				<div class="col-md-3">
				     				<label>Train Number: </label>
				  				</div>
				  				<div class="col-md-3">
				     				<%=rs.getString(4) %>			     				
				  				</div>
							</div>
							<div class="row" >
				 				<div class="col-md-3">
				     				<label>Class: </label>
				  				</div>
				  				<div class="col-md-3">
				     				<%=rs.getString(5) %>			     				
				  				</div>
							</div>
							<%} %>
							<div class="row" >
								<div class="col-md-3">
									<label style="color:#f05f40;">Complaint Reported By  </label>  
								</div>
							</div>
							<div class="row" >
				 				<div class="col-md-3">
				     				<label>Name: </label>
				  				</div>
				  				<div class="col-md-3">
				     				<%=rs.getString(6) %>			     				
				  				</div>
							</div>
							<div class="row" >
				 				<div class="col-md-3">
				     				<label>Contact Number: </label>
				  				</div>
				  				<div class="col-md-3">
				     				<%=rs.getString(7) %>			     				
				  				</div>
							</div>
							<div class="row" >
				 				<div class="col-md-3">
				     				<label>Email: </label>
				  				</div>
				  				<div class="col-md-3">
				     				<%=rs.getString(8) %>			     				
				  				</div>
							</div>
							<div class="row" >
								<div class="col-md-3">
									<label style="color:#f05f40;">Cause Of Complaint  </label>  
								</div>
							</div>
							<div class="row" >
				 				<div class="col-md-3">
				     				<label>Incident Date Time: </label>
				  				</div>
				  				<div class="col-md-3">
				     				<%=rs.getString(9) %>			     				
				  				</div>
							</div>
							<div class="row" >
				 				<div class="col-md-3">
				     				<label>Complaint Reported On: </label>
				  				</div>
				  				<div class="col-md-3">
				     				<%=rs.getString(10) %>			     				
				  				</div>
							</div>
							<div class="row" >
				 				<div class="col-md-3">
				     				<label>Complaint Type: </label>
				  				</div>
				  				<div class="col-md-3">
				     				<%=rs.getString(11) %>			     				
				  				</div>
							</div>
							<div class="row" >
				 				<div class="col-md-3">
				     				<label>Sub Complaint Type: </label>
				  				</div>
				  				<div class="col-md-3">
				     				<%
		      					stmt=con.prepareStatement(subquery);	
			 					stmt.setString(1,rs.getString(12));
			 					ResultSet rs1=stmt.executeQuery();  
			 					while(rs1.next()){
		      						%>
				     				<%=rs1.getString(1) %>
				     			<%} 
				     		%>			     				
				  				</div>
							</div>
							<div class="row" >
				 				<div class="col-md-3">
				     				<label>Name Of Staff: </label>
				  				</div>
				  				<div class="col-md-3">
				     				<%=rs.getString(13) %>			     				
				  				</div>
							</div>
							<div class="row" >
								<div class="col-md-3">
									<label style="color:#f05f40;">Reported Complaint  </label>  
								</div>
							</div>
							<div class="row" >
				 				<div class="col-md-3">
				     				<label>Complaint Mode: </label>
				  				</div>
				  				<div class="col-md-3">
				     				<%if(rs.getString(14).equals("S")){ %>
				      					On Station
				      				<%}%>
				       				<%if(rs.getString(14).equals("T")){ %>
				      					In Train
				      				<%}%>			     				
				  				</div>
							</div>
							<%if(rs.getString(14).equals("T")){ %>
							<%if(rs.getString(4)!=null && !rs.getString(4).equals("")){ %>
								<div class="row">
				 					<div class="col-md-3">
				     					<label>Train Number: </label>
				  					</div>
				  					<div class="col-md-3">
				     					<%=rs.getString(4) %>
				  					</div>
								</div>
								<div class="row">
				 					<div class="col-md-3">
				     					<label>Coach Number: </label>
				  					</div>
				  					<div class="col-md-3">
				     					<%=rs.getString(15) %>
				  					</div>
								</div>
								<div class="row">
				 					<div class="col-md-3">
				     					<label>Berth/Seat Number: </label>
				  					</div>
				  					<div class="col-md-3">
				     					<%=rs.getString(16) %>
				  					</div>
								</div>
								<%}else{ %>
								<div class="row">
				 					<div class="col-md-3">
				     					<label>Station Name: </label>
				  					</div>
				  					<div class="col-md-3">
				     					<%=rs.getString(18) %>
				  					</div>
								</div>
								<%} %>
							<%}%>
							<%if(rs.getString(14).equals("S")){ %>
								<div class="row">
				 					<div class="col-md-3">
				     					<label>Platform Number: </label>
				  					</div>
				  					<div class="col-md-3">
				     					<%=rs.getString(17) %>
				  					</div>
								</div>
								<div class="row">
				 					<div class="col-md-3">
				     					<label>Station Name: </label>
				  					</div>
				  					<div class="col-md-3">
				     					<%=rs.getString(18) %>
				  					</div>
								</div>
							<%}%>
							<div class="row" >
				 				<div class="col-md-3">
				     				<label>Status: </label>
				  				</div>
				  				<div class="col-md-3">
				     				<%=rs.getString(19) %>			     				
				  				</div>
							</div>
							<div class="row">
				 				<div class="col-md-3">
				     				<label style="color:#f05f40;">Complaint Details  </label>  
				  				</div>				  
							</div>
							<div class="row">
				 				<div class="col-md-3">
				     				<label>Complaint Description: </label>
				  				</div>
				  				<div class="col-md-9">
				     				<%=rs.getString(20) %>
				  				</div>
							</div>
							<div class="row">
				 				<div class="col-md-3">
				     				<label>Remarks: </label>
				  				</div>
				  				<div class="col-md-9">
				     				<%=rs.getString(21) %>
				  				</div>
							</div>
							<% if(rs.getString(22)!=null && rs.getString(22).equals("")){ %>
								<div class="row">
				 					<div class="col-md-3">
				     					<label style="color:#f05f40;">Complaint Uploaded by on Duty Staff  </label>  
				  					</div>				  
								</div>
								<div class="row">
				 					<div class="col-md-3">
				     					<label>Name: </label>
				  					</div>
				  					<div class="col-md-9">
				     					<%=rs.getString(22) %>
				  					</div>
								</div>
								<div class="row">
				 					<div class="col-md-3">
				     					<label>Designation: </label>
				  					</div>
				  					<div class="col-md-9">
				     					<%=rs.getString(23) %>
				  					</div>
								</div>
							<%} %>
							<div class="row">
				 				<div class="col-md-4">
				  				</div>
				  				<div class="col-md-4">
				    				<input id="complaintRegister:print" type="submit" name="complaintRegister:print" value="Print Receipt" onclick="window.print();return false;" >
				  				</div>
							</div>
						</div>
					</div>
				</div>    
     			
 			</div>
		</div>
		<%@ include file="footer.jsp" %>
    	<script src="js/jquery/jquery.min.js"></script>
    	<script src="js/js/bootstrap.bundle.min.js"></script>
    	<script src="js/jquery-easing/jquery.easing.min.js"></script>
    	<script src="js/jsdash/sb-admin.min.js"></script>
	</body>
	<%} %>
</html>