<%@page import="java.io.Console"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@page import ="utility.DbConnection" import="java.sql.*" %>
<html>
	<head>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
		
		<script src="js/bootstrap.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js" type="text/javascript"></script>
		<script src="js/IncomingMessageAssignJS.js?version=1"></script>
		<script src="js/complaintintrain.js?version=1"></script>
		<script src="js/Validation.js"></script>
		
	
		<link href="css/bootstrap.min.css" rel="stylesheet"/>
 		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
 		<link href="css/bootstrap-datetimepicker.css" rel="stylesheet"/>
		<link href="css/bootstrap-datetimepicker.min.css" rel="stylesheet"/>
	
	
	
	
	</head>
	<style>
		.bg {
    		background-position: center center;
    		background-size: cover;
		}
		
		
		 table {
            width: 100%;
        }

        thead, tbody, tr, td, th { display: block; }

        tr:after {
            content: ' ';
            display: block;
            visibility: hidden;
            clear: both;
        }

        thead th {
            height: 50px;

            /*text-align: left;*/
        }

        tbody {
            height: 425px;
            overflow-y: auto;
        }

        thead {
            /* fallback */
        }


        tbody td, thead th {
            width: 16.5%;
            float: left;
        }
	</style>

	<%   
		String zone="";
		String div="";
		String groupid="";
		String complaint=null;
		String mobile=null;
		int id=0;
		if(session.getAttribute("login_id")==null){
			
			System.out.println("******************Session Expired **********************");
            response.sendRedirect("AdminLogin.jsp");
		}
		else{
			zone=session.getAttribute("zn_cd").toString();
			div=session.getAttribute("div_cd").toString();
			groupid=session.getAttribute("group_id").toString();
			
			/*System.out.println("count :"+request.getParameter("count"));
			 //id=request.getParameter("id");
			int count=Integer.parseInt( request.getParameter("count"));
			System.out.println("count :"+count);
			
			 for (int i=1;i<count;i++)
			   {
				
				 if(request.getParameter("id"+i)==null || request.getParameter("id"+i).equals(""))
				     {  }
				 else {
					id = Integer.parseInt(request.getParameter("id"+i));
				   }
			   } */
			   
			   
			 id=Integer.parseInt( request.getParameter("id"));  
			System.out.println("Id  :"+id);
			
			
			
			PreparedStatement stmt=null;
			ResultSet rs=null;
			DbConnection dbConnection=new DbConnection();
			Connection con=dbConnection.dbConnect();
			String query="SELECT from_mobile,sms_text FROM rly_incoming_sms WHERE id=?";
			
			stmt=con.prepareStatement(query);
			stmt.setInt(1,id);
			rs=stmt.executeQuery();
			while(rs.next()){
				complaint=rs.getString(2);
				mobile=rs.getString(1);
			}
			System.out.println("Complaint :"+complaint);
		}
		
		
		
	%>

	<body id="page-top" style="overflow-x: hidden">
  		<!--    header start  -->
    	<%@ include file="top.jsp" %>
    	<div  id="content-wrapper">    
     		<%@ include file="sidebar.jsp" %>	     
    		<!--    header end   -->   
			<div class="container-fluid" style=" padding-top:35px;width:100%; padding-bottom:120px;">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header bg-info text-white">
							<p style="font-size: 25px;">
								<b><center>
									<h2>Register Incoming Messages Complaint</h2>
								</center></b>
							</p>
						</div>
						<div class="card-body" style="background-color: #F7F7F7;">
							<div class="row container-fluid">
								<div class="col-md-12">
								
								<div class="row">
	   <p style="color:#f05f40;">Complaint Detail  </p>
    </div>
    
    	<form action="SmsComplaints" method="post"> 
    	
    	 <input type="hidden" name="sms_id" value="<%=id %>">
	          <div class="row"  >
	          
	            <div class="col-md-4">
                  <div class="form-group">
                   <label for="mode_type">Place Of Occurrence<span style="color:#f05f40;">*</span></label>
                  <div id="place" >
                  <select class='form-control' id='place_type' name='place_type' required>
                  <option value="">--Select--</option>
                   <option value="s">On Station</option>
                   <option value="t">In Train</option>
                  </select>
                  </div>                   
                 </div>
                </div>
	          
	            <div class="col-md-4">
                  <div class="form-group">
                   <label for="complaint_type">Complaint <span style="color:#f05f40;">*</span></label>
                   <div id="selectComplaint" ></div>
                 </div>
                </div>
                <div class="col-md-4">
                 <div class="form-group">
                  <label for="sub_complaint_type">Sub Complaint <span style="color:#f05f40;">*</span></label>
                  <div id="selectSubComplaint" ></div>                   
                 </div>
               </div>
              
              </div>
              
              
               <div class="row"  >
			     
			       <div class="col-md-12">
                     <div class="form-group">
                       <label for="complaint_desc">Complaint Description <span style="color:#f05f40;">*</span></label>
                      <textarea id="complaint_desc" name="complaint_desc"  class="form-control"  readonly><%=complaint%></textarea>
                     </div>
                  </div>
			     </div>
			     
			     
			      <div class="row"  >
	          
	            
	            
                <div class="col-md-4">
                 <div class="form-group">
                  <label for="contact">Contact No. <span style="color:#f05f40;">*</span></label>
                 <input type="text" name="contact_no"  class="form-control" id ="contact_no" value=<%=mobile %> readonly>                
                 </div>
               </div>
              
              </div>
              
              
              <div id="details" >                 </div>   
               
               
               <div class="row" id="Trainno">
			   
			   
			    <div class="col-md-4">
                <div class="form-group">
                <label for="train_no">Train No <span style="color:#f05f40;font-size:30px">*</span></label>
                    
                     <div id="trainnoo" >
                   </div>   
                   
                          </div>
            </div>
            </div>
               
               
                 
                 
                 <div class="row" id="PNR">
			   
			   
			    <div class="col-md-4">
                <div class="form-group">
                    <label for="pnr_uts">PNR No <span style="color:#f05f40;font-size:30px">*</span></label>
                    <input id="pnr_uts" type="text" name="pnr_uts" class="form-control"  maxlength="10" title="Please Enter PNR Number">
                    
                </div>
            </div>
           <!--   <div class="col-md-4">
                <div class="form-group">
                    </br>  
                    <button id="pnr_utsbtn" type="button" name="pnr_utsbtn" class="btn btn-primary" >FETCH PNR</button>
                    
                </div>
            </div> -->
			   <div id="pnr_error" class="col-md-4" style="color:red;"></div>
			   
			  </div>
                 
			    
			     
              
              <div id="pnr_desc">
			  
			  <!--                  HIDDEN FIELDS                   -->
			  
			  
			   <input id="day" type="hidden" name="day" class="form-control"    >
			  <input id="month" type="hidden" name="month" class="form-control"    >
			   <input id="year" type="hidden" name="year" class="form-control"    >
			     <input id="nextstn" type="hidden" name="nextstn" class="form-control"  >
			  
			  <!--                  HIDDEN FIELDS ENDS                   -->
			  
			 <div class="row"  >
			    
            <div class="col-md-3">
                <div class="form-group">
                    <label for="train_no">Train Number</label>
                    <input id="train_no" type="text" name="train_no" class="form-control"   readonly >
                    
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="from_stn">From Station</label>
                    <input id="from_stn" type="text" name="from_stn" class="form-control"  readonly >
                   
                </div>
            </div>
        
       <div class="col-md-3">
                <div class="form-group">
                    <label for="to_stn">To Station</label>
                    <input id="to_stn" type="text" name="to_stn" class="form-control" readonly >
                    
                </div>
            </div>
			     
			      <div class="col-md-3">
                <div class="form-group">
                    <label for="board_stn">Boarding Station</label>
                    <input id="board_stn" type="text" name="board_stn" class="form-control" readonly >
                    
                </div>
            </div>
			      
			     
			     
			     </div>
			     
			    
			     
			   <div class="row"  >
			    
            <div class="col-md-3">
                <div class="form-group">
                    <label for="berth_class">Berth Class</label>
                    <input id="berth_class" type="text" name="berth_class" class="form-control"  readonly  >
                    
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="total_pass">Total Passenger</label>
                    <input id="total_pass" type="text" name="total_pass" class="form-control"  readonly >
                   
                </div>
            </div>
        
       <div class="col-md-3">
                <div class="form-group">
                    <label for="total_fare">Total Fare</label>
                    <input id="total_fare" type="text" name="total_fare" class="form-control" readonly >
                    
                </div>
            </div>
			     
			      <div class="col-md-3">
                <div class="form-group">
                    <label for="coach_no">Coach No.</label>
                    <input id="coach_no" type="text" name="coach_no" class="form-control" readonly >
                    
                </div>
            </div>
			      
			     
			     
			     </div> 
			     
			     <div class="row"> 
			      <div class="col-md-3">
                <div class="form-group">
                    <label for="berth_no">Berth/Seat No.</label>
                    <input id="berth_no" type="text" name="berth_no" class="form-control" readonly >
                    
                </div>
            </div> 


            <div class="col-md-9">
                <br>
                    <p  style="color:#f05f40;font-size:20px">Please confirm your journey details before proceeding...</p>
                   
                    
                
            </div> 
			     </div>
			     
			     </div>
			     
			     
			     
			     
			    
			     
			     <!--   <div class="row"  >
	            <div class="col-md-4">
                  <div class="form-group">
                   <label for="zone">Zone <span style="color:#f05f40;">*</span></label>
                   <div id="zone_id" ></div>
                 </div>
                </div>
                <div class="col-md-4">
                 <div class="form-group">
                  <label for="division">Division <span style="color:#f05f40;">*</span></label>
                  <div id="division_id" ></div>                   
                 </div>
                </div>
                <div class="col-md-4">
                 <div class="form-group">
                  <label for="department">Department <span style="color:#f05f40;">*</span></label>
                  <div id="department_id" ></div>                   
                 </div>
                </div>
      
              </div> -->
              
			     
								
							
							
							
							
	                             <button  type="submit" class="btn btn-primary" >Register Complaint </button>
                    						
                                </form>		
						
						
						
						
									
									
								</div>
							</div>
						</div>
					</div>
					</div>
					</div>
					</div>
					
				  
     				     			<!--    footer start  -->
     				      			<%@ include file="footer.jsp" %>
		    		
	    		 			
	    		
			
	
		
    	<script src="js/jquery/jquery.min.js"></script>
    	<script src="js/js/bootstrap.bundle.min.js"></script>
    	<script src="js/jquery-easing/jquery.easing.min.js"></script>
    	<script src="js/jsdash/sb-admin.min.js"></script>
	</body>
</html>