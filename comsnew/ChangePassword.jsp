<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@page import="utility.SendEmail"%>
<%@page import="utility.SmsUtil"%>
<%@page import="javax.mail.MessagingException"%>
<%@page import="java.util.ArrayList"%>




<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta charset="utf-8">
  		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>COMS</title>	
		<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>	
	</head>
	
	<style>
		
.panel-login {
	border-color: #ccc;
	-webkit-box-shadow: 0px 2px 3px 0px rgba(0,0,0,0.2);
	-moz-box-shadow: 0px 2px 3px 0px rgba(0,0,0,0.2);
	box-shadow: 0px 2px 3px 0px rgba(0,0,0,0.2);
}
.panel-login>.panel-heading {
	color: #00415d;
	background-color: #fff;
	border-color: #fff;
	text-align:center;
}
.panel-login>.panel-heading a{
	text-decoration: none;
	color: #666;
	font-weight: bold;
	font-size: 15px;
	-webkit-transition: all 0.1s linear;
	-moz-transition: all 0.1s linear;
	transition: all 0.1s linear;
}
.panel-login>.panel-heading a.active{
	color: #029f5b;
	font-size: 18px;
}
.panel-login>.panel-heading hr{
	margin-top: 10px;
	margin-bottom: 0px;
	clear: both;
	border: 0;
	height: 1px;
	background-image: -webkit-linear-gradient(left,rgba(0, 0, 0, 0),rgba(0, 0, 0, 0.15),rgba(0, 0, 0, 0));
	background-image: -moz-linear-gradient(left,rgba(0,0,0,0),rgba(0,0,0,0.15),rgba(0,0,0,0));
	background-image: -ms-linear-gradient(left,rgba(0,0,0,0),rgba(0,0,0,0.15),rgba(0,0,0,0));
	background-image: -o-linear-gradient(left,rgba(0,0,0,0),rgba(0,0,0,0.15),rgba(0,0,0,0));
}
.panel-login input[type="text"],.panel-login input[type="email"],.panel-login input[type="password"] {
	height: 45px;
	border: 1px solid #ddd;
	font-size: 16px;
	-webkit-transition: all 0.1s linear;
	-moz-transition: all 0.1s linear;
	transition: all 0.1s linear;
}
.panel-login input:hover,
.panel-login input:focus {
	outline:none;
	-webkit-box-shadow: none;
	-moz-box-shadow: none;
	box-shadow: none;
	border-color: #ccc;
}
.btn-login {
	background-color: #59B2E0;
	outline: none;
	color: #fff;
	font-size: 14px;
	height: auto;
	font-weight: normal;
	padding: 14px 0;
	text-transform: uppercase;
	border-color: #59B2E6;
}
.btn-login:hover,
.btn-login:focus {
	color: #fff;
	background-color: #53A3CD;
	border-color: #53A3CD;
}
.forgot-password {
	text-decoration: underline;
	color: #888;
}
.forgot-password:hover,
.forgot-password:focus {
	text-decoration: underline;
	color: #666;
}

.btn-register {
	background-color: #1CB94E;
	outline: none;
	color: #fff;
	font-size: 14px;
	height: auto;
	font-weight: normal;
	padding: 14px 0;
	text-transform: uppercase;
	border-color: #1CB94A;
}
.btn-register:hover,
.btn-register:focus {
	color: #fff;
	background-color: #1CA347;
	border-color: #1CA347;
}
	</style>
	<style> 
 .navbar-inverse{
  	background-color:#ac0202;
  }
body
{
font-family: 'Arial Narrow',Arial, sans-serif;
font-size:13px!important;
}

</style>
<body>

<%String userName="";
     	if(session.getAttribute("user_name")==null){
     	
     	System.out.println("Session Value: "+session.getAttribute("user_name"));
     	System.out.println("******************Session Expired **********************");
     	
     
         response.sendRedirect("signin.jsp");
     }
     	else{
     		userName=session.getAttribute("user_name").toString();
     	}

	%>


<div class="container-fluid" style="padding:0px;">
   
<div class="row" style="background-color:#FFFFFF;padding-top:10px;padding-bottom:20px;">
<div class="col-md-1" align="center" >
<a href="">
</a>
<img class="img-responsive" alt="National Emblem of India" src="img/logo.png" style="height:100px">
</div>
<div class="col-md-1">
<img class="img-responsive" alt="Mahatma Gandhi" src="img/Mahatma.PNG" align="right" >
</div>

<div class="col-md-8"><center>
<font style="font-size:40px;text-align:center;">
SUGGESTIONS AND COMPLAINTS<div style="margin-top: -24px;"> PORTAL</br>
</font></center>
</div>


            
<div class="col-md-2"  align="center">

<img class="img-responsive" alt="Cris" src="img/indianrail_cris_logo.jpg" height=100px;>
</div>
</div>


<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="index.jsp"><img src="img/home.png" style="height:30px;"></a>
    </div>
    
    <ul class="nav navbar-nav navbar-right">
    
     
     <%
     			if(session.getAttribute("user_name")==null){
     		%>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="signin.jsp" style="color:white!important; font-size: 22px; text-transform: capitalize!important;"><h5 style="font-size: 18px;">Login</h5></a>
            </li>
            <%}else{%>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="" style="color:white!important; font-size: 22px; cursor: default; pointer-events: none; text-transform: capitalize!important;"><h5 style="font-size: 18px;">Hi <%= session.getAttribute("name").toString()%></h5></a>
            </li>
             <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="usercomplaintHistory.jsp" style="color:white!important; font-size: 22px; text-transform: capitalize!important;"><h5 style="font-size: 18px;">Previous Complaint History</h5></a>
            </li>
             <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="ChangePassword.jsp" style="color:white!important; font-size: 22px;  text-transform: capitalize!important;"><h5 style="font-size: 18px;">Change Password</h5></a>
            </li>
           <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="Logout.jsp" style="color:white!important; font-size: 22px; text-transform: capitalize!important;"><h5  style="font-size: 18px;" >Logout</h5></a>
            </li>
            <%}%>
     
      
     
    </ul>
  </div>
</nav>
  </div>
<div class="container" style="padding-top:10px;padding-bottom:10px;  margin-left: auto; margin-right: auto; width: 100%;  min-height: 100%;">
    	<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<div class="panel panel-login">
					<div class="row" align="center">
                
                <div class="  col-md-12 " >
               <font color="blue">
					<b>
                    <%
							if(request.getParameter("msg")!=null){
								out.print(request.getParameter("msg"));
							}
						%>
                   </b>
				</font>
                    </div>
                    
                    
               
            </div>
					
					<div class="panel-body">
				
						<div class="row">
							<div class="col-lg-12">
								
								<form id="changepassword-form" action="ChangePassword" method="post" role="form" >
									
									<!-- <div class="form-group">
										<input type="text" name="mobile" id="mobile" tabindex="2" class="form-control" placeholder="Mobile Number">
									</div>	
									
									<div class="form-group">
										<div class="divider" align="center"><span class="or font-size-forteen">OR</span></div>
									</div>
									-->
									<div class="form-group">
										<input type="hidden" name="userName" id="userName" tabindex="1" class="form-control" placeholder="User Name" value="<%=userName%>">
									</div>	 
									
									<div class="form-group">
										<input type="Password" name="oldPassword" id="oldPassword" tabindex="1" class="form-control" placeholder="Old Password" value="">
									</div>
									
									<div class="form-group">
										<input type="Password" name="newPassword" id="newPassword" tabindex="1" class="form-control" placeholder="New Password" value="">
									</div>
																		
									<div class="form-group">
										<div class="row">
											<div class="col-sm-6 col-sm-offset-3">
												<input type="submit" name="register-submit" id="register-submit" tabindex="4" class="form-control btn btn-register" value="Submit">
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid" style="padding:0px;padding-top: 145px;">
 			<%@ include file="footer_main.html" %>
 		</div>
  
	</body>
</html>