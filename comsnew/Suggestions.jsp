<html>


<head>
<meta charset="utf-8">
  		<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>

<script src="js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js" type="text/javascript"></script>
<script src="js/Suggestion.js?version=1"></script>

<script src="js/Validation.js"></script>
<script src="js/bootstrap-datetimepicker.min.js"></script>
<link href="css/bootstrap.min.css" rel="stylesheet"/>
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
 <link href="css/bootstrap-datetimepicker.css" rel="stylesheet"/>
<link href="css/bootstrap-datetimepicker.min.css" rel="stylesheet"/>
    

<style>
div#Stationname {
    display: none;
}
div#emailid {
    display: none;
}
body
{
font-family: 'Arial Narrow',Arial, sans-serif;
font-size:13px!important;
}
</style>

 
<script type="text/javascript">
    $(function () {
       
        
        $("#pmode1").change(function () {
            if ($(this).val() == "Train") {
                $("#Trainno").show();
                $("#Stationname").hide();
               
            } else {
                $("#Trainno").hide();
               
            }
        });
        
        $("#pmode1").change(function () {
            if ($(this).val() == "Station") {
                $("#Stationname").show();
                $("#Trainno").hide();
               
            } else {
                $("#Stationname").hide();
               
            }
        });

        
        $("#contact_detail").change(function () {
            if ($(this).val() == "Contact") {
                $("#mobileno").show();
                $("#emailid").hide();   
            } 
        });
        
        $("#contact_detail").change(function () {
            if ($(this).val() == "Email") {
               // $("#UTS").show();
                $("#mobileno").hide();
                $("#emailid").show();
               
            }
        });
        
    });
    
    function validateEmail(emailField){
        var reg = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;

        if (reg.test(emailField.value) == false) 
        {
            alert('Invalid Email Address');
            return false;
        }

        return true;

}
</script>
        





</head>

<style>
.bg {
   
    background-image: url(img/trainpic.jpg);
    background-position: center center;
    background-size: cover;
}
</style>
<body class="bg">

<%
     	if(session.getAttribute("user_name")==null){
     	
     	System.out.println("Session Value: "+session.getAttribute("user_name"));
     	System.out.println("******************Session Expired **********************");
     	
     	session.setAttribute("url", "Suggestions.jsp");
         response.sendRedirect("signin.jsp");
     }
	%>


 <%@ include file="header_main.jsp" %>

 


 <div class="container-fluid" style="padding-top:10px;padding-bottom:10px;  margin-left: auto;
    margin-right: auto;
    width: 100%;  min-height: 100%;">
    
    <!-- <div class="container-fluid" style="padding:0px;"> -->

<div class="col-md-12">


<div class="card" style="font-size:20px!important;" >
<div class="card-header bg-info text-white"> 
<div class="row">
<div class="col-md-12" align="right">
 <a href="index.jsp" class="close-thin" Style="text-decoration:none;color:white;">X</a>
 
 </div>
<div class="col-md-12">
 <center> <p style="font-size:22px;"><b>Suggestions</b></p></center>
 </div>


</div>
</div>
<div class="card-body" style="background-color:#F7F7F7;" >
<div class="row container-fluid">
<div class="col-md-12" >
<form method="post"  name="complaint" id="complaint">
<div id="detail_panel">
<div class="row" align="center">
 
  <div class="col-md-12">
  <font color="blue">
                   <%if(request.getParameter("strMSG")!=null){
				   		out.print(request.getParameter("strMSG"));
				   }%>
					</font>
  </div>
 
			  
			   </div>
<div class="row">
	<p style="color:#f05f40;">Suggestions Detail  </p>
</div>

			     <div class="row"  >
			    
            <div class="col-md-4">
                <div class="form-group">
                    <label for="complaint_type">Suggestion <span style="color:#f05f40; font-size:30px">*</span></label>
                    <div id="selectComplaint" ></div>
                    
                </div>
            </div>
           
			     
	
			     
			     
			     </div>
			    
			     <div class="row"  >
			     
			       <div class="col-md-4">
                <div class="form-group">
                    <label for="complaint_desc">Description <span style="color:#f05f40; font-size:30px">*</span></label>
                    <textarea id="complaint_desc" name="complaint_desc"  class="form-control" required title="Please Enter Suggestion Description" style="font-size:20px!important;"></textarea>
                    
                </div>
            </div>
			     
			     <div class="col-md-4" id="UTS">
                <div class="form-group">
                <label >Journey Details <span style="color:#f05f40;font-size:30px">*</span></label>
                   <select  name="pmode1" id="pmode1" class="form-control" style="font-size:20px!important;">
          <option value="Train">Train</option>
          <option value="Station">Station</option>
         
	      
          
       </select> 
                </div>
            </div>
			     
			     
			     <div class="col-md-4" id="Trainno">
                <div class="form-group">
                <label for="train_no">Train No <span style="color:#f05f40;font-size:30px">*</span></label>
                    
                     <div id="trainnoo" >
                   </div>   
                   
                          </div>
            </div>
            <div class="col-md-4" id="Stationname">
                <div class="form-group">
                   <label for="station_name">Station Name <span style="color:#f05f40;font-size:30px">*</span></label>
                    
                     <div id="stationName1" >
                   </div>
                          </div>
            </div>
			     </div>
			   
			    
			   
			  
			 
		 
			 
			   
			
			    
			     
			  
			  
			     
			    <div class="row">
			   <p style="color:#f05f40;">Passenger Details  </p>
			   </div>
			   
			   
			  	     
			    
			   
			   
			     <div class="row"> 
			       
       <div class="col-md-4">
                <div class="form-group">
                    <label for="name">Name<span style="color:#f05f40;font-size:30px">*</span></label><div id="name_condition">
                    <input id="name" type="text" name="name" class="form-control txtOnly" title="Please Enter Name" style="font-size:20px!important;">
                    </div>
                    
                </div>
            </div>
			     
			      	    <div class="col-md-4" >
                <div class="form-group">
                <label for="contact_no">Contact <span style="color:#f05f40;font-size:30px">*</span></label>
                   <select  name="contact_detail" id="contact_detail" class="form-control " style="font-size:20px!important;">
          				<option value="Contact">Mobile No. </option>
         				 <option value="Email">Email</option>
         
	      
          
       				</select> 
                </div>
            </div>
            
			     <div class="col-md-4" id="mobileno">
                <div class="form-group">
                    <label for="contact_no">Mobile No. <span style="color:#f05f40;font-size:30px">*</span></label>
                    <input id="contact_no" type="text" name="contact_no" required class="form-control "  maxlength="10" title="Please Enter Contact Number" style="font-size:20px!important;">
                    
                </div>
            </div>
			      <div class="col-md-4" id="emailid">
                <div class="form-group">
                    <label for="contact_no">Email <span style="color:#f05f40;font-size:30px">*</span></label>
                    <input id="email" type="text" name="email" required class="form-control " onblur="validateEmail(this);" maxlength="50" title="Please Enter Email Id" style="font-size:20px!important;">
                    
                </div>
            </div>
           
			     
			     </div>
			     
			    
			     <div class="row">
			     <div class="col-md-12" style="text-align:center;">
                 <div class="form-group">
                    
                    <button id="generate" type="button" name="generate" class="btn btn-primary" >GENERATE OTP</button>
                    
                </div>
            </div></div>
            
            
           
            </div>
			        <div id="otp_panel" class="container-fluid" >
			    
			    <div id="result"></div>
			   <center> <b> <p>OTP has been sent on your contact detail</p> </b></center>
			    
			     <div class="row">
			     <div class="col-md-4"></div>
			     
			     <div class="col-md-4 " style="text-align:center;">
                <div class="form-group">
                    <label for="otp">Enter the OTP Generated</label>
                    <input id="otp" type="text" name="otp" value="1902" class="form-control" title="Please Enter OTP" style="font-size:20px!important;" >
                    
                </div>
            </div>
			     </div>
			     <div class="row">
			     <div class="col-md-12" style="text-align:center;">
                <div class="form-group">
                    
                    <button id="submitbtn" type="submit" name="submitbtn" class="btn btn-primary" >Submit</button>
                    
                </div>
            </div></div>
			     </div>
			     <div id="result_register" class="container-fluid" >
			      <p>Your Suggestions has successfully registered.</p> 
			      <div id="suggestion_id"></div>
			      <div class="form-group">
                   <center> 
                   <button  class="btn btn-primary" > <a href ="index.jsp" style="color:white;"> OK</a></button>
                   </center> 
                </div>
			    </div>
			     
			     
			     </form>

</div>
</div>
</div>

</div>





</div>
<div class="col-md-2"></div>
</div>

<div class="container-fluid"
style="padding:0px;">
 <%@ include file="footer_main.html" %>
 </div>
</body>




</html>