
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>COMS</title>
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/fontawesome-free/css/all.css" rel="stylesheet" type="text/css">
   
    <link href="css/creative.min.css" rel="stylesheet"> 
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</head>
<style>

body
{
font-family: 'Arial Narrow',Arial, sans-serif;
font-size:13px!important;
}
.btn_station {
    background-color: #0c407a;
    border-left: 10px solid #fe7600;
    padding: 20px 25px;
    color: white;
    cursor: pointer;
    font-size: 21px;
    text-align: left;
    
}
.bg-light-blue-active, .modal-primary .modal-header, .modal-primary .modal-footer {
    background-color: #357ca5 !important;
}
.btn{

    text-align: left;
    text-decoration: none;
    display: inline-block;
    font-size: 15px;
    margin: 4px 2px;
    -webkit-transition-duration: 0.4s;
    transition-duration: 0.4s;
    cursor: pointer;
    width: 50%;
}
.bg-dark {
    background-color: #343a40!important;
}
 
 
 @media (max-width:1024px) and (min-width:320px){
 .font-class{
  font-size: larger!important;
 }
   .btn{

    text-align: left;
    text-decoration: none;
    display: inline-block;
    font-size: 15px;
    margin: 4px 2px;
    -webkit-transition-duration: 0.4s;
    transition-duration: 0.4s;
    cursor: pointer;
    width: 100%;
}
.btn_train {
    background-color: #034895;
    border-left: 10px solid #de4638;
    padding: 20px 25px;
    text-decoration: none!important;
    padding: 0px;
    font-size: large!important;
    color: white;
    cursor: pointer;
    font-size: 21px;
    text-align: left;
} 
.btn_station {
    background-color: #0c407a;
    border-left: 10px solid #fe7600;
    padding: 20px 25px;
    color: white;
    cursor: pointer;
    font-size: 21px;
    text-align: left;
    text-decoration: none!important;
    padding: 0px;
    font-size: large!important;
    
}

 }
.btn_train {
    background-color: #034895;
    border-left: 10px solid #de4638;
   padding: 20px 25px;
    font-size: large!important;
    color: white;
    cursor: pointer;
    font-size: 21px;
    text-align: left;
} 

button, input, optgroup, select, keygen::-webkit-keygen-select, select[_moz-type="-mozilla-keygen"], textarea {
    color: inherit;
    font: inherit;
    margin: 0;
    margin-top: 0.5em;
}
button, input, select, textarea {
    font-family: inherit;
    font-size: inherit;
    line-height: inherit;
}
button, html input[type=button], input[type=reset], input[type=submit] {
    -webkit-appearance: button;
    cursor: pointer;
}


</style>

<style>
/* CSS reset */


.slideshow,
.slideshow:after {
    position: fixed;
    width: 100%;
    height: 100%;
    top: 0px;
    left: 0px;
    z-index: 0;
}
.slideshow:after {
    content: '';
    background: transparent url() repeat top left;
}
.slideshow li span {
    width: 100%;
    height: 100%;
    position: absolute;
    top: 0px;
    left: 0px;
    color: transparent;
    background-size: cover;
    background-position: 50% 50%;
    background-repeat: none;
    opacity: 0;
    z-index: 0;
	-webkit-backface-visibility: hidden;
    -webkit-animation: imageAnimation 36s linear infinite 0s;
    -moz-animation: imageAnimation 36s linear infinite 0s;
    -o-animation: imageAnimation 36s linear infinite 0s;
    -ms-animation: imageAnimation 36s linear infinite 0s;
    animation: imageAnimation 36s linear infinite 0s;
}
.slideshow li div {
    z-index: 1000;
    position: absolute;
    bottom: 30px;
    left: 0px;
    width: 100%;
    text-align: center;
    opacity: 0;
    -webkit-animation: titleAnimation 36s linear infinite 0s;
    -moz-animation: titleAnimation 36s linear infinite 0s;
    -o-animation: titleAnimation 36s linear infinite 0s;
    -ms-animation: titleAnimation 36s linear infinite 0s;
    animation: titleAnimation 36s linear infinite 0s;
}
.slideshow li div h3 {
  font-family: "helvetica neue", helvetica;
  text-transform: uppercase;
  font-size: 80px;
  padding: 0;
  line-height: 200px;
	color: rgba(255,255,255, 0.8);
}
.slideshow li:nth-child(1) span { background-image: url(img/trainpic.jpg) }
.slideshow li:nth-child(2) span {
    background-image: url(img/trainpic1.jpg);
    -webkit-animation-delay: 6s;
    -moz-animation-delay: 6s;
    -o-animation-delay: 6s;
    -ms-animation-delay: 6s;
    animation-delay: 6s;
}
.slideshow li:nth-child(3) span {
    background-image: url('img/trainpic2.jpg');
    -webkit-animation-delay: 12s;
    -moz-animation-delay: 12s;
    -o-animation-delay: 12s;
    -ms-animation-delay: 12s;
    animation-delay: 12s;
}
.slideshow li:nth-child(4) span {
    background-image: url(img/trainpic3.jpg);
    -webkit-animation-delay: 18s;
    -moz-animation-delay: 18s;
    -o-animation-delay: 18s;
    -ms-animation-delay: 18s;
    animation-delay: 18s;
}
.slideshow li:nth-child(5) span {
    background-image: url(img/trainpic4.jpg);
    -webkit-animation-delay: 24s;
    -moz-animation-delay: 24s;
    -o-animation-delay: 24s;
    -ms-animation-delay: 24s;
    animation-delay: 24s;
}
.slideshow li:nth-child(6) span {
    background-image: url(img/trainpic5.jpg);
    -webkit-animation-delay: 30s;
    -moz-animation-delay: 30s;
    -o-animation-delay: 30s;
    -ms-animation-delay: 30s;
    animation-delay: 30s;
}
.slideshow li:nth-child(2) div {
    -webkit-animation-delay: 6s;
    -moz-animation-delay: 6s;
    -o-animation-delay: 6s;
    -ms-animation-delay: 6s;
    animation-delay: 6s;
}
.slideshow li:nth-child(3) div {
    -webkit-animation-delay: 12s;
    -moz-animation-delay: 12s;
    -o-animation-delay: 12s;
    -ms-animation-delay: 12s;
    animation-delay: 12s;
}
.slideshow li:nth-child(4) div {
    -webkit-animation-delay: 18s;
    -moz-animation-delay: 18s;
    -o-animation-delay: 18s;
    -ms-animation-delay: 18s;
    animation-delay: 18s;
}
.slideshow li:nth-child(5) div {
    -webkit-animation-delay: 24s;
    -moz-animation-delay: 24s;
    -o-animation-delay: 24s;
    -ms-animation-delay: 24s;
    animation-delay: 24s;
}
.slideshow li:nth-child(6) div {
    -webkit-animation-delay: 30s;
    -moz-animation-delay: 30s;
    -o-animation-delay: 30s;
    -ms-animation-delay: 30s;
    animation-delay: 30s;
}
/* Animation for the slideshow images */
@-webkit-keyframes imageAnimation { 
	0% {
	    opacity: 0;
	    -webkit-animation-timing-function: ease-in;
	}
	8% {
	    opacity: 1;
	    -webkit-transform: scale(1.05);
	    -webkit-animation-timing-function: ease-out;
	}
	17% {
	    opacity: 1;
	    -webkit-transform: scale(1.1);
	}
	25% {
	    opacity: 0;
	    -webkit-transform: scale(1.1);
	}
	100% { opacity: 0 }
}
@-moz-keyframes imageAnimation { 
	0% {
	    opacity: 0;
	    -moz-animation-timing-function: ease-in;
	}
	8% {
	    opacity: 1;
	    -moz-transform: scale(1.05);
	    -moz-animation-timing-function: ease-out;
	}
	17% {
	    opacity: 1;
	    -moz-transform: scale(1.1);
	}
	25% {
	    opacity: 0;
	    -moz-transform: scale(1.1);
	}
	100% { opacity: 0 }
}
@-o-keyframes imageAnimation { 
	0% {
	    opacity: 0;
	    -o-animation-timing-function: ease-in;
	}
	8% {
	    opacity: 1;
	    -o-transform: scale(1.05);
	    -o-animation-timing-function: ease-out;
	}
	17% {
	    opacity: 1;
	    -o-transform: scale(1.1);
	}
	25% {
	    opacity: 0;
	    -o-transform: scale(1.1);
	}
	100% { opacity: 0 }
}
@-ms-keyframes imageAnimation { 
	0% {
	    opacity: 0;
	    -ms-animation-timing-function: ease-in;
	}
	8% {
	    opacity: 1;
	    -ms-transform: scale(1.05);
	    -ms-animation-timing-function: ease-out;
	}
	17% {
	    opacity: 1;
	    -ms-transform: scale(1.1);
	}
	25% {
	    opacity: 0;
	    -ms-transform: scale(1.1);
	}
	100% { opacity: 0 }
}
@keyframes imageAnimation { 
	0% {
	    opacity: 0;
	    animation-timing-function: ease-in;
	}
	8% {
	    opacity: 1;
	    transform: scale(1.05);
	    animation-timing-function: ease-out;
	}
	17% {
	    opacity: 1;
	    transform: scale(1.1);
	}
	25% {
	    opacity: 0;
	    transform: scale(1.1);
	}
	100% { opacity: 0 }
}
/* Animation for the title */
@-webkit-keyframes titleAnimation { 
	0% {
	    opacity: 0;
	    -webkit-transform: translateY(200px);
	}
	8% {
	    opacity: 1;
	    -webkit-transform: translateY(0px);
	}
	17% {
	    opacity: 1;
	    -webkit-transform: scale(1);
	}
	19% { opacity: 0 }
	25% {
	    opacity: 0;
	    -webkit-transform: scale(10);
	}
	100% { opacity: 0 }
}
@-moz-keyframes titleAnimation { 
	0% {
	    opacity: 0;
	    -moz-transform: translateY(200px);
	}
	8% {
	    opacity: 1;
	    -moz-transform: translateY(0px);
	}
	17% {
	    opacity: 1;
	    -moz-transform: scale(1);
	}
	19% { opacity: 0 }
	25% {
	    opacity: 0;
	    -moz-transform: scale(10);
	}
	100% { opacity: 0 }
}
@-o-keyframes titleAnimation { 
	0% {
	    opacity: 0;
	    -o-transform: translateY(200px);
	}
	8% {
	    opacity: 1;
	    -o-transform: translateY(0px);
	}
	17% {
	    opacity: 1;
	    -o-transform: scale(1);
	}
	19% { opacity: 0 }
	25% {
	    opacity: 0;
	    -o-transform: scale(10);
	}
	100% { opacity: 0 }
}
@-ms-keyframes titleAnimation { 
	0% {
	    opacity: 0;
	    -ms-transform: translateY(200px);
	}
	8% {
	    opacity: 1;
	    -ms-transform: translateY(0px);
	}
	17% {
	    opacity: 1;
	    -ms-transform: scale(1);
	}
	19% { opacity: 0 }
	25% {
	    opacity: 0;
	    -webkit-transform: scale(10);
	}
	100% { opacity: 0 }
}
@keyframes titleAnimation { 
	0% {
	    opacity: 0;
	    transform: translateY(200px);
	}
	8% {
	    opacity: 1;
	    transform: translateY(0px);
	}
	17% {
	    opacity: 1;
	    transform: scale(1);
	}
	19% { opacity: 0 }
	25% {
	    opacity: 0;
	    transform: scale(10);
	}
	100% { opacity: 0 }
}
/* Show at least something when animations not supported */
.no-cssanimations .slideshow li span{
	opacity: 1;
}
@media screen and (max-width: 1140px) { 
	.slideshow li div h3 { font-size: 100px }
}
@media screen and (max-width: 600px) { 
	.slideshow li div h3 { font-size: 50px }
}



</style>

  <body style="overflow-x:hidden;">
   <ul class="slideshow">
  <li><span>Image 01</span></li>
  <li><span>Image 02</span></li>
  <li><span>Image 03</span></li>
  <li><span>Image 04</span></li>
  <li><span>Image 05</span></li>
  <li><span>Image 06</span></li>
</ul>
   <%@ include file="header_main.jsp" %>

    <header class="masthead text-center text-white d-flex">

   <div class="container ">
        
          <div class="col-lg-12 " align="center" style="padding-top:80px;">
            <h1 class="text-uppercase" style="color:white;/*font-size:52px;*/text-shadow: 1px 2px #7b0909;">
             <strong>SUGGESTIONS  AND  COMPLAINTS PORTAL</strong> 
            </h1>
            <hr>
          </div>
         <div class="row">
         <div class="col-lg-2">
         
            <!-- <p class="text-faded mb-5" style="color:white;    background-color: #0d520900;
            padding-top: 29px"><b>A Passenger is the most important visitor in our premises.<br></b> </p>-->
         </div>
         <div class="col-lg-8   col-md-6 ">
          
          <div class="col-xs-12" align="center">
          <a href="Suggestions.jsp"  target="_self" style="text-decoration:none!important;"><button class=" btn btn_train "  align="center" style="border-radius: 0px!important;">
            <span style="color:#f63e2d">Suggestions</span>
             
          </button></a>
          </div>
     
          <div class="col-xs-12" align="center">
          <a href="ComplaintOnTrain.jsp"  target="_self" style="text-decoration:none!important;"><button class=" btn btn_train "  align="center" style="border-radius: 0px!important;">
            <span style="color:#f63e2d">Complaints</span> -   In Train
             
          </button></a>
      </div>
      <div class="col-xs-12" style="margin-top:2px;" align="center">
          <a href="ComplaintOnStation_new.jsp"  target="_self" style="text-decoration:none!important;"><button class="btn btn_train "  align="center" style="border-radius: 0px!important;">
            <span style="color:#f63e2d">Complaints</span>
             - On Station
            
          </button></a>
      </div>
      <div class="col-xs-12" align="center"> 
          <a href="ComplaintSearch.jsp"  target="_self" style="text-decoration:none!important;"><button class=" btn btn_train" style="border-radius: 0px!important;">
            <span style="color:#f63e2d" align="center">Track Complaint</span>
             
          </button></a>
     
    </div>
  </div>
  </div>
 </div>
</header>

<section id="contact" class="bg-dark text-white" >
      <div class="container text-center">
        <div class="row" >
          
          <div class="col-lg-12 " align="center" style="color:white;">
          <h2 class="section-heading">Let's Get In Touch!</h2>
            <hr class="my-4" >
             <div class="container">
        <div class="row" align="center">
           <div class="col-md-12"> <h3 class="section-heading">Complaints on SMS <a href="tel:+919717630982"><i class="fas fa-fw fa-mobile" style="font-size:48px;color:white!important;"></i>+919717630982</a></h3></div>
           <div class="col-md-12 " ><center><h3 class="font-class" align="center">Helplines  :
            182- Security  <span style="border-left: 6px solid white;"></span>
           138- Others Complaints/Suggestions</h3></center></div>
          </div>
          </div>
          </div>
        </div>
      </div>
    </section>


   

   
<!-- 
    <section class="p-0" id="portfolio">
      <div class="container-fluid p-0">
        <div class="row no-gutters popup-gallery">
          <div class="col-lg-4 col-sm-6  ">
           
              <img class="img-fluid image" src="img/portfolio/thumbnails/01.jpg" alt="">
            
            
          </div>
          <div class="col-lg-4 col-sm-6  ">
            
              <img class="img-fluid image" src="img/portfolio/thumbnails/coach.jpg" alt="">
             
           
          </div>
          <div class="col-lg-4 col-sm-6  ">
            
              <img class="img-fluid image" src="img/portfolio/thumbnails/food.jpg" alt="">
             
            
          </div>
          <div class="col-lg-4 col-sm-6  ">
            
              <img class="img-fluid image" src="img/portfolio/thumbnails/coach1.jpg" alt="">
            
           
          </div>
          <div class="col-lg-4 col-sm-6  ">
        
              <img class="img-fluid image" src="img/portfolio/thumbnails/clean2.jpg" alt="">
             
           
          </div>
          <div class="col-lg-4 col-sm-6  ">
            
              <img class="img-fluid image" src="img/portfolio/thumbnails/catering.jpg"  alt="">
       
          
          </div>
          <div class="col-lg-4 col-sm-6  ">
            
              <img class="img-fluid image" src="img/portfolio/thumbnails/dirty.jpg" alt="">
            
           
          </div>
          <div class="col-lg-4 col-sm-6  ">
        
              <img class="img-fluid image" src="img/portfolio/thumbnails/dirty2.jpg" alt="">
             
           
          </div>
          <div class="col-lg-4 col-sm-6  ">
            
              <img class="img-fluid image" src="img/portfolio/thumbnails/food2.jpg"  alt="">
       
          
          </div>
          <div class="col-lg-4 col-sm-6  ">
            
              <img class="img-fluid image" src="img/portfolio/thumbnails/dirty4.jpg" alt="">
            
           
          </div>
          <div class="col-lg-4 col-sm-6  ">
        
              <img class="img-fluid image" src="img/portfolio/thumbnails/dirty5.jpg" alt="">
             
           
          </div>
          <div class="col-lg-4 col-sm-6  ">
            
              <img class="img-fluid image" src="img/portfolio/thumbnails/dirty6.jpg"  alt="">
       
          
          </div>
        </div>
      </div>
    </section> -->

    <section class="bg-dark text-white">
      <div class="container text-center">
        <h2 class="mb-4" style="color:white;">You may download Complaint Management System Mobile App Here </h2>
        <a class="btn btn-light btn-xl sr-button"  style="text-align: center;" href="https://play.google.com/store/apps/details?id=com.cris.org&hl=en"><img src="img/android_app.png" class="img-fluid"></a>
      </div>
    </section>
    <%@ include file="footer_main.html" %>
  </div>
    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    
    <!-- Custom scripts for this template -->
    <script src="js/creative.min.js"></script>
  </body>
</html>