<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@page import ="utility.DbConnection" import="java.sql.*" %>
<%@page import="java.util.ArrayList"%>

<html>
	<head>
		<meta charset="utf-8">
  		<meta name="viewport" content="width=device-width, initial-scale=1">
  		<title>COMS</title>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js" type="text/javascript"></script>
		<script src="js/CompRegTrainJS.js?version=3"></script>
		<script src="js/complaintintrain.js?version=1"></script>
		<script src="js/Validation.js"></script>
		<script src="js/bootstrap-datetimepicker.min.js"></script>
		<link href="css/bootstrap.min.css" rel="stylesheet"/>
 		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
 		<link href="css/bootstrap-datetimepicker.css" rel="stylesheet"/>
		<link href="css/bootstrap-datetimepicker.min.css" rel="stylesheet"/>
    
		<style>
			div#UTS, #Trainno1, #Stationname {
    			display: none;
			}
			div#emailid {
    			display: none;
			}
			body{
				font-family: 'Arial Narrow',Arial, sans-serif;
				font-size:13px!important;
			}
		</style>

		<script type="text/javascript">
    		$(function () {
        		$("#pmode").change(function () {
            		if ($(this).val() == "PNR") {
                		$("#PNR").show();
                		$("#Trainno1").hide();               
            		}
            		else {
            			$("#PNR").hide();
                		$("#pnr_desc").hide();               
            		}
        		});        
        		$("#pmode").change(function () {
            		if ($(this).val() == "UTS") {               
                		$("#PNR").hide();
                		$("#Trainno1").show();               
            		}
            		else {
                		$("#UTS").hide();              
                		$("#Trainno1").hide();               
            		}
        		});
        		$("#contact_detail").change(function () {
            		if ($(this).val() == "Contact") {
                		$("#mobileno").show();
                		$("#emailid").hide();   
            		} 
        		});       
        		$("#contact_detail").change(function () {
            		if ($(this).val() == "Email") {               
                		$("#mobileno").hide();
                		$("#emailid").show();               
            		}
        		});        
    		});
   
    		function validateEmail(emailField){
        		var reg = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;
        		if (reg.test(emailField.value) == false) {
            		alert('Invalid Email Address');
            		return false;
        		}
        		return true;
			}
		</script>
	</head>

	<style>
		.bg {
   
    		background-image: url(img/trainpic.jpg);
    		background-position: center center;
    		background-size: cover;
		}
	</style>
	
	<body class="bg">
	<%
     	if(session.getAttribute("user_name")==null){
     	
     	System.out.println("Session Value: "+session.getAttribute("user_name"));
     	System.out.println("******************Session Expired **********************");
     	
     	session.setAttribute("url", "ComplaintOnTrain.jsp");
         response.sendRedirect("signin.jsp");
     }
	%>
		
 			<%@ include file="header_main.jsp" %>
 		
		<div class="container-fluid" style="padding-top:10px;padding-bottom:10px;  margin-left: auto; margin-right: auto; width: 100%;  min-height: 100%;">
			<div class="col-md-12">
				<div class="card" style="font-size:20px!important;">
					<div class="card-header bg-info text-white"> 
						<div class="row">	
							<div class="col-md-11">
 								<center><p style="font-size:22px;"><b>Complaint Registration In Train</b> </p>  </center>
 							</div>
 							<div class="col-md-1" align="right">
						 		<a href="index.jsp" class="close-thin" Style="text-decoration:none;color:white;font-size: 22px;">X</a>
							</div>
						</div>
					</div>
					<div class="card-body" style="background-color:#F7F7F7;" >
						<div class="row container-fluid" >
							<div class="col-md-12" >
								<form method="post"  name="complaint" id="complaint" enctype="multipart/form-data">
									<div id="detail_panel">
										<div class="row" align="center">
  											<div class="col-md-12">
  												<font color="blue">
                   									<%if(request.getParameter("strMSG")!=null){
				   										out.print(request.getParameter("strMSG"));
				   									}%>
												</font>
  											</div>			  
			   							</div>		 		   			  
										<div class="row">
 											<div class="col-md-6">
												<p style="color:#f05f40;">Complaint Detail  </p>
											</div>
	 										<div class="col-md-6">
												<p style="text-align: right;color:red;"><span style="color:#f05f40;font-size:30px">*</span> Mandatory Fields</p>
											</div>
										</div>
			     						<div class="row"  >			    
            								<div class="col-md-4">
                								<div class="form-group">
                    								<label for="complaint_type">Complaint <span style="color:#f05f40;font-size:30px">*</span></label>
                    								<div id="selectComplaint" ></div>                    
                								</div>
            								</div>
            								<div class="col-md-4">
                								<div class="form-group">
                    								<label for="sub_complaint_type">Sub Complaint <span style="color:#f05f40;font-size:30px">*</span></label>
                           							<div id="selectSubComplaint" ></div>                   
                								</div>
            								</div>        
       										<div class="col-md-4">
                								<div class="form-group">
                    								<label for="incident_dt">Incident Date <span style="color:#f05f40;font-size:30px">*</span></label>
                   									<input type='text' class="form-control" id='incident_dt' required name="incident_dt" title="Please Enter Incedent Date" style="font-size:20px!important;"/>                    
                								</div>
            								</div>			     
			     						</div>			    
			     						<div class="row"  >			     
			       							<div class="col-md-4">
                								<div class="form-group">
                    								<label for="complaint_desc">Complaint Description</label>
                    								<textarea id="complaint_desc" name="complaint_desc"  class="form-control" title="Please Enter Complaint Description" style="font-size:20px!important;"></textarea>                    
                								</div>
            								</div>			     
			      							<div class="col-md-4">
                								<div class="form-group">
                									<label >Journey Details <span style="color:#f05f40;font-size:30px">*</span></label>
                   									<select  name="pmode" id="pmode" class="form-control" style="font-size:20px!important;">
          												<option value="PNR">PNR</option>
          												<option value="UTS">UTS</option>
       												</select> 
                								</div>
            								</div>			   
			    							<div class="col-md-4" id="Trainno1">
                								<div class="form-group">
                									<label for="train_no">Train No <span style="color:#f05f40;font-size:30px">*</span></label>
                    								<input list='trainno3' name='trainno' id='trainno' class='form-control' style='font-size:20px!important;'>
                     								<datalist id='trainno3' >
                     									<%                     
                     										PreparedStatement stmt=null;
             												DbConnection dbConnection=new DbConnection();
             												Connection con=dbConnection.dbConnect();
             												ResultSet rs=null;             		 
             												try{
             													stmt=con.prepareStatement("SELECT train_no,train_name FROM rly_train_master");	
             		  											rs=stmt.executeQuery();
        														while(rs.next())  {
        															%>
        																<option value='<%=rs.getString(1) %> -> <%=rs.getString(2)%>'>
        																<% 
        														}
        													}
        													catch(Exception e){
        														e.printStackTrace();
        													}
                     									%>                     
                    								</datalist>                                                         
                          						</div>
            								</div>			   
			    							<div class="col-md-4" id="PNR">
                								<div class="form-group">
                    								<label for="pnr_uts">PNR No <span style="color:#f05f40;font-size:30px">*</span></label>
                    								<input id="pnr_uts" type="text" name="pnr_uts" class="form-control"  maxlength="10" title="Please Enter PNR Number" style="font-size:20px!important;">                    
                								</div>
            								</div>     
            								<div  class="col-md-4" style="color:red;"></div>	
            								<div class="col-md-4" style="color:red;"></div>	      
			   								<div id="pnr_error" class="col-md-4" style="color:red;"></div>			     
			     						</div>		
			  							<div id="pnr_desc">			  
			  								<!--                  HIDDEN FIELDS                   -->			  			  
			   								<input id="day" type="hidden" name="day" class="form-control"    >
			  								<input id="month" type="hidden" name="month" class="form-control"    >
			   								<input id="year" type="hidden" name="year" class="form-control"    >
			     							<input id="nextstn" type="hidden" name="nextstn" class="form-control"  >			  
			  								<!--                  HIDDEN FIELDS ENDS                   -->			  
			 								<div class="row"  >			    
            									<div class="col-md-3">
                									<div class="form-group">
                    									<label for="train_no">Train Number</label>
                    									<input id="train_no" type="text" name="train_no" class="form-control"   readonly style="font-size:20px!important;">                    
                									</div>
            									</div>
            									<div class="col-md-3">
                									<div class="form-group">
                    									<label for="from_stn">From Station</label>
                    									<input id="from_stn" type="text" name="from_stn" class="form-control"  readonly style="font-size:20px!important;">	                   
                									</div>
            									</div>        
       											<div class="col-md-3">
                									<div class="form-group">
                    									<label for="to_stn">To Station</label>
                    									<input id="to_stn" type="text" name="to_stn" class="form-control" readonly style="font-size:20px!important;" >                    
                									</div>
            									</div>			     
			      								<div class="col-md-3">
                									<div class="form-group">
                    									<label for="board_stn">Boarding Station</label>
                    									<input id="board_stn" type="text" name="board_stn" class="form-control" readonly style="font-size:20px!important;">                    
                									</div>
            									</div>			    
			     							</div>			     			     			     			     			     			    
			   								<div class="row"  >			    
            									<div class="col-md-3">
                									<div class="form-group">
                    									<label for="berth_class">Berth Class</label>
                    									<input id="berth_class" type="text" name="berth_class" class="form-control"  readonly style="font-size:20px!important;" >                    
                									</div>
            									</div>
            									<div class="col-md-3">
                									<div class="form-group">
                    									<label for="total_pass">Total Passenger</label>
                    									<input id="total_pass" type="text" name="total_pass" class="form-control"  readonly style="font-size:20px!important;">                   
                									</div>
            									</div>       
       											<div class="col-md-3">
                									<div class="form-group">
                    									<label for="total_fare">Total Fare</label>
                    									<input id="total_fare" type="text" name="total_fare" class="form-control" readonly style="font-size:20px!important;">                    
                									</div>
            									</div>			     
			      								<div class="col-md-3">
                									<div class="form-group">
                    									<label for="coach_no">Coach No.</label>
                    									<div id="coach_condition">
                    										<input id="coach_no" type="text" name="coach_no" class="form-control" readonly style="font-size:20px!important;">
                    									</div>
                									</div>
            									</div>			     
			     							</div>			     
			     							<div class="row"> 
			      								<div class="col-md-3">
                									<div class="form-group">
                    									<label for="berth_no">Berth/Seat No.</label>
                    									<div id="berth_condition">
                    										<input id="berth_no" type="text" name="berth_no" class="form-control" readonly style="font-size:20px!important;">
                    									</div>
                									</div>
            									</div> 
            									<div class="col-md-9">
                									<br>
                    								<p  style="color:#f05f40;font-size:20px">Please confirm your journey details before proceeding...</p>                
            									</div>
			     							</div>			     
			     						</div>			     
			      						<div class="row">                                           
                 							<div class="col-md-4">
                								<div class="form-group">
                    								<label for="file">Upload File </label>
                    								<input id="file" type="file" name="file" class="form-control" title="Please Upload a Image" >
                    							</div>                    
                							</div>
            							</div>	
			    						<div class="row">
			   								<p style="color:#f05f40;"> Passenger Details  </p>
			   							</div>
			     						<div class="row"> 			       
       										<div class="col-md-4">
                								<div class="form-group">
                    								<label for="name">Name <span style="color:#f05f40;font-size:30px">*</span></label>
                    								<input id="name" type="text" name="name" required class="form-control txtOnly" title="Please Enter Name" style="font-size:20px!important;">                    
                								</div>
            								</div>			    			   			   
			    							<div class="col-md-4" >
                								<div class="form-group">
                									<label for="contact_no">Contact <span style="color:#f05f40;font-size:30px">*</span></label>
                   									<select  name="contact_detail" id="contact_detail"class="form-control" style="font-size:20px!important;">
          												<option value="Contact">Mobile No.</option>
         				 								<option value="Email">Email</option>         	                
       												</select> 
                								</div>
            								</div>
			     							<div class="col-md-4" id="mobileno">
                								<div class="form-group">
                    								<label for="contact_no">Mobile No. <span style="color:#f05f40;font-size:30px">*</span></label>
                    								<input id="contact_no" type="text" name="contact_no" required class="form-control "  maxlength="10" title="Please Enter Contact Number" style="font-size:20px!important;">
                								</div>
            								</div>
			      							<div class="col-md-4" id="emailid">
                								<div class="form-group">
                    								<label for="contact_no">Email <span style="color:#f05f40;font-size:30px">*</span></label>
                    								<input id="email" type="text" name="email" required class="form-control " onblur="validateEmail(this);" maxlength="50" title="Please Enter Email id" style="font-size:20px!important;">                    
                								</div>
            								</div>
            							</div>			    
			     						<div class="row">
			     							<div class="col-md-12" style="text-align:center;">
                 								<div class="form-group">          
                    								<button id="generate" type="button" name="generate" class="btn btn-primary" >GENERATE OTP</button>                    
                								</div>
            								</div>
            							</div>                                   
            						</div>
			       					<div id="otp_panel" class="container-fluid" >			    
			    						<div id="result">
			    						</div>
			   							<center> <b> <p>OTP has been sent on your contact detail</p> </b></center>			    
			     						<div class="row">
			     							<div class="col-md-4">
			     							</div>			     
			     							<div class="col-md-4 " style="text-align:center;">
                								<div class="form-group">
                    								<label for="otp">Enter the OTP Generated</label>
                    								<input id="otp" type="text" name="otp" value="1902" class="form-control"  title="Please Enter OTP" style="font-size:20px!important;">                    
                								</div>
            								</div>
			     						</div>
			     						<div class="row">
			     							<div class="col-md-12" style="text-align:center;">
                								<div class="form-group">                    
                    								<button id="submitbtn" type="submit" name="submitbtn" class="btn btn-primary" >Submit</button>                    
                								</div>
            								</div>
            							</div>
			     					</div>
			     					<div id="result_register" class="container-fluid" >			     
			     						<div id="suggestion_id">
			     						</div>					     
			      						<div class="form-group">
                   							<center>  
                    							<button  class="btn btn-primary" > <a href ="index.jsp" style="color:white;"> OK</a></button>                  
                   							</center> 
                						</div>
			    					</div>			     			     
			     				</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-2">
			</div>
		</div>
		<div class="container-fluid" style="padding:0px;">
 			<%@ include file="footer_main.html" %>
 		</div>
	</body>
</html>