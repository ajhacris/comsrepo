<%@page import="java.io.Console"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="utility.DbConnection" import="java.sql.*"%>
<html>
<head>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>

<script src="js/bootstrap.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js"
	type="text/javascript"></script>



<script src="js/bootstrap-datetimepicker.min.js"></script>
<link href="css/bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<link href="css/bootstrap-datetimepicker.css" rel="stylesheet" />
<link href="css/bootstrap-datetimepicker.min.css" rel="stylesheet" />




</head>
<style>
.bg {
	background-position: center center;
	background-size: cover;
}

table {
	width: 100%;
}

thead, tbody, tr, td, th {
	display: block;
}

tr:after {
	content: ' ';
	display: block;
	visibility: hidden;
	clear: both;
}

thead th {
	height: 50px;

	/*text-align: left;*/
}

tbody {
	height: 482px;
	overflow-y: auto;
}

thead {
	/* fallback */
	
}

tbody td, thead th {
	width: 15.5%;
	float: left;
}
</style>






<%
  
String zone = "";
	String div = "";
	String groupid = "";
	String depid="";
	String flag_sla_value="";
	
	if (session.getAttribute("login_id") == null) {

		System.out.println("******************Session Expired **********************");
		response.sendRedirect("AdminLogin.jsp");
	} else {
		zone = session.getAttribute("zn_cd").toString();
		div = session.getAttribute("div_cd").toString();
		groupid = session.getAttribute("group_id").toString();
		depid = session.getAttribute("dept_cd").toString();
	}
	flag_sla_value=session.getAttribute("flag_sla").toString();
%>

<body id="page-top" style="overflow-x: hidden">
	<!--    header start  -->
	<%@ include file="top.jsp"%>
	<div id="content-wrapper">
		<%@ include file="sidebar.jsp"%>
		<!--    header end   -->
		<div class="container-fluid"
			style="padding-top: 35px; width: 100%; padding-bottom: 120px;">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header bg-info text-white">
						<p style="font-size: 25px;">
							<b><center>
									<h2>Closed Complaints  </h2>
									
								</center></b>
						</p>
					</div>
					<div class="card-body" style="background-color: #F7F7F7;">
						<div class="row container-fluid">
							<div class="col-md-12">
								<form action="" name="msg" method="post">
									<table class="table table-striped">
										<thead>
											<tr>
												<th style="width: 6%;">S.No</th>
												<!-- <th>Select</th> -->
												<th>Complaint Ref. No.</th>
												<th >Zone</th>
												<th >Division</th>
												<th >Department</th>
												<th >Group_id</th>
												<th >History</th>
												
											</tr>
										</thead>
										<tbody>


											<%
												int total = 1;
												PreparedStatement stmt = null;
												ResultSet rs = null;
												DbConnection dbConnection = new DbConnection();
												Connection con = dbConnection.dbConnect();
												String condition="";
												if(flag_sla_value.equals("2") || flag_sla_value.equals("1")){
													condition=" zn_cd=? AND div_cd=? AND dept_cd= ?";
												}
												else{
													condition=" zn_cd=? AND div_cd=? ";
												}
												String query = "SELECT complaint_ref_no ,zn_cd,div_cd,dept_cd,user_group_id FROM rly_complaint WHERE "+
												condition+" AND status='Closed'";
											//	System.out.println(query);
												try{
												stmt = con.prepareStatement(query);
												
												stmt.setString(1,zone);
												stmt.setString(2,div);
												if( flag_sla_value.equals("2")|| flag_sla_value.equals("1")){
													stmt.setString(3,depid);
													}
												rs = stmt.executeQuery();
												while (rs.next()) {
											%>


											<tr>
												<td class="filterable-cell" style="width: 6%;"><%=total%></td>
												<td  class="filterable-cell"><%=rs.getString(1)%></td>
												<td  class="filterable-cell"><%=rs.getString(2)%></td>
												<td  class="filterable-cell"><%=rs.getString(3)%></td>
												<td  class="filterable-cell"><%=rs.getString(4)%></td>
												<td  class="filterable-cell"><%=rs.getString(5)%></td>
												<td class="filterable-cell ctd" style="width: 12.5%;"><a href="ComplaintHistory.jsp?Id=<%=rs.getString(1)%>">
									             <button type="button" class="btn btn-info" id="history" >  History </button> </a></td>
											</tr>


											<%
												total++;
												}
												
											}
											catch (Exception ex){}
											finally {
											    if (rs != null) {
											        try {
											            rs.close();
											        } catch (SQLException e) { /* ignored */}
											    }
											    if (stmt != null) {
											        try {
											            stmt.close();
											        } catch (SQLException e) { /* ignored */}
											    }
											    if (con != null) {
											        try {
											            con.close();
											        } catch (SQLException e) { /* ignored */}
											    }
											}
												
												
												
											%>


										</tbody>

									</table>
									
									

								</form>






							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>










	<!--    footer start  -->
	<%@ include file="footer.jsp"%>






	<script src="js/jquery/jquery.min.js"></script>
	<script src="js/js/bootstrap.bundle.min.js"></script>
	<script src="js/jquery-easing/jquery.easing.min.js"></script>
	<script src="js/jsdash/sb-admin.min.js"></script>
</body>
</html>