<%@page import ="utility.DbConnection" import="java.sql.*" %>

<html>


<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>

<script src="js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js" type="text/javascript"></script>
<script src="js/CompalintActionJS.js"></script>
<script src="js/bootstrap-datetimepicker.min.js"></script>
<link href="css/bootstrap.min.css" rel="stylesheet"/>
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
 <link href="css/bootstrap-datetimepicker.css" rel="stylesheet"/>
<link href="css/bootstrap-datetimepicker.min.css" rel="stylesheet"/>
    
</head>
<style>
.bg {
   
   
   
    background-position: center center;
    background-size: cover;
}
.w3-example {
    width: 120%;
    background-color: #d4cece;
    padding: 0.01em 1px;
    margin: 20px 0;
    box-shadow: 0 2px 4px 0 rgba(0,0,0,0.16),0 2px 10px 0 rgba(0,0,0,0.12)!important;
}
</style>

<%
/*
String login_id="";
login_id=(String)session.getAttribute("login_id"); 

if(login_id==null || login_id.equals("")){
	response.sendRedirect("index.jsp");
}

*/
%>

<%
 
        
        if(session.getAttribute("login_id")==null)
            {

                System.out.println("******************Session Expired **********************");


                    response.sendRedirect("AdminLogin.jsp");

          }
        %>

<%
String groupid=session.getAttribute("group_id").toString();
System.out.println("GROUP ID : "+groupid);
PreparedStatement stmt=null;
DbConnection dbConnection=new DbConnection();
Connection con=dbConnection.dbConnect();
String complaint_ref_no="";
complaint_ref_no=request.getParameter("Id");
String mobile="";
String email="";

String query_comp="select head_subhead_en from rly_heads_subheads_master where id=?";
String query="SELECT complaint_ref_no,channel_type,complaint_type,complaint_sub_type,complaint,complaint_mode,"
+"platform_no,trainno,STATUS,dept_name,complainant_name,email_id,mobile,image,station_name FROM rly_complaint,rly_department "
+"WHERE complaint_ref_no=?  AND rly_complaint.dept_cd=rly_department.dept_code";

int flag=0;
ResultSet rs=null;
String query_history="SELECT zn_cd,div_cd,dept_cd, user_group_id,remarks FROM  rly_complaint_response WHERE complaint_ref_no=?";
String query_forward_button="SELECT user_group_id,remarks FROM  rly_complaint_response WHERE complaint_ref_no=?";
stmt=con.prepareStatement(query_forward_button);	
stmt.setString(1,complaint_ref_no);
rs=stmt.executeQuery();  
	while(rs.next())  {
		System.out.println("GROUP ID OF COMPLAINT ASSIGNED : "+rs.getString(1));
		if(rs.getString(1).equals(groupid)){ flag=1;}
		
	}

System.out.print("query: "+query);
stmt=con.prepareStatement(query);	

stmt.setString(1,complaint_ref_no);
System.out.print(complaint_ref_no);
  rs=stmt.executeQuery();  
	while(rs.next())  {
		mobile=rs.getString(13);
		email=rs.getString(12);
		
	
   
%>
<body id="page-top">

    <%@ include file="top.jsp" %>


    <div  id="content-wrapper">
   
     <%@ include file="sidebar.jsp" %>
 <div class="container-fluid" style="padding-top:35px;">
  
    
  <div class="col-md-12">
  
       <div class="card-header bg-info text-white"> <span> </span> <p style="font-size:25px;"><b><center><h2>Follow Up Complaint</h2></center></b></p>
       </div>
       <div class="card-body" style="background-color:#F7F7F7;" >
        <div class="row container-fluid">
         <div class="col-md-8" >
         
           
           
           
            <table class="table">
    <tbody>
      <tr>
        <td>Reference No</td>
        <td><%=rs.getString(1) %></td>
     </tr>
   
    
      <tr>
        <td>Source</td>
        <td><%if(rs.getString(2).equals("W")){ %>Web<%} %>
        <%if(rs.getString(2).equals("L")){ %>Helpline-138<%} %>
        <%if(rs.getString(2).equals("P")){ %>Helpline-182<%} %>
        <%if(rs.getString(2).equals("M")){ %>Social Media<%} %>
        <%if(rs.getString(2).equals("D")){ %>Manual Dak<%} %>
        <%if(rs.getString(2).equals("C")){ %>Catering Complaints, Rly Board<%} %>
         <%if(rs.getString(2).equals("H")){ %>Helpline-139<%} %>
        <%if(rs.getString(2).equals("S")){ %>SMS<%} %>
        </td>
        
      </tr>      
      <tr >
        <td>Complaint</td>
        <td><% stmt=con.prepareStatement(query_comp);
               stmt.setString(1,rs.getString(3));
               ResultSet rs1=stmt.executeQuery();  
	           while(rs1.next())  {%><%=rs1.getString(1) %><%} %> </td>
        
      </tr>
     
      <tr >
      <td>Sub Complaint</td>
        <td><% stmt=con.prepareStatement(query_comp);
               stmt.setString(1,rs.getString(4));
               ResultSet rs2=stmt.executeQuery();  
	           while(rs2.next())  {%><%=rs2.getString(1) %><%} %> </td>
        
      </tr>
      
       <tr >
        <td>Complaint Description</td>
        <td><textarea class="form-control" rows="5"  ><%=rs.getString(5) %></textarea></td>
        
      </tr>
      <%if(rs.getString(14)==null || rs.getString(14).equals("")){ %>
      <%}else{ %>
      <tr >
        <td>Complaint Image</td>
        <td><img  class="img-thumbnail" src="image.jsp?Id=<%=rs.getString(1)%>" width=200px height=100px></td>
        
      </tr>
      <%} %>
       <tr >
        <td>Complaint Mode </td>
        <td><%if(rs.getString(6).equals("S")){ %>Station<%} %>
        <%if(rs.getString(6).equals("T")){ %>Train<%} %>
        </td>
        
      </tr>
      
       <%if(rs.getString(7)==null || rs.getString(7).equals("")){ }else{%>
       <tr >
        <td>Platform No</td>
        <td><%=rs.getString(7) %></td>
        
      </tr>
      <%} %>
      
      <tr >
      <%if(rs.getString(8)==null || rs.getString(8).equals("")){%>
        <td>Station Name </td>
        <td><%=rs.getString(15) %></td>
        <%}else{ %>
        <td>Train No. </td>
        <td><%=rs.getString(8) %></td>
        <%} %>
      </tr>
      
       <tr >
        <td>Department </td>
        <td><%=rs.getString(10) %></td>
        
      </tr>
      
       <tr >
        <td>Status </td>
        <td><%=rs.getString(9) %></td>
        
      </tr>
      
    </tbody>
  </table>   
              
         

        </div>
        <div class="col-md-4"  >
<h4>Contact Details</h4>
   
            <table class="table" style="background-color:#caaaaa">
    <tbody>
      <tr class="success">
        <td>Name</td>
        <td><%=rs.getString(11) %></td>
     </tr>
   
    
      <tr class="success">
        <td>Email</td>
        <td><%=rs.getString(12) %></td>
        
      </tr>   
      <tr class="success">
        <td>Contact No</td>
        <td><%=rs.getString(13) %></td>
        
      </tr>  
       </tbody>
  </table> 
     
       <%} %>
  
 <center> <h3>History :</h3></center>
  <% 
  int i=0;
  stmt=con.prepareStatement(query_history);
               stmt.setString(1,complaint_ref_no);
               ResultSet rs2=stmt.executeQuery();  
	           while(rs2.next())  { if(i>0){ %>
  <center> <img src="img/arrow.png" width="50" height="40"></img></center>
  <%} %>
   <div class="w3-example">
   
   <table class="table table-condensed">
   <tbody>
   <tr>
   <td>Zone </td>
   <td><%=rs2.getString(1) %></td>
    <td>Division </td>
   <td><%=rs2.getString(2) %></td>
   </tr>
   
   <tr>
   <td>Department </td>
   <td><%=rs2.getString(3) %></td>
    <td>Concern person </td>
   <td><%=rs2.getString(4) %></td>
   </tr>
   
    <tr>
   <td>Remark </td>
   <td colspan="3" ><%=rs2.getString(5) %></td>
   
   </tr>
   
   </tbody>
   </table>
<%--  <div class="row">
<div class="col-sm-2"><b> Zone:</b></div>


<div class="col-sm-2"><%=rs2.getString(1) %></div>
<div class="col-sm-2"><b> Division:</b></div>
<div class="col-sm-1"><b> </b></div>
<div class="col-sm-3"><%=rs2.getString(2) %></div>
</div>
<div class="row">
<div class="col-sm-3"><b>Department:</b></div>
<div class="col-sm-3"><%=rs2.getString(3) %></div>
<div class="col-sm-3"><b>Concern Person:</b></div>
<div class="col-sm-3"><%=rs2.getString(4) %></div>
</div> 
 <div class="row">   
 <div class="col-sm-3"><b>Remark: </b> </div>   
 <div class="col-sm-9"> <%=rs2.getString(5) %> </div>     
  </div> --%>

</div>
<% i++;} %>
     
     
      </div>
      <div class="row">
      <%if(flag==1){ %>
      <div class="col-md-6">
      <button type="button" class="btn btn-primary" value="forward" id="forward" onClick="admSelectCheck(this);">Forward</button>
      </div>
      <%} %>
       <div class="col-md-6">
        <button type="button" class="btn btn-primary" value="close" id="close" onClick="admSelectCheck(this);">Close</button>
        </div>
      </div>
       
       
        
        
        
      </div>
    </div>

 
</div>
  <div class="col-md-12">
         <div id="admDivCheck" style="display:none;">
       <form method="post" action="ComplaintActionSubmit" >
 <input type= hidden value="Forwarded" name="status" id ="status">
 <input type= hidden value="<%=groupid %>" name="group_id" id ="group_id">
  <input type= hidden value="<%=complaint_ref_no %>" name="comp_ref_no" id ="comp_ref_no">
 <div class="row"  >
			    
            <div class="col-md-2">
                <div class="form-group">
                    <label for="zone">Zone *</label>
                    <div id="zone_id" ></div>
                    
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label for="division">Division *</label>
                           <div id="division_id" ></div>                   
                </div>
            </div>
        
       <div class="col-md-3">
                <div class="form-group">
                    <label for="stationId">Station </label>
                   <input type='text' class="form-control" id='station' name="station"/>
                    
                </div>
            </div>
            
            <div class="col-md-3">
                <div class="form-group">
                    <label for="departmentId">Department  *</label>
                   <div id="department_id" ></div>  
                    
                </div>
            </div>
            
            <div class="col-md-2">
                <div class="form-group">
                    <label for="designationId">Designation *</label>
                   <div id="designation_id" ></div>  
                    
                </div>
            </div>
               
		 </div>
		 <div class="col-md-12">
		 <div id="remark_id">
		<div class="form-group">
                    <label for="designationId">Remark *</label>
        <textarea class="form-control" rows="5" name="remark" placeholder="Add Remarks ..."></textarea>
        </div>
       <button type="submit" class="btn btn-primary" >Submit</button>  
		 </div>
		 </div>

</form>
        </div>
         </div>
          
         
          <div class="col-md-12">
         <div id="admDivCheck1" style="display:none;">
           <form method="post" action="ComplaintActionSubmit" >
            <input type= hidden value="<%=complaint_ref_no %>" name="comp_ref_no" id ="comp_ref_no">
            <input type= hidden value="Closed" name="status" id ="status">
            <input type= hidden value="<%=groupid %>" name="group_id" id ="group_id">
             <div class="form-group">
         
                    <label for="designationId">Remark *</label>
                   <textarea class="form-control" rows="5" name="remark" placeholder="Add Remarks ..."></textarea>
       
             </div>
              <button type="submit" class="btn btn-primary" >Submit</button>
             </form>
        </div>
         </div>
         
<%
String subquery="select head_subhead_en from rly_heads_subheads_master where id=?";
System.out.println("mobile :"+mobile);
System.out.println("email :"+email);
String value=null;
String queryHistroy=null;
if(!mobile.equals(null)){
	 queryHistroy="SELECT complaint_ref_no,zn_cd,div_cd,dept_cd,STATUS FROM rly_complaint WHERE mobile=? ";
	 value=mobile;
}
if(!email.equals(null)){
	 queryHistroy="SELECT complaint_ref_no,zn_cd,div_cd,dept_cd,STATUS FROM rly_complaint WHERE email_id=? ";
	 value=email;
}

                   		stmt=con.prepareStatement(queryHistroy);	
							stmt.setString(1,value);
							rs=stmt.executeQuery();  
                    		
%>
<div class="row">
	<div class="col-md-12">
	 <div class="table-responsive">   
	 	<center><h5 style="color: #0062cc;">Previous Complaint History</h5></center>
		 <table class='table table-hover' border="1px">
		   <thead>
		    <tr>
		    <th>
		     Reference No
		    </th>	   
		   <th>
		     Zone
		    </th>
		    <th >
		     Division
		    </th>
		    <th >
		     Department
		    </th>
		    <th>
		     Status
		    </th>
		    
		    <th>
		     Action
		    </th>
		   
		    </tr>
		   
		   </thead>
		    <% while(rs.next())  {
				 i++; %>
		     <tr>
		     <td>
		     <%=rs.getString(1) %>
		     </td>
		     <td>
		     <%=rs.getString(2) %>
		     </td>
		      <td>
		     <%=rs.getString(3) %>
		     </td>
		     
		      <td>
		     <%=rs.getString(4) %>
		     </td>
		      <td>
		     <%=rs.getString(5) %>
		     </td>
		    
		      
		     
		    <td class="filterable-cell ctd" style="width: 12.5%;"><a href="ComplaintHistory.jsp?Id=<%=rs.getString(1)%>">
									             <button type="button" class="btn btn-info" id="history" >  History </button> </a></td>
		     </tr>
		   <%} %>
		 </table>	
		</div>
		</div>	
		
	</div>
	<% if(i==0){ %>
	<div class="row">
	<div class="col-md-12">
	<center><h5>Record Not found</h5></center>
	</div>
	</div>
	<%} %>

</div>


</div>
     

    <%@ include file="footer.jsp" %>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
      
    </a>
 </div>
</div>
    <script src="js/jquery/jquery.min.js"></script>
    <script src="js/js/bootstrap.bundle.min.js"></script>

    <script src="js/jquery-easing/jquery.easing.min.js"></script>

    <script src="js/jsdash/sb-admin.min.js"></script>

</body>
<script>
function admSelectCheck(nameSelect)
{
 if(nameSelect){
	 if(nameSelect.value == "forward"){
		 document.getElementById("admDivCheck").style.display = "block";
         document.getElementById("admDivCheck1").style.display = "none";
	 }
	 if(nameSelect.value == "close"){
		 document.getElementById("admDivCheck1").style.display = "block";
         document.getElementById("admDivCheck").style.display = "none";
	 }
	 
	 
 }
	
 /* 
 
     admOptionValue = document.getElementById("forward").value;
     alert(nameSelect.value);
     if(admOptionValue == nameSelect.value){
    	 
         document.getElementById("admDivCheck").style.display = "block";
         document.getElementById("action").style.display = "block";
         document.getElementById("admDivCheck1").style.display = "none";
     }
     else{
    	 
         document.getElementById("admDivCheck").style.display = "none";
     }
 }
 else{
     document.getElementById("admDivCheck").style.display = "none";
 }
 
 if(nameSelect){
     admOptionValue = document.getElementById("close").value;
     alert(nameSelect.value);
     if(admOptionValue == nameSelect.value){
    	
         document.getElementById("admDivCheck1").style.display = "block";
         document.getElementById("action").style.display = "block";
         document.getElementById("admDivCheck").style.display = "none";
     }
     else{
    	
         document.getElementById("admDivCheck1").style.display = "none";
     }
 }
 else{
     document.getElementById("admDivCheck1").style.display = "none";
 } */
 
}

</script>



</html>